
<!--tri mod start-->
<script type="text/javascript">
function removeVoucher(code) {
  $.ajax({
    url: 'index.php?route=module/deluxe_voucher/removeVoucher&code=' + code,
    success: function() {
      location.reload();
    }
  });
} //function removeVoucher end
</script>
<!--tri mod end-->
      
<footer>
  <div class="container">
    <div class="row">
      <?php if ($informations) { ?>
      <div class="col-sm-3">
        <h5><?php echo $text_information; ?></h5>
        <ul class="list-unstyled">
          <?php foreach ($informations as $information) { ?>
          <li><a href="<?php echo $information['href']; ?>"><?php echo $information['title']; ?></a></li>
          <?php } ?>
        </ul>
      </div>
      <?php } ?>
      <div class="col-sm-3">
        <h5><?php echo $text_service; ?></h5>
        <ul class="list-unstyled">
          <li><a href="<?php echo $contact; ?>"><?php echo $text_contact; ?></a></li>

				<li><a href="<?php echo $testimonial; ?>"><?php echo $text_testimonial; ?></a></li>
			
          <li><a href="<?php echo $return; ?>"><?php echo $text_return; ?></a></li>
          <li><a href="<?php echo $sitemap; ?>"><?php echo $text_sitemap; ?></a></li>
        </ul>
      </div>
      <div class="col-sm-3">
        <h5><?php echo $text_extra; ?></h5>
        <ul class="list-unstyled">
          <li><a href="<?php echo $manufacturer; ?>"><?php echo $text_manufacturer; ?></a></li>
          <li><a href="<?php echo $voucher; ?>"><?php echo $text_voucher; ?></a></li>
          <li><a href="<?php echo $affiliate; ?>"><?php echo $text_affiliate; ?></a></li>
          <li><a href="<?php echo $special; ?>"><?php echo $text_special; ?></a></li>
        </ul>
      </div>
      <div class="col-sm-3">
        <h5><?php echo $text_account; ?></h5>
        <ul class="list-unstyled">
          <li><a href="<?php echo $account; ?>"><?php echo $text_account; ?></a></li>
          <li><a href="<?php echo $order; ?>"><?php echo $text_order; ?></a></li>
          <li><a href="<?php echo $wishlist; ?>"><?php echo $text_wishlist; ?></a></li>
          <li><a href="<?php echo $newsletter; ?>"><?php echo $text_newsletter; ?></a></li>
        </ul>
      </div>
    </div>
    <hr>
    <p><?php echo $powered; ?></p>
  </div>
<?php echo $microdatapro; $microdatapro_main_flag = 1; //microdatapro 7.0 - 1 - main ?>
</footer>

<!--
OpenCart is open source software and you are free to remove the powered by OpenCart if you want, but its generally accepted practise to make a small donation.
Please donate via PayPal to donate@opencart.com
//-->

<!-- Theme created by Welford Media for OpenCart 2.0 www.welfordmedia.co.uk -->


          <?php if (isset($_SESSION['google_remarketing_code']))
          {
            echo $_SESSION['google_remarketing_code']; unset($_SESSION['google_remarketing_code']);
          } ?>
        

			<?php 
			//зум
			//$effect_in = 'scale(1)';
			//$effect_out = 'scale(0)'; 
			//вращение по вертикали
			//$effect_in = 'rotateX(0deg)';
			//$effect_out = 'rotateX(180deg)';
			//вращение по горизонтали
			$effect_in = 'rotateY(0deg)';
			$effect_out = 'rotateY(180deg)';
			//вращение по кругу
			//$effect_in = 'rotate(0deg)';
			//$effect_out = 'rotate(180deg)';
			//слайд влево
			//$effect_in = 'translateX(0px)';
			//$effect_out = 'translateX(-100%)';
			//слайд вправо
			//$effect_in = 'translateX(0px)';
			//$effect_out = 'translateX(-100%)';
			//слайд вверх
			//$effect_in = 'translateY(0px)';
			//$effect_out = 'translateY(-100%)';
			//вращение по вертикали плюс зум
			//$effect_in = 'rotateX(0deg) scale(1)';
			//$effect_out = 'rotateX(180deg) scale(0)';
			?>
			<style>
				.image div, .image span{z-index:2;}
				.box-product .image a,
				.box-content .image a,
				.product-thumb .image a,
				.item .image a,
				.product-list .image a,
				.product-grid .image a
				{display:inline-block;position:relative;z-index:0;overflow:hidden;margin:0 auto;text-align:center;}
				.product-thumb .image a:hover {opacity: 1;}
				.image .main{
					transform:<?php echo $effect_in; ?>;
					-o-transform:<?php echo $effect_in; ?>;
					-moz-transform:<?php echo $effect_in; ?>;
					-webkit-transform:<?php echo $effect_in; ?>;
					transition: all ease-in-out .4s;
				}
				.image .additional{
					position:absolute;
					top:0;
					left:0;
					opacity:0;
					cursor:pointer;
					transform:<?php echo $effect_out; ?>;
					-o-transform:<?php echo $effect_out; ?>;
					-moz-transform:<?php echo $effect_out; ?>;
					-webkit-transform:<?php echo $effect_out; ?>;
					transition: all ease-in-out .4s;
				}
				.image:hover .main{
					transform:<?php echo $effect_out; ?>;
					-o-transform:<?php echo $effect_out; ?>;
					-moz-transform:<?php echo $effect_out; ?>;
					-webkit-transform:<?php echo $effect_out; ?>;
				}
				.image:hover .additional{
					opacity:1;
					background:#fff;
					transform:<?php echo $effect_in; ?>;
					-o-transform:<?php echo $effect_in; ?>;
					-moz-transform:<?php echo $effect_in; ?>;
					-webkit-transform:<?php echo $effect_in; ?>;
				}
				.product-price .image .main{
					transform:none !important;
				}
				.product-price .image .additional{
					display:none;
				}
			</style>
			<script>
			$(window).load(function() {
				$('.image').each(function () {
					if($(this).find('img').attr('data-additional')) {
						$(this).find('img').first().addClass('main');
						$(this).children('a').append('<img src="'+$(this).find('.main').attr('data-additional')+'" class="additional" title="'+$(this).find('.main').attr('alt')+'" />');
					}
				});
			});
			</script>
		

				<?php if ($loadmore_status) {?>
					<style>
						a.load_more {
							<?php if (isset($loadmore_style)) {echo $loadmore_style;} else {?>
								display:inline-block; margin:0 auto 20px auto; padding: 0.5em 2em; border: 1px solid #000; color: #000;  text-decoration:none; text-transform:uppercase;
							<?php } ?>
						}
					</style>		
					<div id="load_more" style="display:none;">
						<div class="row text-center">
							<a href="#" class="load_more"><?php echo $loadmore_button; ?></a>
						</div>
					</div>
				<?php } ?>
            

				<?php if ($loadmore_arrow_status) {?>
					<a id="arrow_top" style="display:none;" onclick="scroll_to_top();"></a>
				<?php } ?>
            
<?php if(!isset($microdatapro_main_flag)){echo $microdatapro;  $microdatapro_main_flag = 1;} //microdatapro 7.0 - 2 - extra ?>
<?php if(!isset($microdatapro_main_flag)){echo $microdatapro;} //microdatapro 7.0 - 3 - extra ?>
</body></html>