<!DOCTYPE html>
<!--[if IE]><![endif]-->
<!--[if IE 8 ]><html <?php echo $tc_og_prefix; //microdatapro 7.0 ?> dir="<?php echo $direction; ?>" lang="<?php echo $lang; ?>" class="ie8"><![endif]-->
<!--[if IE 9 ]><html <?php echo $tc_og_prefix; //microdatapro 7.0 ?> dir="<?php echo $direction; ?>" lang="<?php echo $lang; ?>" class="ie9"><![endif]-->
<!--[if (gt IE 9)|!(IE)]><!-->
<html <?php echo $tc_og_prefix; //microdatapro 7.0 ?> dir="<?php echo $direction; ?>" lang="<?php echo $lang; ?>">
<!--<![endif]-->
<head prefix="og:http://ogp.me/ns# fb:http://ogp.me/ns/fb# product:http://ogp.me/ns/product#">
<meta charset="UTF-8" />
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<title><?php echo $title; ?></title>

<?php if ($noindex) { ?>
<!-- OCFilter Start -->
<meta name="robots" content="noindex,nofollow" />
<!-- OCFilter End -->
<?php } ?>
      
<base href="<?php echo $base; ?>" />

	<?php if ($robots) { ?>
	<meta name="robots" content="<?php echo $robots; ?>" />
	<?php } ?>
<?php if ($description) { ?>
<meta name="description" content="<?php echo $description; ?>" />
<?php } ?>
<?php if ($keywords) { ?>
<meta name="keywords" content= "<?php echo $keywords; ?>" />
<?php } ?>
<link href="https://alands.com.ua/image/favicon.png" type="image/x-icon" rel="shortcut icon"> 
 


<script src="catalog/view/javascript/jquery/jquery-2.1.1.min.js" type="text/javascript"></script>
<link href="catalog/view/javascript/bootstrap/css/bootstrap.min.css" rel="stylesheet" media="screen" />
<script src="catalog/view/javascript/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
<link href="catalog/view/javascript/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css" />
<link href="catalog/view/theme/brand/stylesheet/stylesheet.css" rel="stylesheet">
<link href="catalog/view/theme/brand/stylesheet/checkbox.css" rel="checkbox">
<?php foreach ($styles as $style) { ?>
<link href="<?php echo $style['href']; ?>" type="text/css" rel="<?php echo $style['rel']; ?>" media="<?php echo $style['media']; ?>" />
<?php } ?>
<script src="catalog/view/javascript/common.js" type="text/javascript"></script>
<script src="catalog/view/javascript/jquery.equalheights.min.js"></script>  

<?php if (!empty($mlseo_meta)) { echo $mlseo_meta; } ?>
      
<?php foreach ($links as $link) { ?>
<link href="<?php echo $link['href']; ?>" rel="<?php echo $link['rel']; ?>" />
<?php } ?>
<?php if (!empty($mlseo_fix_search)) { ?><script type="text/javascript">$(document).ready(function() { $('#header input[name=\'search\']').unbind('keydown'); $('.button-search').unbind('click'); $('#header input[name=\'search\']').on('keydown', function(e){ if (e.keyCode == 13) {$('.button-search').click();}}); $('.button-search').on('click', function() { var url = '<?php echo isset($csp_search_url) ? $csp_search_url : "$('base').attr('href') + 'index.php?route=product/search"; ?>'; var value = $('input[name=\'search\']').attr('value'); if (value) {if (url.indexOf('?') > -1) {url += '&search=' + encodeURIComponent(value);} else {url += '?search=' + encodeURIComponent(value);}} location = url; });});</script><?php } ?>
<?php foreach ($scripts as $script) { ?>
<script src="<?php echo $script; ?>" type="text/javascript"></script>
<?php } ?>
<?php echo $tc_og; $microdatapro_main_flag = 1; //microdatapro 7.0 - 1 - main ?>
<?php foreach ($analytics as $analytic) { ?>
<?php echo $analytic; ?>
<?php } ?>


<!-- Global site tag (gtag.js) - AdWords: 819047199 -->
<script async src="https://www.googletagmanager.com/gtag/js?id=AW-819047199"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'AW-819047199');
</script>
<script>
  gtag('event', 'page_view', {
    'send_to': 'AW-819047199',
    'ecomm_pagetype': 'replace with value',
    'ecomm_prodid': 'replace with value',
    'ecomm_totalvalue': 'replace with value',
    'user_id': 'replace with value'
  });
</script>



<script type="text/javascript" >
$(document).ready(function() {
$('.my_minus').click(function () {
var $input = $(this).parent().find('.quant');
var count = parseInt($input.val()) - 1;
count = count < 1 ? 1 : count;
$input.val(count);
$input.change();
return false;
});
$('.my_plus').click(function () {
var $input = $(this).parent().find('.quant');
$input.val(parseInt($input.val()) + 1);
$input.change();
return false;
});
});
</script>
<!-- Yandex.Metrika counter -->
<!-- Yandex.Metrika counter -->
<script type="text/javascript" >
    (function (d, w, c) {
        (w[c] = w[c] || []).push(function() {
            try {
                w.yaCounter47523964 = new Ya.Metrika2({
                    id:47523964,
                    clickmap:true,
                    trackLinks:true,
                    accurateTrackBounce:true,
                    webvisor:true,
                    trackHash:true
                });
            } catch(e) { }
        });

        var n = d.getElementsByTagName("script")[0],
            s = d.createElement("script"),
            f = function () { n.parentNode.insertBefore(s, n); };
        s.type = "text/javascript";
        s.async = true;
        s.src = "https://mc.yandex.ru/metrika/tag.js";

        if (w.opera == "[object Opera]") {
            d.addEventListener("DOMContentLoaded", f, false);
        } else { f(); }
    })(document, window, "yandex_metrika_callbacks2");
</script>
<noscript><div><img src="https://mc.yandex.ru/watch/47523964" style="position:absolute; left:-9999px;" alt="" /></div></noscript>


<!-- /Yandex.Metrika counter -->
<?php echo isset($tc_og)?$tc_og:''; //microdatapro 7.0 ?>
<script>
	(function(i,s,o,g,r,a,m){
	i["esSdk"] = r;
	i[r] = i[r] || function() {
		(i[r].q = i[r].q || []).push(arguments)
	}, a=s.createElement(o), m=s.getElementsByTagName(o)[0]; a.async=1; a.src=g;
	m.parentNode.insertBefore(a,m)}
	) (window, document, "script", "https://esputnik.com/scripts/v1/public/scripts?apiKey=eyJhbGciOiJSUzI1NiJ9.eyJzdWIiOiI0NTI0ZWZhYTJkYzI2MGRmYTM4YTE1NDBlMWFlYmU0OGQ2OTAzNTEyM2M0MGRlODA0MGE5NGY3MjljOTY0ZDRmNmFlM2Y3MzE1YWJlYmUyOTJjMDk1N2Y0OTM3Mjg3NWM1ZWI1YjcxZjQxNzUzNWVkZjE0OTE0ODM2YTU5YmU0MWE5Y2FiMzI2NzIyYzNiMDViNzZiYjEyM2IxODg0MDE0MDE4ZWUwOTI5YjFhNjUxMyJ9.ZevhLFV-QQAKgQjm9B9HhzJ3bTi1uDcuT0YEjOaAdQZXMr7GovmwzFWTkEr7xYTBU2mwOthBoRSSo_1AhFYPYw&domain=DD466438-522A-4870-8CA6-1F252DF54B8F", "es");
	es("pushOn");
</script>
<!-- Facebook Pixel Code -->
<script>
  !function(f,b,e,v,n,t,s)
  {if(f.fbq)return;n=f.fbq=function(){n.callMethod?
  n.callMethod.apply(n,arguments):n.queue.push(arguments)};
  if(!f._fbq)f._fbq=n;n.push=n;n.loaded=!0;n.version='2.0';
  n.queue=[];t=b.createElement(e);t.async=!0;
  t.src=v;s=b.getElementsByTagName(e)[0];
  s.parentNode.insertBefore(t,s)}(window, document,'script',
  'https://connect.facebook.net/en_US/fbevents.js');
  fbq('init', '1258211120975496');
  fbq('track', 'PageView');
</script>
<noscript><img height="1" width="1" style="display:none"
  src="https://www.facebook.com/tr?id=1258211120975496&ev=PageView&noscript=1"
/></noscript>
<!-- End Facebook Pixel Code -->
			
<script type="text/javascript" src="catalog/view/javascript/jquery/colorbox/jquery.colorbox.js"></script>
<link rel="stylesheet" href="catalog/view/javascript/jquery/colorbox/colorbox.css" />
<script type="text/javascript" src="catalog/view/javascript/jquery/colorbox/jquery.colorbox-min.js"></script>
<script type="text/javascript">
$(document).ready(function(){
							
				$(".pergunta").colorbox({iframe:true, innerWidth:320, innerHeight:477});
							
			});
</script> 
<script type="text/javascript">
jQuery.colorbox.settings.maxWidth = '95%';
jQuery.colorbox.settings.maxHeight = '95%';
// ColorBox resize function
var resizeTimer;
function resizeColorBox()
{
if (resizeTimer) clearTimeout(resizeTimer);
resizeTimer = setTimeout(function() {
if (jQuery('‪#‎cboxOverlay‬').is(':visible')) {
jQuery.colorbox.load(true);
}
}, 300);
}
// Resize ColorBox when resizing window or changing mobile device orientation
jQuery(window).resize(resizeColorBox);
window.addEventListener("orientationchange", resizeColorBox, false);
</script> 
			

				<script type="text/javascript" src="catalog/view/javascript/ajax-product-page-loader.js"></script>
				<style>
					#ajax_loader {
						width: 100%;
						height: 30px;
						margin-top: 15px;
						text-align: center;
						border: none!important;	
					}
					#arrow_top {
						background: url("../../../../../image/chevron_up.png") no-repeat transparent;
						background-size: cover;
						position: fixed;
						bottom: 50px;
						right: 15px;
						cursor: pointer;
						height: 50px;
						width: 50px;
					}
				</style>
            

    					<!-- Membership --> 					
    					<?php if ($membership_nearest_discount || $membership_nearest_gift) { ?>
				        <script src="catalog/view/javascript/membership_nearest.js" type="text/javascript"></script>
<link href="catalog/view/theme/default/stylesheet/membership_nearest.css" rel="stylesheet" type="text/css" />
				        <?php } ?>			        
    					<!-- Membership -->
    				
<?php if(!isset($microdatapro_main_flag)){echo $tc_og;} //microdatapro 7.0 - 2 - extra ?>

					<!-- Top stickers start-->
						<?php if ($topstickers_status) { ?>
							<style type="text/css">
								.topstickers_wrapper {
									position: absolute;
									z-index: 999;
									color: #fff;
									text-transform: uppercase;
									font-weight: bold;
									line-height: 1.75;
								}
								<?php if ($topstickers_position == 0) { ?>
									.topstickers_wrapper {
										top: 5px;
										left: 15px;
										right: auto;
									}
								<?php } else { ?>
									.topstickers_wrapper {
										top: 5px;
										right: 15px;
										left: auto;
									}
								<?php } ?>
								.topstickers {
									padding: 0 10px;
									margin-bottom: 5px;
								}
								<?php if ($topstickers_position == 0) { ?>
									.topstickers {
										box-shadow: 2px 2px 2px #000;
									}
								<?php } else { ?>
									.topstickers {
										box-shadow: -2px 2px 2px #000;
									}
								<?php } ?>
								.topstickers_sold {
									background-color: <?php echo $topstickers_sold_bg; ?>;
								}
								.topstickers_sale {
									background-color: <?php echo $topstickers_sale_bg; ?>;
								}
								.topstickers_bestseller {
									background-color: <?php echo $topstickers_bestseller_bg; ?>;
								}
								.topstickers_novelty {
									background-color: <?php echo $topstickers_novelty_bg; ?>;
								}
								.topstickers_last {
									background-color: <?php echo $topstickers_last_bg; ?>;
								}
								.topstickers_freeshipping {
									background-color: <?php echo $topstickers_freeshipping_bg; ?>;
								}
							</style>
						<?php } ?>
					<!-- Top stickers end-->
				

				<script type="text/javascript" src="catalog/view/theme/default/js/scrolltopcontrol.js"></script>
			
</head>

    				<!-- Membership -->
    				<?php if ($membership_nearest_discount) { ?>
    				<div id="membership-nearest-discount-panel" style="display: none;">
    					<div id="membership-d"><span></span> <i class="fa fa-credit-card" aria-hidden="true"></i></div>
						<div id="membership-nearest-discount-panel-hidden">
						    <div id="membership-c-d-t"></div>
						    <div id="membership-n-d-t"></div>
						</div>
					</div>
					<?php } ?>
					<?php if ($membership_nearest_gift) { ?>
    				<div id="membership-nearest-gift-panel" style="display: none;">
    					<div id="membership-g"><i class="fa fa-gift" aria-hidden="true"></i></div>
						<div id="membership-nearest-gift-panel-hidden">
						    <div id="membership-c-g-t"></div>
						    <div id="membership-n-g-t"></div>
						</div>
					</div>
					<?php } ?>
    				<!-- Membership -->
    			
<body class="<?php echo $class; ?>">
<!-- Google Tag Manager (noscript) -->
<noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-5PMZ7JH"
height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
<!-- End Google Tag Manager (noscript) -->
<!-- Global site tag (gtag.js) - AdWords: 819047199 -->
<script async src="https://www.googletagmanager.com/gtag/js?id=AW-819047199"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'AW-819047199');
</script>


<nav id="top">
  <div class="container">
    <div class="row">
      <div class="col-sm-12 ">
        <ul>
        <li><a href=""></a>Экспресс-доставка 1-2 дня</li>
        <li><a href=""></a>Легкий Возврат и Обмен</li>
        <li><a href=""></a>Оплата после получения</li>
        <li><a href=""></a>Скидки до 10% по Дисконту</li>
      </ul></div>
    </div> 
  </div>
</nav>
<header>
  <div class="container">
    <div class="row nav-row">
      <div class="col-sm-2 logo text-center">
          <?php if ($logo) { ?>
		  
		 
		  
		  
		  
          <a href="<?php echo $home; ?>"><!-- <img src="<?php echo $logo; ?>" title="<?php echo $name; ?>" alt="<?php echo $name; ?>" class="img-responsive" /> --> <div class="logos-b">aland's</div></a><p><span>Мировые Бренды</span></p>
          <?php } else { ?>
          <h1><a href="<?php echo $home; ?>"><?php echo $name; ?></a></h1>
          <?php } ?>
      </div>
      <div class="col-sm-2 col-sm-offset-5"><!--<button class="btn nav-btn" data-toggle="modal" data-target="#myModal">Заказать звонок</button>--></div>

      <div class="col-sm-1 acc-block"><div class="dropdown"><a href="<?php echo $account; ?>" title="<?php echo $text_account; ?>" class="dropdown-toggle" data-toggle="dropdown"><img src="/catalog/view/theme/brand/image/user.png" width=16px; height=16px; alt=""><!-- <span class="hidden-xs hidden-sm hidden-md"><?php echo $text_account; ?></span>  --><span class="caret"></span></a>
          <ul class="dropdown-menu dropdown-menu-right">
            <?php if ($logged) { ?>
            <li><a href="<?php echo $account; ?>"><?php echo $text_account; ?></a></li>
            <li><a href="<?php echo $order; ?>"><?php echo $text_order; ?></a></li>
<!--             <li><a href="<?php echo $transaction; ?>"><?php echo $text_transaction; ?></a></li>
            <li><a href="<?php echo $download; ?>"><?php echo $text_download; ?></a></li> -->
            <li><a href="<?php echo $logout; ?>"><?php echo $text_logout; ?></a></li>
            <?php } else { ?>
            <li><a href="<?php echo $register; ?>"><?php echo $text_register; ?></a></li>
            <li><a href="<?php echo $login; ?>"><?php echo $text_login; ?></a></li>
            <?php } ?>
          </ul>
        </div>
      </div>
      <div class="col-sm-1"><?php echo $cart; ?></div>
    </div>
  </div>
</header>
        <div class="serch serch-block"><?php echo $search; ?> </div>

			<?php if($megamenu_status){ ?>
<script type="text/javascript" src="catalog/view/javascript/megamenu/megamenu.js?v3"></script>
<script type="text/javascript" src="catalog/view/javascript/megamenu/jquery.menu-aim.js?v3"></script>
<link rel="stylesheet" href="catalog/view/theme/default/stylesheet/megamenu.css">
<?php if($config_fixed_panel_top) { ?>
<script type="text/javascript">
<?php if($config_main_menu_selection =='0') { ?>
jQuery(function($){
		var scrolled = false;
		$(window).scroll(function(){
			    if(200<$(window).scrollTop() && !scrolled){                   
					if(!$('#top-fixed #horizontal-menu').length){
						$('#top-fixed').addClass("sticky-header-2");
						var menu_2 = $('.menu-fixed').html();					
						$('#top-fixed').append('<div>'+ menu_2 +'</div>');					
						scrolled = true;
					$('#top-fixed').hover(function() {
						$('#top-fixed #horizontal-menu .menu-full-width .dropdown-menu').each(function() {
							var menu = $('#horizontal-menu .container').offset();
							var dropdown = $(this).parent().offset();		
							var i = (dropdown.left + $(this).outerWidth()) - (menu.left + $('#horizontal-menu .container').outerWidth());	
							
							if (i > 0) {
								$(this).css('margin-left', '-' + (i + 10) + 'px');
							} 
							var width = $('#horizontal-menu .container').outerWidth();
							$(this).css('width', '' + (width - 23) + 'px');
						});
					});
						
					}
				}
				if(200>=$(window).scrollTop() && scrolled){
					$('#top-fixed').removeClass("sticky-header-2");
				    $('#top-fixed div').remove();
					scrolled = false;
				}
		});
	});
<?php } ?>
</script>
<?php } ?>
<div id="top-fixed" class="hidden-xs hidden-sm">
	<div class="container"></div>
</div>
<?php if($config_main_menu_selection =='0') { ?>
<?php if ($items) { ?>
<div class="menu-fixed">
	<?php if($hmenu_type =='1') { ?>
		<nav id="horizontal-menu" class="navbar">
	<?php } ?>
  <div class="container">
  	<?php if($hmenu_type =='0') { ?>
		<nav id="horizontal-menu" class="navbar hmenu_type">
	<?php } ?>
		<div class="navbar-header"><span id="category" class="visible-xs"><?php echo $text_category; ?></span>
		  <button type="button" class="btn btn-navbar navbar-toggle" data-toggle="collapse" data-target=".navbar-ex1-collapse"><i class="fa fa-bars"></i></button>
		</div>
		<div class="collapse navbar-collapse navbar-ex1-collapse">
		  <ul class="nav navbar-nav">
		  
			<?php foreach ($items as $item) { ?>
				<?php if ($item['children']) { ?>
				<li class="dropdown">
					<a href="<?php echo $item['href']; ?>" <?php if($item['new_blank'] == 1) {echo 'target="_blank" data-target="link"';} else {echo 'class="dropdown-toggle dropdown-img" data-toggle="dropdown"';} ?>>
						<?php if($item['thumb']){?>
							<img alt="<?php echo $item['name'][$lang_id]; ?>" title="<?php echo $item['name'][$lang_id]; ?>" class="nsmenu-thumb" src="<?php echo $item['thumb']?>"/>
						<?php } ?>
							<?php if($item['sticker_parent'] !='0'){?>
								<?php if($item['sticker_parent'] =='new') { ?>
									<span class="cat-label cat-label-label1"><?php echo $item['sticker_parent'];?></span>
								<?php } elseif($item['sticker_parent'] =='sale') { ?>
									<span class="cat-label cat-label-label2"><?php echo $item['sticker_parent'];?></span>
								<?php } else { ?>
									<span class="cat-label cat-label-label3"><?php echo $item['sticker_parent'];?></span>
								<?php } ?>	
							<?php } ?>
						<?php echo $item['name'][$lang_id]; ?>
					</a>
					
					<?php if($item['type']=="category"){ ?>
					<?php if($item['subtype']=="simple"){ ?>
					  <div class="dropdown-menu nsmenu-type-category-simple">
						<div class="dropdown-inner">
						  <?php foreach (array_chunk($item['children'], ceil(count($item['children']) / 1)) as $children) { ?>  
							<ul class="list-unstyled nsmenu-haschild">
								<?php foreach ($children as $child) { ?>
								<li class="<?php if(count($child['children'])){?> nsmenu-issubchild<?php }?>">
									<a href="<?php echo $child['href']; ?>">
									<?php echo $child['name']; ?>
										<?php if($child['sticker_category'] !='0'){?>
											<?php if($child['sticker_category'] =='new') { ?>
												<span class="cat-label cat-label-label1"><?php echo $child['sticker_category'];?></span>
											<?php } elseif($child['sticker_category'] =='sale') { ?>
												<span class="cat-label cat-label-label2"><?php echo $child['sticker_category'];?></span>
											<?php } else { ?>
												<span class="cat-label cat-label-label3"><?php echo $child['sticker_category'];?></span>
											<?php } ?>	
										<?php } ?>
									</a>
									<?php if(count($child['children'])){?>
									<ul class="list-unstyled nsmenu-ischild nsmenu-ischild-simple">
									 <?php foreach ($child['children'] as $subchild) { ?>
									<li><a href="<?php echo $subchild['href']; ?>">
										<?php echo $subchild['name']; ?>
											<?php if($subchild['sticker_category'] !='0'){?>
												<?php if($subchild['sticker_category'] =='new') { ?>
													<span class="cat-label cat-label-label1"><?php echo $subchild['sticker_category'];?></span>
												<?php } elseif($subchild['sticker_category'] =='sale') { ?>
													<span class="cat-label cat-label-label2"><?php echo $subchild['sticker_category'];?></span>
												<?php } else { ?>
													<span class="cat-label cat-label-label3"><?php echo $subchild['sticker_category'];?></span>
												<?php } ?>	
											<?php } ?>
									</a></li>				
									<?php } ?>
									</ul>
									<?php } ?>				
								</li>
								<?php } ?>
							</ul>
						  <?php } ?>
						</div>            
						</div>
						<?php } ?>	
						<?php } ?>
						
						<?php if($item['type']=="category"){?>
						<?php if($item['subtype']=="full"){?>
						  <div class="dropdown-menu nsmenu-type-category-full nsmenu-bigblock">
							<div class="dropdown-inner">
							  <?php foreach (array_chunk($item['children'], ceil(count($item['children']) / 1)) as $children) { ?>							
								<?php if($item['add_html']){ ?>
								<div style="" class="menu-add-html">									
									<?php echo html_entity_decode($item['add_html'][$lang_id], ENT_QUOTES, 'UTF-8'); ?>
								</div>
								<?php } ?>
								<ul class="list-unstyled nsmenu-haschild">
								<?php foreach ($children as $child) { ?>
								<li class="nsmenu-parent-block<?php if(count($child['children'])){?> nsmenu-issubchild<?php }?>">
								<a class="nsmenu-parent-title" href="<?php echo $child['href']; ?>">
									<?php echo $child['name']; ?>
										<?php if($child['sticker_category'] !='0'){?>
											<?php if($child['sticker_category'] =='new') { ?>
												<span class="cat-label cat-label-label1"><?php echo $child['sticker_category'];?></span>
											<?php } elseif($child['sticker_category'] =='sale') { ?>
												<span class="cat-label cat-label-label2"><?php echo $child['sticker_category'];?></span>
											<?php } else { ?>
												<span class="cat-label cat-label-label3"><?php echo $child['sticker_category'];?></span>
											<?php } ?>	
										<?php } ?>
								</a>
									<?php if(count($child['children'])){?>
									<ul class="list-unstyled nsmenu-ischild">
									 <?php foreach ($child['children'] as $subchild) { ?>
									<li><a href="<?php echo $subchild['href']; ?>">
										<?php echo $subchild['name']; ?>
											<?php if($subchild['sticker_category'] !='0'){?>
												<?php if($subchild['sticker_category'] =='new') { ?>
													<span class="cat-label cat-label-label1"><?php echo $subchild['sticker_category'];?></span>
												<?php } elseif($subchild['sticker_category'] =='sale') { ?>
													<span class="cat-label cat-label-label2"><?php echo $subchild['sticker_category'];?></span>
												<?php } else { ?>
													<span class="cat-label cat-label-label3"><?php echo $subchild['sticker_category'];?></span>
												<?php } ?>	
											<?php } ?>
									</a></li>				
									<?php }?>
									</ul>
									<?php }?>				
								</li>
								<?php } ?>
								</ul>
							  <?php } ?>
							</div>            
							</div>
							<?php } ?>	
							<?php } ?>
							<?php if($item['type']=="category"){?>
							<?php if($item['subtype']=="full_image"){?>
							  <div class="dropdown-menu nsmenu-type-category-full-image nsmenu-bigblock">
								<div class="dropdown-inner">
								  <?php foreach (array_chunk($item['children'], ceil(count($item['children']) / 1)) as $children) { ?>
								
									<?php if($item['add_html']){?>
								  <div style="" class="menu-add-html">
									<?php echo html_entity_decode($item['add_html'][$lang_id], ENT_QUOTES, 'UTF-8'); ?>
									</div>
									<?php }?>
								  
									<ul class="list-unstyled nsmenu-haschild">
									<?php foreach ($children as $child) { ?>
									<li class="nsmenu-parent-block <?php if(count($child['children'])){?> nsmenu-issubchild<?php } ?>">
									<a class="nsmenu-parent-img" href="<?php echo $child['href']; ?>"><img src="<?php echo $child['thumb']; ?>" alt="<?php echo $child['name']; ?>" title="<?php echo $child['name']; ?>"/></a>
									<a class="nsmenu-parent-title" href="<?php echo $child['href']; ?>">
									<?php echo $child['name']; ?>
										<?php if($child['sticker_category'] !='0'){?>
											<?php if($child['sticker_category'] =='new') { ?>
												<span class="cat-label cat-label-label1"><?php echo $child['sticker_category'];?></span>
											<?php } elseif($child['sticker_category'] =='sale') { ?>
												<span class="cat-label cat-label-label2"><?php echo $child['sticker_category'];?></span>
											<?php } else { ?>
												<span class="cat-label cat-label-label3"><?php echo $child['sticker_category'];?></span>
											<?php } ?>	
										<?php } ?>
									</a>
									
									<?php if(count($child['children'])){?>
									<ul class="list-unstyled nsmenu-ischild">
									 <?php foreach ($child['children'] as $subchild) { ?>
									<li><a href="<?php echo $subchild['href']; ?>">
										<?php echo $subchild['name']; ?>
											<?php if($subchild['sticker_category'] !='0'){?>
												<?php if($subchild['sticker_category'] =='new') { ?>
													<span class="cat-label cat-label-label1"><?php echo $subchild['sticker_category'];?></span>
												<?php } elseif($subchild['sticker_category'] =='sale') { ?>
													<span class="cat-label cat-label-label2"><?php echo $subchild['sticker_category'];?></span>
												<?php } else { ?>
													<span class="cat-label cat-label-label3"><?php echo $subchild['sticker_category'];?></span>
												<?php } ?>	
											<?php } ?>
									</a></li>				
									<?php } ?>
									</ul>
									<?php } ?>				
									</li>
									<?php } ?>
									</ul>
								  <?php } ?>
								</div>            
								</div>
								<?php }?>	
								<?php }?>
								
								<?php if($item['type']=="html"){?>
								  <div class="dropdown-menu nsmenu-type-html">
									<div class="dropdown-inner">
										<ul class="list-unstyled nsmenu-haschild">										
											<li class="nsmenu-parent-block">
												<div class="nsmenu-html-block">				
													<?php echo html_entity_decode($item['html'][$lang_id], ENT_QUOTES, 'UTF-8'); ?>
												</div>
											</li>
										</ul>									
									</div>            
								   </div>	
								<?php } ?>
								
								<?php if($item['type']=="manufacturer"){?>
									<div class="dropdown-menu nsmenu-type-manufacturer <?php if($item['add_html']){?>nsmenu-bigblock<?php } ?>">
										<div class="dropdown-inner">
											<?php foreach (array_chunk($item['children'], ceil(count($item['children']) / 1)) as $children) { ?>
												<?php if($item['add_html']){ ?>
													<div class="menu-add-html">
														<?php echo html_entity_decode($item['add_html'][$lang_id], ENT_QUOTES, 'UTF-8'); ?>
													</div>
												<?php } ?>									  
												<ul class="list-unstyled nsmenu-haschild <?php if($item['add_html']){?>nsmenu-blockwithimage<?php } ?>">
												<?php foreach ($children as $child) { ?>
													<li class="nsmenu-parent-block">
														<a class="nsmenu-parent-img" href="<?php echo $child['href']; ?>"><img src="<?php echo $child['thumb']; ?>" alt="<?php echo $child['name']; ?>" title="<?php echo $child['name']; ?>" /></a>
														<a class="nsmenu-parent-title" href="<?php echo $child['href']; ?>"><?php echo $child['name']; ?></a>
													</li>
												<?php } ?>
												</ul>
											<?php } ?>
										</div>            
									</div>
								<?php } ?>
								
								<?php if($item['type']=="information"){?>	
									<div class="dropdown-menu nsmenu-type-information <?php if($item['add_html']){?>nsmenu-bigblock<?php } ?>">
										<div class="dropdown-inner">
											<?php foreach (array_chunk($item['children'], ceil(count($item['children']) / 1)) as $children) { ?>
												<?php if($item['add_html']){?>
													<div class="menu-add-html">
														<?php echo html_entity_decode($item['add_html'][$lang_id], ENT_QUOTES, 'UTF-8'); ?>
													</div>
												<?php }?>									  
												<ul class="list-unstyled nsmenu-haschild <?php if($item['add_html']){?>nsmenu-blockwithimage<?php } ?>">
													<?php foreach ($children as $child) { ?>
														<li class=""><a href="<?php echo $child['href']; ?>"><?php echo $child['name']; ?></a></li>
													<?php } ?>
												</ul>
											<?php } ?>
										</div>            
									</div>
								<?php } ?>
								
								<?php if($item['type']=="product"){?>
									<div class="dropdown-menu nsmenu-type-product <?php if($item['add_html']){?>nsmenu-bigblock<?php } ?>">
										<div class="dropdown-inner">
											<?php foreach (array_chunk($item['children'], ceil(count($item['children']) / 1)) as $children) { ?>
												<?php if($item['add_html']){?>
													<div style="" class="menu-add-html">
													<?php echo html_entity_decode($item['add_html'][$lang_id], ENT_QUOTES, 'UTF-8'); ?>
													</div>
												<?php } ?>
												<ul class="list-unstyled nsmenu-haschild <?php if($item['add_html']){?>nsmenu-blockwithimage<?php }?>">
													<?php foreach ($children as $child) { ?>
														<li class="nsmenu-parent-block">
															<a class="nsmenu-parent-img" href="<?php echo $child['href']; ?>"><img src="<?php echo $child['thumb']; ?>" alt="<?php echo $child['name']; ?>" title="<?php echo $child['name']; ?>" /></a>
															<a class="nsmenu-parent-title" href="<?php echo $child['href']; ?>"><?php echo $child['name']; ?></a>
															<div class="price">
																		<?php if (!$child['special']) { ?>
																			<?php echo $child['price']; ?>
																		<?php } else { ?>
																			<span class="price-old"><?php echo $child['price']; ?></span> 
																			<span class="price-new"><?php echo $child['special']; ?></span>
																		<?php } ?>
																	</div>					
														</li>
													<?php } ?>
												</ul>
											<?php } ?>
										</div>            
									</div>
								<?php } ?>
					</li>
				<?php } else {  ?>
					<li><a <?php if($item['new_blank'] == 1) {echo 'target="_blank" data-target="link"';} else {echo 'class="dropdown-toggle dropdown-img" data-toggle="dropdown"';} ?> href="<?php echo $item['href']; ?>"><?php if($item['thumb']){?>
							<img class="nsmenu-thumb" src="<?php echo $item['thumb']?>"/>
						<?php } ?>
						<?php if($item['sticker_parent'] !='0'){?>
								<?php if($item['sticker_parent'] =='new') { ?>
									<span class="cat-label cat-label-label1"><?php echo $item['sticker_parent'];?></span>
								<?php } elseif($item['sticker_parent'] =='sale') { ?>
									<span class="cat-label cat-label-label2"><?php echo $item['sticker_parent'];?></span>
								<?php } else { ?>
									<span class="cat-label cat-label-label3"><?php echo $item['sticker_parent'];?></span>
								<?php } ?>	
							<?php } ?>
						<?php echo $item['name'][$lang_id]; ?></a></li>
				<?php } ?>
			<?php } ?>
		  </ul>
		</div>
		<?php if($hmenu_type =='0') { ?>
			</nav>
		<?php } ?>
	</div>
		<?php if($hmenu_type =='1') { ?>
			</nav>
		<?php } ?>
 </div>
<script type="text/javascript">
$(window).bind("load resize",function() {
var $horizontal_menu = $('#horizontal-menu .navbar-nav');
if($(window).width() > 922){
	$horizontal_menu.menuAim('switchToHover');
} else {
    $horizontal_menu.menuAim('switchToClick');
}

	$(function() {	
		$horizontal_menu.menuAim({
			triggerEvent:       'hover',
			activateCallback:   activateAttidionamenu,
			deactivateCallback: deactivateAttidionamenu,
			submenuDirection:   'below',
			openClassName:      'open',
			activationDelay:    200
		});
		function activateAttidionamenu(row) {
			$(row).addClass('open');
		}
		function deactivateAttidionamenu(row) {
			$(row).removeClass('open');
		}
	});
	
});
    </script>
<?php } ?>
<?php } ?>
<?php if($config_main_menu_selection =='1') { ?>
<div class="container"> 
	<div class="row">   
	   <div class="col-md-9 col-md-push-3 hidden-xs hidden-sm clearfix">	   	
		<?php if($additional) {?>
		<nav id="additional-menu" class="navbar hmenu_type">
			<div class="navbar-header"><span id="category" class="visible-xs"><?php echo $text_category; ?></span>
			  <button type="button" class="btn btn-navbar navbar-toggle" data-toggle="collapse" data-target=".navbar-ex1-collapse"><i class="fa fa-bars"></i></button>
			</div>
		<div class="collapse navbar-collapse navbar-ex1-collapse">
		  <ul class="nav navbar-nav">
			<?php foreach ($items as $item) { ?>
				<?php if ($item['children']) { ?>
				<li class="dropdown">
				<?php if($item['additional_menu']=="additional"){ ?>
					<a href="<?php echo $item['href']; ?>" <?php if($item['new_blank'] == 1) {echo 'target="_blank" data-target="link"';} else {echo 'class="dropdown-toggle dropdown-img" data-toggle="dropdown"';} ?>>
						<?php if($item['thumb']){?>
							<img alt="<?php echo $item['name'][$lang_id]; ?>" class="nsmenu-thumb" src="<?php echo$item['thumb']?>"/>
						<?php } ?>
							<?php if($item['sticker_parent'] !='0'){?>
								<?php if($item['sticker_parent'] =='new') { ?>
									<span class="cat-label cat-label-label1"><?php echo $item['sticker_parent'];?></span>
								<?php } elseif($item['sticker_parent'] =='sale') { ?>
									<span class="cat-label cat-label-label2"><?php echo $item['sticker_parent'];?></span>
								<?php } else { ?>
									<span class="cat-label cat-label-label3"><?php echo $item['sticker_parent'];?></span>
								<?php } ?>	
							<?php } ?>
						<?php echo $item['name'][$lang_id]; ?>
					</a>
					<?php } ?>
					<?php if($item['type']=="category"){ ?>
					<?php if($item['subtype']=="simple"){ ?>
					<?php if($item['additional_menu']=="additional"){ ?>
					  <div class="dropdown-menu nsmenu-type-category-simple">
						<div class="dropdown-inner">
						  <?php foreach (array_chunk($item['children'], ceil(count($item['children']) / 1)) as $children) { ?>  
							<ul class="list-unstyled nsmenu-haschild">
								<?php foreach ($children as $child) { ?>
								<li class="<?php if(count($child['children'])){?> nsmenu-issubchild<?php }?>">
									<a href="<?php echo $child['href']; ?>">
									<?php echo $child['name']; ?>
										<?php if($child['sticker_category'] !='0'){?>
											<?php if($child['sticker_category'] =='new') { ?>
												<span class="cat-label cat-label-label1"><?php echo $child['sticker_category'];?></span>
											<?php } elseif($child['sticker_category'] =='sale') { ?>
												<span class="cat-label cat-label-label2"><?php echo $child['sticker_category'];?></span>
											<?php } else { ?>
												<span class="cat-label cat-label-label3"><?php echo $child['sticker_category'];?></span>
											<?php } ?>	
										<?php } ?>
									</a>
									<?php if(count($child['children'])){?>
									<ul class="list-unstyled nsmenu-ischild nsmenu-ischild-simple">
									 <?php foreach ($child['children'] as $subchild) { ?>
									<li><a href="<?php echo $subchild['href']; ?>">
										<?php echo $subchild['name']; ?>
											<?php if($subchild['sticker_category'] !='0'){?>
												<?php if($subchild['sticker_category'] =='new') { ?>
													<span class="cat-label cat-label-label1"><?php echo $subchild['sticker_category'];?></span>
												<?php } elseif($subchild['sticker_category'] =='sale') { ?>
													<span class="cat-label cat-label-label2"><?php echo $subchild['sticker_category'];?></span>
												<?php } else { ?>
													<span class="cat-label cat-label-label3"><?php echo $subchild['sticker_category'];?></span>
												<?php } ?>	
											<?php } ?>
									</a></li>				
									<?php } ?>
									</ul>
									<?php } ?>				
								</li>
								<?php } ?>
							</ul>
						  <?php } ?>
						</div>            
						</div>
						<?php } ?>	
						<?php } ?>
						<?php } ?>
						<!--END SIMPLE-->
						<?php if($item['type']=="category"){?>
						<?php if($item['subtype']=="full"){?>
						<?php if($item['additional_menu']=="additional"){ ?>
						  <div class="dropdown-menu nsmenu-type-category-full nsmenu-bigblock-additional">
							<div class="dropdown-inner">
							  <?php foreach (array_chunk($item['children'], ceil(count($item['children']) / 1)) as $children) { ?>							
								<?php if($item['add_html']){ ?>
								<div class="menu-add-html">									
									<?php echo html_entity_decode($item['add_html'][$lang_id], ENT_QUOTES, 'UTF-8'); ?>
								</div>
								<?php } ?>
								<ul class="list-unstyled nsmenu-haschild">
								<?php foreach ($children as $child) { ?>
								<li class="nsmenu-parent-block<?php if(count($child['children'])){?> nsmenu-issubchild<?php }?>">
								<a class="nsmenu-parent-title" href="<?php echo $child['href']; ?>">
									<?php echo $child['name']; ?>
										<?php if($child['sticker_category'] !='0'){?>
											<?php if($child['sticker_category'] =='new') { ?>
												<span class="cat-label cat-label-label1"><?php echo $child['sticker_category'];?></span>
											<?php } elseif($child['sticker_category'] =='sale') { ?>
												<span class="cat-label cat-label-label2"><?php echo $child['sticker_category'];?></span>
											<?php } else { ?>
												<span class="cat-label cat-label-label3"><?php echo $child['sticker_category'];?></span>
											<?php } ?>	
										<?php } ?>
								</a>
									<?php if(count($child['children'])){?>
									<ul class="list-unstyled nsmenu-ischild">
									 <?php foreach ($child['children'] as $subchild) { ?>
									<li><a href="<?php echo $subchild['href']; ?>">
										<?php echo $subchild['name']; ?>
											<?php if($subchild['sticker_category'] !='0'){?>
												<?php if($subchild['sticker_category'] =='new') { ?>
													<span class="cat-label cat-label-label1"><?php echo $subchild['sticker_category'];?></span>
												<?php } elseif($subchild['sticker_category'] =='sale') { ?>
													<span class="cat-label cat-label-label2"><?php echo $subchild['sticker_category'];?></span>
												<?php } else { ?>
													<span class="cat-label cat-label-label3"><?php echo $subchild['sticker_category'];?></span>
												<?php } ?>	
											<?php } ?>
									</a></li>				
									<?php }?>
									</ul>
									<?php }?>				
								</li>
								<?php } ?>
								</ul>
							  <?php } ?>
							</div>            
							</div>
							<?php }?>	
							<?php }?>
							<?php }?>
							<!--END FULL-->
							<?php if($item['type']=="category"){?>
							<?php if($item['subtype']=="full_image"){?>
							<?php if($item['additional_menu']=="additional"){ ?>
							  <div class="dropdown-menu nsmenu-type-category-full-image nsmenu-bigblock-additional">
								<div class="dropdown-inner">
								  <?php foreach (array_chunk($item['children'], ceil(count($item['children']) / 1)) as $children) { ?>
								
									<?php if($item['add_html']){?>
								  <div style="" class="menu-add-html">
									<?php echo html_entity_decode($item['add_html'][$lang_id], ENT_QUOTES, 'UTF-8'); ?>
									</div>
									<?php }?>
								  
									<ul class="list-unstyled nsmenu-haschild">
									<?php foreach ($children as $child) { ?>
									<li class="nsmenu-parent-block<?php if(count($child['children'])){?> nsmenu-issubchild<?php }?>">
									<a class="nsmenu-parent-img" href="<?php echo $child['href']; ?>"><img src="<?php echo $child['thumb']; ?>" alt="<?php echo $child['name']; ?>" title="<?php echo $child['name']; ?>"/></a>
									<a class="nsmenu-parent-title" href="<?php echo $child['href']; ?>">
									<?php echo $child['name']; ?>
										<?php if($child['sticker_category'] !='0'){?>
											<?php if($child['sticker_category'] =='new') { ?>
												<span class="cat-label cat-label-label1"><?php echo $child['sticker_category'];?></span>
											<?php } elseif($child['sticker_category'] =='sale') { ?>
												<span class="cat-label cat-label-label2"><?php echo $child['sticker_category'];?></span>
											<?php } else { ?>
												<span class="cat-label cat-label-label3"><?php echo $child['sticker_category'];?></span>
											<?php } ?>	
										<?php } ?>
									</a>
									
									<?php if(count($child['children'])){?>
									<ul class="list-unstyled nsmenu-ischild">
									 <?php foreach ($child['children'] as $subchild) { ?>
									<li><a href="<?php echo $subchild['href']; ?>">
										<?php echo $subchild['name']; ?>
											<?php if($subchild['sticker_category'] !='0'){?>
												<?php if($subchild['sticker_category'] =='new') { ?>
													<span class="cat-label cat-label-label1"><?php echo $subchild['sticker_category'];?></span>
												<?php } elseif($subchild['sticker_category'] =='sale') { ?>
													<span class="cat-label cat-label-label2"><?php echo $subchild['sticker_category'];?></span>
												<?php } else { ?>
													<span class="cat-label cat-label-label3"><?php echo $subchild['sticker_category'];?></span>
												<?php } ?>	
											<?php } ?>
									</a></li>				
									<?php }?>
									</ul>
									<?php }?>				
									</li>
									<?php } ?>
									</ul>
								  <?php } ?>
								</div>            
								</div>
								<?php }?>	
								<?php }?>
								<?php }?>
								<!--END FULL-IMAGE-->
								<?php if($item['type']=="html"){?>
								<?php if($item['additional_menu']=="additional"){ ?>
								  <div class="dropdown-menu nsmenu-type-html">
									<div class="dropdown-inner">
										<ul class="list-unstyled nsmenu-haschild">										
											<li class="nsmenu-parent-block">
												<div class="nsmenu-html-block">				
													<?php echo html_entity_decode($item['html'][$lang_id], ENT_QUOTES, 'UTF-8'); ?>
												</div>
											</li>
										</ul>									
									</div>            
								   </div>	
								<?php } ?>
								<?php } ?>
								<!--END HTML-->
								<?php if($item['type']=="manufacturer"){?>
								<?php if($item['additional_menu']=="additional"){ ?>
									<div class="dropdown-menu nsmenu-type-manufacturer <?php if($item['add_html']){?>nsmenu-bigblock-additional<?php }?>">
										<div class="dropdown-inner">
											<?php foreach (array_chunk($item['children'], ceil(count($item['children']) / 1)) as $children) { ?>
												<?php if($item['add_html']){?>
													<div style="" class="menu-add-html">
														<?php echo html_entity_decode($item['add_html'][$lang_id], ENT_QUOTES, 'UTF-8'); ?>
													</div>
												<?php }?>									  
												<ul class="list-unstyled nsmenu-haschild <?php if($item['add_html']){?>nsmenu-blockwithimage<?php }?>">
												<?php foreach ($children as $child) { ?>
													<li class="nsmenu-parent-block">
														<a class="nsmenu-parent-img" href="<?php echo $child['href']; ?>"><img src="<?php echo $child['thumb']; ?>" alt="<?php echo $child['name']; ?>" title="<?php echo $child['name']; ?>" /></a>
														<a class="nsmenu-parent-title" href="<?php echo $child['href']; ?>"><?php echo $child['name']; ?></a>
													</li>
												<?php } ?>
												</ul>
											<?php } ?>
										</div>            
									</div>
								<?php }?>
								<?php }?>
								<!--END MANUFACTURES-->
								<?php if($item['type']=="information"){?>
								<?php if($item['additional_menu']=="additional"){ ?>								
									<div class="dropdown-menu nsmenu-type-information <?php if($item['add_html']){?>nsmenu-bigblock-additional<?php }?>">
										<div class="dropdown-inner">
											<?php foreach (array_chunk($item['children'], ceil(count($item['children']) / 1)) as $children) { ?>
												<?php if($item['add_html']){?>
													<div style="" class="menu-add-html">
														<?php echo html_entity_decode($item['add_html'][$lang_id], ENT_QUOTES, 'UTF-8'); ?>
													</div>
												<?php }?>									  
												<ul class="list-unstyled nsmenu-haschild <?php if($item['add_html']){?>nsmenu-blockwithimage<?php }?>">
													<?php foreach ($children as $child) { ?>
														<li class=""><a href="<?php echo $child['href']; ?>"><?php echo $child['name']; ?></a></li>
													<?php } ?>
												</ul>
											<?php } ?>
										</div>            
									</div>
								<?php } ?>
								<?php } ?>
								<!--END INFORMATION-->
								<?php if($item['type']=="product"){?>
								<?php if($item['additional_menu']=="additional"){ ?>
									<div class="dropdown-menu nsmenu-type-product <?php if($item['add_html']){?>nsmenu-bigblock-additional<?php }?>">
										<div class="dropdown-inner">
											<?php foreach (array_chunk($item['children'], ceil(count($item['children']) / 1)) as $children) { ?>
												<?php if($item['add_html']){?>
													<div style="" class="menu-add-html">
													<?php echo html_entity_decode($item['add_html'][$lang_id], ENT_QUOTES, 'UTF-8'); ?>
													</div>
												<?php } ?>
												<ul class="list-unstyled nsmenu-haschild <?php if($item['add_html']){?>nsmenu-blockwithimage<?php }?>">
													<?php foreach ($children as $child) { ?>
														<li class="nsmenu-parent-block">
															<a class="nsmenu-parent-img" href="<?php echo $child['href']; ?>"><img src="<?php echo $child['thumb']; ?>" alt="<?php echo $child['name']; ?>" title="<?php echo $child['name']; ?>" /></a>
															<a class="nsmenu-parent-title" href="<?php echo $child['href']; ?>"><?php echo $child['name']; ?></a>
															<div class="price">
																		<?php if (!$child['special']) { ?>
																			<?php echo $child['price']; ?>
																		<?php } else { ?>
																			<span class="price-old"><?php echo $child['price']; ?></span> 
																			<span class="price-new"><?php echo $child['special']; ?></span>
																		<?php } ?>
																	</div>					
														</li>
													<?php } ?>
												</ul>
											<?php } ?>
										</div>            
									</div>
								<?php }?>
								<?php }?>
					</li>
				<?php } else {  ?>
					<?php if($item['additional_menu']=="additional"){ ?>
						<li><a <?php if($item['new_blank'] == 1) {echo 'target="_blank" data-target="link"';} else {echo 'class="dropdown-toggle dropdown-img" data-toggle="dropdown"';} ?> class="<?php if(!$item['thumb']){?>no-img-parent-link<?php } ?>" href="<?php echo $item['href']; ?>">
						<?php if($item['thumb']){?>
							<img class="nsmenu-thumb" src="<?php echo$item['thumb']?>"/>
						<?php } ?>
						<?php if($item['sticker_parent'] !='0'){?>
								<?php if($item['sticker_parent'] =='new') { ?>
									<span class="cat-label cat-label-label1"><?php echo $item['sticker_parent'];?></span>
								<?php } elseif($item['sticker_parent'] =='sale') { ?>
									<span class="cat-label cat-label-label2"><?php echo $item['sticker_parent'];?></span>
								<?php } else { ?>
									<span class="cat-label cat-label-label3"><?php echo $item['sticker_parent'];?></span>
								<?php } ?>	
							<?php } ?>
						<?php echo $item['name'][$lang_id]; ?></a></li>
					<?php } ?>
				<?php } ?>
			<?php } ?>
		  </ul>
		</div>		
	   </nav>   
<script type="text/javascript">
$(function() {
    var $additional_menu = $('#additional-menu .navbar-nav');
	$additional_menu.menuAim({
        triggerEvent:       'hover',
        activateCallback:   activateAttidionamenu,
        deactivateCallback: deactivateAttidionamenu,
        submenuDirection:   'below',
        openClassName:      'open',
        activationDelay:    200
    });
    function activateAttidionamenu(row) {
		$(row).addClass('open');
    }
    function deactivateAttidionamenu(row) {
        $(row).removeClass('open');
    }
});
</script>
<?php } ?>	   
	  </div>
		<div class="col-sm-12 col-md-3 col-md-pull-9">
			<?php if ($items) { ?>
			<nav id="menu-vertical" class="btn-group btn-block">
				<button type="button" class="btn btn-menu btn-block dropdown-toggle" data-toggle="dropdown">
					<i class="fa fa-bars"></i>
					<?php echo $text_category; ?>
				</button>
				<ul id="menu-vertical-list" class="dropdown-menu">
				<?php $m_item = 0;?>
					<?php foreach ($items as $item) { ?>
						<?php if ($item['children']) { ?>
							<li data-submenu-id="menu-amazon-<?php echo $m_item;?>" class="dropdown <?php if($item['additional_menu']=="additional"){ ?>hidden-md hidden-lg<?php } ?>">
							<span class="toggle-child">
								<i class="fa fa-plus plus"></i>
								<i class="fa fa-minus minus"></i>
							</span>
								<a  href="<?php echo $item['href']; ?>" <?php if($item['new_blank'] == 1) {echo 'target="_blank" data-target="link" class="parent-link dropdown-toggle"'; } else {echo 'class="parent-link  dropdown-toggle dropdown-img" data-toggle="dropdown"';} ?>>
									<?php if($item['thumb']){?>
										<img alt="<?php echo $item['name'][$lang_id]; ?>" class="nsmenu-thumb" src="<?php echo$item['thumb']?>"/>
									<?php } ?><i class="fa fa-angle-down arrow"></i>
										<?php if($item['sticker_parent'] !='0'){?>
											<?php if($item['sticker_parent'] =='new') { ?>
												<span class="cat-label cat-label-label1"><?php echo $item['sticker_parent'];?></span>
											<?php } elseif($item['sticker_parent'] =='sale') { ?>
												<span class="cat-label cat-label-label2"><?php echo $item['sticker_parent'];?></span>
											<?php } else { ?>
												<span class="cat-label cat-label-label3"><?php echo $item['sticker_parent'];?></span>
											<?php } ?>	
										<?php } ?>
									<?php echo $item['name'][$lang_id]; ?>
								</a>
							
								<!--SIMPLE CATEGORY-->
								<?php if($item['type']=="category"){ ?>
								<?php if($item['subtype']=="simple"){ ?>
								
								  <div id="menu-amazon-<?php echo $m_item;?>" class="ns-dd dropdown-menu-simple nsmenu-type-category-simple">
									<div class="dropdown-inner">
									  <?php foreach (array_chunk($item['children'], ceil(count($item['children']) / 1)) as $children) { ?>  
										<ul class="list-unstyled nsmenu-haschild">
											<?php foreach ($children as $child) { ?>
											<li class="<?php if(count($child['children'])){?> nsmenu-issubchild<?php }?>">
												<a href="<?php echo $child['href']; ?>"><?php if(count($child['children'])){?><i class="fa fa-angle-down arrow"></i><?php }?>
												<?php echo $child['name']; ?>
												<?php if($child['sticker_category'] !='0'){?>
													<?php if($child['sticker_category'] =='new') { ?>
														<span class="cat-label cat-label-label1"><?php echo $child['sticker_category'];?></span>
													<?php } elseif($child['sticker_category'] =='sale') { ?>
														<span class="cat-label cat-label-label2"><?php echo $child['sticker_category'];?></span>
													<?php } else { ?>
														<span class="cat-label cat-label-label3"><?php echo $child['sticker_category'];?></span>
													<?php } ?>	
												<?php } ?>
												</a>
												<?php if(count($child['children'])){?>
												<ul class="list-unstyled nsmenu-ischild nsmenu-ischild-simple">
												 <?php foreach ($child['children'] as $subchild) { ?>
												<li><a href="<?php echo $subchild['href']; ?>">
													<?php echo $subchild['name']; ?>
												<?php if($subchild['sticker_category'] !='0'){?>
													<?php if($subchild['sticker_category'] =='new') { ?>
														<span class="cat-label cat-label-label1"><?php echo $subchild['sticker_category'];?></span>
													<?php } elseif($subchild['sticker_category'] =='sale') { ?>
														<span class="cat-label cat-label-label2"><?php echo $subchild['sticker_category'];?></span>
													<?php } else { ?>
														<span class="cat-label cat-label-label3"><?php echo $subchild['sticker_category'];?></span>
													<?php } ?>	
												<?php } ?>
												</a>
												</li>				
												<?php } ?>
												</ul>
												<?php } ?>				
											</li>
											<?php } ?>
										</ul>
									  <?php } ?>
									</div>            
									</div>
									
									<?php } ?>	
									<?php } ?>
									<!--END SIMPLE CATEGORY-->
									
									<!-- FULL CATEGORY-->
									<?php if($item['type']=="category"){?>
									<?php if($item['subtype']=="full"){?>
									
									  <div id="menu-amazon-<?php echo $m_item;?>" class="ns-dd dropdown-menu-full nsmenu-type-category-full box-col-3">
										<div class="dropdown-inner">
										  <?php foreach (array_chunk($item['children'], ceil(count($item['children']) / 1)) as $children) { ?>							
											<?php if($item['add_html']){ ?>
											<div style="" class="menu-add-html">									
												<?php echo html_entity_decode($item['add_html'][$lang_id], ENT_QUOTES, 'UTF-8'); ?>
											</div>
											<?php } ?>
											<ul class="list-unstyled nsmenu-haschild">
											<?php foreach ($children as $child) { ?>
											<li class="nsmenu-parent-block<?php if(count($child['children'])){?> nsmenu-issubchild<?php }?>">
											<a class="nsmenu-parent-title" href="<?php echo $child['href']; ?>">
											<?php echo $child['name']; ?>
												<?php if($child['sticker_category'] !='0'){?>
													<?php if($child['sticker_category'] =='new') { ?>
														<span class="cat-label cat-label-label1"><?php echo $child['sticker_category'];?></span>
													<?php } elseif($child['sticker_category'] =='sale') { ?>
														<span class="cat-label cat-label-label2"><?php echo $child['sticker_category'];?></span>
													<?php } else { ?>
														<span class="cat-label cat-label-label3"><?php echo $child['sticker_category'];?></span>
													<?php } ?>	
												<?php } ?>
											</a>
												<?php if(count($child['children'])){?>
												<ul class="list-unstyled nsmenu-ischild">
												
												 <?php foreach ($child['children'] as $subchild) { ?>						
												<li><a href="<?php echo $subchild['href']; ?>"><i class="fa fa-level-up fa-rotate-90"></i> 
												<?php echo $subchild['name']; ?>
												<?php if($subchild['sticker_category'] !='0'){?>
													<?php if($subchild['sticker_category'] =='new') { ?>
														<span class="cat-label cat-label-label1"><?php echo $subchild['sticker_category'];?></span>
													<?php } elseif($subchild['sticker_category'] =='sale') { ?>
														<span class="cat-label cat-label-label2"><?php echo $subchild['sticker_category'];?></span>
													<?php } else { ?>
														<span class="cat-label cat-label-label3"><?php echo $subchild['sticker_category'];?></span>
													<?php } ?>	
												<?php } ?>	
												</a></li>				
												<?php }?>
												</ul>
												<?php }?>				
											</li>
											<?php } ?>
											</ul>
										  <?php } ?>
										</div>            
										</div>
										
										<?php }?>
										<?php }?>
										<!-- END FULL CATEGORY -->
										<!-- FULL-IMAGE CATEGORY -->
										<?php if($item['type']=="category"){?>
										<?php if($item['subtype']=="full_image"){?>
										
										  <div id="menu-amazon-<?php echo $m_item;?>" class="ns-dd dropdown-menu-full-image nsmenu-type-category-full-image box-col-3">
											<div class="dropdown-inner">
											  <?php foreach (array_chunk($item['children'], ceil(count($item['children']) / 1)) as $children) { ?>
											
												<?php if($item['add_html']){?>
											  <div class="menu-add-html">
												<?php echo html_entity_decode($item['add_html'][$lang_id], ENT_QUOTES, 'UTF-8'); ?>
												</div>
												<?php }?>
											  
												<ul class="list-unstyled nsmenu-haschild">
												<?php foreach ($children as $child) { ?>
												<li class="nsmenu-parent-block<?php if(count($child['children'])){?> nsmenu-issubchild<?php }?>">
												<a class="nsmenu-parent-img" href="<?php echo $child['href']; ?>"><img src="<?php echo $child['thumb']; ?>" alt="<?php echo $child['name']; ?>" title="<?php echo $child['name']; ?>"/></a>
												<a class="nsmenu-parent-title" href="<?php echo $child['href']; ?>">
												<?php echo $child['name']; ?>
												<?php if($child['sticker_category'] !='0'){?>
													<?php if($child['sticker_category'] =='new') { ?>
														<span class="cat-label cat-label-label1"><?php echo $child['sticker_category'];?></span>
													<?php } elseif($child['sticker_category'] =='sale') { ?>
														<span class="cat-label cat-label-label2"><?php echo $child['sticker_category'];?></span>
													<?php } else { ?>
														<span class="cat-label cat-label-label3"><?php echo $child['sticker_category'];?></span>
													<?php } ?>	
												<?php } ?>
												</a>
												
												<?php if(count($child['children'])){?>
												<ul class="list-unstyled nsmenu-ischild">
												 <?php foreach ($child['children'] as $subchild) { ?>
												<li><a href="<?php echo $subchild['href']; ?>">
												<i class="fa fa-level-up fa-rotate-90"></i> 
												<?php echo $subchild['name']; ?>
												<?php if($subchild['sticker_category'] !='0'){?>
													<?php if($subchild['sticker_category'] =='new') { ?>
														<span class="cat-label cat-label-label1"><?php echo $subchild['sticker_category'];?></span>
													<?php } elseif($subchild['sticker_category'] =='sale') { ?>
														<span class="cat-label cat-label-label2"><?php echo $subchild['sticker_category'];?></span>
													<?php } else { ?>
														<span class="cat-label cat-label-label3"><?php echo $subchild['sticker_category'];?></span>
													<?php } ?>	
												<?php } ?>	
												</a></li>				
												<?php }?>
												</ul>
												<?php }?>				
												</li>
												<?php } ?>
												</ul>
											  <?php } ?>
											</div>            
											</div>
											
											<?php }?>
											<?php }?>
										<!-- END FULL-IMAGE CATEGORY -->
										
										<!-- HTML-BLOCK -->
										<?php if($item['type']=="html"){?>
										
										  <div id="menu-amazon-<?php echo $m_item;?>" class="ns-dd dropdown-menu-html-block nsmenu-type-html box-col-2">
											<div class="dropdown-inner">
												<ul class="list-unstyled nsmenu-haschild">										
													<li class="nsmenu-parent-block">
														<div class="nsmenu-html-block">				
															<?php echo html_entity_decode($item['html'][$lang_id], ENT_QUOTES, 'UTF-8'); ?>
														</div>
													</li>
												</ul>									
											</div>            
										   </div>	
										
										<?php } ?>
										<!-- END HTML-BLOCK -->
										
										<!-- MANUFACTURER-BLOCK -->
										<?php if($item['type']=="manufacturer"){?>
										
											<div id="menu-amazon-<?php echo $m_item;?>" class="ns-dd dropdown-menu-manufacturer nsmenu-type-manufacturer <?php if($item['add_html']){?>box-col-3<?php } else{ ?> box-col-2 <?php } ?>">
												<div class="dropdown-inner">
													<?php foreach (array_chunk($item['children'], ceil(count($item['children']) / 1)) as $children) { ?>
														<?php if($item['add_html']){?>
															<div class="menu-add-html">
																<?php echo html_entity_decode($item['add_html'][$lang_id], ENT_QUOTES, 'UTF-8'); ?>
															</div>
														<?php }?>									  
														<ul class="list-unstyled nsmenu-haschild <?php if($item['add_html']){?>nsmenu-blockwithimage<?php }?>">
														<?php foreach ($children as $child) { ?>
															<li class="nsmenu-parent-block">
																<a class="nsmenu-parent-img" href="<?php echo $child['href']; ?>"><img src="<?php echo $child['thumb']; ?>" alt="<?php echo $child['name']; ?>" title="<?php echo $child['name']; ?>" /></a>
																<a class="nsmenu-parent-title" href="<?php echo $child['href']; ?>"><?php echo $child['name']; ?></a>
															</li>
														<?php } ?>
														</ul>
													<?php } ?>
												</div>            
											</div>
									
										<?php }?>
										<!-- END MANUFACTURER-BLOCK -->
										
										<!-- INFORMATION-BLOCK -->
										<?php if($item['type']=="information"){?>
																		
											<div id="menu-amazon-<?php echo $m_item;?>" class="ns-dd dropdown-menu-information nsmenu-type-information <?php if($item['add_html']){?>box-col-3<?php } else{ ?> box-col-2 <?php } ?>">
												<div class="dropdown-inner">
													<?php foreach (array_chunk($item['children'], ceil(count($item['children']) / 1)) as $children) { ?>
														<?php if($item['add_html']){?>
															<div class="menu-add-html">
																<?php echo html_entity_decode($item['add_html'][$lang_id], ENT_QUOTES, 'UTF-8'); ?>
															</div>
														<?php }?>									  
														<ul class="list-unstyled nsmenu-haschild <?php if($item['add_html']){?>nsmenu-blockwithimage<?php }?>">
															<?php foreach ($children as $child) { ?>
																<li><a href="<?php echo $child['href']; ?>"><?php echo $child['name']; ?></a></li>
															<?php } ?>
														</ul>
													<?php } ?>
												</div>            
											</div>
										
										<?php } ?>
										<!-- END INFORMATION-BLOCK -->
										
										<!-- PRODUCT-BLOCK -->
										<?php if($item['type']=="product"){ ?>
										
											<div id="menu-amazon-<?php echo $m_item;?>" class="ns-dd dropdown-menu-product nsmenu-type-product box-col-2">
												<div class="dropdown-inner">
													<?php foreach (array_chunk($item['children'], ceil(count($item['children']) / 1)) as $children) { ?>
														<?php if($item['add_html']){?>
															<div style="" class="menu-add-html">
															<?php echo html_entity_decode($item['add_html'][$lang_id], ENT_QUOTES, 'UTF-8'); ?>
															</div>
														<?php } ?>
														<ul class="list-unstyled nsmenu-haschild <?php if($item['add_html']){?>nsmenu-blockwithimage<?php }?>">
															<?php foreach ($children as $child) { ?>
																<li class="nsmenu-parent-block">
																	<a class="nsmenu-parent-img" href="<?php echo $child['href']; ?>"><img src="<?php echo $child['thumb']; ?>" alt="<?php echo $child['name']; ?>" title="<?php echo $child['name']; ?>" /></a>
																	<a class="nsmenu-parent-title" href="<?php echo $child['href']; ?>"><?php echo $child['name']; ?></a>
																	<div class="price">
																		<?php if (!$child['special']) { ?>
																			<?php echo $child['price']; ?>
																		<?php } else { ?>
																			<span class="price-old"><?php echo $child['price']; ?></span> 
																			<span class="price-new"><?php echo $child['special']; ?></span>
																		<?php } ?>
																	</div>				
																</li>
															<?php } ?>
														</ul>
													<?php } ?>
												</div>            
											</div>
										
										<?php } ?>
										<!-- END PRODUCT-BLOCK -->
										
							</li>
						<?php } else {  ?>
								<li <?php if($item['additional_menu']=="additional"){ ?>class="hidden-md hidden-lg"<?php } ?>>						
									<a class="dropdown-toggle" <?php if($item['new_blank'] == 1) {echo 'target="_blank" data-target="link"';} ?> href="<?php echo $item['href']; ?>">
										<?php if($item['thumb']){?>
											<img class="nsmenu-thumb" src="<?php echo$item['thumb']?>"/>
										<?php } ?>
										<?php if($item['sticker_parent'] !='0'){?>
											<?php if($item['sticker_parent'] =='new') { ?>
												<span class="cat-label cat-label-label1"><?php echo $item['sticker_parent'];?></span>
											<?php } elseif($item['sticker_parent'] =='sale') { ?>
												<span class="cat-label cat-label-label2"><?php echo $item['sticker_parent'];?></span>
											<?php } else { ?>
												<span class="cat-label cat-label-label3"><?php echo $item['sticker_parent'];?></span>
											<?php } ?>	
										<?php } ?>
										<?php echo $item['name'][$lang_id]; ?>
									</a>
								</li>							
						<?php } ?>
					<?php $m_item++;?>
					<?php } ?>
				</ul>
			</nav>
			<div id="menuMask"></div>	
			<?php } ?>
		</div>	
<script type="text/javascript">
$(window).bind("load resize",function(e) {
	var $menu = $("#menu-vertical-list");
	if($(window).width() > 922){
		$menu.menuAim('switchToHover');
	} else {
        $menu.menuAim('switchToClick');
	}
		
        $menu.menuAim({
            activateCallback: activateSubmenu,
            deactivateCallback: deactivateSubmenu
        });
		
        function activateSubmenu(row) {
            var $row = $(row),
                submenuId = $row.data("submenuId"),
                $submenu = $("#" + submenuId),
                height = $menu.outerHeight(),
                width = $menu.outerWidth();
            $submenu.css({
                display: "block",
				top:-1
            });
            $row.find("a").addClass("maintainHover");
        }
        function deactivateSubmenu(row) {
            var $row = $(row),
                submenuId = $row.data("submenuId"),
                $submenu = $("#" + submenuId);
            $submenu.css("display", "none");
            $row.find("a").removeClass("maintainHover");
        }
        $("#menu-vertical .dropdown-menu li").click(function(e) {
            e.stopPropagation();
        });
        $("#menu-vertical .dropdown-menu li").click(function(e) {
            $("a.maintainHover").removeClass("maintainHover");
        });
	
});
		
	$(".ns-dd").hover(function() {$(this).parent().find('.parent-link').toggleClass('hover');});
	$(".child-box").hover(function() {$(this).parent().find('.with-child').toggleClass('hover');});
	$(".toggle-child").click(function() {
		$(this).toggleClass('open');
		$(this).parent().parent().parent().toggleClass('activemenu');
		$(this).parent().find('.dropdown-menu-simple').slideToggle(200);
		$(this).parent().find('.dropdown-menu-full').slideToggle(200);
		$(this).parent().find('.dropdown-menu-full-image').slideToggle(200);
		$(this).parent().find('.dropdown-menu-html-block').slideToggle(200);
		$(this).parent().find('.dropdown-menu-manufacturer').slideToggle(200);
		$(this).parent().find('.dropdown-menu-information').slideToggle(200);
		$(this).parent().find('.dropdown-menu-product').slideToggle(200);
	});
</script>
    </div>
  </div>
<?php } ?>

<?php } ?>
<?php if ($categories && !$megamenu_status) { ?>
			
<div class="container-fluid mainmenu">
  <div class="container">
  <nav id="menu" class="navbar">
    <div class="navbar-header"><span id="category" class="visible-xs"><?php echo $text_category; ?></span>
      <button type="button" class="btn btn-navbar navbar-toggle" data-toggle="collapse" data-target=".navbar-ex1-collapse"><i class="fa fa-bars"></i></button>
    </div>
    <div class="collapse navbar-collapse navbar-ex1-collapse">
      <ul class="nav navbar-nav">
        <?php foreach ($categories as $category) { ?>
        <?php if ($category['children']) { ?>
        <li class="dropdown"><a href="<?php echo $category['href']; ?>" class="dropdown-toggle" data-toggle="dropdown"><?php echo $category['name']; ?></a>
          <div class="dropdown-menu">
            <div class="dropdown-inner">
              <?php foreach (array_chunk($category['children'], ceil(count($category['children']) / $category['column'])) as $children) { ?>
              <ul class="list-unstyled">
                <?php foreach ($children as $child) { ?>
                <li><a href="<?php echo $child['href']; ?>"><?php echo $child['name']; ?></a></li>
                <?php } ?>
              </ul>
              <?php } ?>
            </div>
            <a href="<?php echo $category['href']; ?>" class="see-all"><?php echo $text_all; ?> <?php echo $category['name']; ?></a> </div>
        </li>
        <?php } else { ?>
        <li><a href="<?php echo $category['href']; ?>"><?php echo $category['name']; ?></a></li>
        <?php } ?>
        <?php } ?>
        <li><a href="/index.php?route=product/manufacturer">БРЕНДЫ</a></li>
        <li><a href="/index.php?route=product/special"><span class="red">SALE</span></a></li>
        <li><a href="/novinki">Новинки</a></li>
        <li><a href="/podarochnye-karty"><span class="red">ПОДАРОЧНЫЕ КАРТЫ</span></a></li>
        <li><a href="/kontakty"><span class="red">КОНТАКТЫ</span></a></li>
      </ul>
    </div>
    </div>
  </nav>
</div>

<?php } ?>