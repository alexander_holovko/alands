<?php echo $header; ?>
<div class="container">
   
				<ul class="breadcrumb" >
		<?php
		$breadcount =count($breadcrumbs)-1;
		foreach ($breadcrumbs as $iterator => $breadcrumb) {
		if ($iterator != $breadcount) {
		echo '<li itemscope itemtype="http://data-vocabulary.org/Breadcrumb"><a href="'.$breadcrumb['href'].'" itemprop="url"><span itemprop="title">'.$breadcrumb['text'].'</span></a></li>';
		} else {
		echo '<li itemscope itemtype="http://data-vocabulary.org/Breadcrumb" class="end"><a href="'.$breadcrumb['href'].'" itemprop="url"></a><span itemprop="title">'.$breadcrumb['text'].'</span></li>';
		}
		} ?>
    	</ul>
		
			
  <div class="relog">SALE до - 70%</div>
</ul>
  <div class="row"><?php echo $column_left; ?>
    <?php if ($column_left && $column_right) { ?>
    <?php $class = 'col-sm-6'; ?>
    <?php } elseif ($column_left || $column_right) { ?>
    <?php $class = 'col-sm-12'; ?>
    <?php } else { ?>
    <?php $class = 'col-sm-12'; ?>
    <?php } ?>
    <div id="content" class="<?php echo $class; ?>"><?php echo $content_top; ?>
      <h1><?php echo $heading_title; ?></h1>
      <?php if ($products) { ?>
      <!-- <p><a href="<?php echo $compare; ?>" id="compare-total"><?php echo $text_compare; ?></a></p> -->
      <div class="row">
        <div class="col-sm-3">
          <div class="btn-group hidden-xs">
            <button type="button" id="list-view" class="btn btn-default" data-toggle="tooltip" title="<?php echo $button_list; ?>"><i class="fa fa-th-list"></i></button>
            <button type="button" id="grid-view" class="btn btn-default" data-toggle="tooltip" title="<?php echo $button_grid; ?>"><i class="fa fa-th"></i></button>
          </div>
        </div>
<!--         <div class="col-sm-1 col-sm-offset-2 text-right">
          <label class="control-label" for="input-sort"><?php echo $text_sort; ?></label>
        </div> -->
        <div class="col-sm-3 col-sm-offset-6 text-right">
          <select id="input-sort" class="form-control col-sm-3" onchange="location = this.value;">
            <?php foreach ($sorts as $sorts) { ?>
            <?php if ($sorts['value'] == $sort . '-' . $order) { ?>
            <option value="<?php echo $sorts['href']; ?>" selected="selected"><?php echo $text_sort; ?><!-- <?php echo $sorts['text']; ?> --></option>
            <?php } else { ?>
            <option value="<?php echo $sorts['href']; ?>"><?php echo $sorts['text']; ?></option>
            <?php } ?>
            <?php } ?>
          </select>
        </div>
<!--         <div class="col-sm-1 text-right">
          <label class="control-label" for="input-limit"><?php echo $text_limit; ?></label>
        </div>
        <div class="col-sm-2 text-right">
          <select id="input-limit" class="form-control" onchange="location = this.value;">
            <?php foreach ($limits as $limits) { ?>
            <?php if ($limits['value'] == $limit) { ?>
            <option value="<?php echo $limits['href']; ?>" selected="selected"><?php echo $limits['text']; ?></option>
            <?php } else { ?>
            <option value="<?php echo $limits['href']; ?>"><?php echo $limits['text']; ?></option>
            <?php } ?>
            <?php } ?>
          </select>
        </div> -->
      </div>
      <br />
      <div class="row">
        <?php foreach ($products as $product) {  ?>
        <div class="product-layout product-list col-xs-12">
          <div class="product-thumb">

					<!-- Top stickers start-->
						<?php if ($topstickers_status) { ?>
							<div class="topstickers_wrapper">
								<?php if ($product['product_sold_sticker']) { ?>
									<div class="topstickers topstickers_sold">
										<?php echo $product['product_sold_sticker']; ?>
									</div>
								<?php } ?>
								<?php if ($product['product_sale_sticker']) { ?>
									<div class="topstickers topstickers_sale">
										<?php echo $product['product_sale_sticker']; ?>
									</div>
								<?php } ?>
								<?php if ($product['product_bestseller_sticker']) { ?>
									<div class="topstickers topstickers_bestseller">
										<?php echo $product['product_bestseller_sticker']; ?>
									</div>
								<?php } ?>
								<?php if ($product['product_novelty_sticker']) { ?>
									<div class="topstickers topstickers_novelty">
										<?php echo $product['product_novelty_sticker']; ?>
									</div>
								<?php } ?>
								<?php if ($product['product_last_sticker']) { ?>
									<div class="topstickers topstickers_last">
										<?php echo $product['product_last_sticker']; ?>
									</div>
								<?php } ?>
								<?php if ($product['product_freeshipping_sticker']) { ?>
									<div class="topstickers topstickers_freeshipping">
										<?php echo $product['product_freeshipping_sticker']; ?>
									</div>
								<?php } ?>
								<?php if (!empty($product['product_custom_topstickers'])) { ?>
									<?php foreach ($product['product_custom_topstickers'] as $custom_topsticker) { ?>
										<div class="topstickers topstickers_<?php echo $custom_topsticker['name']; ?>" style="background:<?php echo $custom_topsticker['bg_color']; ?>;">
											<?php echo $custom_topsticker['text']; ?>
										</div>
									<?php } ?>
								<?php } ?>
							</div>
						<?php } ?>
					<!-- Top stickers end-->
				
            <div class="image"><a href="<?php echo $product['href']; ?>"><img src="<?php echo $product['thumb']; ?>" <?php foreach ($product['additional_img'] as $additional_img) { ?>data-additional="<?php echo $additional_img;?>"<?php } ?> alt="<?php echo $product['name']; ?>" title="<?php echo $product['name']; ?>" class="img-responsive" /></a></div>
            <?php if ($product['special']) { ?><div class="ribbon"><span><?php echo $product['percent']; ?></span></div><?php } ?>
            <div class="caption">
            <h5 style="font-size :17px; font-weight: bold;text-align: center;"><?php echo $product['manufacturer'];   ?></h5>
              <h4><a href="<?php echo $product['href']; ?>"><?php echo $product['name']; ?></a></h4>
             <!-- <p><?php echo $product['model']; ?></p>-->
              <!-- <p><?php echo $product['description']; ?></p> -->
              <?php if ($product['rating']) { ?>
              <div class="rating">
                <?php for ($i = 1; $i <= 5; $i++) { ?>
                <?php if ($product['rating'] < $i) { ?>
                <span class="fa fa-stack"><i class="fa fa-star-o fa-stack-2x"></i></span>
                <?php } else { ?>
                <span class="fa fa-stack"><i class="fa fa-star fa-stack-2x"></i><i class="fa fa-star-o fa-stack-2x"></i></span>
                <?php } ?>
                <?php } ?>
              </div>
              <?php } ?>
              <?php if ($product['price']) { ?>
              <p class="price">
                <?php if (!$product['special']) { ?>
                <?php echo $product['price']; ?>
                <?php } else { ?>
                 <span class="price-old"><?php echo $product['price']; ?></span><span class="price-new"><?php echo $product['special']; ?></span>
                <?php } ?>
                <?php if ($product['tax']) { ?>
                <span class="price-tax"><?php echo $text_tax; ?> <?php echo $product['tax']; ?></span>
                <?php } ?>
              </p>
              <?php } ?>
            </div>
            <div class="button-group">
              <button type="button" onclick="cart.add('<?php echo $product['product_id']; ?>', '<?php echo $product['minimum']; ?>');"><i class="fa fa-shopping-cart"></i> <span class="hidden-xs hidden-sm hidden-md"><?php echo $button_cart; ?></span></button>
              <button type="button" data-toggle="tooltip" title="<?php echo $button_wishlist; ?>" onclick="wishlist.add('<?php echo $product['product_id']; ?>');"><i class="fa fa-heart"></i></button>
              <button type="button" data-toggle="tooltip" title="<?php echo $button_compare; ?>" onclick="compare.add('<?php echo $product['product_id']; ?>');"><i class="fa fa-exchange"></i></button>
            </div>
          </div>
        </div>
        <?php } ?>
      </div>
      <div class="row">
        <div class="col-sm-6 text-left"><?php echo $pagination; ?></div>
        <div class="col-sm-6 text-right"><?php echo $results; ?></div>
      </div>
      <?php } else { ?>
      <p><?php echo $text_empty; ?></p>
      <div class="buttons">
        <div class="pull-right"><a href="<?php echo $continue; ?>" class="btn btn-primary"><?php echo $button_continue; ?></a></div>
      </div>
      <?php } ?>
      <?php echo $content_bottom; ?></div>
    <?php echo $column_right; ?></div>
</div>
<?php echo $footer; ?>