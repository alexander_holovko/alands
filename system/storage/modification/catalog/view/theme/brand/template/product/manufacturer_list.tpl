<?php echo $header; ?>
<div class="container">
   
				<ul class="breadcrumb" >
		<?php
		$breadcount =count($breadcrumbs)-1;
		foreach ($breadcrumbs as $iterator => $breadcrumb) {
		if ($iterator != $breadcount) {
		echo '<li itemscope itemtype="http://data-vocabulary.org/Breadcrumb"><a href="'.$breadcrumb['href'].'" itemprop="url"><span itemprop="title">'.$breadcrumb['text'].'</span></a></li>';
		} else {
		echo '<li itemscope itemtype="http://data-vocabulary.org/Breadcrumb" class="end"><a href="'.$breadcrumb['href'].'" itemprop="url"></a><span itemprop="title">'.$breadcrumb['text'].'</span></li>';
		}
		} ?>
    	</ul>
		
			
    <?php if ($column_left && $column_right) { ?>
    <?php $class = 'col-sm-6'; ?>
    <?php } elseif ($column_left || $column_right) { ?>
    <?php $class = 'col-sm-9'; ?>
    <?php } else { ?>
    <?php $class = 'col-sm-12'; ?>
    <?php } ?>
    <div id="content" class="<?php echo $class; ?>"><?php echo $content_top; ?>
      <h1 class="text-center"><?php echo $heading_title; ?></h1>
      <?php if ($categories) { ?>
      <br/>
      <p class="text-center"><strong>Топ-бренды</strong>
        <?php foreach ($categories as $category) { ?>
        &nbsp;&nbsp;&nbsp;<a href="index.php?route=product/manufacturer#<?php echo $category['name']; ?>"><?php echo $category['name']; ?></a>
        <?php } ?>
      </p>
      <br/>
      <br/>
      <h2 class=" text-center ">Топ-бренды</h2>
      <div class="row text-center brands-row">
      <?php foreach ($categories as $category) { ?> 
       <span id="<?php echo $category['name']; ?>"><?php echo $category['name']; ?> </span>
      <?php if ($category['manufacturer']) { ?>
      <?php foreach (array_chunk($category['manufacturer'], 4) as $manufacturers) { ?> 
        <?php foreach ($manufacturers as $manufacturer) { ?>
        <div class="col-sm-3 brands-logo brands-logo-hidd">
<!-- ManufacturerLogo -->
		<?php if ($manufacturer['logo']) { ?>
		<a href="<?php echo $manufacturer['href']; ?>"><img src="<?php echo $manufacturer['logo']; ?>" alt="<?php echo $manufacturer['name']; ?>" class="img-responsive" /></a>
		<?php } ?>
		<a href="<?php echo $manufacturer['href']; ?>"><?php echo $manufacturer['name']; ?></a>
<!-- ManufacturerLogo end -->
			</div>
        <?php } ?> 
      <?php } ?>
      <?php } ?>
      <?php } ?> 
      <?php } else { ?>
      <p><?php echo $text_empty; ?></p>
<!--       <div class="buttons clearfix">
        <div class="pull-right"><a href="<?php echo $continue; ?>" class="btn btn-primary"><?php echo $button_continue; ?></a></div>
      </div> -->
      <?php } ?>
      </div>
<p class="text-center"><button class="spoile-more btn-default">Смотреть все <i class="fa fa-angle-right" aria-hidden="true"></i></button></p>
      <?php echo $content_bottom; ?></div>
    <?php echo $column_right; ?></div>
</div>
<?php echo $footer; ?>