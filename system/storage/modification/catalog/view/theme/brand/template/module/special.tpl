<h3><?php echo $heading_title; ?></h3>
<div class="row">
  <?php foreach ($products as $product) { ?>
  <div class="product-layout col-lg-3 col-md-3 col-sm-6 col-xs-12">
    <div class="product-thumb transition">

					<!-- Top stickers start-->
						<?php if ($topstickers_status) { ?>
							<div class="topstickers_wrapper">
								<?php if ($product['product_sold_sticker']) { ?>
									<div class="topstickers topstickers_sold">
										<?php echo $product['product_sold_sticker']; ?>
									</div>
								<?php } ?>
								<?php if ($product['product_sale_sticker']) { ?>
									<div class="topstickers topstickers_sale">
										<?php echo $product['product_sale_sticker']; ?>
									</div>
								<?php } ?>
								<?php if ($product['product_bestseller_sticker']) { ?>
									<div class="topstickers topstickers_bestseller">
										<?php echo $product['product_bestseller_sticker']; ?>
									</div>
								<?php } ?>
								<?php if ($product['product_novelty_sticker']) { ?>
									<div class="topstickers topstickers_novelty">
										<?php echo $product['product_novelty_sticker']; ?>
									</div>
								<?php } ?>
								<?php if ($product['product_last_sticker']) { ?>
									<div class="topstickers topstickers_last">
										<?php echo $product['product_last_sticker']; ?>
									</div>
								<?php } ?>
								<?php if ($product['product_freeshipping_sticker']) { ?>
									<div class="topstickers topstickers_freeshipping">
										<?php echo $product['product_freeshipping_sticker']; ?>
									</div>
								<?php } ?>
								<?php if (!empty($product['product_custom_topstickers'])) { ?>
									<?php foreach ($product['product_custom_topstickers'] as $custom_topsticker) { ?>
										<div class="topstickers topstickers_<?php echo $custom_topsticker['name']; ?>" style="background:<?php echo $custom_topsticker['bg_color']; ?>;">
											<?php echo $custom_topsticker['text']; ?>
										</div>
									<?php } ?>
								<?php } ?>
							</div>
						<?php } ?>
					<!-- Top stickers end-->
				
      <div class="image"><a href="<?php echo $product['href']; ?>"><img src="<?php echo $product['thumb']; ?>" <?php foreach ($product['additional_img'] as $additional_img) { ?>data-additional="<?php echo $additional_img;?>"<?php } ?> alt="<?php echo $product['name']; ?>" title="<?php echo $product['name']; ?>" class="img-responsive" /></a></div>
<?php if ($product['special']) { ?><div class="ribbon"><span><?php echo $product['percent']; ?></span></div><?php } ?>
      <div class="caption">
        <h4><a href="<?php echo $product['href']; ?>"><?php echo $product['name']; ?></a></h4>
        <p><?php echo $product['description']; ?></p>
        <?php if ($product['rating']) { ?>
        <div class="rating">
          <?php for ($i = 1; $i <= 5; $i++) { ?>
          <?php if ($product['rating'] < $i) { ?>
          <span class="fa fa-stack"><i class="fa fa-star-o fa-stack-2x"></i></span>
          <?php } else { ?>
          <span class="fa fa-stack"><i class="fa fa-star fa-stack-2x"></i><i class="fa fa-star-o fa-stack-2x"></i></span>
          <?php } ?>
          <?php } ?>
        </div>
        <?php } ?>
        <?php if ($product['price']) { ?>
        <p class="price">
          <?php if (!$product['special']) { ?>
          <?php echo $product['price']; ?>
          <?php } else { ?>
          <span class="price-new"><?php echo $product['special']; ?></span> <span class="price-old"><?php echo $product['price']; ?></span>
          <?php } ?>
          <?php if ($product['tax']) { ?>
          <span class="price-tax"><?php echo $text_tax; ?> <?php echo $product['tax']; ?></span>
          <?php } ?>
        </p>
        <?php } ?>
      </div>
      <p>Hello</p>
      <div class="button-group">
        <button type="button" onclick="cart.add('<?php echo $product['product_id']; ?>');"><i class="fa fa-shopping-cart"></i> <span class="hidden-xs hidden-sm hidden-md"><?php echo $button_cart; ?></span></button>
        <button type="button" data-toggle="tooltip" title="<?php echo $button_wishlist; ?>" onclick="wishlist.add('<?php echo $product['product_id']; ?>');"><i class="fa fa-heart"></i></button>
        <button type="button" data-toggle="tooltip" title="<?php echo $button_compare; ?>" onclick="compare.add('<?php echo $product['product_id']; ?>');"><i class="fa fa-exchange"></i></button>
      </div>
    </div>
  </div>
  <?php } ?>
</div>
