<?php echo $header; ?>
<div class="container">
   
				<ul class="breadcrumb" >
		<?php
		$breadcount =count($breadcrumbs)-1;
		foreach ($breadcrumbs as $iterator => $breadcrumb) {
		if ($iterator != $breadcount) {
		echo '<li itemscope itemtype="http://data-vocabulary.org/Breadcrumb"><a href="'.$breadcrumb['href'].'" itemprop="url"><span itemprop="title">'.$breadcrumb['text'].'</span></a></li>';
		} else {
		echo '<li itemscope itemtype="http://data-vocabulary.org/Breadcrumb" class="end"><a href="'.$breadcrumb['href'].'" itemprop="url"></a><span itemprop="title">'.$breadcrumb['text'].'</span></li>';
		}
		} ?>
    	</ul>
			
    <?php if ($column_left && $column_right) { ?>
    <?php $class = 'col-sm-6'; ?>
    <?php } elseif ($column_left || $column_right) { ?>
    <?php $class = 'col-sm-9'; ?>
    <?php } else { ?>
    <?php $class = 'col-sm-12'; ?>
    <?php } ?>
    <div id="content" class="<?php echo $class; ?>"><?php echo $content_top; ?>
      <h1><?php echo $heading_title; ?></h1>
      <?php echo $text_message; ?>
      <div class="buttons">
        <div class="pull-right"><a href="<?php echo $continue; ?>" class="btn btn-primary"><?php echo $button_continue; ?></a></div>
      </div>
      <?php echo $content_bottom; ?></div>
    <?php echo $column_right; ?></div>
	<!-- НАЧАЛО кода модуля опроса -->
<script src="https://apis.google.com/js/platform.js?onload=renderOptIn"
  async defer>
</script>

<script>
  window.renderOptIn = function() { 
    window.gapi.load('surveyoptin', function() {
      window.gapi.surveyoptin.render(
        {
          // ОБЯЗАТЕЛЬНО
          "merchant_id":"124326170",
          "order_id": "<?php echo $text_order; ?>",
          "email": "<?php echo $email; ?>",
          "delivery_country": "<?php echo $text_shipping_address; ?>",
          "estimated_delivery_date": "<?php echo $delivery_date ?>",

          // НЕОБЯЗАТЕЛЬНО
          "opt_in_style": "BOTTOM_LEFT_DIALOG"
        }); 
     });
  }
</script>
<!-- КОНЕЦ кода модуля опроса -->
</div>
<!-- НАЧАЛО кода модуля опроса -->
<script src="https://apis.google.com/js/platform.js?onload=renderOptIn"
  async defer>
</script>

<script>
  window.renderOptIn = function() { 
    window.gapi.load('surveyoptin', function() {
      window.gapi.surveyoptin.render(
        {
          // ОБЯЗАТЕЛЬНО
          "merchant_id":"124326170",
          "order_id": "<?php echo $text_order; ?>",
          "email": "<?php echo $email; ?>",
          "delivery_country": "<?php echo $text_shipping_address; ?>",
          "estimated_delivery_date": "<?php echo $delivery_date ?>",

          // НЕОБЯЗАТЕЛЬНО
          "opt_in_style": "BOTTOM_LEFT_DIALOG"
        }); 
     });
  }
</script>
<!-- КОНЕЦ кода модуля опроса -->

<?php if (isset($gtag_event) && !empty($gtag_event)) { echo $gtag_event; } ?>
      
<?php echo $footer; ?>