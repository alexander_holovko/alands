<div class="simplecheckout-block" id="simplecheckout_cart" <?php echo $hide ? 'data-hide="true"' : '' ?> <?php echo $has_error ? 'data-error="true"' : '' ?>>
<?php if ($display_header) { ?>
    <div class="checkout-heading panel-heading"><?php echo $text_cart ?></div>
<?php } ?>
<?php if ($attention) { ?>
    <div class="alert alert-danger simplecheckout-warning-block"><?php echo $attention; ?></div>
<?php } ?>
<?php if ($error_warning) { ?>
    <div class="alert alert-danger simplecheckout-warning-block"><?php echo $error_warning; ?></div>
<?php } ?>
    <div class="table-responsive">
        <table class="simplecheckout-cart">
            <colgroup>
                <col class="image">
                <col class="name">
                <col class="model">
                <col class="quantity">
                <col class="price">
                <col class="total">
                <col class="remove">
            </colgroup>
<!--             <thead>
                <tr>
                    <th class="image"><?php echo $column_image; ?></th>
                    <th class="name"><?php echo $column_name; ?></th> 
                    <th class="quantity"><?php echo $column_quantity; ?></th>  
                    <th class="remove"></th>
                </tr>
            </thead> -->
            <tbody>
            <?php foreach ($products as $product) { ?>
                <?php if (!empty($product['recurring'])) { ?>
                    <tr>
                        <td class="simplecheckout-recurring-product" style="border:none;"><img src="<?php echo $additional_path ?>catalog/view/theme/default/image/reorder.png" alt="" title="" style="float:left;" />
                            <span style="float:left;line-height:18px; margin-left:10px;">
                            <strong><?php echo $text_recurring_item ?></strong>
                            <?php echo $product['profile_description'] ?>
                            </span>
                        </td>
                    </tr>
                <?php } ?>
                <tr>
                    <td class="image">
                        <?php if ($product['thumb']) { ?>
                            <a href="<?php echo $product['href']; ?>"><img  style="width: 100px;text-transform: uppercase;  text-align: center;" src="<?php echo $product['thumb']; ?>" alt="<?php echo $product['name']; ?>"  title="<?php echo $product['name']; ?>" /></a>
                        <?php } ?>
                    </td>
                    <td class="name">
                        <?php if ($product['thumb']) { ?>
                            <div class="image">
                                <a href="<?php echo $product['href']; ?>"><img src="<?php echo $product['thumb']; ?>" alt="<?php echo $product['name']; ?>" title="<?php echo $product['name']; ?>" /></a>
                            </div>
                        <?php } ?>
                        <a style="font-size: 16px;font-weight: bold;text-transform: uppercase;" href="<?php echo $product['href']; ?>"><?php echo $product['name']; ?></a>
                        <?php if (!$product['stock'] && ($config_stock_warning || !$config_stock_checkout)) { ?>
                            <span class="product-warning">***</span>
                        <?php } ?>
                        
                        <?php if ($product['reward']) { ?>
                        <small><?php echo $product['reward']; ?></small>
                        <?php } ?>
                        <div class="other-items">
                        <small class="cheks-sku">Арт. <?php echo $product['model']; ?></small>
                        <br/>
                        <small class="checks-price"><?php echo $column_price; ?>: <?php echo $product['price']; ?></small>
                        <br/>
                      <div class="options" style="margin: 0px 0px 0px -4px;">
                        <?php foreach ($product['option'] as $option) { ?>
                        &nbsp;<small><?php echo $option['name']; ?>: <?php echo $option['value']; ?></small><br />
                        <?php } ?>
                        <?php if (!empty($product['recurring'])) { ?>
                        <small><?php echo $text_payment_profile ?>: <?php echo $product['profile_name'] ?></small>
                        <?php } ?>
                        </div>
					  
                    </td>  
                    <td class="quantity">
                        <div class="input-group btn-block" style="max-width: 200px;">
                           
                            
                                <button class="btn btn-remove" data-onclick="removeProduct" data-product-key="<?php echo !empty($product['cart_id']) ? $product['cart_id'] : $product['key'] ?>" data-toggle="tooltip" type="button">
                                    <i class="fa fa-close" style="color:#dcdcdc"></i>
                                </button>
                            </span>
                        </div>
                    </td> 
                </tr>
                <?php } ?>
                <?php foreach ($vouchers as $voucher_info) { ?>
                    <tr>
                        <td class="image"></td>
                        <td class="name"><?php echo $voucher_info['description']; ?></td>
                        <td class="model"></td>
<!--                         <td class="quantity">
                            <div class="input-group btn-block" style="max-width: 200px;">
                                <span class="input-group-btn">
                                    <button class="btn btn-primary" data-onclick="decreaseProductQuantity" data-toggle="tooltip" type="submit">
                                        <i class="fa fa-minus"></i>
                                    </button>
                                </span>
                                <input class="form-control" type="text" value="1" disabled size="1" />
                                <span class="input-group-btn">
                                    <button class="btn btn-danger" data-onclick="removeGift" data-gift-key="<?php echo $voucher_info['key']; ?>" type="button">
                                        <i class="fa fa-close"></i>
                                    </button>
                                </span>
                            </div>
                        </td>
                        <td class="price"><?php echo $voucher_info['amount']; ?></td>
                        <td class="total"><?php echo $voucher_info['amount']; ?></td> -->
                    </tr>
                <?php } ?>
            </tbody>
        </table>
    </div>
<style>
.del { display: none; }
.del:not(:checked) + label + * { display: none; } /* фактически нужна только одна строка */

/* вид CSS кнопки */
.del:not(:checked) + label,
.del:checked + label {
      border: 1px solid gray;
    width: 100%;
    height: 39px;
    color: #1f1f1f;
    font-size: 18px;
    padding-left: 21px;
    padding-top: 5px;
}
.del:checked + label {
 
}
</style>
 <style type="text/css">
  .agree {
   width: 80%; /* Ширина в процентах */
  }
  </style>
<input type="checkbox" id="raz" class="del" "/>
<label for="raz" class="del">Есть промокод?<i class="fa fa-chevron-right" style="float: right;padding-right: 11px;margin-top: 4px;     color: gray;" aria-hidden="true"></i>
</label>
<div><?php if (isset($modules['coupon'])) { ?>
<div class="simplecheckout-cart-total">
 <span class="inputs"><label for="coupon">Промокод</label><input class="form-control" class="agree"  type="text" data-onchange="reloadAll" name="coupon" value="<?php echo $coupon; ?>" /></span>
 </div>
<?php } ?>
</div>
<input type="checkbox" id="raz1" class="del1" /><label for="raz1" class="del1">Подарочная карта<i class="fa fa-chevron-right" style="    float: right;
    padding-right: 11px;
    margin-top: 4px;     color: gray;" aria-hidden="true"></i>
</label><div>
<?php if (isset($modules['voucher'])) { ?>
    <div class="simplecheckout-cart-total">
        <span class="inputs"><label for="voucher">Подарочная карта</label><input class="form-control" type="text" name="voucher" data-onchange="reloadAll" value="<?php echo $voucher; ?>" /></span>
    </div>
<?php } ?>
	
	
	

<br>
<style>
.del1 { display: none; }
.del1:not(:checked) + label + * { display: none; } /* фактически нужна только одна строка */

/* вид CSS кнопки */
.del1:not(:checked) + label,
.del1:checked + label {
        border: 1px solid gray;
    width: 100%;
    height: 39px;
    color: #1f1f1f;
    font-size: 18px;
    padding-left: 21px;
    padding-top: 5px;border-top: 1px;
}
.del1:checked + label {
  
}
</style> 


</div>
<input type="checkbox" id="raz5" class="del" "/>
<label for="raz5" class="del">Дисконтная карта<i class="fa fa-chevron-right" style="float: right;padding-right: 11px;margin-top: 4px;     color: gray;" aria-hidden="true"></i>
</label>
<div>

    					<!-- Membership -->
    					<?php if (isset($modules['membership_card'])) { ?>
						    <div class="simplecheckout-cart-total">
						        <span class="inputs"><?php echo $entry_membership_card; ?>&nbsp;<input class="form-control" type="text" data-onchange="reloadAll" name="membership_card" id="input-membership-card" value="<?php echo $membership_card; ?>" /></span>
						    	<?php if ($card_request) { ?>
								<button class="btn btn-primary" id="button-membership-card-request" data-loading-text="..." type="button"><i class="fa fa-credit-card" aria-hidden="true"></i> <i class="fa fa-hand-o-left" aria-hidden="true"></i> <?php echo $button_membership_card_request; ?></button>
								<script type="text/javascript"><!--
								$('#button-membership-card-request').on('click', function(e) {								
									$.ajax({
										url: 'index.php?route=total/membership_card/cardRequest',
										type: 'post',
										dataType: 'json',
										beforeSend: function() {
											$('.alert').remove();
											$('#' + e.currentTarget.id).button('loading');
										},
										complete: function() {
											$('#' + e.currentTarget.id).button('reset');
										},
										success: function(json) {
											if (json['error']) {
												$('.breadcrumb').after('<div class="alert alert-danger"><i class="fa fa-exclamation-circle"></i> ' + json['error'] + '<button type="button" class="close" data-dismiss="alert">&times;</button></div>');
											}
											
											if (json['success']) {
												$('.breadcrumb').after('<div class="alert alert-success"><i class="fa fa-check-circle"></i> ' + json['success'] + '<button type="button" class="close" data-dismiss="alert">&times;</button></div>');
											}
											
											$('html, body').animate({ scrollTop: 0 }, 'slow');

											if (json['reload']) {
												$('#input-membership-card').val(json['membership_card_code']);
												
												setTimeout(function() { $('.alert').remove(); reloadAll(); }, 2000);
											}
										}
									});
								});
								//--></script> 
								<?php } ?>   
						    </div>
						<?php } ?>
						<!-- Membership -->
					
<?php if (isset($modules['reward']) && $points > 0) { ?>
    <div class="simplecheckout-cart-total">
        <span class="inputs"><label for="reward">Дисконтная карта</label><input class="form-control"  type="text" name="reward" data-onchange="reloadAll" value="111" /></span>
    </div>
<?php } ?>

</div>




<?php if (isset($modules['coupon']) || (isset($modules['reward']) && $points > 0) || isset($modules['voucher'])) { ?>
    <div class="simplecheckout-cart-total simplecheckout-cart-buttons">
        <span class="inputs buttons"><a id="simplecheckout_button_cart" style="background-color: #fff;color: #000;" data-onclick="reloadAll" class="button btn-primary button_oc btn"><span>ПРИМЕНИТЬ ></span></a></span>
    </div>
<?php } ?>
<?php foreach ($totals as $total) { ?>
    <div class="simplecheckout-cart-total" id="total_<?php echo $total['code']; ?>">
        <span><b><?php echo $total['title']; ?>:</b></span>
        <span class="simplecheckout-cart-total-value"><?php echo $total['text']; ?></span>
        <span class="simplecheckout-cart-total-remove">
            <?php if ($total['code'] == 'coupon') { ?>
                <i data-onclick="removeCoupon" title="<?php echo $button_remove; ?>" class="fa fa-times-circle"></i>
            <?php } ?>

    					<!-- Membership -->
    					<?php if ($total['code'] == 'membership_card') { ?>
			                <i onclick="$('#input-membership-card').val('').trigger('change');" title="<?php echo $button_remove; ?>" class="fa fa-times-circle"></i>
			            <?php } ?>
						<!-- Membership -->
					
            <?php if ($total['code'] == 'voucher') { ?>
                <i data-onclick="removeVoucher" title="<?php echo $button_remove; ?>" class="fa fa-times-circle"></i>
            <?php } ?>
            <?php if ($total['code'] == 'reward') { ?>
                <i data-onclick="removeReward" title="<?php echo $button_remove; ?>" class="fa fa-times-circle"></i>
            <?php } ?>
        </span>
    </div>
<?php } ?>
<input type="hidden" name="remove" value="" id="simplecheckout_remove">
<div style="display:none;" id="simplecheckout_cart_total"><?php echo $cart_total ?></div>
<?php if ($display_weight) { ?>
    <div style="display:none;" id="simplecheckout_cart_weight"><?php echo $weight ?></div>
<?php } ?>
<?php if (!$display_model) { ?>
    <style>
    .simplecheckout-cart col.model,
    .simplecheckout-cart th.model,
    .simplecheckout-cart td.model {
        display: none;
    }
    </style>
<?php } ?>
</div>