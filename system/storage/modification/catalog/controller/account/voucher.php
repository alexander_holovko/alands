<?php
class ControllerAccountVoucher extends Controller {
	private $error = array();

	public function index() {
		$this->load->language('account/voucher');

		$this->document->setTitle($this->language->get('heading_title'));

		if (!isset($this->session->data['vouchers'])) {
			$this->session->data['vouchers'] = array();
		}

		if (($this->request->server['REQUEST_METHOD'] == 'POST') && $this->validate()) {
			
//tri mod start
$code = empty($this->request->post['code']) ? mt_rand() : $this->request->post['code'];
$this->session->data['vouchers'][$code] = array(
  'deliverydate' => (isset($this->request->post['deliverydate']) ? $this->request->post['deliverydate'] : ''),
  'description' => sprintf($this->language->get('text_for'), $this->currency->format($this->currency->convert($this->request->post['amount'], $this->session->data['currency'], $this->config->get('config_currency')), $this->session->data['currency']), $this->request->post['to_name']),
//tri mod end
      
				'to_name'          => $this->request->post['to_name'],
				'to_email'         => $this->request->post['to_email'],
				'from_name'        => $this->request->post['from_name'],
				'from_email'       => $this->request->post['from_email'],
				'voucher_theme_id' => $this->request->post['voucher_theme_id'],
				'message'          => $this->request->post['message'],
				'amount'           => $this->currency->convert($this->request->post['amount'], $this->currency->getCode(), $this->config->get('config_currency'))
			);

			$this->response->redirect($this->url->link('account/voucher/success'));
		}

		$data['breadcrumbs'] = array();

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('text_home'),
			'href' => $this->url->link('common/home')
		);

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('text_account'),
			'href' => $this->url->link('account/account', '', 'SSL')
		);

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('text_voucher'),
			'href' => $this->url->link('account/voucher', '', 'SSL')
		);

		$data['heading_title'] = $this->language->get('heading_title');

		$data['text_description'] = $this->language->get('text_description');
		$data['text_agree'] = $this->language->get('text_agree');

		$data['entry_to_name'] = $this->language->get('entry_to_name');
		$data['entry_to_email'] = $this->language->get('entry_to_email');
		$data['entry_from_name'] = $this->language->get('entry_from_name');
		$data['entry_from_email'] = $this->language->get('entry_from_email');
		$data['entry_theme'] = $this->language->get('entry_theme');
		$data['entry_message'] = $this->language->get('entry_message');
		$data['entry_amount'] = $this->language->get('entry_amount');

		$data['help_message'] = $this->language->get('help_message');
		$data['help_amount'] = sprintf($this->language->get('help_amount'), $this->currency->format($this->config->get('config_voucher_min')), $this->currency->format($this->config->get('config_voucher_max')));

		$data['button_continue'] = $this->language->get('button_continue');

		if (isset($this->error['warning'])) {
			$data['error_warning'] = $this->error['warning'];
		} else {
			$data['error_warning'] = '';
		}

		if (isset($this->error['to_name'])) {
			$data['error_to_name'] = $this->error['to_name'];
		} else {
			$data['error_to_name'] = '';
		}

		if (isset($this->error['to_email'])) {
			$data['error_to_email'] = $this->error['to_email'];
		} else {
			$data['error_to_email'] = '';
		}

		if (isset($this->error['from_name'])) {
			$data['error_from_name'] = $this->error['from_name'];
		} else {
			$data['error_from_name'] = '';
		}

		if (isset($this->error['from_email'])) {
			$data['error_from_email'] = $this->error['from_email'];
		} else {
			$data['error_from_email'] = '';
		}

		if (isset($this->error['theme'])) {
			$data['error_theme'] = $this->error['theme'];
		} else {
			$data['error_theme'] = '';
		}

		if (isset($this->error['amount'])) {
			$data['error_amount'] = $this->error['amount'];
		} else {
			$data['error_amount'] = '';
		}

		$data['action'] = $this->url->link('account/voucher', '', 'SSL');

		if (isset($this->request->post['to_name'])) {
			$data['to_name'] = $this->request->post['to_name'];
		} else {
			$data['to_name'] = '';
		}

		if (isset($this->request->post['to_email'])) {
			$data['to_email'] = $this->request->post['to_email'];
		} else {
			$data['to_email'] = '';
		}

		if (isset($this->request->post['from_name'])) {
			$data['from_name'] = $this->request->post['from_name'];
		} elseif ($this->customer->isLogged()) {
			$data['from_name'] = $this->customer->getFirstName() . ' '  . $this->customer->getLastName();
		} else {
			$data['from_name'] = '';
		}

		if (isset($this->request->post['from_email'])) {
			$data['from_email'] = $this->request->post['from_email'];
		} elseif ($this->customer->isLogged()) {
			$data['from_email'] = $this->customer->getEmail();
		} else {
			$data['from_email'] = '';
		}

		$this->load->model('total/voucher_theme');

		$data['voucher_themes'] = $this->model_total_voucher_theme->getVoucherThemes();

		if (isset($this->request->post['voucher_theme_id'])) {
			$data['voucher_theme_id'] = $this->request->post['voucher_theme_id'];
		} else {
			$data['voucher_theme_id'] = '';
		}

		if (isset($this->request->post['message'])) {
			$data['message'] = $this->request->post['message'];
		} else {
			$data['message'] = '';
		}

		if (isset($this->request->post['amount'])) {
			$data['amount'] = $this->request->post['amount'];
		} else {
			$data['amount'] = $this->currency->format($this->config->get('config_voucher_min'), $this->config->get('config_currency'), false, false);
		}

		if (isset($this->request->post['agree'])) {
			$data['agree'] = $this->request->post['agree'];
		} else {
			$data['agree'] = false;
		}

		$data['column_left'] = $this->load->controller('common/column_left');
		$data['column_right'] = $this->load->controller('common/column_right');
		$data['content_top'] = $this->load->controller('common/content_top');
		$data['content_bottom'] = $this->load->controller('common/content_bottom');
		$data['footer'] = $this->load->controller('common/footer');

//tri mod start
if ($this->config->get('deluxe_voucher_status') && isset($data['voucher_themes'])) {
  $data['lng'] = $this->load->language('module/deluxe_voucher');
  $this->load->model('tool/image');

  $data['amount'] = isset($this->request->post['amount']) ? $this->request->post['amount'] : $this->config->get('deluxe_voucher_default_price');
  $data['deliverydate'] = isset($this->request->post['deliverydate']) ? $this->request->post['deliverydate'] : '';
  $data['hide_date'] = $this->config->get('deluxe_voucher_hide_date');
  $data['min_date'] = date('Y-m-d', strtotime('tomorrow'));
  $data['max_date'] = ($this->config->get('deluxe_voucher_max_day') ? date('Y-m-d', strtotime('+ ' . $this->config->get('deluxe_voucher_max_day') . ' days')) : '');

  $data['prices'] = array();
  if ($this->config->get('deluxe_voucher_prices')) {
    $prices = explode(',', $this->config->get('deluxe_voucher_prices'));
    foreach ($prices as $price) {
      $data['prices'][(float)$price] = $this->currency->format((float)$price, $this->session->data['currency']);
    }
  }

  $width = !$this->config->get('deluxe_voucher_width') ? 50 : $this->config->get('deluxe_voucher_width');
  $height = !$this->config->get('deluxe_voucher_height') ? 50 : $this->config->get('deluxe_voucher_height');

  foreach ($data['voucher_themes'] as $r) {
    $voucher_themes[] = array(
      'name' => ($this->config->get('deluxe_voucher_hide_label') ? '' : $r['name']),
      'voucher_theme_id' => $r['voucher_theme_id'],
      'thumb' => ($r['image'] && !$this->config->get('deluxe_voucher_hide_image') ? $this->model_tool_image->resize($r['image'], $width, $height) : '')
    );
  }
  $data['voucher_themes'] = $voucher_themes;

  $code = isset($this->request->get['code']) ? $this->request->get['code'] : '';
  $data['code'] = $code;

  if ($code) {
    $data['to_name'] = $this->session->data['vouchers'][$code]['to_name'];
    $data['to_email'] = $this->session->data['vouchers'][$code]['to_email'];
    $data['from_name'] = $this->session->data['vouchers'][$code]['from_name'];
    $data['from_email'] = $this->session->data['vouchers'][$code]['from_email'];
    $data['message'] = $this->session->data['vouchers'][$code]['message'];
    $data['amount'] = $this->session->data['vouchers'][$code]['amount'];
    $data['voucher_theme_id'] = $this->session->data['vouchers'][$code]['voucher_theme_id'];
    $data['deliverydate'] = $this->session->data['vouchers'][$code]['deliverydate'];
  }

  $this->document->addScript('catalog/view/javascript/jquery/datetimepicker/moment.js');
  $this->document->addScript('catalog/view/javascript/jquery/datetimepicker/bootstrap-datetimepicker.min.js');
  $this->document->addStyle('catalog/view/javascript/jquery/datetimepicker/bootstrap-datetimepicker.min.css');
}
//tri mod end
      
		$data['header'] = $this->load->controller('common/header');

		if (file_exists(DIR_TEMPLATE . $this->config->get('config_template') . '/template/account/voucher.tpl')) {
			$this->response->setOutput($this->load->view($this->config->get('config_template') . '/template/account/voucher.tpl', $data));
		} else {
			$this->response->setOutput($this->load->view('default/template/account/voucher.tpl', $data));
		}
	}

	public function success() {
		$this->load->language('account/voucher');

		$this->document->setTitle($this->language->get('heading_title'));

		$data['breadcrumbs'] = array();

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('text_home'),
			'href' => $this->url->link('common/home')
		);

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('heading_title'),
			'href' => $this->url->link('account/voucher')
		);

		$data['heading_title'] = $this->language->get('heading_title');

		$data['text_message'] = $this->language->get('text_message');

		$data['button_continue'] = $this->language->get('button_continue');

		$data['continue'] = $this->url->link('checkout/cart');

		$data['column_left'] = $this->load->controller('common/column_left');
		$data['column_right'] = $this->load->controller('common/column_right');
		$data['content_top'] = $this->load->controller('common/content_top');
		$data['content_bottom'] = $this->load->controller('common/content_bottom');
		$data['footer'] = $this->load->controller('common/footer');

//tri mod start
if ($this->config->get('deluxe_voucher_status') && isset($data['voucher_themes'])) {
  $data['lng'] = $this->load->language('module/deluxe_voucher');
  $this->load->model('tool/image');

  $data['amount'] = isset($this->request->post['amount']) ? $this->request->post['amount'] : $this->config->get('deluxe_voucher_default_price');
  $data['deliverydate'] = isset($this->request->post['deliverydate']) ? $this->request->post['deliverydate'] : '';
  $data['hide_date'] = $this->config->get('deluxe_voucher_hide_date');
  $data['min_date'] = date('Y-m-d', strtotime('tomorrow'));
  $data['max_date'] = ($this->config->get('deluxe_voucher_max_day') ? date('Y-m-d', strtotime('+ ' . $this->config->get('deluxe_voucher_max_day') . ' days')) : '');

  $data['prices'] = array();
  if ($this->config->get('deluxe_voucher_prices')) {
    $prices = explode(',', $this->config->get('deluxe_voucher_prices'));
    foreach ($prices as $price) {
      $data['prices'][(float)$price] = $this->currency->format((float)$price, $this->session->data['currency']);
    }
  }

  $width = !$this->config->get('deluxe_voucher_width') ? 50 : $this->config->get('deluxe_voucher_width');
  $height = !$this->config->get('deluxe_voucher_height') ? 50 : $this->config->get('deluxe_voucher_height');

  foreach ($data['voucher_themes'] as $r) {
    $voucher_themes[] = array(
      'name' => ($this->config->get('deluxe_voucher_hide_label') ? '' : $r['name']),
      'voucher_theme_id' => $r['voucher_theme_id'],
      'thumb' => ($r['image'] && !$this->config->get('deluxe_voucher_hide_image') ? $this->model_tool_image->resize($r['image'], $width, $height) : '')
    );
  }
  $data['voucher_themes'] = $voucher_themes;

  $code = isset($this->request->get['code']) ? $this->request->get['code'] : '';
  $data['code'] = $code;

  if ($code) {
    $data['to_name'] = $this->session->data['vouchers'][$code]['to_name'];
    $data['to_email'] = $this->session->data['vouchers'][$code]['to_email'];
    $data['from_name'] = $this->session->data['vouchers'][$code]['from_name'];
    $data['from_email'] = $this->session->data['vouchers'][$code]['from_email'];
    $data['message'] = $this->session->data['vouchers'][$code]['message'];
    $data['amount'] = $this->session->data['vouchers'][$code]['amount'];
    $data['voucher_theme_id'] = $this->session->data['vouchers'][$code]['voucher_theme_id'];
    $data['deliverydate'] = $this->session->data['vouchers'][$code]['deliverydate'];
  }

  $this->document->addScript('catalog/view/javascript/jquery/datetimepicker/moment.js');
  $this->document->addScript('catalog/view/javascript/jquery/datetimepicker/bootstrap-datetimepicker.min.js');
  $this->document->addStyle('catalog/view/javascript/jquery/datetimepicker/bootstrap-datetimepicker.min.css');
}
//tri mod end
      
		$data['header'] = $this->load->controller('common/header');

		if (file_exists(DIR_TEMPLATE . $this->config->get('config_template') . '/template/common/success.tpl')) {
			$this->response->setOutput($this->load->view($this->config->get('config_template') . '/template/common/success.tpl', $data));
		} else {
			$this->response->setOutput($this->load->view('default/template/common/success.tpl', $data));
		}
	}

	protected function validate() {
		if ((utf8_strlen($this->request->post['to_name']) < 1) || (utf8_strlen($this->request->post['to_name']) > 64)) {
			$this->error['to_name'] = $this->language->get('error_to_name');
		}

		if ((utf8_strlen($this->request->post['to_email']) > 96) || !preg_match('/^[^\@]+@.*.[a-z]{2,15}$/i', $this->request->post['to_email'])) {
			$this->error['to_email'] = $this->language->get('error_email');
		}

		if ((utf8_strlen($this->request->post['from_name']) < 1) || (utf8_strlen($this->request->post['from_name']) > 64)) {
			$this->error['from_name'] = $this->language->get('error_from_name');
		}

		if ((utf8_strlen($this->request->post['from_email']) > 96) || !preg_match('/^[^\@]+@.*.[a-z]{2,15}$/i', $this->request->post['from_email'])) {
			$this->error['from_email'] = $this->language->get('error_email');
		}

		if (!isset($this->request->post['voucher_theme_id'])) {
			$this->error['theme'] = $this->language->get('error_theme');
		}

		if (($this->currency->convert($this->request->post['amount'], $this->currency->getCode(), $this->config->get('config_currency')) < $this->config->get('config_voucher_min')) || ($this->currency->convert($this->request->post['amount'], $this->currency->getCode(), $this->config->get('config_currency')) > $this->config->get('config_voucher_max'))) {
			$this->error['amount'] = sprintf($this->language->get('error_amount'), $this->currency->format($this->config->get('config_voucher_min')), $this->currency->format($this->config->get('config_voucher_max')));
		}

		if (!isset($this->request->post['agree'])) {
			$this->error['warning'] = $this->language->get('error_agree');
		}

		return !$this->error;
	}
}
