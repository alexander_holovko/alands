<?php
class ControllerProductSpecial extends Controller {
	public function index() {
		$this->load->language('product/special');

		$this->load->model('catalog/product');

		$this->load->model('tool/image');

		if (isset($this->request->get['sort'])) {
			$sort = $this->request->get['sort'];
		} else {
			$sort = 'p.sort_order';
		}

		if (isset($this->request->get['order'])) {
			$order = $this->request->get['order'];
		} else {
			$order = 'ASC';
		}

		if (isset($this->request->get['page'])) {
			$page = $this->request->get['page'];
		} else {
			$page = 1;
		}

		if (isset($this->request->get['limit'])) {
			$limit = (int)$this->request->get['limit'];
		} else {
			$limit = $this->config->get('config_product_limit');
		}

		$this->document->setTitle($this->language->get('heading_title'));

			if (
			isset($this->request->get['page']) ||
			isset($this->request->get['limit']) ||
			isset($this->request->get['order'])
			) {
				$this->document->setRobots('noindex,follow');
			}
			
			


					// Top stickers start
						$this->load->model('setting/setting');

						// Statuses
						$data['topstickers_status'] = $this->config->get('topstickers_status');
						$data['topstickers_sold_status'] = $this->config->get('topstickers_sold_status');
						$data['topstickers_sale_status'] = $this->config->get('topstickers_sale_status');
						$data['topstickers_bestseller_status'] = $this->config->get('topstickers_bestseller_status');
						$data['topstickers_novelty_status'] = $this->config->get('topstickers_novelty_status');
						$data['topstickers_last_status'] = $this->config->get('topstickers_last_status');
						$data['topstickers_freeshipping_status'] = $this->config->get('topstickers_freeshipping_status');

						// Text
						$current_language_id = $this->config->get('config_language_id');
						$data['topstickers_sold_text'] = $this->config->get('topstickers_sold_text')[$current_language_id];
						$data['topstickers_sale_text'] = $this->config->get('topstickers_sale_text')[$current_language_id];
						$data['topstickers_bestseller_text'] = $this->config->get('topstickers_bestseller_text')[$current_language_id];
						$data['topstickers_novelty_text'] = $this->config->get('topstickers_novelty_text')[$current_language_id];
						$data['topstickers_last_text'] = $this->config->get('topstickers_last_text')[$current_language_id];
						$data['topstickers_freeshipping_text'] = $this->config->get('topstickers_freeshipping_text')[$current_language_id];

						// Additional data
						$data['topstickers_bestseller_numbers'] = $this->config->get('topstickers_bestseller_numbers');
						$data['topstickers_novelty_days'] = $this->config->get('topstickers_novelty_days');
						$data['topstickers_last_numbers'] = $this->config->get('topstickers_last_numbers');
						$data['topstickers_freeshipping_price'] = $this->config->get('topstickers_freeshipping_price');
					// Top stickers end
				
		$data['breadcrumbs'] = array();

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('text_home'),
			'href' => $this->url->link('common/home')
		);

		$url = '';

				if( ! empty( $this->request->get['mfp'] ) ) {
					$url .= '&mfp=' . $this->request->get['mfp'];
				}
			

		if (isset($this->request->get['sort'])) {
			$url .= '&sort=' . $this->request->get['sort'];
		}

		if (isset($this->request->get['order'])) {
			$url .= '&order=' . $this->request->get['order'];
		}

		if (isset($this->request->get['page'])) {
			$url .= '&page=' . $this->request->get['page'];
		}

		if (isset($this->request->get['limit'])) {
			$url .= '&limit=' . $this->request->get['limit'];
		}

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('heading_title'),
			'href' => $this->url->link('product/special', $url)
		);

		$data['heading_title'] = $this->language->get('heading_title');

		$data['text_empty'] = $this->language->get('text_empty');
		$data['text_quantity'] = $this->language->get('text_quantity');
		$data['text_manufacturer'] = $this->language->get('text_manufacturer');
		$data['text_model'] = $this->language->get('text_model');
		$data['text_price'] = $this->language->get('text_price');
		$data['text_tax'] = $this->language->get('text_tax');
		$data['text_points'] = $this->language->get('text_points');
		$data['text_compare'] = sprintf($this->language->get('text_compare'), (isset($this->session->data['compare']) ? count($this->session->data['compare']) : 0));
		$data['text_sort'] = $this->language->get('text_sort');
		$data['text_limit'] = $this->language->get('text_limit');

		$data['button_cart'] = $this->language->get('button_cart');
		$data['button_wishlist'] = $this->language->get('button_wishlist');
		$data['button_compare'] = $this->language->get('button_compare');
		$data['button_list'] = $this->language->get('button_list');
		$data['button_grid'] = $this->language->get('button_grid');
		$data['button_continue'] = $this->language->get('button_continue');

		$data['compare'] = $this->url->link('product/compare');

		$data['products'] = array();

		$filter_data = array(
			'sort'  => $sort,
			'order' => $order,
			'start' => ($page - 1) * $limit,
			'limit' => $limit
		);


				if( isset( $this->request->get['category_id'] ) ) {
					$filter_data['filter_category_id'] = (int) $this->request->get['category_id'];
				}
			
		$product_total = $this->model_catalog_product->getTotalProductSpecials();

		$results = $this->model_catalog_product->getProductSpecials($filter_data);

		foreach ($results as $result) {
		
			$results_img = $this->model_catalog_product->getProductImages($result['product_id']);
			$additional_img = array();
			foreach ($results_img as $result_img) {
				if ($result_img['image']) {
					$additional_image = $this->model_tool_image->resize($result_img['image'], $this->config->get('config_image_product_width'), $this->config->get('config_image_product_height'));
				} else {
					$additional_image = false;
				}
				$additional_img[1] = $additional_image;
				break;
			}
            
			if ($result['image']) {
				$image = $this->model_tool_image->resize($result['image'], $this->config->get('config_image_product_width'), $this->config->get('config_image_product_height'));
			} else {
				$image = $this->model_tool_image->resize('placeholder.png', $this->config->get('config_image_product_width'), $this->config->get('config_image_product_height'));
			}

			if (($this->config->get('config_customer_price') && $this->customer->isLogged()) || !$this->config->get('config_customer_price')) {
				$price = $this->currency->format($this->tax->calculate($result['price'], $result['tax_class_id'], $this->config->get('config_tax')));
			} else {
				$price = false;
			}

			if ((float)$result['special']) {
				$special = $this->currency->format($this->tax->calculate($result['special'], $result['tax_class_id'], $this->config->get('config_tax')));
			} else {
				$special = false;
			}

			if ($this->config->get('config_tax')) {
				$tax = $this->currency->format((float)$result['special'] ? $result['special'] : $result['price']);
			} else {
				$tax = false;
			}

			if ($this->config->get('config_review_status')) {
				$rating = (int)$result['rating'];
			} else {
				$rating = false;
			}


					// Top stickers start
						// Stickers for current product
						$product_info = $this->model_catalog_product->getProduct($result['product_id']);

							// Sold sticker
							$data['product_sold_sticker'] = false;
							if ($data['topstickers_sold_status'] && $data['topstickers_sold_text'] && $data['topstickers_sold_text'] != '' && $product_info['quantity'] <= 0) {
								$data['product_sold_sticker'] = $data['topstickers_sold_text'];
							}

							// Sale sticker
							$data['product_sale_sticker'] = false;
							if ($data['topstickers_sale_status'] && $data['topstickers_sale_text'] && $data['topstickers_sale_text'] != '' && (float)$product_info['special']) {
								$data['product_sale_sticker'] = $data['topstickers_sale_text'];
							}

							// Bestseller sticker
							$data['product_bestseller_sticker'] = false;
							if ($data['topstickers_bestseller_status'] && $data['topstickers_bestseller_text'] && $data['topstickers_bestseller_text'] != '') {
								$bestsellers = $this->model_catalog_product->getBestSellerProducts( $data['topstickers_bestseller_numbers']);
								foreach ($bestsellers as $bestseller) {
									if ($bestseller['product_id'] == $result['product_id']) {
										$data['product_bestseller_sticker'] = $data['topstickers_bestseller_text'];
									}
								}
							}

							// Novelty sticker
							$data['product_novelty_sticker'] = false;
							if ($data['topstickers_novelty_status'] && $data['topstickers_novelty_text'] && $data['topstickers_novelty_text'] != '' && (strtotime($product_info['date_added']) + intval($data['topstickers_novelty_days']) * 24 * 3600) > time()) {
								$data['product_novelty_sticker'] = $data['topstickers_novelty_text'];
							}

							// Last sticker
							$data['product_last_sticker'] = false;
							if ($data['topstickers_last_status'] && $data['topstickers_last_text'] && $data['topstickers_last_text'] != '' && $product_info['quantity'] <= intval($data['topstickers_last_numbers']) && $product_info['quantity'] > 0) {
								$data['product_last_sticker'] = $data['topstickers_last_text'];
							}

							// Freeshipping sticker
							$data['product_freeshipping_sticker'] = false;
							if ($data['topstickers_freeshipping_status'] && $data['topstickers_freeshipping_text'] && $data['topstickers_freeshipping_text'] != '') {
								if ((float)$product_info['special'] && (float)$product_info['special'] >= $data['topstickers_freeshipping_price']) {
									$data['product_freeshipping_sticker'] = $data['topstickers_freeshipping_text'];
								} elseif ($product_info['price'] >= $data['topstickers_freeshipping_price']) {
									$data['product_freeshipping_sticker'] = $data['topstickers_freeshipping_text'];
								} else {
									$data['product_freeshipping_sticker'] = false;
								}
							}
					// Top stickers end

					// Top stickers custom start
						$current_language_id = $this->config->get('config_language_id');
						$product_custom_topstickers = array();
						foreach ($this->model_catalog_product->getProductTopStickers($result['product_id']) as $topsticker) {
							$product_custom_topsticker_text = json_decode( $topsticker['text'], true);
							$product_custom_topstickers[] = array(
								'topsticker_id'	=> $topsticker['topsticker_id'],
								'name'  		=> $topsticker['name'],
								'text' 			=> $product_custom_topsticker_text[$current_language_id],
								'bg_color'  	=> $topsticker['bg_color'],
								'status'    	=> $topsticker['status']
							);
						}
					// Top stickers custom end
				
			$data['products'][] = array(
'percent'     => sprintf($this->language->get('-%s'), (round((($result['price'] - $result['special'])/$result['price']) * 100 ,0 ))) . ' %',
				'percent'     => sprintf($this->language->get('-%s'), (round((($result['price'] - $result['special'])/$result['price']) * 100 ,0 ))) . ' %',
				'product_id'  => $result['product_id'],
				'thumb'       => $image,
		
			'additional_img' => $additional_img,
            
				'name'        => $result['name'],
				'model'        => $result['model'],
                'manufacturer'        => $result['manufacturer'],
				'description' => utf8_substr(strip_tags(html_entity_decode($result['description'], ENT_QUOTES, 'UTF-8')), 0, $this->config->get('config_product_description_length')) . '..',
				'price'       => $price,
				'special'     => $special,
				'tax'         => $tax,
				'minimum'     => $result['minimum'] > 0 ? $result['minimum'] : 1,
				'rating'      => $result['rating'],

					// Top stickers start
						'product_sold_sticker' => $data['product_sold_sticker'],
						'product_sale_sticker' => $data['product_sale_sticker'],
						'product_bestseller_sticker' => $data['product_bestseller_sticker'],
						'product_novelty_sticker' => $data['product_novelty_sticker'],
						'product_last_sticker' => $data['product_last_sticker'],
						'product_freeshipping_sticker' => $data['product_freeshipping_sticker'],
						'product_custom_topstickers' => $product_custom_topstickers,
					// Top stickers end
				
				'href'        => $this->url->link('product/product', 'product_id=' . $result['product_id'] . $url)
			);
		}

		$url = '';

				if( ! empty( $this->request->get['mfp'] ) ) {
					$url .= '&mfp=' . $this->request->get['mfp'];
				}
			

		if (isset($this->request->get['limit'])) {
			$url .= '&limit=' . $this->request->get['limit'];
		}

		$data['sorts'] = array();

		$data['sorts'][] = array(
			'text'  => $this->language->get('text_default'),
			'value' => 'p.sort_order-ASC',
			'href'  => $this->url->link('product/special', 'sort=p.sort_order&order=ASC' . $url)
		);
$data['sorts'][] = array(
				'text'  => $this->language->get('Популярные'),
				'value' => 'p.viewed-DESC',
				'href'  => $this->url->link('product/special', '&sort=p.viewed&order=DESC' . $url)
			);
		// $data['sorts'][] = array(
		// 	'text'  => $this->language->get('text_name_asc'),
		// 	'value' => 'pd.name-ASC',
		// 	'href'  => $this->url->link('product/special', 'sort=pd.name&order=ASC' . $url)
		// );

		// $data['sorts'][] = array(
		// 	'text'  => $this->language->get('text_name_desc'),
		// 	'value' => 'pd.name-DESC',
		// 	'href'  => $this->url->link('product/special', 'sort=pd.name&order=DESC' . $url)
		// );

		$data['sorts'][] = array(
			'text'  => $this->language->get('text_price_asc'),
			'value' => 'ps.price-ASC',
			'href'  => $this->url->link('product/special', 'sort=ps.price&order=ASC' . $url)
		);

		$data['sorts'][] = array(
			'text'  => $this->language->get('text_price_desc'),
			'value' => 'ps.price-DESC',
			'href'  => $this->url->link('product/special', 'sort=ps.price&order=DESC' . $url)
		);

		if ($this->config->get('config_review_status')) {
			$data['sorts'][] = array(
				'text'  => $this->language->get('text_rating_desc'),
				'value' => 'rating-DESC',
				'href'  => $this->url->link('product/special', 'sort=rating&order=DESC' . $url)
			);

			$data['sorts'][] = array(
				'text'  => $this->language->get('text_rating_asc'),
				'value' => 'rating-ASC',
				'href'  => $this->url->link('product/special', 'sort=rating&order=ASC' . $url)
			);
		}

		// $data['sorts'][] = array(
		// 		'text'  => $this->language->get('text_model_asc'),
		// 		'value' => 'p.model-ASC',
		// 		'href'  => $this->url->link('product/special', 'sort=p.model&order=ASC' . $url)
		// );

		// $data['sorts'][] = array(
		// 	'text'  => $this->language->get('text_model_desc'),
		// 	'value' => 'p.model-DESC',
		// 	'href'  => $this->url->link('product/special', 'sort=p.model&order=DESC' . $url)
		// );
		$data['sorts'][] = array(
				'text'  => $this->language->get('Скидка(высокая>низкая)'),
				'value' => 'special_percent-DESC',
				'href'  => $this->url->link('product/special', 'sort=special_percent&order=ASC' . $url)
		);

		$data['sorts'][] = array(
			'text'  => $this->language->get('Скидка(низкая>высокая)'),
			'value' => 'special_percent-DESC',
			'href'  => $this->url->link('product/special', 'sort=special_percent&order=DESC' . $url)
		);
 
		$data['sorts'][] = array(
				'text'  => $this->language->get('По дате добавления(ранее)'),
				'value' => 'p.date_added-ASC',
				'href'  => $this->url->link('product/special', 'sort=date_added&order=ASC' . $url)
		);

		$data['sorts'][] = array(
			'text'  => $this->language->get('По дате добавления(последние)'),
			'value' => 'p.date_added-DESC',
			'href'  => $this->url->link('product/special', 'sort=date_added&order=DESC' . $url)
		);
		$url = '';

				if( ! empty( $this->request->get['mfp'] ) ) {
					$url .= '&mfp=' . $this->request->get['mfp'];
				}
			

		if (isset($this->request->get['sort'])) {
			$url .= '&sort=' . $this->request->get['sort'];
		}

		if (isset($this->request->get['order'])) {
			$url .= '&order=' . $this->request->get['order'];
		}

		$data['limits'] = array();

		$limits = array_unique(array($this->config->get('config_product_limit'), 25, 50, 75, 100));

		sort($limits);

		foreach($limits as $value) {
			$data['limits'][] = array(
				'text'  => $value,
				'value' => $value,
				'href'  => $this->url->link('product/special', $url . '&limit=' . $value)
			);
		}

		$url = '';

				if( ! empty( $this->request->get['mfp'] ) ) {
					$url .= '&mfp=' . $this->request->get['mfp'];
				}
			

		if (isset($this->request->get['sort'])) {
			$url .= '&sort=' . $this->request->get['sort'];
		}

		if (isset($this->request->get['order'])) {
			$url .= '&order=' . $this->request->get['order'];
		}

		if (isset($this->request->get['limit'])) {
			$url .= '&limit=' . $this->request->get['limit'];
		}

		$pagination = new Pagination();
		$pagination->total = $product_total;
		$pagination->page = $page;
		$pagination->limit = $limit;
		$pagination->url = $this->url->link('product/special', $url . '&page={page}');

		$data['pagination'] = $pagination->render();

		$data['results'] = sprintf($this->language->get('text_pagination'), ($product_total) ? (($page - 1) * $limit) + 1 : 0, ((($page - 1) * $limit) > ($product_total - $limit)) ? $product_total : ((($page - 1) * $limit) + $limit), $product_total, ceil($product_total / $limit));

		// http://googlewebmastercentral.blogspot.com/2011/09/pagination-with-relnext-and-relprev.html
		if ($page == 1) {
		    $this->document->addLink($this->url->link('product/special', '', 'SSL'), 'canonical');
		} elseif ($page == 2) {
		    $this->document->addLink($this->url->link('product/special', '', 'SSL'), 'prev');
		} else {
		    $this->document->addLink($this->url->link('product/special', 'page='. ($page - 1), 'SSL'), 'prev');
		}

		if ($limit && ceil($product_total / $limit) > $page) {
		    $this->document->addLink($this->url->link('product/special', 'page='. ($page + 1), 'SSL'), 'next');
		}

		$data['sort'] = $sort;
		$data['order'] = $order;
		$data['limit'] = $limit;

		$data['continue'] = $this->url->link('common/home');

		$data['column_left'] = $this->load->controller('common/column_left');
		$data['column_right'] = $this->load->controller('common/column_right');
		$data['content_top'] = $this->load->controller('common/content_top');
		$data['content_bottom'] = $this->load->controller('common/content_bottom');
		$data['footer'] = $this->load->controller('common/footer');
		$data['header'] = $this->load->controller('common/header');

		if (file_exists(DIR_TEMPLATE . $this->config->get('config_template') . '/template/product/special.tpl')) {

				$this->load->model( 'module/mega_filter' );
				
				$data = $this->model_module_mega_filter->prepareData( $data );
			
			$this->response->setOutput($this->load->view($this->config->get('config_template') . '/template/product/special.tpl', $data));
		} else {

				$this->load->model( 'module/mega_filter' );
				
				$data = $this->model_module_mega_filter->prepareData( $data );
			
			$this->response->setOutput($this->load->view('default/template/product/special.tpl', $data));
		}
	}
}
