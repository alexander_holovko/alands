<?php
class ControllerProductProduct extends Controller {
	private $error = array();

	public function index() {
 if (function_exists('Wga')) Wga(); //Lightning 
		$this->load->language('product/product');

		$data['breadcrumbs'] = array();

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('text_home'),
			'href' => $this->url->link('common/home')
		);

		$this->load->model('catalog/category');


      // path manager
      if (isset($this->request->get['product_id']) && ((!isset($this->request->get['path']) && $this->config->get('mlseo_fpp_breadcrumbs') == '1') || ($this->config->get('mlseo_fpp_breadcrumbs') == '2')) && is_array($this->request->get)) {
        unset($this->request->get['path']);
        $this->load->model('tool/path_manager');
        $this->request->get = $this->model_tool_path_manager->getFullProductPath($this->request->get['product_id'], true) + $this->request->get;
      }
      
		if (isset($this->request->get['path'])) {
			$path = '';

			$parts = explode('_', (string)$this->request->get['path']);

			$category_id = (int)array_pop($parts);

			foreach ($parts as $path_id) {
				if (!$path) {
					$path = $path_id;
				} else {
					$path .= '_' . $path_id;
				}

				$category_info = $this->model_catalog_category->getCategory($path_id);

				if ($category_info) {
					$data['breadcrumbs'][] = array(
						'text' => $category_info['name'],
						'href' => $this->url->link('product/category', 'path=' . $path)
					);
				}
			}

			// Set the last category breadcrumb
			$category_info = $this->model_catalog_category->getCategory($category_id);

			if ($category_info) {
				$url = '';

				if (isset($this->request->get['sort'])) {
					$url .= '&sort=' . $this->request->get['sort'];
				}

				if (isset($this->request->get['order'])) {
					$url .= '&order=' . $this->request->get['order'];
				}

				if (isset($this->request->get['page'])) {
					$url .= '&page=' . $this->request->get['page'];
				}

				if (isset($this->request->get['limit'])) {
					$url .= '&limit=' . $this->request->get['limit'];
				}

				$data['breadcrumbs'][] = array(
					'text' => $category_info['name'],
					'href' => $this->url->link('product/category', 'path=' . $this->request->get['path'] . $url)
				);
			}
		}

		$this->load->model('catalog/manufacturer');

		if (isset($this->request->get['manufacturer_id'])) {
			$data['breadcrumbs'][] = array(
				'text' => $this->language->get('text_brand'),
				'href' => $this->url->link('product/manufacturer')
			);

			$url = '';

			if (isset($this->request->get['sort'])) {
				$url .= '&sort=' . $this->request->get['sort'];
			}

			if (isset($this->request->get['order'])) {
				$url .= '&order=' . $this->request->get['order'];
			}

			if (isset($this->request->get['page'])) {
				$url .= '&page=' . $this->request->get['page'];
			}

			if (isset($this->request->get['limit'])) {
				$url .= '&limit=' . $this->request->get['limit'];
			}

			$manufacturer_info = $this->model_catalog_manufacturer->getManufacturer($this->request->get['manufacturer_id']);

			if ($manufacturer_info) {
				$data['breadcrumbs'][] = array(
					'text' => $manufacturer_info['name'],
					'href' => $this->url->link('product/manufacturer/info', 'manufacturer_id=' . $this->request->get['manufacturer_id'] . $url)
				);
			}
		}

		if (isset($this->request->get['search']) || isset($this->request->get['tag'])) {
			$url = '';

			if (isset($this->request->get['search'])) {
				$url .= '&search=' . $this->request->get['search'];
			}

			if (isset($this->request->get['tag'])) {
				$url .= '&tag=' . $this->request->get['tag'];
			}

			if (isset($this->request->get['description'])) {
				$url .= '&description=' . $this->request->get['description'];
			}

			if (isset($this->request->get['category_id'])) {
				$url .= '&category_id=' . $this->request->get['category_id'];
			}

			if (isset($this->request->get['sub_category'])) {
				$url .= '&sub_category=' . $this->request->get['sub_category'];
			}

			if (isset($this->request->get['sort'])) {
				$url .= '&sort=' . $this->request->get['sort'];
			}

			if (isset($this->request->get['order'])) {
				$url .= '&order=' . $this->request->get['order'];
			}

			if (isset($this->request->get['page'])) {
				$url .= '&page=' . $this->request->get['page'];
			}

			if (isset($this->request->get['limit'])) {
				$url .= '&limit=' . $this->request->get['limit'];
			}

			$data['breadcrumbs'][] = array(
				'text' => $this->language->get('text_search'),
				'href' => $this->url->link('product/search', $url)
			);
		}


					// Top stickers start
						$this->load->model('setting/setting');

						// Statuses
						$data['topstickers_status'] = $this->config->get('topstickers_status');
						$data['topstickers_sold_status'] = $this->config->get('topstickers_sold_status');
						$data['topstickers_sale_status'] = $this->config->get('topstickers_sale_status');
						$data['topstickers_bestseller_status'] = $this->config->get('topstickers_bestseller_status');
						$data['topstickers_novelty_status'] = $this->config->get('topstickers_novelty_status');
						$data['topstickers_last_status'] = $this->config->get('topstickers_last_status');
						$data['topstickers_freeshipping_status'] = $this->config->get('topstickers_freeshipping_status');

						// Text
						$current_language_id = $this->config->get('config_language_id');
						$data['topstickers_sold_text'] = $this->config->get('topstickers_sold_text')[$current_language_id];
						$data['topstickers_sale_text'] = $this->config->get('topstickers_sale_text')[$current_language_id];
						$data['topstickers_bestseller_text'] = $this->config->get('topstickers_bestseller_text')[$current_language_id];
						$data['topstickers_novelty_text'] = $this->config->get('topstickers_novelty_text')[$current_language_id];
						$data['topstickers_last_text'] = $this->config->get('topstickers_last_text')[$current_language_id];
						$data['topstickers_freeshipping_text'] = $this->config->get('topstickers_freeshipping_text')[$current_language_id];

						// Additional data
						$data['topstickers_bestseller_numbers'] = $this->config->get('topstickers_bestseller_numbers');
						$data['topstickers_novelty_days'] = $this->config->get('topstickers_novelty_days');
						$data['topstickers_last_numbers'] = $this->config->get('topstickers_last_numbers');
						$data['topstickers_freeshipping_price'] = $this->config->get('topstickers_freeshipping_price');
					// Top stickers end
				
		if (isset($this->request->get['product_id'])) {
			$product_id = (int)$this->request->get['product_id'];
		} else {
			$product_id = 0;
		}

		$this->load->model('catalog/product');

		$product_info = $this->model_catalog_product->getProduct($product_id);

		if ($product_info) {

			// PriceAlert
			$this->language->load('module/pricealert');
			$this->load->model('setting/setting');
			$this->document->addScript('catalog/view/javascript/pricealert/pricealert.js');
			$moduleSettingsPriceAlert			= $this->model_setting_setting->getSetting('PriceAlert', $this->config->get('config_store_id'));
			$data['PriceAlert']					= isset($moduleSettingsPriceAlert['PriceAlert']) ? ($moduleSettingsPriceAlert['PriceAlert']) : array();
			$data['language_id']				= $this->config->get('config_language_id');
			$data['PriceAlert_Button'] 			= $this->language->get('PriceAlert_Button');
			if (file_exists(DIR_TEMPLATE . $this->config->get('config_template') . '/stylesheet/pricealert.css')) {
				$this->document->addStyle('catalog/view/theme/' . $this->config->get('config_template') . '/stylesheet/pricealert.css');
			} else {
				$this->document->addStyle('catalog/view/theme/default/stylesheet/pricealert.css');
			}
			// PriceAlert
			
			$url = '';


      // path manager
      if (isset($this->request->get['product_id']) && ((!isset($this->request->get['path']) && $this->config->get('mlseo_fpp_breadcrumbs') == '1') || ($this->config->get('mlseo_fpp_breadcrumbs') == '2')) && is_array($this->request->get)) {
        unset($this->request->get['path']);
        $this->load->model('tool/path_manager');
        $this->request->get = $this->model_tool_path_manager->getFullProductPath($this->request->get['product_id'], true) + $this->request->get;
      }
      
			if (isset($this->request->get['path'])) {
				$url .= '&path=' . $this->request->get['path'];
			}

			if (isset($this->request->get['filter'])) {
				$url .= '&filter=' . $this->request->get['filter'];
			}

			if (isset($this->request->get['manufacturer_id'])) {
				$url .= '&manufacturer_id=' . $this->request->get['manufacturer_id'];
			}

			if (isset($this->request->get['search'])) {
				$url .= '&search=' . $this->request->get['search'];
			}

			if (isset($this->request->get['tag'])) {
				$url .= '&tag=' . $this->request->get['tag'];
			}

			if (isset($this->request->get['description'])) {
				$url .= '&description=' . $this->request->get['description'];
			}

			if (isset($this->request->get['category_id'])) {
				$url .= '&category_id=' . $this->request->get['category_id'];
			}

			if (isset($this->request->get['sub_category'])) {
				$url .= '&sub_category=' . $this->request->get['sub_category'];
			}

			if (isset($this->request->get['sort'])) {
				$url .= '&sort=' . $this->request->get['sort'];
			}

			if (isset($this->request->get['order'])) {
				$url .= '&order=' . $this->request->get['order'];
			}

			if (isset($this->request->get['page'])) {
				$url .= '&page=' . $this->request->get['page'];
			}

			if (isset($this->request->get['limit'])) {
				$url .= '&limit=' . $this->request->get['limit'];
			}

			$data['breadcrumbs'][] = array(
				'text' => $product_info['name'],
				'href' => $this->url->link('product/product', $url . '&product_id=' . $this->request->get['product_id'])
			);

			
      if ($this->config->get('mlseo_enabled')) {
        $this->document->setTitle(!empty($product_info['meta_title']) ? $product_info['meta_title'] : $product_info['name']);
        if (version_compare(VERSION, '2', '>=')) {
          $data['image_alt'] = !empty($product_info['image_alt']) ? $product_info['image_alt'] : '';
          $data['image_title'] = !empty($product_info['image_title']) ? $product_info['image_title'] : '';
        } else {
          $this->data['image_alt'] = !empty($product_info['image_alt']) ? $product_info['image_alt'] : '';
          $this->data['image_title'] = !empty($product_info['image_title']) ? $product_info['image_title'] : '';
        }
      } else {
        $this->document->setTitle($product_info['meta_title']);
      }
      
			$this->document->setDescription($product_info['meta_description']);
			$this->document->setKeywords($product_info['meta_keyword']);
			$this->document->addLink($this->url->link('product/product', 'product_id=' . $this->request->get['product_id']), 'canonical');
			$this->document->addScript('catalog/view/javascript/jquery/magnific/jquery.magnific-popup.min.js');
			$this->document->addStyle('catalog/view/javascript/jquery/magnific/magnific-popup.css');
			$this->document->addScript('catalog/view/javascript/jquery/datetimepicker/moment.js');
			$this->document->addScript('catalog/view/javascript/jquery/datetimepicker/bootstrap-datetimepicker.min.js');
			$this->document->addStyle('catalog/view/javascript/jquery/datetimepicker/bootstrap-datetimepicker.min.css');

			$data['heading_title'] = $product_info['name'];


      if (version_compare(VERSION, '2', '>=')) {
        if ($this->config->get('mlseo_fpp_noprodbreadcrumb')) {
          array_pop($data['breadcrumbs']);
        }
        
        //$data['heading_title'] = $product_info['name'];
        
        $data["heading_title"] = !empty($product_info['seo_h1']) && $this->config->get('mlseo_enabled') ? $product_info['seo_h1'] : $product_info['name'];
        $data['seo_h1'] = !empty($product_info['seo_h1']) ? $product_info['seo_h1'] : '';
        $data['seo_h2'] = !empty($product_info['seo_h2']) ? $product_info['seo_h2'] : '';
        $data['seo_h3'] = !empty($product_info['seo_h3']) ? $product_info['seo_h3'] : '';
      } else {
        if ($this->config->get('mlseo_fpp_noprodbreadcrumb')) {
          array_pop($this->data['breadcrumbs']);
        }
        
        //$this->data['heading_title'] = $product_info['name'];
        
        $this->data["heading_title"] = !empty($product_info['seo_h1']) && $this->config->get('mlseo_enabled') ? $product_info['seo_h1'] : $product_info['name'];
        $this->data['seo_h1'] = !empty($product_info['seo_h1']) ? $product_info['seo_h1'] : '';
        $this->data['seo_h2'] = !empty($product_info['seo_h2']) ? $product_info['seo_h2'] : '';
        $this->data['seo_h3'] = !empty($product_info['seo_h3']) ? $product_info['seo_h3'] : '';
      }
      

				$data['text_one_click_header']     		= $this->language->get('text_one_click_header');
				$data['text_one_click_button']  		= $this->language->get('text_one_click_button');
				$data['text_one_click_placeholder']  	= $this->language->get('text_one_click_placeholder');
				$data['text_one_click_help']  			= $this->language->get('text_one_click_help');
				$data['text_one_click_mask']  			= $this->language->get('text_one_click_mask');
				
      $this->load->model('catalog/review');
      
      $data['seo_reviews'] = '';
      
      if ($this->config->get('mlseo_reviews')) {
        $gkd_seo_reviews = $this->model_catalog_review->getReviewsByProductId($this->request->get['product_id'], 0, (int)$this->config->get('mlseo_reviews'));
        
        if (count($gkd_seo_reviews)) {
          $data['seo_reviews'] .= '<div class="seo_reviews">';
            foreach ($gkd_seo_reviews as $review) {
              $data['seo_reviews'] .= '<table class="table table-striped table-bordered seo_review">';
              $data['seo_reviews'] .= '<tr>';
              $data['seo_reviews'] .= '  <td style="width: 50%;"><strong>' . $review['author']. '</strong></td>';
              $data['seo_reviews'] .= '  <td class="text-right">' . $review['date_added']. '</td>';
              $data['seo_reviews'] .= '</tr>';
              $data['seo_reviews'] .= '<tr>';
              $data['seo_reviews'] .= '  <td colspan="2"><p>' . $review['text']. '</p>';
              for ($i = 1; $i <= 5; $i++) { 
                if ($review['rating'] < $i) {
                  $data['seo_reviews'] .= '    <span class="fa fa-stack"><i class="fa fa-star-o fa-stack-2x"></i></span>';
                } else {
                  $data['seo_reviews'] .= '    <span class="fa fa-stack"><i class="fa fa-star fa-stack-2x"></i><i class="fa fa-star-o fa-stack-2x"></i></span>';
                }
              }
              $data['seo_reviews'] .= '  </td>';
              $data['seo_reviews'] .= '</tr>';
              $data['seo_reviews'] .= '</table>';
            }
          $data['seo_reviews'] .= '</div>';
        }
      }
      
      if (!empty($product_info['meta_robots'])) {
        $this->document->addSeoMeta('<meta name="robots" content="'.$product_info['meta_robots'].'"/>'."\n");
      }
      
      if ($this->config->get('mlseo_header_lm_product')) {
        $array_lm = array(strtotime($product_info['date_modified']));
        
        if (strtotime($product_info['date_available']) < strtotime(date('Y-m-d'))) {
          $array_lm[] = strtotime($product_info['date_available']);
        }
        
        $special_query = $this->db->query("SELECT date_start, date_end FROM " . DB_PREFIX . "product_special ps WHERE ps.product_id = '".(int)$product_id."' AND ps.customer_group_id = '" . (int)$this->config->get('config_customer_group_id') . "' AND ((ps.date_start = '0000-00-00' OR ps.date_start < NOW()) AND (ps.date_end = '0000-00-00' OR ps.date_end > NOW()))")->row;
        
        if (!empty($special_query['date_start']) && strtotime($special_query['date_start']) < strtotime(date('Y-m-d'))) {
          $array_lm[] = strtotime($special_query['date_start']);
        }
        
        if (!empty($special_query['date_end']) && strtotime($special_query['date_end']) < strtotime(date('Y-m-d'))) {
          $array_lm[] = strtotime($special_query['date_end']);
        }
        
        $review_query = $this->db->query("SELECT date_modified FROM " . DB_PREFIX . "review WHERE product_id = '" . (int)$product_id . "' AND status = '1' ORDER BY date_modified DESC LIMIT 1")->row;
        
        if (!empty($review_query['date_modified']) && strtotime($review_query['date_modified']) < strtotime(date('Y-m-d'))) {
          $array_lm[] = strtotime($review_query['date_modified']);
        }
        
        $gkd_header_lm_date = max($array_lm);
        
        $this->response->addHeader('Last-Modified: '.date('D, d M Y H:i:s', $gkd_header_lm_date).' GMT');
      }
      
			$data['text_select'] = $this->language->get('text_select');
			$data['text_manufacturer'] = $this->language->get('text_manufacturer');
			$data['text_model'] = $this->language->get('text_model');
			$data['text_reward'] = $this->language->get('text_reward');
			$data['text_points'] = $this->language->get('text_points');
			$data['text_stock'] = $this->language->get('text_stock');
			$data['text_discount'] = $this->language->get('text_discount');
			$data['text_tax'] = $this->language->get('text_tax');
			$data['text_option'] = $this->language->get('text_option');
			$data['text_minimum'] = sprintf($this->language->get('text_minimum'), $product_info['minimum']);
			$data['text_write'] = $this->language->get('text_write');
			$data['text_login'] = sprintf($this->language->get('text_login'), $this->url->link('account/login', '', 'SSL'), $this->url->link('account/register', '', 'SSL'));
			$data['text_note'] = $this->language->get('text_note');
			$data['text_tags'] = $this->language->get('text_tags');
			$data['text_related'] = $this->language->get('text_related');
			$data['text_payment_recurring'] = $this->language->get('text_payment_recurring');
			$data['text_loading'] = $this->language->get('text_loading');

			$data['entry_qty'] = $this->language->get('entry_qty');
			$data['entry_name'] = $this->language->get('entry_name');
			$data['entry_review'] = $this->language->get('entry_review');
			$data['entry_rating'] = $this->language->get('entry_rating');
			$data['entry_good'] = $this->language->get('entry_good');
			$data['entry_bad'] = $this->language->get('entry_bad');

			$data['button_cart'] = $this->language->get('button_cart');
			$data['button_wishlist'] = $this->language->get('button_wishlist');
			$data['button_compare'] = $this->language->get('button_compare');
			$data['button_upload'] = $this->language->get('button_upload');
			$data['button_continue'] = $this->language->get('button_continue');


				$data['text_one_click_header']     		= $this->language->get('text_one_click_header');
				$data['text_one_click_button']  		= $this->language->get('text_one_click_button');
				$data['text_one_click_placeholder']  	= $this->language->get('text_one_click_placeholder');
				$data['text_one_click_help']  			= $this->language->get('text_one_click_help');
				$data['text_one_click_mask']  			= $this->language->get('text_one_click_mask');
				
			$this->load->model('catalog/review');

			$data['tab_description'] = $this->language->get('tab_description');
			$data['tab_attribute'] = $this->language->get('tab_attribute');
			$data['tab_review'] = sprintf($this->language->get('tab_review'), $product_info['reviews']);

			$data['product_id'] = (int)$this->request->get['product_id'];
			$data['manufacturer'] = $product_info['manufacturer'];
			$data['manufacturers'] = $this->url->link('product/manufacturer/info', 'manufacturer_id=' . $product_info['manufacturer_id']);
			$data['model'] = $product_info['model'];
			$data['reward'] = $product_info['reward'];
			$data['points'] = $product_info['points'];
			$data['description'] = html_entity_decode($product_info['description'], ENT_QUOTES, 'UTF-8');

			if ($product_info['quantity'] <= 0) {
				$data['stock'] = $product_info['stock_status'];
			} elseif ($this->config->get('config_stock_display')) {
				$data['stock'] = $product_info['quantity'];
			} else {
				$data['stock'] = $this->language->get('text_instock');
			}

			$this->load->model('tool/image');

			if ($product_info['image']) {
 $data['popup'] = 'image/'. $product_info['image'];
 } else {
				$data['popup'] = '';
			}

			if ($product_info['image']) {
				if ($product_id == 59 ) {
				$data['thumb'] = $this->model_tool_image->resize($product_info['image'], 450,294);
			} else {
				$data['thumb'] = $this->model_tool_image->resize($product_info['image'], $this->config->get('config_image_thumb_width'), $this->config->get('config_image_thumb_height'));
			}
			} else {
				$data['thumb'] = '';
			}

			$data['images'] = array();

			$results = $this->model_catalog_product->getProductImages($this->request->get['product_id']);

			foreach ($results as $result) {
$data['images'][] = array(
'popup' => 'image/' . $result['image'],
					'thumb' => $this->model_tool_image->resize($result['image'], $this->config->get('config_image_additional_width'), $this->config->get('config_image_additional_height'))
				);
			}

			if (($this->config->get('config_customer_price') && $this->customer->isLogged()) || !$this->config->get('config_customer_price')) {
				$data['price'] = $this->currency->format($this->tax->calculate($product_info['price'], $product_info['tax_class_id'], $this->config->get('config_tax')));
			} else {
				$data['price'] = false;
			}

			if ((float)$product_info['special']) {
$data['percent'] = sprintf($this->language->get('-%s'), (round((($product_info['price'] -  $product_info['special'])/$product_info['price']) * 100 ,0))). ' %';
				$data['special'] = $this->currency->format($this->tax->calculate($product_info['special'], $product_info['tax_class_id'], $this->config->get('config_tax')));
			} else {
				$data['special'] = false;
			}

			if ($this->config->get('config_tax')) {
				$data['tax'] = $this->currency->format((float)$product_info['special'] ? $product_info['special'] : $product_info['price']);
			} else {
				$data['tax'] = false;
			}

			$discounts = $this->model_catalog_product->getProductDiscounts($this->request->get['product_id']);

			$data['discounts'] = array();

			foreach ($discounts as $discount) {
				$data['discounts'][] = array(
					'quantity' => $discount['quantity'],
					'price'    => $this->currency->format($this->tax->calculate($discount['price'], $product_info['tax_class_id'], $this->config->get('config_tax')))
				);
			}

			$data['options'] = array();

			foreach ($this->model_catalog_product->getProductOptions($this->request->get['product_id']) as $option) {
				$product_option_value_data = array();

				foreach ($option['product_option_value'] as $option_value) {
					if (!$option_value['subtract'] || ($option_value['quantity'] > 0)) {
						if ((($this->config->get('config_customer_price') && $this->customer->isLogged()) || !$this->config->get('config_customer_price')) && (float)$option_value['price']) {
							$price = $this->currency->format($this->tax->calculate($option_value['price'], $product_info['tax_class_id'], $this->config->get('config_tax') ? 'P' : false));
						} else {
							$price = false;
						}

						$product_option_value_data[] = array(
							'product_option_value_id' => $option_value['product_option_value_id'],
							'option_value_id'         => $option_value['option_value_id'],
							'name'                    => $option_value['name'],
							'image'                   => $this->model_tool_image->resize($option_value['image'], 50, 50),
							'price'                   => $price,
							'price_prefix'            => $option_value['price_prefix']
						);
					}
				}

				$data['options'][] = array(
					'product_option_id'    => $option['product_option_id'],
					'product_option_value' => $product_option_value_data,
					'option_id'            => $option['option_id'],
					'name'                 => $option['name'],
					'type'                 => $option['type'],
					'value'                => $option['value'],
					'required'             => $option['required']
				);
			}

			if ($product_info['minimum']) {
				$data['minimum'] = $product_info['minimum'];
			} else {
				$data['minimum'] = 1;
			}

			$data['review_status'] = $this->config->get('config_review_status');

			if ($this->config->get('config_review_guest') || $this->customer->isLogged()) {
				$data['review_guest'] = true;
			} else {
				$data['review_guest'] = false;
			}

			if ($this->customer->isLogged()) {
				$data['customer_name'] = $this->customer->getFirstName() . '&nbsp;' . $this->customer->getLastName();
			} else {
				$data['customer_name'] = '';
			}

			$data['reviews'] = sprintf($this->language->get('text_reviews'), (int)$product_info['reviews']);
			$data['rating'] = (int)$product_info['rating'];

			// Captcha
			if ($this->config->get($this->config->get('config_captcha') . '_status') && in_array('review', (array)$this->config->get('config_captcha_page'))) {
				$data['captcha'] = $this->load->controller('captcha/' . $this->config->get('config_captcha'));
			} else {
				$data['captcha'] = '';
			}

			$data['attribute_groups'] = $this->model_catalog_product->getProductAttributes($this->request->get['product_id']);

			$data['products'] = array();

			$results = $this->model_catalog_product->getProductRelated($this->request->get['product_id']);

			foreach ($results as $result) {
		
			$results_img = $this->model_catalog_product->getProductImages($result['product_id']);
			$additional_img = array();
			foreach ($results_img as $result_img) {
				if ($result_img['image']) {
					$additional_image = $this->model_tool_image->resize($result_img['image'], $this->config->get('config_image_product_width'), $this->config->get('config_image_product_height'));
				} else {
					$additional_image = false;
				}
				$additional_img[1] = $additional_image;
				break;
			}
            
				if ($result['image']) {
					$image = $this->model_tool_image->resize($result['image'], $this->config->get('config_image_related_width'), $this->config->get('config_image_related_height'));
				} else {
					$image = $this->model_tool_image->resize('placeholder.png', $this->config->get('config_image_related_width'), $this->config->get('config_image_related_height'));
				}

				if (($this->config->get('config_customer_price') && $this->customer->isLogged()) || !$this->config->get('config_customer_price')) {
					$price = $this->currency->format($this->tax->calculate($result['price'], $result['tax_class_id'], $this->config->get('config_tax')));
				} else {
					$price = false;
				}

				if ((float)$result['special']) {
					$special = $this->currency->format($this->tax->calculate($result['special'], $result['tax_class_id'], $this->config->get('config_tax')));
				} else {
					$special = false;
				}

				if ($this->config->get('config_tax')) {
					$tax = $this->currency->format((float)$result['special'] ? $result['special'] : $result['price']);
				} else {
					$tax = false;
				}

				if ($this->config->get('config_review_status')) {
					$rating = (int)$result['rating'];
				} else {
					$rating = false;
				}


					// Top stickers start
						// Stickers for related products
						$related_product_info = $this->model_catalog_product->getProduct($result['product_id']);

							// Sold sticker
							$related_product_sold_sticker = false;
							if ($data['topstickers_sold_status'] && $data['topstickers_sold_text'] && $data['topstickers_sold_text'] != '' && $related_product_info['quantity'] <= 0) {
								$related_product_sold_sticker = $data['topstickers_sold_text'];
							}

							// Sale sticker
							$related_product_sale_sticker = false;
							if ($data['topstickers_sale_status'] && $data['topstickers_sale_text'] && $data['topstickers_sale_text'] != '' && (float)$related_product_info['special']) {
								$related_product_sale_sticker = $data['topstickers_sale_text'];
							}

							// Bestseller sticker
							$related_product_bestseller_sticker = false;
							if ($data['topstickers_bestseller_status'] && $data['topstickers_bestseller_text'] && $data['topstickers_bestseller_text'] != '') {
								$bestsellers = $this->model_catalog_product->getBestSellerProducts( $data['topstickers_bestseller_numbers']);
								foreach ($bestsellers as $bestseller) {
									if ($bestseller['product_id'] == $result['product_id']) {
										$related_product_bestseller_sticker = $data['topstickers_bestseller_text'];
									}
								}
							}

							// Novelty sticker
							$related_product_novelty_sticker = false;
							if ($data['topstickers_novelty_status'] && $data['topstickers_novelty_text'] && $data['topstickers_novelty_text'] != '' && (strtotime($related_product_info['date_added']) + intval($data['topstickers_novelty_days']) * 24 * 3600) > time()) {
								$related_product_novelty_sticker = $data['topstickers_novelty_text'];
							}

							// Last sticker
							$related_product_last_sticker = false;
							if ($data['topstickers_last_status'] && $data['topstickers_last_text'] && $data['topstickers_last_text'] != '' && $related_product_info['quantity'] <= intval($data['topstickers_last_numbers']) && $related_product_info['quantity'] > 0) {
								$related_product_last_sticker = $data['topstickers_last_text'];
							}

							// Freeshipping sticker
							$related_product_freeshipping_sticker = false;
							if ($data['topstickers_freeshipping_status'] && $data['topstickers_freeshipping_text'] && $data['topstickers_freeshipping_text'] != '') {
								if ((float)$related_product_info['special'] && (float)$related_product_info['special'] >= $data['topstickers_freeshipping_price']) {
									$related_product_freeshipping_sticker = $data['topstickers_freeshipping_text'];
								} elseif ($related_product_info['price'] >= $data['topstickers_freeshipping_price']) {
									$related_product_freeshipping_sticker = $data['topstickers_freeshipping_text'];
								} else {
									$related_product_freeshipping_sticker = false;
								}
							}
					// Top stickers end

					// Top stickers custom start
						$current_language_id = $this->config->get('config_language_id');
						$related_custom_topstickers = array();
						foreach ($this->model_catalog_product->getProductTopStickers($result['product_id']) as $topsticker) {
							$related_custom_topsticker_text = json_decode( $topsticker['text'], true);
							$related_custom_topstickers[] = array(
								'topsticker_id'	=> $topsticker['topsticker_id'],
								'name'  		=> $topsticker['name'],
								'text' 			=> $related_custom_topsticker_text[$current_language_id],
								'bg_color'  	=> $topsticker['bg_color'],
								'status'    	=> $topsticker['status']
							);
						}
					// Top stickers custom end
				
				$data['products'][] = array(
'percent'     => sprintf($this->language->get('-%s'), (round((($result['price'] - $result['special'])/$result['price']) * 100 ,0 ))) . ' %',
					'product_id'  => $result['product_id'],
					'thumb'       => $image,
		
			'additional_img' => $additional_img,
            
					'name'        => $result['name'],
					'description' => utf8_substr(strip_tags(html_entity_decode($result['description'], ENT_QUOTES, 'UTF-8')), 0, $this->config->get('config_product_description_length')) . '..',
					'price'       => $price,
					'special'     => $special,
					'tax'         => $tax,
					'minimum'     => $result['minimum'] > 0 ? $result['minimum'] : 1,
					'rating'      => $rating,

					// Top stickers start
						'product_sold_sticker' => $related_product_sold_sticker,
						'product_sale_sticker' => $related_product_sale_sticker,
						'product_bestseller_sticker' => $related_product_bestseller_sticker,
						'product_novelty_sticker' => $related_product_novelty_sticker,
						'product_last_sticker' => $related_product_last_sticker,
						'product_freeshipping_sticker' => $related_product_freeshipping_sticker,
						'product_custom_topstickers' => $related_custom_topstickers,
					// Top stickers end
				
					'manufacturer' => $result['manufacturer'],
					'href'        => $this->url->link('product/product', 'product_id=' . $result['product_id'])
				);
			}

			$data['tags'] = array();

			if ($product_info['tag']) {
				$tags = explode(',', $product_info['tag']);

				foreach ($tags as $tag) {
					$data['tags'][] = array(
						'tag'  => trim($tag),
						'href' => $this->url->link('product/search', 'tag=' . trim($tag))
					);
				}
			}


					// Top stickers start
						// Stickers for current product
							// Sold sticker
							$data['product_sold_sticker'] = false;
							if ($data['topstickers_sold_status'] && $data['topstickers_sold_text'] && $data['topstickers_sold_text'] != '' && $product_info['quantity'] <= 0) {
								$data['product_sold_sticker'] = $data['topstickers_sold_text'];
							}

							// Sale sticker
							$data['product_sale_sticker'] = false;
							if ($data['topstickers_sale_status'] && $data['topstickers_sale_text'] && $data['topstickers_sale_text'] != '' && (float)$product_info['special']) {
								$data['product_sale_sticker'] = $data['topstickers_sale_text'];
							}

							// Bestseller sticker
							$data['product_bestseller_sticker'] = false;
							if ($data['topstickers_bestseller_status'] && $data['topstickers_bestseller_text'] && $data['topstickers_bestseller_text'] != '') {
								$bestsellers = $this->model_catalog_product->getBestSellerProducts( $data['topstickers_bestseller_numbers']);
								foreach ($bestsellers as $bestseller) {
									if ($bestseller['product_id'] == $product_id) {
										$data['product_bestseller_sticker'] = $data['topstickers_bestseller_text'];
									}
								}
							}

							// Novelty sticker
							$data['product_novelty_sticker'] = false;
							if ($data['topstickers_novelty_status'] && $data['topstickers_novelty_text'] && $data['topstickers_novelty_text'] != '' && (strtotime($product_info['date_added']) + intval($data['topstickers_novelty_days']) * 24 * 3600) > time()) {
								$data['product_novelty_sticker'] = $data['topstickers_novelty_text'];
							}

							// Last sticker
							$data['product_last_sticker'] = false;
							if ($data['topstickers_last_status'] && $data['topstickers_last_text'] && $data['topstickers_last_text'] != '' && $product_info['quantity'] <= intval($data['topstickers_last_numbers']) && $product_info['quantity'] > 0) {
								$data['product_last_sticker'] = $data['topstickers_last_text'];
							}

							// Freeshipping sticker
							$data['product_freeshipping_sticker'] = false;
							if ($data['topstickers_freeshipping_status'] && $data['topstickers_freeshipping_text'] && $data['topstickers_freeshipping_text'] != '') {
								if ((float)$product_info['special'] && (float)$product_info['special'] >= (float)$data['topstickers_freeshipping_price']) {
									$data['product_freeshipping_sticker'] = $data['topstickers_freeshipping_text'];
								} elseif ((float)$product_info['price'] >= (float)$data['topstickers_freeshipping_price']) {
									$data['product_freeshipping_sticker'] = $data['topstickers_freeshipping_text'];
								} else {
									$data['product_freeshipping_sticker'] = false;
								}
							}
					// Top stickers end

					// Top stickers custom start
						$current_language_id = $this->config->get('config_language_id');
						$data['custom_topstickers'] = array();
						foreach ($this->model_catalog_product->getProductTopStickers($this->request->get['product_id']) as $topsticker) {
							$custom_topsticker_text = json_decode( $topsticker['text'], true);
							$data['custom_topstickers'][] = array(
								'topsticker_id'	=> $topsticker['topsticker_id'],
								'name'  		=> $topsticker['name'],
								'text' 			=> $custom_topsticker_text[$current_language_id],
								'bg_color'  	=> $topsticker['bg_color'],
								'status'    	=> $topsticker['status']
							);
						}
					// Top stickers custom end
				
			$data['recurrings'] = $this->model_catalog_product->getProfiles($this->request->get['product_id']);


						//microdatapro 7.0 start - 1 - main
						$data['microdatapro_data'] = $product_info;
						$this->document->setTc_og($this->load->controller('module/microdatapro/tc_og', $data));
						$this->document->setTc_og_prefix($this->load->controller('module/microdatapro/tc_og_prefix'));
						$data['microdatapro'] = $this->load->controller('module/microdatapro/product', $data);
						$microdatapro_main_flag = 1;
						//microdatapro 7.0 end - 1 - main
					
			$this->model_catalog_product->updateViewed($this->request->get['product_id']);

      if ($this->config->get('mlseo_enabled')) {
        $this->load->model('tool/seo_package');
        
        if ($this->config->get('mlseo_opengraph')) {
          if (version_compare(VERSION, '2', '>=')) {
            $this->document->addSeoMeta($this->model_tool_seo_package->rich_snippet('opengraph', 'product', $data + array('product_info' => $product_info)));
          } else {
            $this->document->addSeoMeta($this->model_tool_seo_package->rich_snippet('opengraph', 'product', $this->data + array('product_info' => $product_info)));
          }
        }

        if ($this->config->get('mlseo_tcard')) {
          if (version_compare(VERSION, '2', '>=')) {
            $this->document->addSeoMeta($this->model_tool_seo_package->rich_snippet('tcard', 'product', $data + array('product_info' => $product_info)));
          } else {
            $this->document->addSeoMeta($this->model_tool_seo_package->rich_snippet('tcard', 'product', $this->data + array('product_info' => $product_info)));
          }
        }

        if ($this->config->get('mlseo_microdata')) {
          if (version_compare(VERSION, '2', '>=')) {
            $this->document->addSeoMeta($this->model_tool_seo_package->rich_snippet('microdata', 'product', $data + array('product_info' => $product_info)));
          } else {
            $this->document->addSeoMeta($this->model_tool_seo_package->rich_snippet('microdata', 'product', $this->data + array('product_info' => $product_info)));
          }
        }
      }
      


						//microdatapro 7.0 start - 2 - extra
						if(!isset($microdatapro_main_flag)){
							if(isset($product_info) && $product_info){
								$data['microdatapro_data'] = $product_info;
								$this->document->setTc_og($this->load->controller('module/microdatapro/tc_og', $data));
								$this->document->setTc_og_prefix($this->load->controller('module/microdatapro/tc_og_prefix'));
								$data['microdatapro'] = $this->load->controller('module/microdatapro/product', $data);
								$microdatapro_main_flag = 1;
							}
						}
						//microdatapro 7.0 end - 2 - extra
					
			$data['column_left'] = $this->load->controller('common/column_left');
			$data['column_right'] = $this->load->controller('common/column_right');
			$data['content_top'] = $this->load->controller('common/content_top');
			$data['content_bottom'] = $this->load->controller('common/content_bottom');
			$data['footer'] = $this->load->controller('common/footer');
			$data['header'] = $this->load->controller('common/header');

			if ($product_info['image']) {
				if ($product_id == 59 ) {
				$data['thumb'] = $this->model_tool_image->resize($product_info['image'], 450,294);
			} else {
				$data['thumb'] = $this->model_tool_image->resize($product_info['image'], $this->config->get('config_image_thumb_width'), $this->config->get('config_image_thumb_height'));
			}
			} else {
				$data['thumb'] = '';
			}
			
			
			
			
			
			 $this->load->language('product/pergunta');	
			 $data['button_pergunta'] = $this->language->get('button_pergunta');				
			 if (file_exists(DIR_TEMPLATE . $this->config->get('config_template') . '/template/product/product.tpl')) {		
			
			if ($product_id == 59 ) {
				$this->response->setOutput($this->load->view($this->config->get('config_template') . '/template/product/product_karta.tpl', $data));
			} else {
				
            $template = $this->config->get('config_template') . '/template/product/product.tpl';

            // Custom template module
            $this->load->model('setting/setting');

            $custom_template_module = $this->model_setting_setting->getSetting('custom_template_module');

            $customer_group_id = $this->customer->getGroupId();

            if(!empty($custom_template_module['custom_template_module'])){
                if(isset($this->request->get['path'])){
                    foreach ($custom_template_module['custom_template_module'] as $key => $module) {
                        if (($module['type'] == 4) && !empty($module['product_categories'])) {
                            if ((isset($module['customer_groups']) && in_array($customer_group_id, $module['customer_groups'])) || !isset($module['customer_groups']) || empty($module['customer_groups'])){

                                $category_id = explode('_', $this->request->get['path']);
                                $category_id = (int)end($category_id);
                                if (in_array($category_id, $module['product_categories'])) {
                                    if (file_exists(DIR_TEMPLATE . $this->config->get('config_template') . '/template/' . $module['template_name'] . '.tpl')) {
                                        $template = $this->config->get('config_template') . '/template/' . $module['template_name'] . '.tpl';
                                    }
                                }

                            } // customer groups

                        }
                    }
                }

                foreach ($custom_template_module['custom_template_module'] as $key => $module) {
                    if (($module['type'] == 5) && !empty($module['product_manufacturers'])) {

                        if ((isset($module['customer_groups']) && in_array($customer_group_id, $module['customer_groups'])) || !isset($module['customer_groups']) || empty($module['customer_groups'])){

                            $manufacturer_id = $product_info['manufacturer_id'];
                            if (in_array($manufacturer_id, $module['product_manufacturers'])) {
                                if (file_exists(DIR_TEMPLATE . $this->config->get('config_template') . '/template/' . $module['template_name'] . '.tpl')) {
                                    $template = $this->config->get('config_template') . '/template/' . $module['template_name'] . '.tpl';
                                }
                            }

                        } // customer groups

                    }
                }

                foreach ($custom_template_module['custom_template_module'] as $key => $module) {
                    if (($module['type'] == 1) && !empty($module['products'])) {
                        if ((isset($module['customer_groups']) && in_array($customer_group_id, $module['customer_groups'])) || !isset($module['customer_groups']) || empty($module['customer_groups'])){

                            $products = explode(',', $module['products']);
                            if (in_array($product_id, $products)) {
                                if (file_exists(DIR_TEMPLATE . $this->config->get('config_template') . '/template/' . $module['template_name'] . '.tpl')) {
                                    $template = $this->config->get('config_template') . '/template/' . $module['template_name'] . '.tpl';
                                }
                            }

                        } // customer groups

                    }
                }
            }
            $this->response->setOutput($this->load->view($template, $data));
            // Custom template module
            
			}
			}
			else {
				$this->response->setOutput($this->load->view('default/template/product/product.tpl', $data));
			}
		} else {
			$url = '';


      // path manager
      if (isset($this->request->get['product_id']) && ((!isset($this->request->get['path']) && $this->config->get('mlseo_fpp_breadcrumbs') == '1') || ($this->config->get('mlseo_fpp_breadcrumbs') == '2')) && is_array($this->request->get)) {
        unset($this->request->get['path']);
        $this->load->model('tool/path_manager');
        $this->request->get = $this->model_tool_path_manager->getFullProductPath($this->request->get['product_id'], true) + $this->request->get;
      }
      
			if (isset($this->request->get['path'])) {
				$url .= '&path=' . $this->request->get['path'];
			}

			if (isset($this->request->get['filter'])) {
				$url .= '&filter=' . $this->request->get['filter'];
			}

			if (isset($this->request->get['manufacturer_id'])) {
				$url .= '&manufacturer_id=' . $this->request->get['manufacturer_id'];
			}

			if (isset($this->request->get['search'])) {
				$url .= '&search=' . $this->request->get['search'];
			}

			if (isset($this->request->get['tag'])) {
				$url .= '&tag=' . $this->request->get['tag'];
			}

			if (isset($this->request->get['description'])) {
				$url .= '&description=' . $this->request->get['description'];
			}

			if (isset($this->request->get['category_id'])) {
				$url .= '&category_id=' . $this->request->get['category_id'];
			}

			if (isset($this->request->get['sub_category'])) {
				$url .= '&sub_category=' . $this->request->get['sub_category'];
			}

			if (isset($this->request->get['sort'])) {
				$url .= '&sort=' . $this->request->get['sort'];
			}

			if (isset($this->request->get['order'])) {
				$url .= '&order=' . $this->request->get['order'];
			}

			if (isset($this->request->get['page'])) {
				$url .= '&page=' . $this->request->get['page'];
			}

			if (isset($this->request->get['limit'])) {
				$url .= '&limit=' . $this->request->get['limit'];
			}

			$data['breadcrumbs'][] = array(
				'text' => $this->language->get('text_error'),
				'href' => $this->url->link('product/product', $url . '&product_id=' . $product_id)
			);

			$this->document->setTitle($this->language->get('text_error'));

			$data['heading_title'] = $this->language->get('text_error');

			$data['text_error'] = $this->language->get('text_error');

			$data['button_continue'] = $this->language->get('button_continue');

			$data['continue'] = $this->url->link('common/home');

			$this->response->addHeader($this->request->server['SERVER_PROTOCOL'] . ' 404 Not Found');


						//microdatapro 7.0 start - 2 - extra
						if(!isset($microdatapro_main_flag)){
							if(isset($product_info) && $product_info){
								$data['microdatapro_data'] = $product_info;
								$this->document->setTc_og($this->load->controller('module/microdatapro/tc_og', $data));
								$this->document->setTc_og_prefix($this->load->controller('module/microdatapro/tc_og_prefix'));
								$data['microdatapro'] = $this->load->controller('module/microdatapro/product', $data);
								$microdatapro_main_flag = 1;
							}
						}
						//microdatapro 7.0 end - 2 - extra
					
			$data['column_left'] = $this->load->controller('common/column_left');
			$data['column_right'] = $this->load->controller('common/column_right');
			$data['content_top'] = $this->load->controller('common/content_top');
			$data['content_bottom'] = $this->load->controller('common/content_bottom');
			$data['footer'] = $this->load->controller('common/footer');
			$data['header'] = $this->load->controller('common/header');

			if (file_exists(DIR_TEMPLATE . $this->config->get('config_template') . '/template/error/not_found.tpl')) {
				$this->response->setOutput($this->load->view($this->config->get('config_template') . '/template/error/not_found.tpl', $data));
			} else {
				$this->response->setOutput($this->load->view('default/template/error/not_found.tpl', $data));
			}
		}
	}

	public function review() {

    // SEO Package - redirect non-ajax requests
    if($this->config->get('mlseo_redir_reviews') && !(!empty($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest')) {
      header('HTTP/1.1 301 Moved Permanently');
      header('Location: ' . $this->url->link('product/product', 'product_id=' . $this->request->get['product_id']));
    }
      
		$this->load->language('product/product');


				$data['text_one_click_header']     		= $this->language->get('text_one_click_header');
				$data['text_one_click_button']  		= $this->language->get('text_one_click_button');
				$data['text_one_click_placeholder']  	= $this->language->get('text_one_click_placeholder');
				$data['text_one_click_help']  			= $this->language->get('text_one_click_help');
				$data['text_one_click_mask']  			= $this->language->get('text_one_click_mask');
				
		$this->load->model('catalog/review');

		$data['text_no_reviews'] = $this->language->get('text_no_reviews');

		if (isset($this->request->get['page'])) {
			$page = $this->request->get['page'];
		} else {
			$page = 1;
		}

		$data['reviews'] = array();

		$review_total = $this->model_catalog_review->getTotalReviewsByProductId($this->request->get['product_id']);

		$results = $this->model_catalog_review->getReviewsByProductId($this->request->get['product_id'], ($page - 1) * 5, 5);

		foreach ($results as $result) {
			$data['reviews'][] = array(
				'author'     => $result['author'],
				'text'       => nl2br($result['text']),
				'rating'     => (int)$result['rating'],
				'date_added' => date($this->language->get('date_format_short'), strtotime($result['date_added']))
			);
		}

		$pagination = new Pagination();
		$pagination->total = $review_total;
		$pagination->page = $page;
		$pagination->limit = 5;
		$pagination->url = $this->url->link('product/product/review', 'product_id=' . $this->request->get['product_id'] . '&page={page}');

		$data['pagination'] = $pagination->render();

		$data['results'] = sprintf($this->language->get('text_pagination'), ($review_total) ? (($page - 1) * 5) + 1 : 0, ((($page - 1) * 5) > ($review_total - 5)) ? $review_total : ((($page - 1) * 5) + 5), $review_total, ceil($review_total / 5));

		if (file_exists(DIR_TEMPLATE . $this->config->get('config_template') . '/template/product/review.tpl')) {
			$this->response->setOutput($this->load->view($this->config->get('config_template') . '/template/product/review.tpl', $data));
		} else {
			$this->response->setOutput($this->load->view('default/template/product/review.tpl', $data));
		}
	}

	public function write() {
		$this->load->language('product/product');

		$json = array();

		if ($this->request->server['REQUEST_METHOD'] == 'POST') {
			if ((utf8_strlen($this->request->post['name']) < 3) || (utf8_strlen($this->request->post['name']) > 25)) {
				$json['error'] = $this->language->get('error_name');
			}

			if ((utf8_strlen($this->request->post['text']) < 25) || (utf8_strlen($this->request->post['text']) > 1000)) {
				$json['error'] = $this->language->get('error_text');
			}

			if (empty($this->request->post['rating']) || $this->request->post['rating'] < 0 || $this->request->post['rating'] > 5) {
				$json['error'] = $this->language->get('error_rating');
			}

			// Captcha
			if ($this->config->get($this->config->get('config_captcha') . '_status') && in_array('review', (array)$this->config->get('config_captcha_page'))) {
				$captcha = $this->load->controller('captcha/' . $this->config->get('config_captcha') . '/validate');

				if ($captcha) {
					$json['error'] = $captcha;
				}
			}

			if (!isset($json['error'])) {

				$data['text_one_click_header']     		= $this->language->get('text_one_click_header');
				$data['text_one_click_button']  		= $this->language->get('text_one_click_button');
				$data['text_one_click_placeholder']  	= $this->language->get('text_one_click_placeholder');
				$data['text_one_click_help']  			= $this->language->get('text_one_click_help');
				$data['text_one_click_mask']  			= $this->language->get('text_one_click_mask');
				
				$this->load->model('catalog/review');

				$this->model_catalog_review->addReview($this->request->get['product_id'], $this->request->post);

				$json['success'] = $this->language->get('text_success');
			}
		}

		$this->response->addHeader('Content-Type: application/json');
		$this->response->setOutput(json_encode($json));
	}

	public function getRecurringDescription() {
		$this->language->load('product/product');
		$this->load->model('catalog/product');

		if (isset($this->request->post['product_id'])) {
			$product_id = $this->request->post['product_id'];
		} else {
			$product_id = 0;
		}

		if (isset($this->request->post['recurring_id'])) {
			$recurring_id = $this->request->post['recurring_id'];
		} else {
			$recurring_id = 0;
		}

		if (isset($this->request->post['quantity'])) {
			$quantity = $this->request->post['quantity'];
		} else {
			$quantity = 1;
		}

		$product_info = $this->model_catalog_product->getProduct($product_id);
		$recurring_info = $this->model_catalog_product->getProfile($product_id, $recurring_id);

		$json = array();

		if ($product_info && $recurring_info) {
			if (!$json) {
				$frequencies = array(
					'day'        => $this->language->get('text_day'),
					'week'       => $this->language->get('text_week'),
					'semi_month' => $this->language->get('text_semi_month'),
					'month'      => $this->language->get('text_month'),
					'year'       => $this->language->get('text_year'),
				);

				if ($recurring_info['trial_status'] == 1) {
					$price = $this->currency->format($this->tax->calculate($recurring_info['trial_price'] * $quantity, $product_info['tax_class_id'], $this->config->get('config_tax')));
					$trial_text = sprintf($this->language->get('text_trial_description'), $price, $recurring_info['trial_cycle'], $frequencies[$recurring_info['trial_frequency']], $recurring_info['trial_duration']) . ' ';
				} else {
					$trial_text = '';
				}

				$price = $this->currency->format($this->tax->calculate($recurring_info['price'] * $quantity, $product_info['tax_class_id'], $this->config->get('config_tax')));

				if ($recurring_info['duration']) {
					$text = $trial_text . sprintf($this->language->get('text_payment_description'), $price, $recurring_info['cycle'], $frequencies[$recurring_info['frequency']], $recurring_info['duration']);
				} else {
					$text = $trial_text . sprintf($this->language->get('text_payment_cancel'), $price, $recurring_info['cycle'], $frequencies[$recurring_info['frequency']], $recurring_info['duration']);
				}

				$json['success'] = $text;
			}
		}

		$this->response->addHeader('Content-Type: application/json');
		$this->response->setOutput(json_encode($json));
	}
}
