<?php
class ControllerProductManufacturer extends Controller {
	public function index() {
		$this->load->language('product/manufacturer');

		$this->load->model('catalog/manufacturer');

		$this->load->model('tool/image');

		$this->document->setTitle($this->language->get('heading_title'));

			if (
			isset($this->request->get['page']) ||
			isset($this->request->get['limit']) ||
			isset($this->request->get['order'])
			) {
				$this->document->setRobots('noindex,follow');
			}
			
			


		$data['heading_title'] = $this->language->get('heading_title');

		$data['text_index'] = $this->language->get('text_index');
		$data['text_empty'] = $this->language->get('text_empty');

		$data['button_continue'] = $this->language->get('button_continue');


					// Top stickers start
						$this->load->model('setting/setting');

						// Statuses
						$data['topstickers_status'] = $this->config->get('topstickers_status');
						$data['topstickers_sold_status'] = $this->config->get('topstickers_sold_status');
						$data['topstickers_sale_status'] = $this->config->get('topstickers_sale_status');
						$data['topstickers_bestseller_status'] = $this->config->get('topstickers_bestseller_status');
						$data['topstickers_novelty_status'] = $this->config->get('topstickers_novelty_status');
						$data['topstickers_last_status'] = $this->config->get('topstickers_last_status');
						$data['topstickers_freeshipping_status'] = $this->config->get('topstickers_freeshipping_status');

						// Text
						$current_language_id = $this->config->get('config_language_id');
						$data['topstickers_sold_text'] = $this->config->get('topstickers_sold_text')[$current_language_id];
						$data['topstickers_sale_text'] = $this->config->get('topstickers_sale_text')[$current_language_id];
						$data['topstickers_bestseller_text'] = $this->config->get('topstickers_bestseller_text')[$current_language_id];
						$data['topstickers_novelty_text'] = $this->config->get('topstickers_novelty_text')[$current_language_id];
						$data['topstickers_last_text'] = $this->config->get('topstickers_last_text')[$current_language_id];
						$data['topstickers_freeshipping_text'] = $this->config->get('topstickers_freeshipping_text')[$current_language_id];

						// Additional data
						$data['topstickers_bestseller_numbers'] = $this->config->get('topstickers_bestseller_numbers');
						$data['topstickers_novelty_days'] = $this->config->get('topstickers_novelty_days');
						$data['topstickers_last_numbers'] = $this->config->get('topstickers_last_numbers');
						$data['topstickers_freeshipping_price'] = $this->config->get('topstickers_freeshipping_price');
					// Top stickers end
				
		$data['breadcrumbs'] = array();

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('text_home'),
			'href' => $this->url->link('common/home')
		);

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('text_brand'),
			'href' => $this->url->link('product/manufacturer')
		);

		$data['categories'] = array();

		$results = $this->model_catalog_manufacturer->getManufacturers();

		foreach ($results as $result) {
			if (is_numeric(utf8_substr($result['name'], 0, 1))) {
				$key = '0 - 9';
			} else {
				$key = utf8_substr(utf8_strtoupper($result['name']), 0, 1);
			}

			if (!isset($data['categories'][$key])) {
				$data['categories'][$key]['name'] = $key;
			}

			$data['categories'][$key]['manufacturer'][] = array(

	// ManufacturerLogo
				'logo' => $this->model_tool_image->resize($result['image'], 100, 100),
	// ManufacturerLogo end
			
				'name' => $result['name'],
				'href' => $this->url->link('product/manufacturer/info', 'manufacturer_id=' . $result['manufacturer_id'])
			);
		}

		$data['continue'] = $this->url->link('common/home');

      if ($this->config->get('mlseo_enabled')) {
        $this->load->model('tool/seo_package');
        
        if ($this->config->get('mlseo_microdata')) {
          if (version_compare(VERSION, '2', '>=')) {
            $this->document->addSeoMeta($this->model_tool_seo_package->rich_snippet('microdata', 'manufacturer', $data));
          } else {
            $this->document->addSeoMeta($this->model_tool_seo_package->rich_snippet('microdata', 'manufacturer', $this->data));
          }
        }
      }
      


					//microdatapro 7.0 start - 2 - extra
					if(!isset($microdatapro_main_flag)){
						if(isset($manufacturer_info) && $manufacturer_info){
							$data['microdatapro_data'] = $manufacturer_info;
							$this->document->setTc_og($this->load->controller('module/microdatapro/tc_og', $data));
							$this->document->setTc_og_prefix($this->load->controller('module/microdatapro/tc_og_prefix'));
							$data['microdatapro'] = $this->load->controller('module/microdatapro/manufacturer', $data);
						}
					}
					//microdatapro 7.0 end - 2 - extra
				
		$data['column_left'] = $this->load->controller('common/column_left');
		$data['column_right'] = $this->load->controller('common/column_right');
		$data['content_top'] = $this->load->controller('common/content_top');
		$data['content_bottom'] = $this->load->controller('common/content_bottom');
		$data['footer'] = $this->load->controller('common/footer');
		$data['header'] = $this->load->controller('common/header');

		if (file_exists(DIR_TEMPLATE . $this->config->get('config_template') . '/template/product/manufacturer_list.tpl')) {

				$this->load->model( 'module/mega_filter' );
				
				$data = $this->model_module_mega_filter->prepareData( $data );
			
			$this->response->setOutput($this->load->view($this->config->get('config_template') . '/template/product/manufacturer_list.tpl', $data));
		} else {

				$this->load->model( 'module/mega_filter' );
				
				$data = $this->model_module_mega_filter->prepareData( $data );
			
			$this->response->setOutput($this->load->view('default/template/product/manufacturer_list.tpl', $data));
		}
	}

	public function info() {
		$this->load->language('product/manufacturer');

		$this->load->model('catalog/manufacturer');

		$this->load->model('catalog/product');

		$this->load->model('tool/image');

		if (isset($this->request->get['manufacturer_id'])) {
			$manufacturer_id = (int)$this->request->get['manufacturer_id'];
		} else {
			$manufacturer_id = 0;
		}

		if (isset($this->request->get['sort'])) {
			$sort = $this->request->get['sort'];
		} else {
			$sort = 'p.sort_order';
		}

		if (isset($this->request->get['order'])) {
			$order = $this->request->get['order'];
		} else {
			$order = 'ASC';
		}

		if (isset($this->request->get['page'])) {
			$page = $this->request->get['page'];
		} else {
			$page = 1;
		}

		if (isset($this->request->get['limit'])) {
			$limit = (int)$this->request->get['limit'];
		} else {
			$limit = (int)$this->config->get('config_product_limit');
		}


					// Top stickers start
						$this->load->model('setting/setting');

						// Statuses
						$data['topstickers_status'] = $this->config->get('topstickers_status');
						$data['topstickers_sold_status'] = $this->config->get('topstickers_sold_status');
						$data['topstickers_sale_status'] = $this->config->get('topstickers_sale_status');
						$data['topstickers_bestseller_status'] = $this->config->get('topstickers_bestseller_status');
						$data['topstickers_novelty_status'] = $this->config->get('topstickers_novelty_status');
						$data['topstickers_last_status'] = $this->config->get('topstickers_last_status');
						$data['topstickers_freeshipping_status'] = $this->config->get('topstickers_freeshipping_status');

						// Text
						$current_language_id = $this->config->get('config_language_id');
						$data['topstickers_sold_text'] = $this->config->get('topstickers_sold_text')[$current_language_id];
						$data['topstickers_sale_text'] = $this->config->get('topstickers_sale_text')[$current_language_id];
						$data['topstickers_bestseller_text'] = $this->config->get('topstickers_bestseller_text')[$current_language_id];
						$data['topstickers_novelty_text'] = $this->config->get('topstickers_novelty_text')[$current_language_id];
						$data['topstickers_last_text'] = $this->config->get('topstickers_last_text')[$current_language_id];
						$data['topstickers_freeshipping_text'] = $this->config->get('topstickers_freeshipping_text')[$current_language_id];

						// Additional data
						$data['topstickers_bestseller_numbers'] = $this->config->get('topstickers_bestseller_numbers');
						$data['topstickers_novelty_days'] = $this->config->get('topstickers_novelty_days');
						$data['topstickers_last_numbers'] = $this->config->get('topstickers_last_numbers');
						$data['topstickers_freeshipping_price'] = $this->config->get('topstickers_freeshipping_price');
					// Top stickers end
				
		$data['breadcrumbs'] = array();

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('text_home'),
			'href' => $this->url->link('common/home')
		);

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('text_brand'),
			'href' => $this->url->link('product/manufacturer')
		);

		$manufacturer_info = $this->model_catalog_manufacturer->getManufacturer($manufacturer_id);

		if ($manufacturer_info) {
			
      if ($this->config->get('mlseo_enabled')) {
        $this->document->setTitle(($manufacturer_info['meta_title'] ? $manufacturer_info['meta_title'] : $manufacturer_info['name']));
        $this->document->setKeywords($manufacturer_info['meta_keyword']);
        $this->document->setDescription($manufacturer_info['meta_description']);
      } else {
        $this->document->setTitle($manufacturer_info['name']);
      }
      

			if (
			isset($this->request->get['page']) ||
			isset($this->request->get['limit']) ||
			isset($this->request->get['order'])
			) {
				$this->document->setRobots('noindex,follow');
			}
			
			

			$url = '';

				if( ! empty( $this->request->get['mfp'] ) ) {
					$url .= '&mfp=' . $this->request->get['mfp'];
				}
			

			if (isset($this->request->get['sort'])) {
				$url .= '&sort=' . $this->request->get['sort'];
			}

			if (isset($this->request->get['order'])) {
				$url .= '&order=' . $this->request->get['order'];
			}

			if (isset($this->request->get['page'])) {
				$url .= '&page=' . $this->request->get['page'];
			}

			if (isset($this->request->get['limit'])) {
				$url .= '&limit=' . $this->request->get['limit'];
			}

			$data['breadcrumbs'][] = array(
				'text' => $manufacturer_info['name'],
				'href' => $this->url->link('product/manufacturer/info', 'manufacturer_id=' . $this->request->get['manufacturer_id'] . $url)
			);

			$data['heading_title'] = !empty($manufacturer_info['seo_h1']) && $this->config->get('mlseo_enabled') ? $manufacturer_info['seo_h1'] : $manufacturer_info['name'];
        
        if (version_compare(VERSION, '2', '>=')) {
          $data['seo_h1'] = !empty($manufacturer_info['seo_h1']) ? $manufacturer_info['seo_h1'] : '';
          $data['seo_h2'] = !empty($manufacturer_info['seo_h2']) ? $manufacturer_info['seo_h2'] : '';
          $data['seo_h3'] = !empty($manufacturer_info['seo_h3']) ? $manufacturer_info['seo_h3'] : '';
          $data['description'] = !empty($manufacturer_info['description']) ? $manufacturer_info['description'] : '';
        } else {
          $this->data['seo_h1'] = !empty($manufacturer_info['seo_h1']) ? $manufacturer_info['seo_h1'] : '';
          $this->data['seo_h2'] = !empty($manufacturer_info['seo_h2']) ? $manufacturer_info['seo_h2'] : '';
          $this->data['seo_h3'] = !empty($manufacturer_info['seo_h3']) ? $manufacturer_info['seo_h3'] : '';
          $this->data['description'] = !empty($manufacturer_info['description']) ? $manufacturer_info['description'] : '';
        }
      
$data['description'] = html_entity_decode($manufacturer_info['descriptionm'], ENT_QUOTES, 'UTF-8');
       
            if ($manufacturer_info['image']) {
                $data['thumb'] = $this->model_tool_image->resize($manufacturer_info['image'], $this->config->get('config_image_category_width'), $this->config->get('config_image_category_height'));
            } else {
                $data['thumb'] = '';
            }
			$data['text_empty'] = $this->language->get('text_empty');
			$data['text_quantity'] = $this->language->get('text_quantity');
			$data['text_manufacturer'] = $this->language->get('text_manufacturer');
			$data['text_model'] = $this->language->get('text_model');
			$data['text_price'] = $this->language->get('text_price');
			$data['text_tax'] = $this->language->get('text_tax');
			$data['text_points'] = $this->language->get('text_points');
			$data['text_compare'] = sprintf($this->language->get('text_compare'), (isset($this->session->data['compare']) ? count($this->session->data['compare']) : 0));
			$data['text_sort'] = $this->language->get('text_sort');
			$data['text_limit'] = $this->language->get('text_limit');

			$data['button_cart'] = $this->language->get('button_cart');
			$data['button_wishlist'] = $this->language->get('button_wishlist');
			$data['button_compare'] = $this->language->get('button_compare');
			$data['button_continue'] = $this->language->get('button_continue');
			$data['button_list'] = $this->language->get('button_list');
			$data['button_grid'] = $this->language->get('button_grid');

			$data['compare'] = $this->url->link('product/compare');

			$data['products'] = array();

			$filter_data = array(
				'filter_manufacturer_id' => $manufacturer_id,
				'sort'                   => $sort,
				'order'                  => $order,
				'start'                  => ($page - 1) * $limit,
				'limit'                  => $limit
			);


				$filter_data['mfp_overwrite_path'] = true;
			
			$product_total = $this->model_catalog_product->getTotalProducts($filter_data);

			$results = $this->model_catalog_product->getProducts($filter_data);

			foreach ($results as $result) {
		
			$results_img = $this->model_catalog_product->getProductImages($result['product_id']);
			$additional_img = array();
			foreach ($results_img as $result_img) {
				if ($result_img['image']) {
					$additional_image = $this->model_tool_image->resize($result_img['image'], $this->config->get('config_image_product_width'), $this->config->get('config_image_product_height'));
				} else {
					$additional_image = false;
				}
				$additional_img[1] = $additional_image;
				break;
			}
            
				if ($result['image']) {
					$image = $this->model_tool_image->resize($result['image'], $this->config->get('config_image_product_width'), $this->config->get('config_image_product_height'));
				} else {
					$image = $this->model_tool_image->resize('placeholder.png', $this->config->get('config_image_product_width'), $this->config->get('config_image_product_height'));
				}

				if (($this->config->get('config_customer_price') && $this->customer->isLogged()) || !$this->config->get('config_customer_price')) {
					$price = $this->currency->format($this->tax->calculate($result['price'], $result['tax_class_id'], $this->config->get('config_tax')));
				} else {
					$price = false;
				}

				if ((float)$result['special']) {
					$special = $this->currency->format($this->tax->calculate($result['special'], $result['tax_class_id'], $this->config->get('config_tax')));
				} else {
					$special = false;
				}

				if ($this->config->get('config_tax')) {
					$tax = $this->currency->format((float)$result['special'] ? $result['special'] : $result['price']);
				} else {
					$tax = false;
				}

				if ($this->config->get('config_review_status')) {
					$rating = (int)$result['rating'];
				} else {
					$rating = false;
				}
$options= array();

                foreach ($this->model_catalog_product->getProductOptions($result['product_id']) as $option) {
                    $product_option_value_data = array();

                    foreach ($option['product_option_value'] as $option_value) {
                        if (!$option_value['subtract'] || ($option_value['quantity'] > 0)) {
                            if ((($this->config->get('config_customer_price') && $this->customer->isLogged()) || !$this->config->get('config_customer_price')) && (float)$option_value['price']) {
                                $oprice = $this->currency->format($this->tax->calculate($option_value['price'], $result['tax_class_id'], $this->config->get('config_tax') ? 'P' : false));
                            } else {
                                $oprice = false;
                            }

                            $product_option_value_data[] = array(
                                'product_option_value_id' => $option_value['product_option_value_id'],
                                'option_value_id'         => $option_value['option_value_id'],
                                'name'                    => $option_value['name'],
                                'image'                   => $this->model_tool_image->resize($option_value['image'], 50, 50),
                                'price'                   => $oprice,
                                'price_prefix'            => $option_value['price_prefix']
                            );
                        }
                    }

                    $options[] = array(
                        'product_option_id'    => $option['product_option_id'],
                        'product_option_value' => $product_option_value_data,
                        'option_id'            => $option['option_id'],
                        'name'                 => $option['name'],
                        'type'                 => $option['type'],
                        'value'                => $option['value'],
                        'required'             => $option['required']
                    );
                }

					// Top stickers start
						// Stickers for current product
						$product_info = $this->model_catalog_product->getProduct($result['product_id']);

							// Sold sticker
							$data['product_sold_sticker'] = false;
							if ($data['topstickers_sold_status'] && $data['topstickers_sold_text'] && $data['topstickers_sold_text'] != '' && $product_info['quantity'] <= 0) {
								$data['product_sold_sticker'] = $data['topstickers_sold_text'];
							}

							// Sale sticker
							$data['product_sale_sticker'] = false;
							if ($data['topstickers_sale_status'] && $data['topstickers_sale_text'] && $data['topstickers_sale_text'] != '' && (float)$product_info['special']) {
								$data['product_sale_sticker'] = $data['topstickers_sale_text'];
							}

							// Bestseller sticker
							$data['product_bestseller_sticker'] = false;
							if ($data['topstickers_bestseller_status'] && $data['topstickers_bestseller_text'] && $data['topstickers_bestseller_text'] != '') {
								$bestsellers = $this->model_catalog_product->getBestSellerProducts( $data['topstickers_bestseller_numbers']);
								foreach ($bestsellers as $bestseller) {
									if ($bestseller['product_id'] == $result['product_id']) {
										$data['product_bestseller_sticker'] = $data['topstickers_bestseller_text'];
									}
								}
							}

							// Novelty sticker
							$data['product_novelty_sticker'] = false;
							if ($data['topstickers_novelty_status'] && $data['topstickers_novelty_text'] && $data['topstickers_novelty_text'] != '' && (strtotime($product_info['date_added']) + intval($data['topstickers_novelty_days']) * 24 * 3600) > time()) {
								$data['product_novelty_sticker'] = $data['topstickers_novelty_text'];
							}

							// Last sticker
							$data['product_last_sticker'] = false;
							if ($data['topstickers_last_status'] && $data['topstickers_last_text'] && $data['topstickers_last_text'] != '' && $product_info['quantity'] <= intval($data['topstickers_last_numbers']) && $product_info['quantity'] > 0) {
								$data['product_last_sticker'] = $data['topstickers_last_text'];
							}

							// Freeshipping sticker
							$data['product_freeshipping_sticker'] = false;
							if ($data['topstickers_freeshipping_status'] && $data['topstickers_freeshipping_text'] && $data['topstickers_freeshipping_text'] != '') {
								if ((float)$product_info['special'] && (float)$product_info['special'] >= $data['topstickers_freeshipping_price']) {
									$data['product_freeshipping_sticker'] = $data['topstickers_freeshipping_text'];
								} elseif ($product_info['price'] >= $data['topstickers_freeshipping_price']) {
									$data['product_freeshipping_sticker'] = $data['topstickers_freeshipping_text'];
								} else {
									$data['product_freeshipping_sticker'] = false;
								}
							}
					// Top stickers end

					// Top stickers custom start
						$current_language_id = $this->config->get('config_language_id');
						$product_custom_topstickers = array();
						foreach ($this->model_catalog_product->getProductTopStickers($result['product_id']) as $topsticker) {
							$product_custom_topsticker_text = json_decode( $topsticker['text'], true);
							$product_custom_topstickers[] = array(
								'topsticker_id'	=> $topsticker['topsticker_id'],
								'name'  		=> $topsticker['name'],
								'text' 			=> $product_custom_topsticker_text[$current_language_id],
								'bg_color'  	=> $topsticker['bg_color'],
								'status'    	=> $topsticker['status']
							);
						}
					// Top stickers custom end
				
				$data['products'][] = array(
'percent'     => sprintf($this->language->get('-%s'), (round((($result['price'] - $result['special'])/$result['price']) * 100 ,0 ))) . ' %',
					'product_id'  => $result['product_id'],
					'options'     => $options,
					'thumb'       => $image,
		
			'additional_img' => $additional_img,
            
					'name'        => $result['name'],
					'manufacturer'=> $result['manufacturer'],
					'description' => utf8_substr(strip_tags(html_entity_decode($result['description'], ENT_QUOTES, 'UTF-8')), 0, $this->config->get('config_product_description_length')) . '..',
					'price'       => $price,
					'special'     => $special,
					'tax'         => $tax,
					'minimum'     => $result['minimum'] > 0 ? $result['minimum'] : 1,
					'rating'      => $result['rating'],

					// Top stickers start
						'product_sold_sticker' => $data['product_sold_sticker'],
						'product_sale_sticker' => $data['product_sale_sticker'],
						'product_bestseller_sticker' => $data['product_bestseller_sticker'],
						'product_novelty_sticker' => $data['product_novelty_sticker'],
						'product_last_sticker' => $data['product_last_sticker'],
						'product_freeshipping_sticker' => $data['product_freeshipping_sticker'],
						'product_custom_topstickers' => $product_custom_topstickers,
					// Top stickers end
				
					'href'        => $this->url->link('product/product', 'manufacturer_id=' . $result['manufacturer_id'] . '&product_id=' . $result['product_id'])
				);
			}

			$url = '';

				if( ! empty( $this->request->get['mfp'] ) ) {
					$url .= '&mfp=' . $this->request->get['mfp'];
				}
			

			if (isset($this->request->get['limit'])) {
				$url .= '&limit=' . $this->request->get['limit'];
			}

			$data['sorts'] = array();
$data['sorts'][] = array(
				'text'  => $this->language->get('Популярные'),
				'value' => 'p.viewed-DESC',
				'href'  => $this->url->link('product/manufacturer/info', '&sort=p.viewed&order=DESC' . $url)
			);
			$data['sorts'][] = array(
				'text'  => $this->language->get('text_default'),
				'value' => 'p.sort_order-ASC',
				'href'  => $this->url->link('product/manufacturer/info', 'manufacturer_id=' . $this->request->get['manufacturer_id'] . '&sort=p.sort_order&order=ASC' . $url)
			);

			$data['sorts'][] = array(
				'text'  => $this->language->get('text_name_asc'),
				'value' => 'pd.name-ASC',
				'href'  => $this->url->link('product/manufacturer/info', 'manufacturer_id=' . $this->request->get['manufacturer_id'] . '&sort=pd.name&order=ASC' . $url)
			);

			$data['sorts'][] = array(
				'text'  => $this->language->get('text_name_desc'),
				'value' => 'pd.name-DESC',
				'href'  => $this->url->link('product/manufacturer/info', 'manufacturer_id=' . $this->request->get['manufacturer_id'] . '&sort=pd.name&order=DESC' . $url)
			);

			$data['sorts'][] = array(
				'text'  => $this->language->get('text_price_asc'),
				'value' => 'p.price-ASC',
				'href'  => $this->url->link('product/manufacturer/info', 'manufacturer_id=' . $this->request->get['manufacturer_id'] . '&sort=p.price&order=ASC' . $url)
			);

			$data['sorts'][] = array(
				'text'  => $this->language->get('text_price_desc'),
				'value' => 'p.price-DESC',
				'href'  => $this->url->link('product/manufacturer/info', 'manufacturer_id=' . $this->request->get['manufacturer_id'] . '&sort=p.price&order=DESC' . $url)
			);

			if ($this->config->get('config_review_status')) {
				$data['sorts'][] = array(
					'text'  => $this->language->get('text_rating_desc'),
					'value' => 'rating-DESC',
					'href'  => $this->url->link('product/manufacturer/info', 'manufacturer_id=' . $this->request->get['manufacturer_id'] . '&sort=rating&order=DESC' . $url)
				);

				$data['sorts'][] = array(
					'text'  => $this->language->get('text_rating_asc'),
					'value' => 'rating-ASC',
					'href'  => $this->url->link('product/manufacturer/info', 'manufacturer_id=' . $this->request->get['manufacturer_id'] . '&sort=rating&order=ASC' . $url)
				);
			}

			$data['sorts'][] = array(
				'text'  => $this->language->get('text_model_asc'),
				'value' => 'p.model-ASC',
				'href'  => $this->url->link('product/manufacturer/info', 'manufacturer_id=' . $this->request->get['manufacturer_id'] . '&sort=p.model&order=ASC' . $url)
			);

			$data['sorts'][] = array(
				'text'  => $this->language->get('text_model_desc'),
				'value' => 'p.model-DESC',
				'href'  => $this->url->link('product/manufacturer/info', 'manufacturer_id=' . $this->request->get['manufacturer_id'] . '&sort=p.model&order=DESC' . $url)
			);

			$url = '';

				if( ! empty( $this->request->get['mfp'] ) ) {
					$url .= '&mfp=' . $this->request->get['mfp'];
				}
			

			if (isset($this->request->get['sort'])) {
				$url .= '&sort=' . $this->request->get['sort'];
			}

			if (isset($this->request->get['order'])) {
				$url .= '&order=' . $this->request->get['order'];
			}

			$data['limits'] = array();

			$limits = array_unique(array($this->config->get('config_product_limit'), 25, 50, 75, 100));

			sort($limits);

			foreach($limits as $value) {
				$data['limits'][] = array(
					'text'  => $value,
					'value' => $value,
					'href'  => $this->url->link('product/manufacturer/info', 'manufacturer_id=' . $this->request->get['manufacturer_id'] . $url . '&limit=' . $value)
				);
			}

			$url = '';

				if( ! empty( $this->request->get['mfp'] ) ) {
					$url .= '&mfp=' . $this->request->get['mfp'];
				}
			

			if (isset($this->request->get['sort'])) {
				$url .= '&sort=' . $this->request->get['sort'];
			}

			if (isset($this->request->get['order'])) {
				$url .= '&order=' . $this->request->get['order'];
			}

			if (isset($this->request->get['limit'])) {
				$url .= '&limit=' . $this->request->get['limit'];
			}


					//microdatapro 7.0 start - 1 - main
					$data['microdatapro_data'] = $manufacturer_info;
					$this->document->setTc_og($this->load->controller('module/microdatapro/tc_og', $data));
					$this->document->setTc_og_prefix($this->load->controller('module/microdatapro/tc_og_prefix'));
					$data['microdatapro'] = $this->load->controller('module/microdatapro/manufacturer', $data);
					$microdatapro_main_flag = 1;
					//microdatapro 7.0 end - 1 - main
				
			$pagination = new Pagination();
			$pagination->total = $product_total;
			$pagination->page = $page;
			$pagination->limit = $limit;
			$pagination->url = $this->url->link('product/manufacturer/info', 'manufacturer_id=' . $this->request->get['manufacturer_id'] .  $url . '&page={page}');

			$data['pagination'] = $pagination->render();

			$data['results'] = sprintf($this->language->get('text_pagination'), ($product_total) ? (($page - 1) * $limit) + 1 : 0, ((($page - 1) * $limit) > ($product_total - $limit)) ? $product_total : ((($page - 1) * $limit) + $limit), $product_total, ceil($product_total / $limit));

			// http://googlewebmastercentral.blogspot.com/2011/09/pagination-with-relnext-and-relprev.html

      if ($page > 1 AND $this->config->get('mlseo_pagination_canonical')) {
         $this->document->addLink($this->url->link('product/manufacturer/info', 'manufacturer_id=' . $this->request->get['manufacturer_id']), 'canonical');
      }
      
			if ($page == 1) {
			    $this->document->addLink($this->url->link('product/manufacturer/info', 'manufacturer_id=' . $this->request->get['manufacturer_id'], 'SSL'), 'canonical');
			} elseif ($page == 2) {
			    $this->document->addLink($this->url->link('product/manufacturer/info', 'manufacturer_id=' . $this->request->get['manufacturer_id'], 'SSL'), 'prev');
			} else {
			    $this->document->addLink($this->url->link('product/manufacturer/info', 'manufacturer_id=' . $this->request->get['manufacturer_id'] . $url . '&page='. ($page - 1), 'SSL'), 'prev');
			}

			if ($limit && ceil($product_total / $limit) > $page) {
			    $this->document->addLink($this->url->link('product/manufacturer/info', 'manufacturer_id=' . $this->request->get['manufacturer_id'] . $url . '&page='. ($page + 1), 'SSL'), 'next');
			}

			$data['sort'] = $sort;
			$data['order'] = $order;
			$data['limit'] = $limit;

			$data['continue'] = $this->url->link('common/home');

      if ($this->config->get('mlseo_enabled')) {
        $this->load->model('tool/seo_package');
        
        if ($this->config->get('mlseo_microdata')) {
          if (version_compare(VERSION, '2', '>=')) {
            $this->document->addSeoMeta($this->model_tool_seo_package->rich_snippet('microdata', 'manufacturer', $data));
          } else {
            $this->document->addSeoMeta($this->model_tool_seo_package->rich_snippet('microdata', 'manufacturer', $this->data));
          }
        }
      }
      


					//microdatapro 7.0 start - 2 - extra
					if(!isset($microdatapro_main_flag)){
						if(isset($manufacturer_info) && $manufacturer_info){
							$data['microdatapro_data'] = $manufacturer_info;
							$this->document->setTc_og($this->load->controller('module/microdatapro/tc_og', $data));
							$this->document->setTc_og_prefix($this->load->controller('module/microdatapro/tc_og_prefix'));
							$data['microdatapro'] = $this->load->controller('module/microdatapro/manufacturer', $data);
						}
					}
					//microdatapro 7.0 end - 2 - extra
				
			$data['column_left'] = $this->load->controller('common/column_left');
			$data['column_right'] = $this->load->controller('common/column_right');
			$data['content_top'] = $this->load->controller('common/content_top');
			$data['content_bottom'] = $this->load->controller('common/content_bottom');
			$data['footer'] = $this->load->controller('common/footer');
			$data['header'] = $this->load->controller('common/header');

			if (file_exists(DIR_TEMPLATE . $this->config->get('config_template') . '/template/product/manufacturer_info.tpl')) {

				$this->load->model( 'module/mega_filter' );
				
				$data = $this->model_module_mega_filter->prepareData( $data );
			
				
            $template = $this->config->get('config_template') . '/template/product/manufacturer_info.tpl';

            // Custom template module
            $this->load->model('setting/setting');

            $customer_group_id = $this->customer->getGroupId();

            $custom_template_module = $this->model_setting_setting->getSetting('custom_template_module');
            if(!empty($custom_template_module['custom_template_module'])){
                foreach ($custom_template_module['custom_template_module'] as $key => $module) {
                    if (($module['type'] == 3) && !empty($module['manufacturers'])) {
                        if ((isset($module['customer_groups']) && in_array($customer_group_id, $module['customer_groups'])) || !isset($module['customer_groups']) || empty($module['customer_groups'])){

                            if (in_array($manufacturer_id, $module['manufacturers'])) {
                                if (file_exists(DIR_TEMPLATE . $this->config->get('config_template') . '/template/' . $module['template_name'] . '.tpl')) {
                                    $template = $this->config->get('config_template') . '/template/' . $module['template_name'] . '.tpl';
                                }
                            }

                        } // customer groups

                    }
                }
            }
            $this->response->setOutput($this->load->view($template, $data));
            // Custom template module
            
			} else {

				$this->load->model( 'module/mega_filter' );
				
				$data = $this->model_module_mega_filter->prepareData( $data );
			
				$this->response->setOutput($this->load->view('default/template/product/manufacturer_info.tpl', $data));
			}
		} else {
			$url = '';

				if( ! empty( $this->request->get['mfp'] ) ) {
					$url .= '&mfp=' . $this->request->get['mfp'];
				}
			

			if (isset($this->request->get['manufacturer_id'])) {
				$url .= '&manufacturer_id=' . $this->request->get['manufacturer_id'];
			}

			if (isset($this->request->get['sort'])) {
				$url .= '&sort=' . $this->request->get['sort'];
			}

			if (isset($this->request->get['order'])) {
				$url .= '&order=' . $this->request->get['order'];
			}

			if (isset($this->request->get['page'])) {
				$url .= '&page=' . $this->request->get['page'];
			}

			if (isset($this->request->get['limit'])) {
				$url .= '&limit=' . $this->request->get['limit'];
			}

			$data['breadcrumbs'][] = array(
				'text' => $this->language->get('text_error'),
				'href' => $this->url->link('product/manufacturer/info', $url)
			);

			$this->document->setTitle($this->language->get('text_error'));

			if (
			isset($this->request->get['page']) ||
			isset($this->request->get['limit']) ||
			isset($this->request->get['order'])
			) {
				$this->document->setRobots('noindex,follow');
			}
			
			

			$data['heading_title'] = $this->language->get('text_error');

			$data['text_error'] = $this->language->get('text_error');

			$data['button_continue'] = $this->language->get('button_continue');

			$data['continue'] = $this->url->link('common/home');

      if ($this->config->get('mlseo_enabled')) {
        $this->load->model('tool/seo_package');
        
        if ($this->config->get('mlseo_microdata')) {
          if (version_compare(VERSION, '2', '>=')) {
            $this->document->addSeoMeta($this->model_tool_seo_package->rich_snippet('microdata', 'manufacturer', $data));
          } else {
            $this->document->addSeoMeta($this->model_tool_seo_package->rich_snippet('microdata', 'manufacturer', $this->data));
          }
        }
      }
      

			$this->response->addHeader($this->request->server['SERVER_PROTOCOL'] . ' 404 Not Found');

			$data['header'] = $this->load->controller('common/header');
			$data['footer'] = $this->load->controller('common/footer');

					//microdatapro 7.0 start - 2 - extra
					if(!isset($microdatapro_main_flag)){
						if(isset($manufacturer_info) && $manufacturer_info){
							$data['microdatapro_data'] = $manufacturer_info;
							$this->document->setTc_og($this->load->controller('module/microdatapro/tc_og', $data));
							$this->document->setTc_og_prefix($this->load->controller('module/microdatapro/tc_og_prefix'));
							$data['microdatapro'] = $this->load->controller('module/microdatapro/manufacturer', $data);
						}
					}
					//microdatapro 7.0 end - 2 - extra
				
			$data['column_left'] = $this->load->controller('common/column_left');
			$data['column_right'] = $this->load->controller('common/column_right');
			$data['content_top'] = $this->load->controller('common/content_top');
			$data['content_bottom'] = $this->load->controller('common/content_bottom');

			if (file_exists(DIR_TEMPLATE . $this->config->get('config_template') . '/template/error/not_found.tpl')) {

				$this->load->model( 'module/mega_filter' );
				
				$data = $this->model_module_mega_filter->prepareData( $data );
			
				$this->response->setOutput($this->load->view($this->config->get('config_template') . '/template/error/not_found.tpl', $data));
			} else {

				$this->load->model( 'module/mega_filter' );
				
				$data = $this->model_module_mega_filter->prepareData( $data );
			
				$this->response->setOutput($this->load->view('default/template/error/not_found.tpl', $data));
			}
		}
	}
}
