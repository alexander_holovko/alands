<?php
class ControllerProductSearch extends Controller {
	public function index() {

        if (version_compare(VERSION, '2', '>=')) {
          $data['csp_search_url'] = $this->config->get('mlseo_fix_search') ? $this->url->link('product/search') : 'index.php?route=product/search';
        } else {
          $this->data['csp_search_url'] = $this->config->get('mlseo_fix_search') ? $this->url->link('product/search') : 'index.php?route=product/search';
        }
      
		$this->load->language('product/search');

		$this->load->model('catalog/category');

		$this->load->model('catalog/product');

		$this->load->model('tool/image');

		if (isset($this->request->get['search'])) {
			$search = $this->request->get['search'];
		} else {
			$search = '';
		}

		if (isset($this->request->get['tag'])) {
			$tag = $this->request->get['tag'];
		} elseif (isset($this->request->get['search'])) {
			$tag = $this->request->get['search'];
		} else {
			$tag = '';
		}

		if (isset($this->request->get['description'])) {
			$description = $this->request->get['description'];
		} else {
			$description = '';
		}

		if (isset($this->request->get['category_id'])) {
			$category_id = $this->request->get['category_id'];
		} else {
			$category_id = 0;
		}

		if (isset($this->request->get['sub_category'])) {
			$sub_category = $this->request->get['sub_category'];
		} else {
			$sub_category = '';
		}

		if (isset($this->request->get['sort'])) {
			$sort = $this->request->get['sort'];
		} else {
			$sort = 'p.sort_order';
		}

		if (isset($this->request->get['order'])) {
			$order = $this->request->get['order'];
		} else {
			$order = 'ASC';
		}

		if (isset($this->request->get['page'])) {
			$page = $this->request->get['page'];
		} else {
			$page = 1;
		}

		if (isset($this->request->get['limit'])) {
			$limit = (int)$this->request->get['limit'];
		} else {
			$limit = $this->config->get('config_product_limit');
		}

		if (isset($this->request->get['search'])) {
			$this->document->setTitle($this->language->get('heading_title') .  ' - ' . $this->request->get['search']);

				$this->document->setRobots('noindex,follow');
			
		} elseif (isset($this->request->get['tag'])) {
			$this->document->setTitle($this->language->get('heading_title') .  ' - ' . $this->language->get('heading_tag') . $this->request->get['tag']);

				$this->document->setRobots('noindex,follow');
			
		} else {
			$this->document->setTitle($this->language->get('heading_title'));

				$this->document->setRobots('noindex,follow');
			
		}


					// Top stickers start
						$this->load->model('setting/setting');

						// Statuses
						$data['topstickers_status'] = $this->config->get('topstickers_status');
						$data['topstickers_sold_status'] = $this->config->get('topstickers_sold_status');
						$data['topstickers_sale_status'] = $this->config->get('topstickers_sale_status');
						$data['topstickers_bestseller_status'] = $this->config->get('topstickers_bestseller_status');
						$data['topstickers_novelty_status'] = $this->config->get('topstickers_novelty_status');
						$data['topstickers_last_status'] = $this->config->get('topstickers_last_status');
						$data['topstickers_freeshipping_status'] = $this->config->get('topstickers_freeshipping_status');

						// Text
						$current_language_id = $this->config->get('config_language_id');
						$data['topstickers_sold_text'] = $this->config->get('topstickers_sold_text')[$current_language_id];
						$data['topstickers_sale_text'] = $this->config->get('topstickers_sale_text')[$current_language_id];
						$data['topstickers_bestseller_text'] = $this->config->get('topstickers_bestseller_text')[$current_language_id];
						$data['topstickers_novelty_text'] = $this->config->get('topstickers_novelty_text')[$current_language_id];
						$data['topstickers_last_text'] = $this->config->get('topstickers_last_text')[$current_language_id];
						$data['topstickers_freeshipping_text'] = $this->config->get('topstickers_freeshipping_text')[$current_language_id];

						// Additional data
						$data['topstickers_bestseller_numbers'] = $this->config->get('topstickers_bestseller_numbers');
						$data['topstickers_novelty_days'] = $this->config->get('topstickers_novelty_days');
						$data['topstickers_last_numbers'] = $this->config->get('topstickers_last_numbers');
						$data['topstickers_freeshipping_price'] = $this->config->get('topstickers_freeshipping_price');
					// Top stickers end
				
		$data['breadcrumbs'] = array();

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('text_home'),
			'href' => $this->url->link('common/home')
		);

		$url = '';

				if( ! empty( $this->request->get['mfp'] ) ) {
					$url .= '&mfp=' . $this->request->get['mfp'];
				}
			

		if (isset($this->request->get['search'])) {
			$url .= '&search=' . urlencode(html_entity_decode($this->request->get['search'], ENT_QUOTES, 'UTF-8'));
		}

		if (isset($this->request->get['tag'])) {
			$url .= '&tag=' . urlencode(html_entity_decode($this->request->get['tag'], ENT_QUOTES, 'UTF-8'));
		}

		if (isset($this->request->get['description'])) {
			$url .= '&description=' . $this->request->get['description'];
		}

		if (isset($this->request->get['category_id'])) {
			$url .= '&category_id=' . $this->request->get['category_id'];
		}

		if (isset($this->request->get['sub_category'])) {
			$url .= '&sub_category=' . $this->request->get['sub_category'];
		}

		if (isset($this->request->get['sort'])) {
			$url .= '&sort=' . $this->request->get['sort'];
		}

		if (isset($this->request->get['order'])) {
			$url .= '&order=' . $this->request->get['order'];
		}

		if (isset($this->request->get['page'])) {
			$url .= '&page=' . $this->request->get['page'];
		}

		if (isset($this->request->get['limit'])) {
			$url .= '&limit=' . $this->request->get['limit'];
		}

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('heading_title'),
			'href' => $this->url->link('product/search', $url)
		);

		if (isset($this->request->get['search'])) {
			$data['heading_title'] = $this->language->get('heading_title') .  ' - ' . $this->request->get['search'];
		} else {
			$data['heading_title'] = $this->language->get('heading_title');
		}

		$data['text_empty'] = $this->language->get('text_empty');
		$data['text_search'] = $this->language->get('text_search');
		$data['text_keyword'] = $this->language->get('text_keyword');
		$data['text_category'] = $this->language->get('text_category');
		$data['text_sub_category'] = $this->language->get('text_sub_category');
		$data['text_quantity'] = $this->language->get('text_quantity');
		$data['text_manufacturer'] = $this->language->get('text_manufacturer');
		$data['text_model'] = $this->language->get('text_model');
		$data['text_price'] = $this->language->get('text_price');
		$data['text_tax'] = $this->language->get('text_tax');
		$data['text_points'] = $this->language->get('text_points');
		$data['text_compare'] = sprintf($this->language->get('text_compare'), (isset($this->session->data['compare']) ? count($this->session->data['compare']) : 0));
		$data['text_sort'] = $this->language->get('text_sort');
		$data['text_limit'] = $this->language->get('text_limit');

		$data['entry_search'] = $this->language->get('entry_search');
		$data['entry_description'] = $this->language->get('entry_description');

		$data['button_search'] = $this->language->get('button_search');
		$data['button_cart'] = $this->language->get('button_cart');
		$data['button_wishlist'] = $this->language->get('button_wishlist');
		$data['button_compare'] = $this->language->get('button_compare');
		$data['button_list'] = $this->language->get('button_list');
		$data['button_grid'] = $this->language->get('button_grid');

		$data['compare'] = $this->url->link('product/compare');

		$this->load->model('catalog/category');

		// 3 Level Category Search
		$data['categories'] = array();

		$categories_1 = $this->model_catalog_category->getCategories(0);

		foreach ($categories_1 as $category_1) {
			$level_2_data = array();

			$categories_2 = $this->model_catalog_category->getCategories($category_1['category_id']);

			foreach ($categories_2 as $category_2) {
				$level_3_data = array();

				$categories_3 = $this->model_catalog_category->getCategories($category_2['category_id']);

				foreach ($categories_3 as $category_3) {
					$level_3_data[] = array(
						'category_id' => $category_3['category_id'],
						'name'        => $category_3['name'],
					);
				}

				$level_2_data[] = array(
					'category_id' => $category_2['category_id'],
					'name'        => $category_2['name'],
					'children'    => $level_3_data
				);
			}

			$data['categories'][] = array(
				'category_id' => $category_1['category_id'],
				'name'        => $category_1['name'],
				'children'    => $level_2_data
			);
		}

		$data['products'] = array();

		if (isset($this->request->get['search']) || isset($this->request->get['tag'])) {
			$filter_data = array(
				'filter_name'         => $search,
				'filter_tag'          => $tag,
				'filter_description'  => $description,
				'filter_category_id'  => $category_id,
				'filter_sub_category' => $sub_category,
				'sort'                => $sort,
				'order'               => $order,
				'start'               => ($page - 1) * $limit,
				'limit'               => $limit
			);


				$fmSettings = $this->config->get('mega_filter_settings');
		
				if( ! empty( $fmSettings['show_products_from_subcategories'] ) ) {
					if( ! empty( $fmSettings['level_products_from_subcategories'] ) ) {
						$fmLevel = (int) $fmSettings['level_products_from_subcategories'];
						$fmPath = explode( '_', empty( $this->request->get['path'] ) ? '' : $this->request->get['path'] );

						if( $fmPath && count( $fmPath ) >= $fmLevel ) {
							$filter_data['filter_sub_category'] = '1';
						}
					} else {
						$filter_data['filter_sub_category'] = '1';
					}
				}
				
				if( ! empty( $this->request->get['manufacturer_id'] ) ) {
					$filter_data['filter_manufacturer_id'] = (int) $this->request->get['manufacturer_id'];
				}
			

				$filter_data['mfp_overwrite_path'] = true;
			
			$product_total = $this->model_catalog_product->getTotalProducts($filter_data);

			$results = $this->model_catalog_product->getProducts($filter_data);

			foreach ($results as $result) {
		
			$results_img = $this->model_catalog_product->getProductImages($result['product_id']);
			$additional_img = array();
			foreach ($results_img as $result_img) {
				if ($result_img['image']) {
					$additional_image = $this->model_tool_image->resize($result_img['image'], $this->config->get('config_image_product_width'), $this->config->get('config_image_product_height'));
				} else {
					$additional_image = false;
				}
				$additional_img[1] = $additional_image;
				break;
			}
            
				if ($result['image']) {
					$image = $this->model_tool_image->resize($result['image'], $this->config->get('config_image_product_width'), $this->config->get('config_image_product_height'));
				} else {
					$image = $this->model_tool_image->resize('placeholder.png', $this->config->get('config_image_product_width'), $this->config->get('config_image_product_height'));
				}

				if (($this->config->get('config_customer_price') && $this->customer->isLogged()) || !$this->config->get('config_customer_price')) {
					$price = $this->currency->format($this->tax->calculate($result['price'], $result['tax_class_id'], $this->config->get('config_tax')));
				} else {
					$price = false;
				}

				if ((float)$result['special']) {
					$special = $this->currency->format($this->tax->calculate($result['special'], $result['tax_class_id'], $this->config->get('config_tax')));
				} else {
					$special = false;
				}

				if ($this->config->get('config_tax')) {
					$tax = $this->currency->format((float)$result['special'] ? $result['special'] : $result['price']);
				} else {
					$tax = false;
				}

				if ($this->config->get('config_review_status')) {
					$rating = (int)$result['rating'];
				} else {
					$rating = false;
				}


					// Top stickers start
						// Stickers for current product
						$product_info = $this->model_catalog_product->getProduct($result['product_id']);

							// Sold sticker
							$data['product_sold_sticker'] = false;
							if ($data['topstickers_sold_status'] && $data['topstickers_sold_text'] && $data['topstickers_sold_text'] != '' && $product_info['quantity'] <= 0) {
								$data['product_sold_sticker'] = $data['topstickers_sold_text'];
							}

							// Sale sticker
							$data['product_sale_sticker'] = false;
							if ($data['topstickers_sale_status'] && $data['topstickers_sale_text'] && $data['topstickers_sale_text'] != '' && (float)$product_info['special']) {
								$data['product_sale_sticker'] = $data['topstickers_sale_text'];
							}

							// Bestseller sticker
							$data['product_bestseller_sticker'] = false;
							if ($data['topstickers_bestseller_status'] && $data['topstickers_bestseller_text'] && $data['topstickers_bestseller_text'] != '') {
								$bestsellers = $this->model_catalog_product->getBestSellerProducts( $data['topstickers_bestseller_numbers']);
								foreach ($bestsellers as $bestseller) {
									if ($bestseller['product_id'] == $result['product_id']) {
										$data['product_bestseller_sticker'] = $data['topstickers_bestseller_text'];
									}
								}
							}

							// Novelty sticker
							$data['product_novelty_sticker'] = false;
							if ($data['topstickers_novelty_status'] && $data['topstickers_novelty_text'] && $data['topstickers_novelty_text'] != '' && (strtotime($product_info['date_added']) + intval($data['topstickers_novelty_days']) * 24 * 3600) > time()) {
								$data['product_novelty_sticker'] = $data['topstickers_novelty_text'];
							}

							// Last sticker
							$data['product_last_sticker'] = false;
							if ($data['topstickers_last_status'] && $data['topstickers_last_text'] && $data['topstickers_last_text'] != '' && $product_info['quantity'] <= intval($data['topstickers_last_numbers']) && $product_info['quantity'] > 0) {
								$data['product_last_sticker'] = $data['topstickers_last_text'];
							}

							// Freeshipping sticker
							$data['product_freeshipping_sticker'] = false;
							if ($data['topstickers_freeshipping_status'] && $data['topstickers_freeshipping_text'] && $data['topstickers_freeshipping_text'] != '') {
								if ((float)$product_info['special'] && (float)$product_info['special'] >= $data['topstickers_freeshipping_price']) {
									$data['product_freeshipping_sticker'] = $data['topstickers_freeshipping_text'];
								} elseif ($product_info['price'] >= $data['topstickers_freeshipping_price']) {
									$data['product_freeshipping_sticker'] = $data['topstickers_freeshipping_text'];
								} else {
									$data['product_freeshipping_sticker'] = false;
								}
							}
					// Top stickers end

					// Top stickers custom start
						$current_language_id = $this->config->get('config_language_id');
						$product_custom_topstickers = array();
						foreach ($this->model_catalog_product->getProductTopStickers($result['product_id']) as $topsticker) {
							$product_custom_topsticker_text = json_decode( $topsticker['text'], true);
							$product_custom_topstickers[] = array(
								'topsticker_id'	=> $topsticker['topsticker_id'],
								'name'  		=> $topsticker['name'],
								'text' 			=> $product_custom_topsticker_text[$current_language_id],
								'bg_color'  	=> $topsticker['bg_color'],
								'status'    	=> $topsticker['status']
							);
						}
					// Top stickers custom end
				
				$data['products'][] = array(
'percent'     => sprintf($this->language->get('-%s'), (round((($result['price'] - $result['special'])/$result['price']) * 100 ,0 ))) . ' %',
					'product_id'  => $result['product_id'],
					'thumb'       => $image,
		
			'additional_img' => $additional_img,
            
					'name'        => $result['name'],
					'description' => utf8_substr(strip_tags(html_entity_decode($result['description'], ENT_QUOTES, 'UTF-8')), 0, $this->config->get('config_product_description_length')) . '..',
					'price'       => $price,
					'special'     => $special,
					'tax'         => $tax,
					'minimum'     => $result['minimum'] > 0 ? $result['minimum'] : 1,
					'rating'      => $result['rating'],

					// Top stickers start
						'product_sold_sticker' => $data['product_sold_sticker'],
						'product_sale_sticker' => $data['product_sale_sticker'],
						'product_bestseller_sticker' => $data['product_bestseller_sticker'],
						'product_novelty_sticker' => $data['product_novelty_sticker'],
						'product_last_sticker' => $data['product_last_sticker'],
						'product_freeshipping_sticker' => $data['product_freeshipping_sticker'],
						'product_custom_topstickers' => $product_custom_topstickers,
					// Top stickers end
				
					'href'        => $this->config->get('mlseo_fpp_remove_search') ? $this->url->link('product/product', 'product_id=' . $result['product_id']) : $this->url->link('product/product', 'product_id=' . $result['product_id'] . $url)
				);
			}

			$url = '';

				if( ! empty( $this->request->get['mfp'] ) ) {
					$url .= '&mfp=' . $this->request->get['mfp'];
				}
			

			if (isset($this->request->get['search'])) {
				$url .= '&search=' . urlencode(html_entity_decode($this->request->get['search'], ENT_QUOTES, 'UTF-8'));
			}

			if (isset($this->request->get['tag'])) {
				$url .= '&tag=' . urlencode(html_entity_decode($this->request->get['tag'], ENT_QUOTES, 'UTF-8'));
			}

			if (isset($this->request->get['description'])) {
				$url .= '&description=' . $this->request->get['description'];
			}

			if (isset($this->request->get['category_id'])) {
				$url .= '&category_id=' . $this->request->get['category_id'];
			}

			if (isset($this->request->get['sub_category'])) {
				$url .= '&sub_category=' . $this->request->get['sub_category'];
			}

			if (isset($this->request->get['limit'])) {
				$url .= '&limit=' . $this->request->get['limit'];
			}

			$data['sorts'] = array();
$data['sorts'][] = array(
				'text'  => $this->language->get('Популярные'),
				'value' => 'p.viewed-DESC',
				'href'  => $this->url->link('product/search', 'path=' . $this->request->get['path'] . '&sort=p.viewed&order=DESC' . $url)
			);
			$data['sorts'][] = array(
				'text'  => $this->language->get('text_default'),
				'value' => 'p.sort_order-ASC',
				'href'  => $this->url->link('product/search', 'sort=p.sort_order&order=ASC' . $url)
			);

			$data['sorts'][] = array(
				'text'  => $this->language->get('text_name_asc'),
				'value' => 'pd.name-ASC',
				'href'  => $this->url->link('product/search', 'sort=pd.name&order=ASC' . $url)
			);

			$data['sorts'][] = array(
				'text'  => $this->language->get('text_name_desc'),
				'value' => 'pd.name-DESC',
				'href'  => $this->url->link('product/search', 'sort=pd.name&order=DESC' . $url)
			);

			$data['sorts'][] = array(
				'text'  => $this->language->get('text_price_asc'),
				'value' => 'p.price-ASC',
				'href'  => $this->url->link('product/search', 'sort=p.price&order=ASC' . $url)
			);

			$data['sorts'][] = array(
				'text'  => $this->language->get('text_price_desc'),
				'value' => 'p.price-DESC',
				'href'  => $this->url->link('product/search', 'sort=p.price&order=DESC' . $url)
			);

			if ($this->config->get('config_review_status')) {
				$data['sorts'][] = array(
					'text'  => $this->language->get('text_rating_desc'),
					'value' => 'rating-DESC',
					'href'  => $this->url->link('product/search', 'sort=rating&order=DESC' . $url)
				);

				$data['sorts'][] = array(
					'text'  => $this->language->get('text_rating_asc'),
					'value' => 'rating-ASC',
					'href'  => $this->url->link('product/search', 'sort=rating&order=ASC' . $url)
				);
			}

			$data['sorts'][] = array(
				'text'  => $this->language->get('text_percent_asc'),
				'value' => 'special_percent-DESC',
				'href'  => $this->url->link('product/search', 'sort=special_percent&order=ASC' . $url)
			);

			$data['sorts'][] = array(
				'text'  => $this->language->get('text_percent_desc'),
				'value' => 'special_percent-DESC',
				'href'  => $this->url->link('product/search', 'sort=special_percent&order=DESC' . $url)
			);
 

			$url = '';

				if( ! empty( $this->request->get['mfp'] ) ) {
					$url .= '&mfp=' . $this->request->get['mfp'];
				}
			

			if (isset($this->request->get['search'])) {
				$url .= '&search=' . urlencode(html_entity_decode($this->request->get['search'], ENT_QUOTES, 'UTF-8'));
			}

			if (isset($this->request->get['tag'])) {
				$url .= '&tag=' . urlencode(html_entity_decode($this->request->get['tag'], ENT_QUOTES, 'UTF-8'));
			}

			if (isset($this->request->get['description'])) {
				$url .= '&description=' . $this->request->get['description'];
			}

			if (isset($this->request->get['category_id'])) {
				$url .= '&category_id=' . $this->request->get['category_id'];
			}

			if (isset($this->request->get['sub_category'])) {
				$url .= '&sub_category=' . $this->request->get['sub_category'];
			}

			if (isset($this->request->get['sort'])) {
				$url .= '&sort=' . $this->request->get['sort'];
			}

			if (isset($this->request->get['order'])) {
				$url .= '&order=' . $this->request->get['order'];
			}

			$data['limits'] = array();

			$limits = array_unique(array($this->config->get('config_product_limit'), 25, 50, 75, 100));

			sort($limits);

			foreach($limits as $value) {
				$data['limits'][] = array(
					'text'  => $value,
					'value' => $value,
					'href'  => $this->url->link('product/search', $url . '&limit=' . $value)
				);
			}

			$url = '';

				if( ! empty( $this->request->get['mfp'] ) ) {
					$url .= '&mfp=' . $this->request->get['mfp'];
				}
			

			if (isset($this->request->get['search'])) {
				$url .= '&search=' . urlencode(html_entity_decode($this->request->get['search'], ENT_QUOTES, 'UTF-8'));
			}

			if (isset($this->request->get['tag'])) {
				$url .= '&tag=' . urlencode(html_entity_decode($this->request->get['tag'], ENT_QUOTES, 'UTF-8'));
			}

			if (isset($this->request->get['description'])) {
				$url .= '&description=' . $this->request->get['description'];
			}

			if (isset($this->request->get['category_id'])) {
				$url .= '&category_id=' . $this->request->get['category_id'];
			}

			if (isset($this->request->get['sub_category'])) {
				$url .= '&sub_category=' . $this->request->get['sub_category'];
			}

			if (isset($this->request->get['sort'])) {
				$url .= '&sort=' . $this->request->get['sort'];
			}

			if (isset($this->request->get['order'])) {
				$url .= '&order=' . $this->request->get['order'];
			}

			if (isset($this->request->get['limit'])) {
				$url .= '&limit=' . $this->request->get['limit'];
			}

			$pagination = new Pagination();
			$pagination->total = $product_total;
			$pagination->page = $page;
			$pagination->limit = $limit;
			$pagination->url = $this->url->link('product/search', $url . '&page={page}');

			$data['pagination'] = $pagination->render();

			$data['results'] = sprintf($this->language->get('text_pagination'), ($product_total) ? (($page - 1) * $limit) + 1 : 0, ((($page - 1) * $limit) > ($product_total - $limit)) ? $product_total : ((($page - 1) * $limit) + $limit), $product_total, ceil($product_total / $limit));

			// http://googlewebmastercentral.blogspot.com/2011/09/pagination-with-relnext-and-relprev.html
			if ($page == 1) {
			    $this->document->addLink($this->url->link('product/search', (isset($this->request->get['tag']) ? 'tag='.$this->request->get['tag'] : ''), 'SSL'), 'canonical');
			} elseif ($page == 2) {
			    $this->document->addLink($this->url->link('product/search', (isset($this->request->get['tag']) ? 'tag='.$this->request->get['tag'] : ''), 'SSL'), 'prev');
			} else {
			    $this->document->addLink($this->url->link('product/search', $url . '&page='. ($page - 1), 'SSL'), 'prev');
			}

			if ($limit && ceil($product_total / $limit) > $page) {
			    $this->document->addLink($this->url->link('product/search', $url . '&page='. ($page + 1), 'SSL'), 'next');
			}
		}

		$data['search'] = $search;
		$data['description'] = $description;
		$data['category_id'] = $category_id;
		$data['sub_category'] = $sub_category;

		$data['sort'] = $sort;
		$data['order'] = $order;
		$data['limit'] = $limit;

		$data['column_left'] = $this->load->controller('common/column_left');
		$data['column_right'] = $this->load->controller('common/column_right');
		$data['content_top'] = $this->load->controller('common/content_top');
		$data['content_bottom'] = $this->load->controller('common/content_bottom');
		$data['footer'] = $this->load->controller('common/footer');
		$data['header'] = $this->load->controller('common/header');

		if (file_exists(DIR_TEMPLATE . $this->config->get('config_template') . '/template/product/search.tpl')) {

				$this->load->model( 'module/mega_filter' );
				
				$data = $this->model_module_mega_filter->prepareData( $data );
			
			$this->response->setOutput($this->load->view($this->config->get('config_template') . '/template/product/search.tpl', $data));
		} else {

				$this->load->model( 'module/mega_filter' );
				
				$data = $this->model_module_mega_filter->prepareData( $data );
			
			$this->response->setOutput($this->load->view('default/template/product/search.tpl', $data));
		}
	}
}
