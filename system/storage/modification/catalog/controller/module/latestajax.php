<?php
class ControllerModuleLatestAjax extends Controller {
	public function index($setting) {
		$this->load->language('module/latestajax');

		// $data['heading_title'] = $this->language->get('heading_title');
		$data['heading_title'] = $setting['name'];

		$data['text_tax'] = $this->language->get('text_tax');
		$data['more_text'] = $this->language->get('more_text');

		$data['button_cart'] = $this->language->get('button_cart');
		$data['button_wishlist'] = $this->language->get('button_wishlist');
		$data['button_compare'] = $this->language->get('button_compare');

		$this->load->model('catalog/product');

		$this->load->model('tool/image');

		$data['products'] = array();
		
		$full_limit = $setting['limit']*5;
		
		$filter_data = array(
			'sort'  => 'p.date_added',
			'order' => 'DESC',
			'start' => 0,
			'limit' => $full_limit
		);
		
		if (isset($this->request->get['latest_page'])) {
			$latest_page = $this->request->get['latest_page'];
		} else {
			$latest_page = 1;
		}
		
		$full_results = $this->model_catalog_product->getProducts($filter_data);
		
		$results = array_slice($full_results, ($latest_page-1)*(int)$setting['limit'], (int)$setting['limit']);

		if ($results) {
			foreach ($results as $result) {
		
			$results_img = $this->model_catalog_product->getProductImages($result['product_id']);
			$additional_img = array();
			foreach ($results_img as $result_img) {
				if ($result_img['image']) {
					$additional_image = $this->model_tool_image->resize($result_img['image'], $setting['width'], $setting['height']);
				} else {
					$additional_image = false;
				}
				$additional_img[1] = $additional_image;
				break;
			}
            
				if ($result['image']) {
					$image = $this->model_tool_image->resize($result['image'], $setting['width'], $setting['height']);
				} else {
					$image = $this->model_tool_image->resize('placeholder.png', $setting['width'], $setting['height']);
				}

				if (($this->config->get('config_customer_price') && $this->customer->isLogged()) || !$this->config->get('config_customer_price')) {
					$price = $this->currency->format($this->tax->calculate($result['price'], $result['tax_class_id'], $this->config->get('config_tax')));
				} else {
					$price = false;
				}

				if ((float)$result['special']) {
					$special = $this->currency->format($this->tax->calculate($result['special'], $result['tax_class_id'], $this->config->get('config_tax')));
				} else {
					$special = false;
				}

				if ($this->config->get('config_tax')) {
					$tax = $this->currency->format((float)$result['special'] ? $result['special'] : $result['price']);
				} else {
					$tax = false;
				}

				if ($this->config->get('config_review_status')) {
					$rating = $result['rating'];
				} else {
					$rating = false;
				}

				$data['products'][] = array(
					'product_id'  => $result['product_id'],
					'thumb'       => $image,
		
			'additional_img' => $additional_img,
            
					'name'        => $result['name'],
					'description' => utf8_substr(strip_tags(html_entity_decode($result['description'], ENT_QUOTES, 'UTF-8')), 0, $this->config->get('config_product_description_length')) . '..',
					'price'       => $price,
					'special'     => $special,
					'tax'         => $tax,
					'rating'      => $rating,
					'href'        => $this->url->link('product/product', 'product_id=' . $result['product_id'])
				);
			}
			
		$url = '';
		$product_total = count($full_results);
		$page = $latest_page;
		$limit = $setting['limit'];
		
		$pagination = new Pagination();
		$pagination->total = $product_total;
		$pagination->page = $page;
		$pagination->limit = $setting['limit'];
		$pagination->url = $this->url->link('common/home', $url . '&latest_page={page}');

		$data['pagination'] = $pagination->render();
		$data['results'] = sprintf($this->language->get('text_pagination'), ($product_total) ? (($page - 1) * $limit) + 1 : 0, ((($page - 1) * $limit) > ($product_total - $limit)) ? $product_total : ((($page - 1) * $limit) + $limit), $product_total, ceil($product_total / $limit));

		// http://googlewebmastercentral.blogspot.com/2011/09/pagination-with-relnext-and-relprev.html
		if ($page == 1) {
			$this->document->addLink($this->url->link('common/home', '', 'SSL'), 'canonical');
		} elseif ($page == 2) {
			$this->document->addLink($this->url->link('common/home', '', 'SSL'), 'prev');
		} else {
			$this->document->addLink($this->url->link('common/home', '&latest_page='. ($page - 1), 'SSL'), 'prev');
		}

		if ($setting['limit'] && ceil(count($product_total) / $setting['limit']) > $latest_page) {
			$this->document->addLink($this->url->link('common/home', '&latest_page='. ($page + 1), 'SSL'), 'next');
		}			
			

			if (file_exists(DIR_TEMPLATE . $this->config->get('config_template') . '/template/module/latestajax.tpl')) {
				return $this->load->view($this->config->get('config_template') . '/template/module/latestajax.tpl', $data);
			} else {
				return $this->load->view('default/template/module/latestajax.tpl', $data);
			}
		}
		
			
		
	}
}
