<?php
class ControllerModuleFeatured extends Controller {
	public function index($setting) {
		$this->load->language('module/featured');

		$data['heading_title'] = $this->language->get('heading_title');

		$data['text_tax'] = $this->language->get('text_tax');

		$data['button_cart'] = $this->language->get('button_cart');
		$data['button_wishlist'] = $this->language->get('button_wishlist');
		$data['button_compare'] = $this->language->get('button_compare');


					// Top stickers start
						$this->load->model('setting/setting');

						// Statuses
						$data['topstickers_status'] = $this->config->get('topstickers_status');
						$data['topstickers_sold_status'] = $this->config->get('topstickers_sold_status');
						$data['topstickers_sale_status'] = $this->config->get('topstickers_sale_status');
						$data['topstickers_bestseller_status'] = $this->config->get('topstickers_bestseller_status');
						$data['topstickers_novelty_status'] = $this->config->get('topstickers_novelty_status');
						$data['topstickers_last_status'] = $this->config->get('topstickers_last_status');
						$data['topstickers_freeshipping_status'] = $this->config->get('topstickers_freeshipping_status');

						// Text
						$current_language_id = $this->config->get('config_language_id');
						$data['topstickers_sold_text'] = $this->config->get('topstickers_sold_text')[$current_language_id];
						$data['topstickers_sale_text'] = $this->config->get('topstickers_sale_text')[$current_language_id];
						$data['topstickers_bestseller_text'] = $this->config->get('topstickers_bestseller_text')[$current_language_id];
						$data['topstickers_novelty_text'] = $this->config->get('topstickers_novelty_text')[$current_language_id];
						$data['topstickers_last_text'] = $this->config->get('topstickers_last_text')[$current_language_id];
						$data['topstickers_freeshipping_text'] = $this->config->get('topstickers_freeshipping_text')[$current_language_id];

						// Additional data
						$data['topstickers_bestseller_numbers'] = $this->config->get('topstickers_bestseller_numbers');
						$data['topstickers_novelty_days'] = $this->config->get('topstickers_novelty_days');
						$data['topstickers_last_numbers'] = $this->config->get('topstickers_last_numbers');
						$data['topstickers_freeshipping_price'] = $this->config->get('topstickers_freeshipping_price');
					// Top stickers end
				
		$this->load->model('catalog/product');

		$this->load->model('tool/image');

		$data['products'] = array();

		if (!$setting['limit']) {
			$setting['limit'] = 4;
		}

		if (!empty($setting['product'])) {
			$products = array_slice($setting['product'], 0, (int)$setting['limit']);

			foreach ($products as $product_id) {
				$product_info = $this->model_catalog_product->getProduct($product_id);

				if ($product_info) {
		
			$results_img = $this->model_catalog_product->getProductImages($product_info['product_id']);
			$additional_img = array();
			foreach ($results_img as $result_img) {
				if ($result_img['image']) {
					$additional_image = $this->model_tool_image->resize($result_img['image'], $setting['width'], $setting['height']);
				} else {
					$additional_image = false;
				}
				$additional_img[1] = $additional_image;
				break;
			}
            
					if ($product_info['image']) {
						$image = $this->model_tool_image->resize($product_info['image'], $setting['width'], $setting['height']);
					} else {
						$image = $this->model_tool_image->resize('placeholder.png', $setting['width'], $setting['height']);
					}

					if (($this->config->get('config_customer_price') && $this->customer->isLogged()) || !$this->config->get('config_customer_price')) {
						$price = $this->currency->format($this->tax->calculate($product_info['price'], $product_info['tax_class_id'], $this->config->get('config_tax')));
					} else {
						$price = false;
					}

					if ((float)$product_info['special']) {
						$special = $this->currency->format($this->tax->calculate($product_info['special'], $product_info['tax_class_id'], $this->config->get('config_tax')));
					} else {
						$special = false;
					}

					if ($this->config->get('config_tax')) {
						$tax = $this->currency->format((float)$product_info['special'] ? $product_info['special'] : $product_info['price']);
					} else {
						$tax = false;
					}

					if ($this->config->get('config_review_status')) {
						$rating = $product_info['rating'];
					} else {
						$rating = false;
					}


					// Top stickers start
						// Stickers for current product
							// Sold sticker
							$data['product_sold_sticker'] = false;
							if ($data['topstickers_sold_status'] && $data['topstickers_sold_text'] && $data['topstickers_sold_text'] != '' && $product_info['quantity'] <= 0) {
								$data['product_sold_sticker'] = $data['topstickers_sold_text'];
							}

							// Sale sticker
							$data['product_sale_sticker'] = false;
							if ($data['topstickers_sale_status'] && $data['topstickers_sale_text'] && $data['topstickers_sale_text'] != '' && (float)$product_info['special']) {
								$data['product_sale_sticker'] = $data['topstickers_sale_text'];
							}

							// Bestseller sticker
							$data['product_bestseller_sticker'] = false;
							if ($data['topstickers_bestseller_status'] && $data['topstickers_bestseller_text'] && $data['topstickers_bestseller_text'] != '') {
								$bestsellers = $this->model_catalog_product->getBestSellerProducts( $data['topstickers_bestseller_numbers']);
								foreach ($bestsellers as $bestseller) {
									if ($bestseller['product_id'] == $product_info['product_id']) {
										$data['product_bestseller_sticker'] = $data['topstickers_bestseller_text'];
									}
								}
							}

							// Novelty sticker
							$data['product_novelty_sticker'] = false;
							if ($data['topstickers_novelty_status'] && $data['topstickers_novelty_text'] && $data['topstickers_novelty_text'] != '' && (strtotime($product_info['date_added']) + intval($data['topstickers_novelty_days']) * 24 * 3600) > time()) {
								$data['product_novelty_sticker'] = $data['topstickers_novelty_text'];
							}

							// Last sticker
							$data['product_last_sticker'] = false;
							if ($data['topstickers_last_status'] && $data['topstickers_last_text'] && $data['topstickers_last_text'] != '' && $product_info['quantity'] <= intval($data['topstickers_last_numbers']) && $product_info['quantity'] > 0) {
								$data['product_last_sticker'] = $data['topstickers_last_text'];
							}

							// Freeshipping sticker
							$data['product_freeshipping_sticker'] = false;
							if ($data['topstickers_freeshipping_status'] && $data['topstickers_freeshipping_text'] && $data['topstickers_freeshipping_text'] != '') {
								if ((float)$product_info['special'] && (float)$product_info['special'] >= $data['topstickers_freeshipping_price']) {
									$data['product_freeshipping_sticker'] = $data['topstickers_freeshipping_text'];
								} elseif ($product_info['price'] >= $data['topstickers_freeshipping_price']) {
									$data['product_freeshipping_sticker'] = $data['topstickers_freeshipping_text'];
								} else {
									$data['product_freeshipping_sticker'] = false;
								}
							}
					// Top stickers end

					// Top stickers custom start
						$current_language_id = $this->config->get('config_language_id');
						$product_custom_topstickers = array();
						foreach ($this->model_catalog_product->getProductTopStickers($product_info['product_id']) as $topsticker) {
							$product_custom_topsticker_text = json_decode( $topsticker['text'], true);
							$product_custom_topstickers[] = array(
								'topsticker_id'	=> $topsticker['topsticker_id'],
								'name'  		=> $topsticker['name'],
								'text' 			=> $product_custom_topsticker_text[$current_language_id],
								'bg_color'  	=> $topsticker['bg_color'],
								'status'    	=> $topsticker['status']
							);
						}
					// Top stickers custom end
				
					$data['products'][] = array(
'percent'	=> sprintf($this->language->get('-%s'), (round((($product_info['price'] -  $product_info['special'])/$product_info['price']) * 100 ,0))) . ' %',
						'product_id'  => $product_info['product_id'],
						'thumb'       => $image,
		
			'additional_img' => $additional_img,
            
						'name'        => $product_info['name'],
						'description' => utf8_substr(strip_tags(html_entity_decode($product_info['description'], ENT_QUOTES, 'UTF-8')), 0, $this->config->get('config_product_description_length')) . '..',
						'price'       => $price,
						'special'     => $special,
						'tax'         => $tax,
						'rating'      => $rating,

					// Top stickers start
						'product_sold_sticker' => $data['product_sold_sticker'],
						'product_sale_sticker' => $data['product_sale_sticker'],
						'product_bestseller_sticker' => $data['product_bestseller_sticker'],
						'product_novelty_sticker' => $data['product_novelty_sticker'],
						'product_last_sticker' => $data['product_last_sticker'],
						'product_freeshipping_sticker' => $data['product_freeshipping_sticker'],
						'product_custom_topstickers' => $product_custom_topstickers,
					// Top stickers end
				
						'href'        => $this->url->link('product/product', 'product_id=' . $product_info['product_id'])
					);
				}
			}
		}

		if ($data['products']) {
			if (file_exists(DIR_TEMPLATE . $this->config->get('config_template') . '/template/module/featured.tpl')) {
				return $this->load->view($this->config->get('config_template') . '/template/module/featured.tpl', $data);
			} else {
				return $this->load->view('default/template/module/featured.tpl', $data);
			}
		}
	}
}