<?php
class ControllerModuleCategory extends Controller {
	public function index() {
		$this->load->language('module/category');

		$data['heading_title'] = $this->language->get('heading_title');

		if (isset($this->request->get['path'])) {
			$parts = explode('_', (string)$this->request->get['path']);
		} else {
			$parts = array();
		}

		if (isset($parts[0])) {
			$data['category_id'] = $parts[0];
		} else {
			$data['category_id'] = 0;
		}

		if (isset($parts[1])) {
			$data['child_id'] = $parts[1];
		} else {
			$data['child_id'] = 0;
		}
		
		if (isset($parts[2])) {
			$data['lvl3'] = $parts[2];
		} else {
			$data['lvl3'] = 0;
		}	

		if (isset($parts[3])) {
			$data['lvl4'] = $parts[3];
		} else {
			$data['lvl4'] = 0;
		}

		if (isset($parts[4])) {
			$data['lvl5'] = $parts[4];
		} else {
			$data['lvl5'] = 0;
		}

		$this->load->model('catalog/category');

		$this->load->model('catalog/product');

		$data['categories'] = array();

	
	$categories = $this->model_catalog_category->getCategories(0);

		foreach ($categories as $category) {
			$children_data = array();

			if ($category['category_id'] == $data['category_id']) {
				$children = $this->model_catalog_category->getCategories($category['category_id']);

				foreach($children as $child) {
					$filter_data = array('filter_category_id' => $child['category_id'], 				
				'filter_sub_category' => true,
				'mfp_disabled' => true
			);

					
					$level3 = $this->model_catalog_category->getCategories($child['category_id']);
					
					$l3_data = array();
					
					foreach ($level3 as $l3) {
						
					$level4 = $this->model_catalog_category->getCategories($l3['category_id']);
					
					$l4_data = array();
					
					foreach ($level4 as $l4) {
						
					$level4 = $this->model_catalog_category->getCategories($l3['category_id']);
					
					$l4_data = array();
					
					foreach ($level4 as $l4) {
						
					$level5 = $this->model_catalog_category->getCategories($l4['category_id']);
					
					$l5_data = array();
					
					foreach ($level5 as $l5) {
						
						$l5_data[] = array(
							'category_id' => $l5['category_id'],
							'name'        => $l5['name'],
							'href'        => $this->url->link('product/category', 'path=' . $category['category_id'] . '_' . $child['category_id']. '_' . $l3['category_id']. '_' . $l4['category_id']. '_' . $l5['category_id'])
						);
					}
						
						$l4_data[] = array(
							'category_id' => $l4['category_id'],
							'name'        => $l4['name'],
							'level5' => $l5_data,
							'href'        => $this->url->link('product/category', 'path=' . $category['category_id'] . '_' . $child['category_id']. '_' . $l3['category_id']. '_' . $l4['category_id'])
						);
					}
					}
						
						$l3_data[] = array(
							'category_id' => $l3['category_id'],
							'name'        => $l3['name'],
							'level4' => $l4_data,
							'href'        => $this->url->link('product/category', 'path=' . $category['category_id'] . '_' . $child['category_id']. '_' . $l3['category_id'])
						);
					}
					
					$children_data[] = array(
						'category_id' => $child['category_id'],
						'name' => $child['name'] . ($this->config->get('config_product_count') ? ' (' . $this->model_catalog_product->getTotalProducts($filter_data) . ')' : ''),
						'level3' => $l3_data,
						'href' => $this->url->link('product/category', 'path=' . $category['category_id'] . '_' . $child['category_id'])
					);
				}
			}

			$filter_data = array(
				'filter_category_id'  => $category['category_id'],
								
				'filter_sub_category' => true,
				'mfp_disabled' => true
			
			);

			$data['categories'][] = array(
				'category_id' => $category['category_id'],
				'name'        => $category['name'] . ($this->config->get('config_product_count') ? ' (' . $this->model_catalog_product->getTotalProducts($filter_data) . ')' : ''),
				'children'    => $children_data,
				'href'        => $this->url->link('product/category', 'path=' . $category['category_id'])
			);
		}

		if (file_exists(DIR_TEMPLATE . $this->config->get('config_template') . '/template/module/category.tpl')) {
			return $this->load->view($this->config->get('config_template') . '/template/module/category.tpl', $data);
		} else {
			return $this->load->view('default/template/module/category.tpl', $data);
		}
	}
}