<?php
class ControllerInformationInformation extends Controller {
	public function index() {
		$this->load->language('information/information');

		$this->load->model('catalog/information');

		$data['breadcrumbs'] = array();

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('text_home'),
			'href' => $this->url->link('common/home')
		);

		if (isset($this->request->get['information_id'])) {
			$information_id = (int)$this->request->get['information_id'];
		} else {
			$information_id = 0;
		}

		$information_info = $this->model_catalog_information->getInformation($information_id);

		if ($information_info) {
			$this->document->setTitle(!empty($information_info['meta_title']) && $this->config->get('mlseo_enabled') ? $information_info['meta_title'] : $information_info['title']);
			$this->document->setDescription($information_info['meta_description']);
			$this->document->setKeywords($information_info['meta_keyword']);

			$data['breadcrumbs'][] = array(
				'text' => $information_info['title'],
				'href' => $this->url->link('information/information', 'information_id=' .  $information_id)
			);

			$data['heading_title'] = !empty($information_info['seo_h1']) && $this->config->get('mlseo_enabled') ? $information_info['seo_h1'] : $information_info['title'];
        
        if (version_compare(VERSION, '2', '>=')) {
          $data['seo_h1'] = !empty($information_info['seo_h1']) ? $information_info['seo_h1'] : '';
          $data['seo_h2'] = !empty($information_info['seo_h2']) ? $information_info['seo_h2'] : '';
          $data['seo_h3'] = !empty($information_info['seo_h3']) ? $information_info['seo_h3'] : '';
        } else {
          $this->data['seo_h1'] = !empty($information_info['seo_h1']) ? $information_info['seo_h1'] : '';
          $this->data['seo_h2'] = !empty($information_info['seo_h2']) ? $information_info['seo_h2'] : '';
          $this->data['seo_h3'] = !empty($information_info['seo_h3']) ? $information_info['seo_h3'] : '';
        }
        
        $this->load->model('tool/seo_package');
  
        if ($this->config->get('mlseo_opengraph')) {
          if (version_compare(VERSION, '2', '>=')) {
            $this->document->addSeoMeta($this->model_tool_seo_package->rich_snippet('opengraph', 'info', $data));
          } else {
            $this->document->addSeoMeta($this->model_tool_seo_package->rich_snippet('opengraph', 'info', $this->data));
          }
          
          if ($this->config->get('mlseo_microdata')) {
            if (version_compare(VERSION, '2', '>=')) {
              $this->document->addSeoMeta($this->model_tool_seo_package->rich_snippet('microdata', 'information', $data));
            } else {
              $this->document->addSeoMeta($this->model_tool_seo_package->rich_snippet('microdata', 'information', $this->data));
            }
          }
        }
      

			$data['button_continue'] = $this->language->get('button_continue');


					//microdatapro 7.0 start - 1 - main
					$data['microdatapro_data'] = $information_info;
					$data['description'] = html_entity_decode($information_info['description'], ENT_QUOTES, 'UTF-8');
					$this->document->setTc_og($this->load->controller('module/microdatapro/tc_og', $data));
					$this->document->setTc_og_prefix($this->load->controller('module/microdatapro/tc_og_prefix'));
					$data['microdatapro'] = $this->load->controller('module/microdatapro/information', $data);
					$microdatapro_main_flag = 1;
					//microdatapro 7.0 start - 1 - main
				
			$data['description'] = html_entity_decode($information_info['description'], ENT_QUOTES, 'UTF-8');

			$data['continue'] = $this->url->link('common/home');


					//microdatapro 7.0 start - 2 - extra
					if(!isset($microdatapro_main_flag)){
						if(isset($information_info) && $information_info){
							$data['microdatapro_data'] = $information_info;
							$data['description'] = html_entity_decode($information_info['description'], ENT_QUOTES, 'UTF-8');
							$this->document->setTc_og($this->load->controller('module/microdatapro/tc_og', $data));
							$this->document->setTc_og_prefix($this->load->controller('module/microdatapro/tc_og_prefix'));
							$data['microdatapro'] = $this->load->controller('module/microdatapro/information', $data);
						}
					}
					//microdatapro 7.0 end - 2 - extra
				
			$data['column_left'] = $this->load->controller('common/column_left');
			$data['column_right'] = $this->load->controller('common/column_right');
			$data['content_top'] = $this->load->controller('common/content_top');
			$data['content_bottom'] = $this->load->controller('common/content_bottom');
			$data['footer'] = $this->load->controller('common/footer');
			$data['header'] = $this->load->controller('common/header');

			if (file_exists(DIR_TEMPLATE . $this->config->get('config_template') . '/template/information/information.tpl')) {
				
            $template = $this->config->get('config_template') . '/template/information/information.tpl';

            // Custom template module
            $this->load->model('setting/setting');

            $customer_group_id = $this->customer->getGroupId();

            $custom_template_module = $this->model_setting_setting->getSetting('custom_template_module');
            if(!empty($custom_template_module['custom_template_module'])){
                foreach ($custom_template_module['custom_template_module'] as $key => $module) {
                    if (($module['type'] == 2) && !empty($module['informations'])) {
                        if ((isset($module['customer_groups']) && in_array($customer_group_id, $module['customer_groups'])) || !isset($module['customer_groups']) || empty($module['customer_groups'])){

                            if (in_array($information_id, $module['informations'])) {
                                if (file_exists(DIR_TEMPLATE . $this->config->get('config_template') . '/template/' . $module['template_name'] . '.tpl')) {
                                    $template = $this->config->get('config_template') . '/template/' . $module['template_name'] . '.tpl';
                                }
                            }

                        } // customer groups

                    }
                }
            }
            $this->response->setOutput($this->load->view($template, $data));
            // Custom template module
            
			} else {
				$this->response->setOutput($this->load->view('default/template/information/information.tpl', $data));
			}
		} else {
			$data['breadcrumbs'][] = array(
				'text' => $this->language->get('text_error'),
				'href' => $this->url->link('information/information', 'information_id=' . $information_id)
			);

			$this->document->setTitle($this->language->get('text_error'));

			$data['heading_title'] = $this->language->get('text_error');

			$data['text_error'] = $this->language->get('text_error');

			$data['button_continue'] = $this->language->get('button_continue');

			$data['continue'] = $this->url->link('common/home');

			$this->response->addHeader($this->request->server['SERVER_PROTOCOL'] . ' 404 Not Found');


					//microdatapro 7.0 start - 2 - extra
					if(!isset($microdatapro_main_flag)){
						if(isset($information_info) && $information_info){
							$data['microdatapro_data'] = $information_info;
							$data['description'] = html_entity_decode($information_info['description'], ENT_QUOTES, 'UTF-8');
							$this->document->setTc_og($this->load->controller('module/microdatapro/tc_og', $data));
							$this->document->setTc_og_prefix($this->load->controller('module/microdatapro/tc_og_prefix'));
							$data['microdatapro'] = $this->load->controller('module/microdatapro/information', $data);
						}
					}
					//microdatapro 7.0 end - 2 - extra
				
			$data['column_left'] = $this->load->controller('common/column_left');
			$data['column_right'] = $this->load->controller('common/column_right');
			$data['content_top'] = $this->load->controller('common/content_top');
			$data['content_bottom'] = $this->load->controller('common/content_bottom');
			$data['footer'] = $this->load->controller('common/footer');
			$data['header'] = $this->load->controller('common/header');

			if (file_exists(DIR_TEMPLATE . $this->config->get('config_template') . '/template/error/not_found.tpl')) {
				$this->response->setOutput($this->load->view($this->config->get('config_template') . '/template/error/not_found.tpl', $data));
			} else {
				$this->response->setOutput($this->load->view('default/template/error/not_found.tpl', $data));
			}
		}
	}

	public function agree() {
		$this->load->model('catalog/information');

		if (isset($this->request->get['information_id'])) {
			$information_id = (int)$this->request->get['information_id'];
		} else {
			$information_id = 0;
		}

		$output = '';

		$information_info = $this->model_catalog_information->getInformation($information_id);

		if ($information_info) {
			$output .= html_entity_decode($information_info['description'], ENT_QUOTES, 'UTF-8') . "\n";
		}

		$this->response->setOutput($output);
	}
}