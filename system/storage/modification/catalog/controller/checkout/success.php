<?php
class ControllerCheckoutSuccess extends Controller {
	public function index() {

        $data['gtag_event'] = '';

        if (isset($this->session->data['order_id'])) {
            $gtag = false;

            if ($this->config->get('google_analytics_status')) {
                $gtag = true;
            } else if ($this->config->get('config_google_analytics_status')) {
                $gtag = true;
            } else if ($this->config->get('config_google_analytics')) {
                $google_analytics = $this->config->get('config_google_analytics');
                if (!empty($google_analytics)) {
                    $gtag = true;
                }
            }

            if ($gtag) {
                $this->load->model('checkout/order');
                $event_info = $this->model_checkout_order->getGTAG($this->session->data['order_id']);

                if ($event_info) {
                    $data['gtag_event'] .= '<script>'."\n";
                    $data['gtag_event'] .= 'gtag(\'event\', \'purchase\', {'."\n";
                    $data['gtag_event'] .= sprintf('"transaction_id": "%s", "affiliation": "%s", "value": %s, "currency": "%s", "shipping": %s,', $event_info['transaction_id'], $event_info['affiliation'], $event_info['value'], $event_info['currency'], $event_info['shipping'])."\n";
                    if (isset($event_info['items']) && is_array($event_info['items'])) {
                        $data['gtag_event'] .= '"items": [ '."\n";
                        foreach ($event_info['items'] as $item) {
                            $data['gtag_event'] .= sprintf('{ "id": "%s", "name": "%s", "category": "%s", "variant": "%s", "quantity": %s, "price": "%s" },', $item['id'], $item['name'], $item['category'], $item['variant'], $item['quantity'], $item['price'])."\n";
                        }
                        $data['gtag_event'] .= ' ]'."\n";
                    }
                    $data['gtag_event'] .= ' });'."\n";
                    $data['gtag_event'] .= '</script>';
                }
            }
        }
      
		$this->load->language('checkout/success');

		if (isset($this->session->data['order_id'])) {

          $this->load->model('setting/setting');
          $id = $this->config->get('config_store_id');
          
          if($this->config->get('google_remarketing_status_'.$id))
          {
            //DYNAMIC TYPE 
            if($this->config->get('google_remarketing_type_'.$id) == 0)
              $_SESSION['previus_cart'] = $this->cart->getProducts();
          }
        
			$this->cart->clear();

    					/* Membership */     					 	
    					if (!empty($this->session->data['membership_card_savings'])) {
							$this->load->model('total/membership_card');
							
							$card_info = $this->model_total_membership_card->getCard($this->session->data['membership_card']);
							
							if ($card_info) {
								$this->model_total_membership_card->unconfirm($this->session->data['order_id']);
								$this->model_total_membership_card->reduceSavings($card_info['card_id'], $this->session->data['order_id'], $this->session->data['membership_card_savings']);
							}

							unset($this->session->data['membership_card_savings']);
						}
						
						if (!empty($this->session->data['membership_gift'])) {
							$this->load->model('total/membership_gift');
							
							$this->model_total_membership_gift->unconfirm($this->session->data['order_id']);
							$this->model_total_membership_gift->confirm($this->session->data['membership_gift'], $this->session->data['order_id']);
							
							unset($this->session->data['membership_gift']);
						}
						/* Membership */
					

			// Add to activity log
			$this->load->model('account/activity');

			if ($this->customer->isLogged()) {
				$activity_data = array(
					'customer_id' => $this->customer->getId(),
					'name'        => $this->customer->getFirstName() . ' ' . $this->customer->getLastName(),
					'order_id'    => $this->session->data['order_id']
				);

				$this->model_account_activity->addActivity('order_account', $activity_data);
			} else {
				$activity_data = array(
					'name'     => $this->session->data['guest']['firstname'] . ' ' . $this->session->data['guest']['lastname'],
					'order_id' => $this->session->data['order_id']
				);

				$this->model_account_activity->addActivity('order_guest', $activity_data);
			}

			unset($this->session->data['shipping_method']);
			unset($this->session->data['shipping_methods']);
			unset($this->session->data['payment_method']);
			unset($this->session->data['payment_methods']);
			unset($this->session->data['guest']);
			unset($this->session->data['comment']);
			unset($this->session->data['order_id']);
			unset($this->session->data['coupon']);
			unset($this->session->data['reward']);
			unset($this->session->data['voucher']);
			unset($this->session->data['vouchers']);
			unset($this->session->data['totals']);
		}

		$this->document->setTitle($this->language->get('heading_title'));

		$data['breadcrumbs'] = array();

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('text_home'),
			'href' => $this->url->link('common/home')
		);

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('text_basket'),
			'href' => $this->url->link('checkout/cart')
		);

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('text_checkout'),
			'href' => $this->url->link('checkout/checkout', '', 'SSL')
		);

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('text_success'),
			'href' => $this->url->link('checkout/success')
		);

		$data['heading_title'] = $this->language->get('heading_title');

		if ($this->customer->isLogged()) {
			$data['text_message'] = sprintf($this->language->get('text_customer'), $this->url->link('account/account', '', 'SSL'), $this->url->link('account/order', '', 'SSL'), $this->url->link('account/download', '', 'SSL'), $this->url->link('information/contact'));
		} else {
			$data['text_message'] = sprintf($this->language->get('text_guest'), $this->url->link('information/contact'));
		}

		$data['button_continue'] = $this->language->get('button_continue');

		$data['continue'] = $this->url->link('common/home');

		$data['column_left'] = $this->load->controller('common/column_left');
		$data['column_right'] = $this->load->controller('common/column_right');
		$data['content_top'] = $this->load->controller('common/content_top');
		$data['content_bottom'] = $this->load->controller('common/content_bottom');
		$data['footer'] = $this->load->controller('common/footer');
		$data['header'] = $this->load->controller('common/header');

		if (file_exists(DIR_TEMPLATE . $this->config->get('config_template') . '/template/common/success.tpl')) {
			$this->response->setOutput($this->load->view($this->config->get('config_template') . '/template/common/success.tpl', $data));
		} else {
			$this->response->setOutput($this->load->view('default/template/common/success.tpl', $data));
		}
	}
}