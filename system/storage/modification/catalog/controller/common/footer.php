<?php
class ControllerCommonFooter extends Controller {
	public function index() {
		$this->load->language('common/footer');

		$data['scripts'] = $this->document->getScripts('footer');

		$data['text_information'] = $this->language->get('text_information');
		$data['text_service'] = $this->language->get('text_service');
		$data['text_extra'] = $this->language->get('text_extra');
		$data['text_contact'] = $this->language->get('text_contact');
		$data['text_return'] = $this->language->get('text_return');
		$data['text_sitemap'] = $this->language->get('text_sitemap');
		$data['text_manufacturer'] = $this->language->get('text_manufacturer');
		$data['text_voucher'] = $this->language->get('text_voucher');
		$data['text_affiliate'] = $this->language->get('text_affiliate');
		$data['text_special'] = $this->language->get('text_special');
		$data['text_account'] = $this->language->get('text_account');
		$data['text_order'] = $this->language->get('text_order');
		$data['text_wishlist'] = $this->language->get('text_wishlist');
		$data['text_newsletter'] = $this->language->get('text_newsletter');


				$data['text_testimonial'] = $this->language->get('text_testimonial');
			
		$this->load->model('catalog/information');

		$data['informations'] = array();

		foreach ($this->model_catalog_information->getInformations() as $result) {
			if ($result['bottom']) {
				$data['informations'][] = array(
					'title' => $result['title'],
					'href'  => $this->url->link('information/information', 'information_id=' . $result['information_id'])
				);
			}
		}


					//microdatapro 7.0 start - 1 - main
					$data['microdatapro'] = $this->load->controller('module/microdatapro/company');
					$microdatapro_main_flag = 1;
					//microdatapro 7.0 end - 1 - main
					

				$data['testimonial'] = $this->url->link('product/testimonial');
			
		$data['contact'] = $this->url->link('information/contact');
		$data['return'] = $this->url->link('account/return/add', '', 'SSL');
		$data['sitemap'] = $this->url->link('information/sitemap');
		$data['manufacturer'] = $this->url->link('product/manufacturer');
		$data['voucher'] = $this->url->link('account/voucher', '', 'SSL');
		$data['affiliate'] = $this->url->link('affiliate/account', '', 'SSL');
		$data['special'] = $this->url->link('product/special');
		$data['account'] = $this->url->link('account/account', '', 'SSL');
		$data['order'] = $this->url->link('account/order', '', 'SSL');
		$data['wishlist'] = $this->url->link('account/wishlist', '', 'SSL');
		$data['newsletter'] = $this->url->link('account/newsletter', '', 'SSL');


					//microdatapro 7.0 start - 2 - extra
					if(!isset($microdatapro_main_flag)){
						$data['microdatapro'] = $this->load->controller('module/microdatapro/company');
					}
					//microdatapro 7.0 end - 2 - extra
					
		$data['powered'] = sprintf($this->language->get('text_powered'), $this->config->get('config_name'), date('Y', time()));

				$this->load->model('setting/setting');
				$current_language_id = $this->config->get('config_language_id');
				$data['loadmore_button'] = $this->config->get('loadmore_button_name_'.$current_language_id);
				$data['loadmore_status'] = $this->config->get('loadmore_status');
				$data['loadmore_style'] = $this->config->get('loadmore_style');
				$data['loadmore_arrow_status'] = $this->config->get('loadmore_arrow_status');
            
		if (! isset ($this->request->get['route'])) {
            $data['dynx_itemid']='';
            $data['dynx_pagetype'] = "home";
            $data['dynx_totalvalue']='';
            $data['product_id'] = '';
            $data['product_id_q'] = '';
            $data['price'] = '';
        } else {
            $data['dynx_totalvalue']='';
            $data['product_id'] = '';
            $data['product_id_q'] = '';
            $data['price'] = '';
            if ($this->request->get['route'] == 'product/category') {
                $data['dynx_pagetype'] = "other";
            } elseif ($this->request->get['route'] == 'product/product'){
                $data['dynx_pagetype'] = "offerdetail";
            } elseif ($this->request->get['route'] == 'checkout/cart'){
                $data['dynx_pagetype'] = "cart";
            } elseif ($this->request->get['route'] == 'search/?search'){
                $data['dynx_pagetype'] = "searchresults";
            } elseif ($this->request->get['route'] == 'checkout/success'){
                $data['dynx_pagetype'] = "purchase";
            } elseif ($this->request->get['route'] == 'checkout/success'){
                $data['dynx_pagetype'] = "conversion";
            } elseif ($this->request->get['route'] == 'checkout/fastorder/fastorder'){
                $data['dynx_pagetype'] = "conversionintent";
            } elseif ($this->request->get['route'] == ''){
                $data['dynx_pagetype'] = "home";
            } elseif ($this->request->get['route'] == ''){
                $data['dynx_pagetype'] = "other";
            } elseif ($this->request->get['route'] == 'common/home'){
                $data['dynx_pagetype'] = "home";
            } else {
                $data['dynx_pagetype'] = "other";
            }

            if (isset($this->request->get['product_id'])) {
                $Id=$this->request->get['product_id'];
                $product_info = $this->model_catalog_product->getProduct($Id );
                $data['product_id'] = $Id;
                If ($product_info['special'] > 0)
                    $data['price'] = $product_info['special'];
                else
                    $data['price'] = $product_info['price'];
            }

            if ($this->request->get['route'] == 'product/product') {
                $data['dynx_itemid'] = $this->request->get['product_id'];
            } else {
                $data['dynx_itemid'] = "";
            }

            if ($this->request->get['route'] == 'product/product') {
                $data['dynx_itemid'] = $data['product_id'];
                $data['dynx_totalvalue'] = $data['price'];
            } elseif ($this->request->get['route'] == 'checkout/cart'){
                $data['dynx_totalvalue'] = round($this->cart->getTotal(),2);
            } elseif ($this->request->get['route'] == 'checkout/fastorder/fastorder'){
                $data['dynx_totalvalue'] = round($this->cart->getTotal(),2);
            }
        }

		// Whos Online
		if ($this->config->get('config_customer_online')) {
			$this->load->model('tool/online');

			if (isset($this->request->server['REMOTE_ADDR'])) {
				$ip = $this->request->server['REMOTE_ADDR'];
			} else {
				$ip = '';
			}

			if (isset($this->request->server['HTTP_HOST']) && isset($this->request->server['REQUEST_URI'])) {
				$url = 'http://' . $this->request->server['HTTP_HOST'] . $this->request->server['REQUEST_URI'];
			} else {
				$url = '';
			}

			if (isset($this->request->server['HTTP_REFERER'])) {
				$referer = $this->request->server['HTTP_REFERER'];
			} else {
				$referer = '';
			}

			$this->model_tool_online->addOnline($ip, $this->customer->getId(), $url, $referer);
		}

          $id = $this->config->get('config_store_id');
          $this->load->model('setting/setting');

          //IS ENABLED??
          if($this->config->get('google_remarketing_status_'.$id))
          {
            //DYNAMIC TYPE 
            if($this->config->get('google_remarketing_type_'.$id) == 0)
            {
              $this->load->model('module/google_remarketing');
              $_SESSION['google_remarketing_code'] = $this->model_module_google_remarketing->getDynamicRemarketingCode();
            }
            else //STANDARD TYPE
            {
              $_SESSION['google_remarketing_code'] = html_entity_decode($this->config->get('google_remarketing_code_'.$id));
            }
          }     
        

		if (file_exists(DIR_TEMPLATE . $this->config->get('config_template') . '/template/common/footer.tpl')) {
			return $this->load->view($this->config->get('config_template') . '/template/common/footer.tpl', $data);
		} else {
			return $this->load->view('default/template/common/footer.tpl', $data);
		}
	}
}
