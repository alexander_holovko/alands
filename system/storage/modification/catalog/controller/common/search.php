<?php
class ControllerCommonSearch extends Controller {
	public function index() {
		$this->load->language('common/search');

		$data['text_search'] = $this->language->get('text_search');

		if (isset($this->request->get['search'])) {
			$data['search'] = $this->request->get['search'];
		} else {
			$data['search'] = '';
		}

		if (file_exists(DIR_TEMPLATE . $this->config->get('config_template') . '/template/common/search.tpl')) {

        if ($this->config->get('mlseo_fix_search')) {
          $data['csp_search_url'] = $this->url->link('product/search');
          if (file_exists(DIR_TEMPLATE . $this->config->get('config_template') . '/template/common/csp_search.tpl')) {
            return $this->load->view($this->config->get('config_template') . '/template/common/csp_search.tpl', $data);
          } else {
            return $this->load->view('default/template/common/csp_search.tpl', $data);
          }
        }
      
			return $this->load->view($this->config->get('config_template') . '/template/common/search.tpl', $data);
		} else {

        if ($this->config->get('mlseo_fix_search')) {
          $data['csp_search_url'] = $this->url->link('product/search');
          
          return $this->load->view('default/template/common/csp_search.tpl', $data);
        }
      
			return $this->load->view('default/template/common/search.tpl', $data);
		}
	}
}