<?php
class ControllerCommonHeader extends Controller {
	public function index() {

					// Top stickers start
						$this->load->model('setting/setting');
						$data['topstickers_status'] = $this->config->get('topstickers_status');
						$data['topstickers_position'] = $this->config->get('topstickers_position');
						$data['topstickers_sold_bg'] = $this->config->get('topstickers_sold_bg');
						$data['topstickers_sale_bg'] = $this->config->get('topstickers_sale_bg');
						$data['topstickers_bestseller_bg'] = $this->config->get('topstickers_bestseller_bg');
						$data['topstickers_novelty_bg'] = $this->config->get('topstickers_novelty_bg');
						$data['topstickers_last_bg'] = $this->config->get('topstickers_last_bg');
						$data['topstickers_freeshipping_bg'] = $this->config->get('topstickers_freeshipping_bg');
					// Top stickers end
				

      $this->load->model('tool/seo_package');
      $this->model_tool_seo_package->metaRobots();
      $this->model_tool_seo_package->checkCanonical();
      $this->model_tool_seo_package->hrefLang();
      $this->model_tool_seo_package->richSnippets();
      
      if (version_compare(VERSION, '2', '>=')) {
        $data['mlseo_meta'] = $this->document->renderSeoMeta();
      } else {
        $this->data['mlseo_meta'] = $this->document->renderSeoMeta();
      }
      
      $seoTitlePrefix = $this->config->get('mlseo_title_prefix');
      $seoTitlePrefix = isset($seoTitlePrefix[$this->config->get('config_store_id').$this->config->get('config_language_id')]) ? $seoTitlePrefix[$this->config->get('config_store_id').$this->config->get('config_language_id')] : '';
      
      $seoTitleSuffix = $this->config->get('mlseo_title_suffix');
      $seoTitleSuffix = isset($seoTitleSuffix[$this->config->get('config_store_id').$this->config->get('config_language_id')]) ? $seoTitleSuffix[$this->config->get('config_store_id').$this->config->get('config_language_id')] : '';

      if (version_compare(VERSION, '2', '<')) {
        if ($this->config->get('mlseo_fix_search')) {
          $this->data['mlseo_fix_search'] = true;
          $this->data['csp_search_url'] = $this->url->link('product/search');
        }
      }
      
		// Analytics
		$this->load->model('extension/extension');

		$data['analytics'] = array();

		$analytics = $this->model_extension_extension->getExtensions('analytics');

		foreach ($analytics as $analytic) {
			if ($this->config->get($analytic['code'] . '_status')) {
				$data['analytics'][] = $this->load->controller('analytics/' . $analytic['code']);
			}
		}

		if ($this->request->server['HTTPS']) {
			$server = $this->config->get('config_ssl');
		} else {
			$server = $this->config->get('config_url');
		}

		if (is_file(DIR_IMAGE . $this->config->get('config_icon'))) {
			$this->document->addLink($server . 'image/' . $this->config->get('config_icon'), 'icon');
		}

		

					//microdatapro 7.0 start - 1 - main
					$data['tc_og'] = $this->document->getTc_og();
					$data['tc_og_prefix'] = $this->document->getTc_og_prefix();
					$microdatapro_main_flag = 1;
					//microdatapro 7.0 end - 1 - main
					
        //$data['title'] = $this->document->getTitle();

    					/* Membership */
    					$membership_settings = $this->config->get('membership');   
    					$card_settings = $this->config->get('membership_card'); 
    					$gift_settings = $this->config->get('membership_gift');
    					
    					$data['membership_nearest_discount'] = $card_settings['nearest_discount'];
    					$data['membership_nearest_gift'] 	 = $gift_settings['nearest_gift'];
    					
    					if (!$this->config->get('membership_card_status')) {
    						$data['membership_nearest_discount'] = false;
    					}
    					
    					if (!$this->config->get('membership_gift_status')) {
    						$data['membership_nearest_gift'] = false;
    					}
    					
    					if (!$membership_settings['status']) {
    						$data['membership_nearest_discount'] = false;
    						$data['membership_nearest_gift'] = false;
    					}	
    					/* Membership */
					

					//microdatapro 7.0 start - 1 - main
					$data['tc_og'] = $this->document->getTc_og();
					$data['tc_og_prefix'] = $this->document->getTc_og_prefix();
					$microdatapro_main_flag = 1;
					//microdatapro 7.0 end - 1 - main
					
        $data['title'] = (isset($seoTitlePrefix) ? $seoTitlePrefix : '') . $this->document->getTitle() . (isset($seoTitleSuffix) ? $seoTitleSuffix : '');

    					/* Membership */
    					$membership_settings = $this->config->get('membership');   
    					$card_settings = $this->config->get('membership_card'); 
    					$gift_settings = $this->config->get('membership_gift');
    					
    					$data['membership_nearest_discount'] = $card_settings['nearest_discount'];
    					$data['membership_nearest_gift'] 	 = $gift_settings['nearest_gift'];
    					
    					if (!$this->config->get('membership_card_status')) {
    						$data['membership_nearest_discount'] = false;
    					}
    					
    					if (!$this->config->get('membership_gift_status')) {
    						$data['membership_nearest_gift'] = false;
    					}
    					
    					if (!$membership_settings['status']) {
    						$data['membership_nearest_discount'] = false;
    						$data['membership_nearest_gift'] = false;
    					}	
    					/* Membership */
					
      

		$data['base'] = $server;
$data['robots'] = $this->document->getRobots();
		$data['description'] = $this->document->getDescription();
		$data['keywords'] = $this->document->getKeywords();
		$data['links'] = $this->document->getLinks();
		$data['styles'] = $this->document->getStyles();
		$data['scripts'] = $this->document->getScripts();

    // OCFilter start
    $data['noindex'] = $this->document->isNoindex();
    // OCFilter end
      
		$data['lang'] = $this->language->get('code');
		$data['direction'] = $this->language->get('direction');
		
		$data['og_url'] = (isset($this->request->server['HTTPS']) ? HTTPS_SERVER : HTTP_SERVER) . substr($this->request->server['REQUEST_URI'], 1, (strlen($this->request->server['REQUEST_URI'])-1));


					//microdatapro 7.0 start - 2 - extra
					if(!isset($microdatapro_main_flag)){
						$data['tc_og'] = $this->document->getTc_og();
						$data['tc_og_prefix'] = $this->document->getTc_og_prefix();
					}
					//microdatapro 7.0 end - 2 - extra
					
		$data['name'] = $this->config->get('config_name');

		if (is_file(DIR_IMAGE . $this->config->get('config_logo'))) {
			$data['logo'] = $server . 'image/' . $this->config->get('config_logo');
		} else {
			$data['logo'] = '';
		}

		$this->load->language('common/header');

		$data['text_home'] = $this->language->get('text_home');

		// Wishlist
		if ($this->customer->isLogged()) {
			$this->load->model('account/wishlist');

			$data['text_wishlist'] = sprintf($this->language->get('text_wishlist'), $this->model_account_wishlist->getTotalWishlist());
		} else {
			$data['text_wishlist'] = sprintf($this->language->get('text_wishlist'), (isset($this->session->data['wishlist']) ? count($this->session->data['wishlist']) : 0));
		}

		$data['text_shopping_cart'] = $this->language->get('text_shopping_cart');
		$data['text_logged'] = sprintf($this->language->get('text_logged'), $this->url->link('account/account', '', 'SSL'), $this->customer->getFirstName(), $this->url->link('account/logout', '', 'SSL'));

		$data['text_account'] = $this->language->get('text_account');
		$data['text_register'] = $this->language->get('text_register');
		$data['text_login'] = $this->language->get('text_login');
		$data['text_order'] = $this->language->get('text_order');
		$data['text_transaction'] = $this->language->get('text_transaction');
		$data['text_download'] = $this->language->get('text_download');
		$data['text_logout'] = $this->language->get('text_logout');
		$data['text_checkout'] = $this->language->get('text_checkout');
		$data['text_category'] = $this->language->get('text_category');
		$data['text_all'] = $this->language->get('text_all');

		$data['home'] = $this->url->link('common/home');
		$data['wishlist'] = $this->url->link('account/wishlist', '', 'SSL');
		$data['logged'] = $this->customer->isLogged();
		$data['account'] = $this->url->link('account/account', '', 'SSL');
		$data['register'] = $this->url->link('account/register', '', 'SSL');
		$data['login'] = $this->url->link('account/login', '', 'SSL');
		$data['order'] = $this->url->link('account/order', '', 'SSL');
		$data['transaction'] = $this->url->link('account/transaction', '', 'SSL');
		$data['download'] = $this->url->link('account/download', '', 'SSL');
		$data['logout'] = $this->url->link('account/logout', '', 'SSL');
		$data['shopping_cart'] = $this->url->link('checkout/cart');
		$data['checkout'] = $this->url->link('checkout/checkout', '', 'SSL');
		$data['contact'] = $this->url->link('information/contact');
		$data['telephone'] = $this->config->get('config_telephone');

		$status = true;

		if (isset($this->request->server['HTTP_USER_AGENT'])) {
			$robots = explode("\n", str_replace(array("\r\n", "\r"), "\n", trim($this->config->get('config_robots'))));

			foreach ($robots as $robot) {
				if ($robot && strpos($this->request->server['HTTP_USER_AGENT'], trim($robot)) !== false) {
					$status = false;

					break;
				}
			}
		}

		// Menu
		$this->load->model('catalog/category');

		$this->load->model('catalog/product');

		$data['categories'] = array();

		$categories = $this->model_catalog_category->getCategories(0);

		foreach ($categories as $category) {
			if ($category['top']) {
				// Level 2
				$children_data = array();

				$children = $this->model_catalog_category->getCategories($category['category_id']);

				foreach ($children as $child) {
					$filter_data = array(
						'filter_category_id'  => $child['category_id'],
										
				'filter_sub_category' => true,
				'mfp_disabled' => true
			
					);

					$children_data[] = array(
						'name'  => $child['name'] . ($this->config->get('config_product_count') ? ' (' . $this->model_catalog_product->getTotalProducts($filter_data) . ')' : ''),
						'href'  => $this->url->link('product/category', 'path=' . $category['category_id'] . '_' . $child['category_id'])
					);
				}

				// Level 1
				$data['categories'][] = array(
					'name'     => $category['name'],
					'children' => $children_data,
					'column'   => $category['column'] ? $category['column'] : 1,
					'href'     => $this->url->link('product/category', 'path=' . $category['category_id'])
				);
			}
		}

		
			if($this->config->get('config_megamenu_status')=="1"){	
				$this->load->model('module/nsmenu');
				$this->load->model('catalog/category');
				$this->load->model('catalog/product');
				$data['hmenu_type'] = $this->config->get('horizontal_menu_width_setting');
				$data['config_main_menu_selection'] = $this->config->get('config_main_menu_selection');
				$data['config_fixed_panel_top'] = $this->config->get('config_fixed_panel_top');
				$data['lang_id'] = $this->config->get('config_language_id');
				$data['items']=array();
				$data['additional']=array();
				$menu_items_cache = $this->cache->get('newstoremenu.' . (int)$this->config->get('config_language_id').'.'. (int)$this->config->get('config_store_id'));
				
				if (!empty($menu_items_cache)) {
					$data['items'] = $menu_items_cache;
					$config_menu_item = $this->config->get('config_menu_item');
					if(!empty($config_menu_item)) {
						$menu_items = $this->config->get('config_menu_item');
					} else {
						$menu_items = array();
					}		
					foreach($menu_items as $datamenu){
						if($datamenu['additional_menu']=="additional" && $datamenu['status'] !='0')	{
							$data['additional'][] = 'additional';
						}			
					}
					$data['megamenu_status']=true;		
				} else {
					$config_menu_item = $this->config->get('config_menu_item');
					if(!empty($config_menu_item)) {
						$menu_items = $this->config->get('config_menu_item');
					} else {
						$menu_items = array();
					}
					if (!empty($menu_items)){
						foreach ($menu_items as $key => $value) {
							$sort_menu[$key] = $value['sort_menu'];
						} 
						array_multisort($sort_menu, SORT_ASC, $menu_items);
					}
				
					if(count($menu_items)){
						foreach($menu_items as $datamenu){
							if($datamenu['menu_type']=="link" && $datamenu['status'] !='0')	{
								$data['items'][]=$this->model_module_nsmenu->MegaMenuTypeLink($datamenu);
							}
							if($datamenu['additional_menu']=="additional" && $datamenu['status'] !='0')	{
								$data['additional'][] = 'additional';
							}
							if($datamenu['menu_type']=="information" && $datamenu['status'] !='0')	{
								$data['items'][]=$this->model_module_nsmenu->MegaMenuTypeInformation($datamenu);
							}
							if($datamenu['menu_type']=="manufacturer" && $datamenu['status'] !='0')	{
								$data['items'][]=$this->model_module_nsmenu->MegaMenuTypeManufacturer($datamenu);
							}
							if($datamenu['menu_type']=="product" && $datamenu['status'] !='0'){
								if(!empty($datamenu['products_list'])){
									$data['items'][]=$this->model_module_nsmenu->MegaMenuTypeProduct($datamenu);
								}
							}
							if($datamenu['menu_type']=="category" && $datamenu['status'] !='0')	{
								$data['items'][] = $this->model_module_nsmenu->MegaMenuTypeCategory($datamenu);
							}
							if($datamenu['menu_type']=="html" && $datamenu['status'] !='0')	{
								$data['items'][]=$this->model_module_nsmenu->MegaMenuTypeHtml($datamenu);
							}				
						}
					}
					$menu_items_cache = $data['items'];	
					$this->cache->set('newstoremenu.' . (int)$this->config->get('config_language_id') . '.'. (int)$this->config->get('config_store_id'), $menu_items_cache);		
					$data['megamenu_status']=true;
				
				}		
			} else { 
				$data['megamenu_status']=false;
			}
			$data['language'] = $this->load->controller('common/language');
			
		$data['currency'] = $this->load->controller('common/currency');
		$data['search'] = $this->load->controller('common/search');
		$data['cart'] = $this->load->controller('common/cart');

		// For page specific css
		if (isset($this->request->get['route'])) {
			if (isset($this->request->get['product_id'])) {
				$class = '-' . $this->request->get['product_id'];
			} elseif (isset($this->request->get['path'])) {
				$class = '-' . $this->request->get['path'];
			} elseif (isset($this->request->get['manufacturer_id'])) {
				$class = '-' . $this->request->get['manufacturer_id'];
			} else {
				$class = '';
			}

			$data['class'] = str_replace('/', '-', $this->request->get['route']) . $class;
		} else {
			$data['class'] = 'common-home';
		}

		if (file_exists(DIR_TEMPLATE . $this->config->get('config_template') . '/template/common/header.tpl')) {
			return $this->load->view($this->config->get('config_template') . '/template/common/header.tpl', $data);
		} else {
			return $this->load->view('default/template/common/header.tpl', $data);
		}
	}
}
