<?php
class ControllerTotalMembershipGift extends Controller {
	private $error = array();

	public function index() {
		$this->load->language('total/membership_gift');

		$this->document->setTitle($this->language->get('heading_title'));
		
		$this->document->addStyle('view/stylesheet/ocmax/membership.css');

		if (($this->request->server['REQUEST_METHOD'] == 'POST') && $this->validate()) {
			$this->load->model('setting/setting');

			$this->model_setting_setting->editSetting('membership_gift', $this->request->post);
			$this->model_setting_setting->editSettingValue('membership', 'membership_gift_status', $this->request->post['status']);

			$this->session->data['success'] = $this->language->get('text_success');

			$this->response->redirect($this->url->link('extension', 'token=' . $this->session->data['token'] . '&type=total', 'SSL'));
		}

		$data['heading_title'] = $this->language->get('heading_title');
		
		$data['button_save'] = $this->language->get('button_save');
		$data['button_cancel'] = $this->language->get('button_cancel');

		$data['entry_status'] = $this->language->get('entry_status');
		$data['entry_sort_order'] = $this->language->get('entry_sort_order');
		
		$data['help_status'] = $this->language->get('help_status');
		$data['help_sort_order'] = $this->language->get('help_sort_order');
		
		$data['text_edit'] = $this->language->get('text_edit');
		$data['text_enabled'] = $this->language->get('text_enabled');
		$data['text_disabled'] = $this->language->get('text_disabled');

		if (isset($this->error['warning'])) {
			$data['error_warning'] = $this->error['warning'];
		} else {
			$data['error_warning'] = '';
		}

		$data['breadcrumbs'] = array();

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('text_home'),
			'href' => $this->url->link('common/dashboard', 'token=' . $this->session->data['token'], 'SSL')
		);

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('text_total'),
			'href' => $this->url->link('extension/total', 'token=' . $this->session->data['token'], 'SSL')
		);

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('heading_title'),
			'href' => $this->url->link('total/membership_gift', 'token=' . $this->session->data['token'], 'SSL')
		);

		$data['action'] = $this->url->link('total/membership_gift', 'token=' . $this->session->data['token'], 'SSL');
		$data['cancel'] = $this->url->link('extension/total', 'token=' . $this->session->data['token'], 'SSL');

		if (isset($this->request->post['membership_gift_status'])) {
			$data['membership_gift_status'] = $this->request->post['membership_gift_status'];
		} else {
			$data['membership_gift_status'] = $this->config->get('membership_gift_status');
		}

		if (isset($this->request->post['membership_gift_sort_order'])) {
			$data['membership_gift_sort_order'] = $this->request->post['membership_gift_sort_order'];
		} else {
			$data['membership_gift_sort_order'] = $this->config->get('membership_gift_sort_order');
		}

		$data['header'] = $this->load->controller('common/header');
		$data['column_left'] = $this->load->controller('common/column_left');
		$data['footer'] = $this->load->controller('common/footer');

		$this->response->setOutput($this->load->view('total/membership_gift.tpl', $data));
	}

	protected function validate() {
		if (!$this->user->hasPermission('modify', 'total/membership_gift')) {
			$this->error['warning'] = $this->language->get('error_permission');
		}

		return !$this->error;
	}
}