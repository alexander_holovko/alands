<?php
class ControllerModuleDeluxeVoucher extends Controller {
  private $mod_version = '2.9';
  private $error = array();

  public function index() {
    if (!$this->config->get('deluxe_voucher_installed')) $this->install();

    $this->load->language('extension/module');
    $data['lng'] = $this->load->language('module/deluxe_voucher');

    $data['token'] = $this->session->data['token'];

    $this->document->setTitle($this->language->get('heading_title'));

    $data['heading_title'] = $this->language->get('heading_title') . ' - ' . $this->mod_version;

    $data['breadcrumbs'] = array();

    $data['breadcrumbs'][] = array(
      'text' => $this->language->get('text_home'),
      'href' => $this->url->link('common/home', 'token=' . $this->session->data['token'], 'SSL'),
      'separator' => false
    );

    $data['breadcrumbs'][] = array(
      'text' => $this->language->get('module'),
      'href' => $this->url->link('extension/module', 'token=' . $this->session->data['token'], 'SSL'),
      'separator' => ' :: '
    );

    $data['breadcrumbs'][] = array(
      'text' => $this->language->get('heading_title'),
      'href' => $this->url->link('module/deluxe_voucher', 'token=' . $this->session->data['token'], 'SSL'),
      'separator' => ' :: '
    );

    $data['tab'] = isset($this->request->get['tab']) ? $this->request->get['tab'] : 'setting';

    $this->load->model('setting/setting');

    if ($this->request->server['REQUEST_METHOD'] == 'POST' && $this->validate()) {
      $this->model_setting_setting->editSetting('deluxe_voucher', $this->request->post);

      $this->session->data['success'] = $this->language->get('success');
      $this->response->redirect($this->url->link('module/deluxe_voucher', 'token=' . $this->session->data['token'], 'SSL'));
    }

    $data['text_layout'] = sprintf($this->language->get('text_layout'), $this->url->link('design/layout', 'token=' . $this->session->data['token'], 'SSL'));

    $data['action'] = $this->url->link('module/deluxe_voucher', 'token=' . $this->session->data['token'], 'SSL');

    $data['cancel'] = $this->url->link('extension/module', 'token=' . $this->session->data['token'], 'SSL');

    $data['warning'] = isset($this->error['warning']) ? $this->error['warning'] : '';

    $data['success'] = isset($this->session->data['success']) ? $this->session->data['success'] : '';
    unset($this->session->data['success']);

    $data['deluxe_voucher_status'] = $this->config->get('deluxe_voucher_status');
    $data['deluxe_voucher_prices'] = $this->config->get('deluxe_voucher_prices');
    $data['deluxe_voucher_default_price'] = $this->config->get('deluxe_voucher_default_price');
    $data['deluxe_voucher_hide_label'] = $this->config->get('deluxe_voucher_hide_label');
    $data['deluxe_voucher_hide_image'] = $this->config->get('deluxe_voucher_hide_image');
    $data['deluxe_voucher_width'] = $this->config->get('deluxe_voucher_width');
    $data['deluxe_voucher_height'] = $this->config->get('deluxe_voucher_height');
    $data['deluxe_voucher_hide_date'] = $this->config->get('deluxe_voucher_hide_date');
    $data['deluxe_voucher_max_day'] = $this->config->get('deluxe_voucher_max_day');
    $data['deluxe_voucher_disabled'] = $this->config->get('deluxe_voucher_disabled');
    $data['deluxe_voucher_installed'] = $this->config->get('deluxe_voucher_installed');

    $data['cron'] = '0 0 * * * wget -qO- ' . HTTP_CATALOG . 'index.php?route=module/deluxe_voucher/cronSendVoucher &> /dev/null';

    $data['header'] = $this->load->controller('common/header');
    $data['column_left'] = $this->load->controller('common/column_left');
    $data['footer'] = $this->load->controller('common/footer');

    $this->response->setOutput($this->load->view('module/deluxe_voucher.tpl', $data));
  } //function index end

  protected function validate() {
    if (!$this->user->hasPermission('modify', 'module/deluxe_voucher')) {
      $this->error['warning'] = $this->language->get('error_permission');
    }

    if (!$this->error) {
      return true;
    } else {
      return false;
    }
  } //validate end

  public function install() {
    if (version_compare(VERSION, '2.2', '>=')) {
      $query = $this->db->query("SELECT * FROM " . DB_PREFIX . "event WHERE `trigger` = 'catalog/model/checkout/order/addOrderHistory/after' AND `action` = 'total/voucher/send'");
      if (!$query->num_rows || $query->num_rows > 1) {
        $this->db->query("DELETE FROM " . DB_PREFIX . "event WHERE `trigger` = 'catalog/model/checkout/order/addOrderHistory/after' AND `action` = 'total/voucher/send'");

        $this->load->model('extension/event');

        $this->model_extension_event->addEvent('deluxe_voucher', 'catalog/model/checkout/order/addOrderHistory/after', 'total/voucher/send');
      }
    }

    if (!$this->db->query("DESC " . DB_PREFIX . "order_voucher deliverydate")->num_rows) $this->db->query("ALTER TABLE " . DB_PREFIX . "order_voucher ADD deliverydate date NOT NULL");

    if (!$this->db->query("DESC " . DB_PREFIX . "voucher deliverydate")->num_rows) $this->db->query("ALTER TABLE " . DB_PREFIX . "voucher ADD deliverydate date NOT NULL");

    $this->db->query("INSERT INTO " . DB_PREFIX . "setting SET `code` = 'deluxe_voucher', `key` = 'deluxe_voucher_installed', `value` = '1'");

    $this->session->data['success'] = 'Install/upgrade completed successfully!';
    
    if (isset($this->request->get['redirect'])) $this->response->redirect($this->url->link('module/deluxe_voucher', 'token=' . $this->session->data['token'], 'SSL'));
  } //install end

} //class end
?>
