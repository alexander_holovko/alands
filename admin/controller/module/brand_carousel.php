<?php
class ControllerModuleBrandCarousel extends Controller {
	private $error = array();
	public function index() {
		$this->load->language('module/brand_carousel');
		$this->document->setTitle($this->language->get('heading_title'));
		$this->load->model('extension/module');
		if (($this->request->server['REQUEST_METHOD'] == 'POST') && $this->validate()) {
			if (!isset($this->request->get['module_id'])) {
				$this->model_extension_module->addModule('brand_carousel', $this->request->post);
			} else {
				$this->model_extension_module->editModule($this->request->get['module_id'], $this->request->post);
			}
			$this->session->data['success'] = $this->language->get('text_success');
			$this->response->redirect($this->url->link('extension/module', 'token=' . $this->session->data['token'], 'SSL'));
		}
		$data['heading_title'] = $this->language->get('heading_title');
		$data['button_save'] = $this->language->get('button_save');
		$data['button_cancel'] = $this->language->get('button_cancel');
		$data['text_edit'] = $this->language->get('text_edit');	
		$data['text_yes'] = $this->language->get('text_yes');
		$data['text_no'] = $this->language->get('text_no');
		$data['text_enabled'] = $this->language->get('text_enabled');
		$data['text_disabled'] = $this->language->get('text_disabled');
		$data['entry_name'] = $this->language->get('entry_name');
		$data['entry_name_block'] = $this->language->get('entry_name_block');
		$data['entry_infinite'] = $this->language->get('entry_infinite');
		$data['entry_scroll_auto'] = $this->language->get('entry_scroll_auto');
		$data['entry_scroll_pause'] = $this->language->get('entry_scroll_pause');
		$data['entry_animation_speed'] = $this->language->get('entry_animation_speed');
		$data['entry_interval'] = $this->language->get('entry_interval');
		$data['entry_scroll_limit'] = $this->language->get('entry_scroll_limit');
		$data['entry_scroll'] = $this->language->get('entry_scroll');
		$data['entry_image'] = $this->language->get('entry_image');
		$data['entry_width'] = $this->language->get('entry_width');
		$data['entry_height'] = $this->language->get('entry_height');
		$data['entry_status'] = $this->language->get('entry_status');
		$data['support_text'] = $this->language->get('support_text');
		$data['help_name_block'] = $this->language->get('help_name_block');
		$data['tab_general'] = $this->language->get('tab_general');
		$data['tab_support'] = $this->language->get('tab_support');
		$this->load->model('localisation/language');
		$data['languages'] = $this->model_localisation_language->getLanguages(array('sort' => 'code'));
		foreach ($data['languages'] as $key => $language) {
			$flag_img = 'view/image/flags/'.$language['image'];
			if (version_compare(VERSION, '2.2', '>=')) {
				$flag_img = 'language/'.$language['code'].'/'.$language['image'];
			}
			if(!is_file($flag_img)){
				$flag_img = 'language/'.$language['code'].'/'.$language['code'].'.png';
				if(!is_file($flag_img)) $flag_img = null;
			}
			$data['languages'][$key]['flag_img'] = $flag_img;
		}
		if (isset($this->error['warning'])) {
			$data['error_warning'] = $this->error['warning'];
		} else {
			$data['error_warning'] = '';
		}
		if (isset($this->error['name'])) {
			$data['error_name'] = $this->error['name'];
		} else {
			$data['error_name'] = '';
		}
		if (isset($this->error['animation_speed'])) {
			$data['error_animation_speed'] = $this->error['animation_speed'];
		} else {
			$data['error_animation_speed'] = '';
		}
		if (isset($this->error['interval'])) {
			$data['error_interval'] = $this->error['interval'];
		} else {
			$data['error_interval'] = '';
		}
		if (isset($this->error['scroll_limit'])) {
			$data['error_scroll_limit'] = $this->error['scroll_limit'];
		} else {
			$data['error_scroll_limit'] = '';
		}
		if (isset($this->error['scroll'])) {
			$data['error_scroll'] = $this->error['scroll'];
		} else {
			$data['error_scroll'] = '';
		}
		if (isset($this->error['width'])) {
			$data['error_width'] = $this->error['width'];
		} else {
			$data['error_width'] = '';
		}
		if (isset($this->error['height'])) {
			$data['error_height'] = $this->error['height'];
		} else {
			$data['error_height'] = '';
		}
		$data['breadcrumbs'] = array();
		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('text_home'),
			'href' => $this->url->link('common/dashboard', 'token=' . $this->session->data['token'], 'SSL')
		);
		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('text_module'),
			'href' => $this->url->link('extension/module', 'token=' . $this->session->data['token'], 'SSL')
		);
		if (!isset($this->request->get['module_id'])) {
			$data['breadcrumbs'][] = array(
				'text' => $this->language->get('heading_title'),
				'href' => $this->url->link('module/brand_carousel', 'token=' . $this->session->data['token'], 'SSL')
			);
		} else {
			$data['breadcrumbs'][] = array(
				'text' => $this->language->get('heading_title'),
				'href' => $this->url->link('module/brand_carousel', 'token=' . $this->session->data['token'] . '&module_id=' . $this->request->get['module_id'], 'SSL')
			);
		}
		if (!isset($this->request->get['module_id'])) {
			$data['action'] = $this->url->link('module/brand_carousel', 'token=' . $this->session->data['token'], 'SSL');
		} else {
			$data['action'] = $this->url->link('module/brand_carousel', 'token=' . $this->session->data['token'] . '&module_id=' . $this->request->get['module_id'], 'SSL');
		}
		$data['cancel'] = $this->url->link('extension/module', 'token=' . $this->session->data['token'], 'SSL');
		if (isset($this->request->get['module_id']) && ($this->request->server['REQUEST_METHOD'] != 'POST')) {
			$module_info = $this->model_extension_module->getModule($this->request->get['module_id']);
		}
		if (isset($this->request->post['name'])) {
			$data['name'] = $this->request->post['name'];
		} elseif (!empty($module_info['name'])) {
			$data['name'] = $module_info['name'];
		} else {
			$data['name'] = '';
		}
		if (isset($this->request->post['name_block'])) {
			$data['name_block'] = $this->request->post['name_block'];
		} elseif (!empty($module_info['name_block'])) {
			$data['name_block'] = $module_info['name_block'];
		} else {
			$data['name_block'] = '';
		}
		if (isset($this->request->post['infinite'])) {
			$data['infinite'] = $this->request->post['infinite'];
		} elseif (!empty($module_info['infinite'])) {
			$data['infinite'] = $module_info['infinite'];
		} else {
			$data['infinite'] = '0';
		}
		if (isset($this->request->post['scroll_auto'])) {
			$data['scroll_auto'] = $this->request->post['scroll_auto'];
		} elseif (!empty($module_info['scroll_auto'])) {
			$data['scroll_auto'] = $module_info['scroll_auto'];
		} else {
			$data['scroll_auto'] = '0';
		}
		if (isset($this->request->post['scroll_pause'])) {
			$data['scroll_pause'] = $this->request->post['scroll_pause'];
		} elseif (!empty($module_info['scroll_pause'])) {
			$data['scroll_pause'] = $module_info['scroll_pause'];
		} else {
			$data['scroll_pause'] = 'false';
		}
		if (isset($this->request->post['animation_speed'])) {
			$data['animation_speed'] = $this->request->post['animation_speed'];
		} elseif (!empty($module_info['animation_speed'])) {
			$data['animation_speed'] = $module_info['animation_speed'];
		} else {
			$data['animation_speed'] = '600';
		}
		if (isset($this->request->post['interval'])) {
			$data['interval'] = $this->request->post['interval'];
		} elseif (!empty($module_info['interval'])) {
			$data['interval'] = $module_info['interval'];
		} else {
			$data['interval'] = '6000';
		}
		if (isset($this->request->post['scroll_limit'])) {
			$data['scroll_limit'] = $this->request->post['scroll_limit'];
		} elseif (!empty($module_info['scroll_limit'])) {
			$data['scroll_limit'] = $module_info['scroll_limit'];
		} else {
			$data['scroll_limit'] = '5';
		}
		if (isset($this->request->post['scroll'])) {
			$data['scroll'] = $this->request->post['scroll'];
		} elseif (!empty($module_info['scroll'])) {
			$data['scroll'] = $module_info['scroll'];
		} else {
			$data['scroll'] = '1';
		}		
		if (isset($this->request->post['width'])) {
			$data['width'] = $this->request->post['width'];
		} elseif (!empty($module_info['width'])) {
			$data['width'] = $module_info['width'];
		} else {
			$data['width'] = 200;
		}
		if (isset($this->request->post['height'])) {
			$data['height'] = $this->request->post['height'];
		} elseif (!empty($module_info['height'])) {
			$data['height'] = $module_info['height'];
		} else {
			$data['height'] = 200;
		}
		if (isset($this->request->post['status'])) {
			$data['status'] = $this->request->post['status'];
		} elseif (!empty($module_info['status'])) {
			$data['status'] = $module_info['status'];
		} else {
			$data['status'] = '0';
		}
		$data['header'] = $this->load->controller('common/header');
		$data['column_left'] = $this->load->controller('common/column_left');
		$data['footer'] = $this->load->controller('common/footer');
		$this->response->setOutput($this->load->view('module/brand_carousel.tpl', $data));
	}
	protected function validate() {
		if (!$this->user->hasPermission('modify', 'module/brand_carousel')) {
			$this->error['warning'] = $this->language->get('error_permission');
		}
		if ((utf8_strlen($this->request->post['name']) < 3) || (utf8_strlen($this->request->post['name']) > 64)) {
			$this->error['name'] = $this->language->get('error_name');
		}
		if (!$this->request->post['animation_speed']) {
			$this->error['animation_speed'] = $this->language->get('error_animation_speed');
		}
		if (!$this->request->post['interval']) {
			$this->error['interval'] = $this->language->get('error_interval');
		}
		if (!$this->request->post['scroll_limit']) {
			$this->error['scroll_limit'] = $this->language->get('error_scroll_limit');
		}
		if (!$this->request->post['scroll']) {
			$this->error['scroll'] = $this->language->get('error_scroll');
		}
		if (!$this->request->post['width']) {
			$this->error['width'] = $this->language->get('error_width');
		}
		if (!$this->request->post['height']) {
			$this->error['height'] = $this->language->get('error_height');
		}
		return !$this->error;
	}
}
