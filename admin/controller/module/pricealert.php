<?php
class ControllerModulePriceAlert extends Controller {
	private $error = array(); 
	
	private $moduleName				= 'PriceAlert';
	private $moduleNameSmall		= 'pricealert';
	private $moduleData_module		= 'pricealert_module';
	private $moduleModel			= 'model_module_pricealert';
	
	public function index() {   
		$data['moduleName']			= $this->moduleName;
		$data['moduleNameSmall']	= $this->moduleNameSmall;
		$data['moduleData_module']	= $this->moduleData_module;
		$data['moduleModel'] 		= $this->moduleModel;
	
		$this->load->language('module/'.$this->moduleNameSmall);
        $this->load->model('module/'.$this->moduleNameSmall);
		$this->load->model('catalog/product');
		$this->load->model('setting/setting');
		$this->load->model('setting/store');
		$this->load->model('localisation/language');

		$this->document->setTitle($this->language->get('heading_title'));

		$this->document->addStyle('view/stylesheet/'.$this->moduleNameSmall.'/'.$this->moduleNameSmall.'.css');

        if(!isset($this->request->get['store_id'])) {
           $this->request->get['store_id'] = 0; 
        }
		
		$store = $this->getCurrentStore($this->request->get['store_id']);	
		
		if (($this->request->server['REQUEST_METHOD'] == 'POST') && $this->validateForm()) { 	
			if (!empty($_POST['OaXRyb1BhY2sgLSBDb21'])) {
				$this->request->post[$this->moduleName]['LicensedOn'] = $_POST['OaXRyb1BhY2sgLSBDb21'];
			}
			if (!empty($_POST['cHRpbWl6YXRpb24ef4fe'])) {
				$this->request->post[$this->moduleName]['License'] = json_decode(base64_decode($_POST['cHRpbWl6YXRpb24ef4fe']), true);
			}

			$this->model_setting_setting->editSetting($this->moduleName, $this->request->post, $this->request->post['store_id']);
			$this->session->data['success'] = $this->language->get('text_success');
			$this->response->redirect($this->url->link('module/'.$this->moduleNameSmall, 'store_id='.$this->request->post['store_id'] . '&token=' . $this->session->data['token'], 'SSL'));
		}
		
		if (isset($this->session->data['success'])) {
			$data['success'] = $this->session->data['success'];
			unset($this->session->data['success']);
		} else {
			$data['success'] = '';
		}
		
		if (isset($this->error['warning'])) {
			$data['error_warning'] = $this->error['warning'];
		} else {
			$data['error_warning'] = '';
		}
				
		$data['breadcrumbs']   = array();
        $data['breadcrumbs'][] = array(
            'text' => $this->language->get('text_home'),
            'href' => $this->url->link('common/dashboard', 'token=' . $this->session->data['token'], 'SSL'),
        );
        $data['breadcrumbs'][] = array(
            'text' => $this->language->get('text_module'),
            'href' => $this->url->link('extension/module', 'token=' . $this->session->data['token'], 'SSL'),
        );
        $data['breadcrumbs'][] = array(
            'text' => $this->language->get('heading_title'),
            'href' => $this->url->link('module/'.$this->moduleNameSmall, 'token=' . $this->session->data['token'], 'SSL'),
        );
		
		$languageVariables = array('heading_title', 'text_default', 'button_save', 'button_cancel', 'text_enabled', 'text_disabled');
       
        foreach ($languageVariables as $languageVariable) {
            $data[$languageVariable]		= $this->language->get($languageVariable);
        }
		
		$languages								= $this->model_localisation_language->getLanguages();
		$data['languages']						= $languages;
		$firstLanguage							= array_shift($languages);
		$data['firstLanguageCode']				= $firstLanguage['code'];
		$data['store']                  		= $store;
		$data['stores']							= array_merge(array(0 => array('store_id' => '0', 'name' => $this->config->get('config_name') . ' (' . $data['text_default'].')', 'url' => HTTP_SERVER, 'ssl' => HTTPS_SERVER)), $this->model_setting_store->getStores());
        $data['token']                  		= $this->session->data['token'];
        $data['action']                 		= $this->url->link('module/'.$this->moduleNameSmall, 'token=' . $this->session->data['token'], 'SSL');
        $data['cancel']                 		= $this->url->link('extension/module', 'token=' . $this->session->data['token'], 'SSL');
        $data['moduleSettings']					= $this->model_setting_setting->getSetting($this->moduleName, $store['store_id']);
        $data['moduleData']						= (isset($data['moduleSettings'][$this->moduleName])) ? $data['moduleSettings'][$this->moduleName] : array();
		$data['modelCatalogProduct']			= $this->model_catalog_product;
		
		if ($this->config->get($this->moduleNameSmall.'status')) {
			$data[$this->moduleNameSmall.'status'] = $this->config->get($this->moduleNameSmall.'status');
		} else {
			$data[$this->moduleNameSmall.'status'] = '0';
		}
		
		$run_query = $this->db->query("SELECT `product_id`,`customer_notified`, COUNT(`customer_notified`) as cust_count FROM `".DB_PREFIX.$this->moduleNameSmall."` GROUP BY `product_id`, `customer_notified`");
		$data['products'] = array();
		if (isset($run_query->rows)) {
			foreach ($run_query->rows as $row)
			{
				$data['products'][$row['product_id']][$row['customer_notified']] = $row['cust_count'];
			}			
		}

		$data['header']							= $this->load->controller('common/header');
		$data['column_left']					= $this->load->controller('common/column_left');
		$data['footer']							= $this->load->controller('common/footer');
		
		$this->response->setOutput($this->load->view('module/'.$this->moduleNameSmall.'.tpl', $data));
	}

	protected function validateForm() {
		if (!$this->user->hasPermission('modify', 'module/'.$this->moduleNameSmall)) {
			$this->error['warning'] = $this->language->get('error_permission');
		}
		return !$this->error;
	}
	
    public function install() {
	    $this->load->model('module/'.$this->moduleNameSmall);
	    $this->{$this->moduleModel}->install();
    }
    
    public function uninstall() {
        $this->load->model('module/'.$this->moduleNameSmall);
        $this->load->model('setting/store');
        $this->load->model('localisation/language');
        $this->load->model('design/layout');
		
		$this->model_setting_setting->deleteSetting($this->moduleData_module,0);
		$stores=$this->model_setting_store->getStores();
		foreach ($stores as $store) {
			$this->model_setting_setting->deleteSetting($this->moduleData_module, $store['store_id']);
		}
        $this->load->model('module/'.$this->moduleNameSmall);
        $this->{$this->moduleModel}->uninstall();
    }
	
	public function getcustomers() {
		if (!empty($this->request->get['page'])) {
            $page = (int) $this->request->get['page'];
		} else {
			$page = 1;	
		}

		if(!isset($this->request->get['store_id'])) {
           $this->request->get['store_id'] = 0;
        } 
		
        $this->load->model('module/'.$this->moduleNameSmall);
		
		$data['url_link'] = $this->url;

		$data['store_id']			= $this->request->get['store_id'];
		$data['token']				= $this->session->data['token'];
		$data['limit']				= 10;
		$data['total']				= $this->{$this->moduleModel}->getTotalCustomers($this->request->get['store_id']);
		
		$data['sources']			= $this->{$this->moduleModel}->viewcustomers($this->request->get['store_id'], $page, $data['limit'], $data['store_id']);
	    $pagination					= new Pagination();
        $pagination->total			= $data['total'];
        $pagination->page			= $page;
        $pagination->limit			= $data['limit']; 
        $pagination->url			= $this->url->link('module/'.$this->moduleNameSmall.'/getcustomers','token=' . $this->session->data['token'].'&page={page}&store_id='.$this->request->get['store_id'], 'SSL');
		
		$data['pagination']			= $pagination->render();

		$data['results'] 			= sprintf($this->language->get('text_pagination'), ($data['total']) ? (($page - 1) * $data['limit']) + 1 : 0, ((($page - 1) * $data['limit']) > ($data['total'] - $data['limit'])) ? $data['total'] : ((($page - 1) * $data['limit']) + $data['limit']), $data['total'], ceil($data['total'] / $data['limit']));	
			
		$this->response->setOutput($this->load->view('module/'.$this->moduleNameSmall.'/viewcustomers.tpl', $data));
    }
	
	public function getarchive() {
        if (!empty($this->request->get['page'])) {
            $page = (int) $this->request->get['page'];
		} else {
			$page = 1;	
		}

		if(!isset($this->request->get['store_id'])) {
           $this->request->get['store_id'] = 0;
        } 
		
        $this->load->model('module/'.$this->moduleNameSmall);
		
		$data['url_link'] = $this->url;

		$data['store_id']			= $this->request->get['store_id'];
		$data['token']				= $this->session->data['token'];
		$data['limit']				= 10;
		$data['total']				= $this->{$this->moduleModel}->getTotalNotifiedCustomers($this->request->get['store_id']);
		
		$data['sources']			= $this->{$this->moduleModel}->viewnotifiedcustomers($this->request->get['store_id'], $page, $data['limit'], $data['store_id']);
	    $pagination					= new Pagination();
        $pagination->total			= $data['total'];
        $pagination->page			= $page;
        $pagination->limit			= $data['limit']; 
        $pagination->url			= $this->url->link('module/'.$this->moduleNameSmall.'/getarchive','token=' . $this->session->data['token'].'&page={page}&store_id='.$this->request->get['store_id'], 'SSL');
		
		$data['pagination']			= $pagination->render();

		$data['results'] 			= sprintf($this->language->get('text_pagination'), ($data['total']) ? (($page - 1) * $data['limit']) + 1 : 0, ((($page - 1) * $data['limit']) > ($data['total'] - $data['limit'])) ? $data['total'] : ((($page - 1) * $data['limit']) + $data['limit']), $data['total'], ceil($data['total'] / $data['limit']));	
			
		$this->response->setOutput($this->load->view('module/'.$this->moduleNameSmall.'/archive.tpl', $data));
    }
	
	public function removecustomer() {
		if (isset($_POST['pricealert_id'])) {
			$run_query = $this->db->query("DELETE FROM `" . DB_PREFIX . "pricealert` WHERE `pricealert_id`=".(int)$_POST['pricealert_id']);
				if ($run_query) echo "Success!";
		}
	}
		
	public function removeallcustomers() {
		if (isset($_POST['remove']) && ($_POST['remove']==true)) {
			$run_query = $this->db->query("DELETE FROM `" . DB_PREFIX . "pricealert` WHERE `customer_notified`='0' AND `store_id`='".$this->request->get['store_id']."'");
			if ($run_query) echo "Success!";
		}
	}
		
	public function removeallarchive() {
		if (isset($this->request->post['remove']) && ($this->request->post['remove']==true)) {
			$run_query = $this->db->query("DELETE FROM `" . DB_PREFIX . "pricealert` WHERE `customer_notified`='1' AND `store_id`='".$this->request->get['store_id']."'");
			if ($run_query) echo "Success!";
		}
	}
	
	private function getCatalogURL() {
        if (isset($_SERVER['HTTPS']) && (($_SERVER['HTTPS'] == 'on') || ($_SERVER['HTTPS'] == '1'))) {
            $storeURL = HTTPS_CATALOG;
        } else {
            $storeURL = HTTP_CATALOG;
        } 
        return $storeURL;
    }

    private function getServerURL() {
        if (isset($_SERVER['HTTPS']) && (($_SERVER['HTTPS'] == 'on') || ($_SERVER['HTTPS'] == '1'))) {
            $storeURL = HTTPS_SERVER;
        } else {
            $storeURL = HTTP_SERVER;
        } 
        return $storeURL;
    }

    private function getCurrentStore($store_id) {    
        if($store_id && $store_id != 0) {
            $store = $this->model_setting_store->getStore($store_id);
        } else {
            $store['store_id'] = 0;
            $store['name'] = $this->config->get('config_name');
            $store['url'] = $this->getCatalogURL(); 
        }
        return $store;
    }
}
?>