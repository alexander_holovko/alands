<?php
class ControllerModuleTopStickers extends Controller {
	private $error = array();

	public function index() {
		$this->load->language('module/topstickers');
		$this->document->setTitle($this->language->get('heading_header'));
		$this->document->addScript('view/javascript/jquery/colorpicker.js');
		$this->document->addStyle('view/stylesheet/css/colorpicker.css');
		
		$this->load->model('module/topstickers');
		$this->load->model('setting/setting');
		if (($this->request->server['REQUEST_METHOD'] == 'POST') && $this->validate()) {
			$this->model_module_topstickers->truncateTopStickers();
			foreach ($this->request->post['custom_topsticker'] as $custom_topsticker) {
				$this->model_module_topstickers->addTopSticker($custom_topsticker);
			}
			$this->model_setting_setting->editSetting('topstickers_', $this->request->post);
			$this->session->data['success'] = $this->language->get('text_success');
			$this->response->redirect($this->url->link('extension/module', 'token=' . $this->session->data['token'], 'SSL'));
		}

		// Heading
		$data['heading_title'] = $this->language->get('heading_title');

		// Text
		$data['text_edit'] = $this->language->get('text_edit');
		$data['text_enabled'] = $this->language->get('text_enabled');
		$data['text_disabled'] = $this->language->get('text_disabled');
		$data['text_topleft'] = $this->language->get('text_topleft');
		$data['text_topright'] = $this->language->get('text_topright');

		//Buttons
		$data['button_save'] = $this->language->get('button_save');
		$data['button_cancel'] = $this->language->get('button_cancel');
		$data['button_remove'] = $this->language->get('button_remove');
		$data['button_custom_topsticker_add'] = $this->language->get('button_custom_topsticker_add');

		// Tab headers
		$data['text_tab_settings'] = $this->language->get('text_tab_settings');
		$data['text_tab_settings_title'] = $this->language->get('text_tab_settings_title');
		$data['text_tab_sold'] = $this->language->get('text_tab_sold');
		$data['text_tab_sold_title'] = $this->language->get('text_tab_sold_title');
		$data['text_tab_sale'] = $this->language->get('text_tab_sale');
		$data['text_tab_sale_title'] = $this->language->get('text_tab_sale_title');
		$data['text_tab_bestseller'] = $this->language->get('text_tab_bestseller');
		$data['text_tab_bestseller_title'] = $this->language->get('text_tab_bestseller_title');
		$data['text_tab_novelty'] = $this->language->get('text_tab_novelty');
		$data['text_tab_novelty_title'] = $this->language->get('text_tab_novelty_title');
		$data['text_tab_last'] = $this->language->get('text_tab_last');
		$data['text_tab_last_title'] = $this->language->get('text_tab_last_title');
		$data['text_tab_freeshipping'] = $this->language->get('text_tab_freeshipping');
		$data['text_tab_freeshipping_title'] = $this->language->get('text_tab_freeshipping_title');
		$data['text_tab_custom'] = $this->language->get('text_tab_custom');
		$data['text_tab_custom_title'] = $this->language->get('text_tab_custom_title');		

		// Entry
		$data['entry_topstickers_status'] = $this->language->get('entry_topstickers_status');
		$data['entry_topstickers_position'] = $this->language->get('entry_topstickers_position');

		$data['entry_sold_text'] = $this->language->get('entry_sold_text');
		$data['entry_sold_status'] = $this->language->get('entry_sold_status');
		$data['entry_sold_bg'] = $this->language->get('entry_sold_bg');

		$data['entry_sale_text'] = $this->language->get('entry_sale_text');
		$data['entry_sale_status'] = $this->language->get('entry_sale_status');
		$data['entry_sale_bg'] = $this->language->get('entry_sale_bg');

		$data['entry_bestseller_text'] = $this->language->get('entry_bestseller_text');
		$data['entry_bestseller_status'] = $this->language->get('entry_bestseller_status');
		$data['entry_bestseller_bg'] = $this->language->get('entry_bestseller_bg');
		$data['entry_bestseller_numbers'] = $this->language->get('entry_bestseller_numbers');

		$data['entry_novelty_text'] = $this->language->get('entry_novelty_text');
		$data['entry_novelty_status'] = $this->language->get('entry_novelty_status');
		$data['entry_novelty_bg'] = $this->language->get('entry_novelty_bg');
		$data['entry_novelty_days'] = $this->language->get('entry_novelty_days');

		$data['entry_last_text'] = $this->language->get('entry_last_text');
		$data['entry_last_status'] = $this->language->get('entry_last_status');
		$data['entry_last_bg'] = $this->language->get('entry_last_bg');
		$data['entry_last_numbers'] = $this->language->get('entry_last_numbers');

		$data['entry_freeshipping_text'] = $this->language->get('entry_freeshipping_text');
		$data['entry_freeshipping_status'] = $this->language->get('entry_freeshipping_status');
		$data['entry_freeshipping_bg'] = $this->language->get('entry_freeshipping_bg');
		$data['entry_freeshipping_price'] = $this->language->get('entry_freeshipping_price');

		$data['entry_custom_topstickers_title'] = $this->language->get('entry_custom_topstickers_title');
		$data['entry_custom_topstickers_text'] = $this->language->get('entry_custom_topstickers_text');
		$data['entry_custom_topstickers_bg'] = $this->language->get('entry_custom_topstickers_bg');
		$data['entry_custom_topstickers_status'] = $this->language->get('entry_custom_topstickers_status');

		if (isset($this->error['warning'])) {
			$data['error_warning'] = $this->error['warning'];
		} else {
			$data['error_warning'] = '';
		}

		$data['breadcrumbs'] = array();
		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('text_home'),
			'href' => $this->url->link('common/dashboard', 'token=' . $this->session->data['token'], 'SSL')
		);
		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('text_module'),
			'href' => $this->url->link('extension/module', 'token=' . $this->session->data['token'], 'SSL')
		);
		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('heading_title'),
			'href' => $this->url->link('module/topstickers', 'token=' . $this->session->data['token'], 'SSL')
		);

		$data['action'] = $this->url->link('module/topstickers', 'token=' . $this->session->data['token'], 'SSL');
		$data['cancel'] = $this->url->link('extension/module', 'token=' . $this->session->data['token'], 'SSL');

		// Languages
		$this->load->model('localisation/language');
		$data['languages'] = $this->model_localisation_language->getLanguages();

		// Main settings
		if (isset($this->request->post['topstickers_status'])) {
			$data['topstickers_status'] = $this->request->post['topstickers_status'];
		} else {
			$data['topstickers_status'] = $this->config->get('topstickers_status');
		}

		if (isset($this->request->post['topstickers_position'])) {
			$data['topstickers_position'] = $this->request->post['topstickers_position'];
		} elseif ($this->config->get('topstickers_position')) {
			$data['topstickers_position'] = $this->config->get('topstickers_position');
		} else {
			$data['topstickers_position'] = 0;
		}


		// SOLD sticker
		if (isset($this->request->post['topstickers_sold_text'])) {
			$data['topstickers_sold_text'] = $this->request->post['topstickers_sold_text'];
		} elseif ($this->config->get('topstickers_sold_text')) {
			$data['topstickers_sold_text'] = $this->config->get('topstickers_sold_text');
		} else {
			foreach ($data['languages'] as $language) {
				$data['topstickers_sold_text'][$language['language_id']] = $this->language->get('default_sold_text');
			}
		}

		if (isset($this->request->post['topstickers_sold_bg'])) {
			$data['topstickers_sold_bg'] = $this->request->post['topstickers_sold_bg'];
		} elseif ($this->config->get('topstickers_sold_bg')) {
			$data['topstickers_sold_bg'] = $this->config->get('topstickers_sold_bg');
		} else {
			$data['topstickers_sold_bg'] = $this->language->get('default_sold_bg');
		}

		if (isset($this->request->post['topstickers_sold_status'])) {
			$data['topstickers_sold_status'] = $this->request->post['topstickers_sold_status'];
		} else {
			$data['topstickers_sold_status'] = $this->config->get('topstickers_sold_status');
		}


		// SALE sticker
		if (isset($this->request->post['topstickers_sale_text'])) {
			$data['topstickers_sale_text'] = $this->request->post['topstickers_sale_text'];
		} elseif ($this->config->get('topstickers_sale_text')) {
			$data['topstickers_sale_text'] = $this->config->get('topstickers_sale_text');
		} else {
			foreach ($data['languages'] as $language) {
				$data['topstickers_sale_text'][$language['language_id']] = $this->language->get('default_sale_text');
			}
		}

		if (isset($this->request->post['topstickers_sale_bg'])) {
			$data['topstickers_sale_bg'] = $this->request->post['topstickers_sale_bg'];
		} elseif ($this->config->get('topstickers_sale_bg')) {
			$data['topstickers_sale_bg'] = $this->config->get('topstickers_sale_bg');
		} else {
			$data['topstickers_sale_bg'] = $this->language->get('default_sale_bg');
		}

		if (isset($this->request->post['topstickers_sale_status'])) {
			$data['topstickers_sale_status'] = $this->request->post['topstickers_sale_status'];
		} else {
			$data['topstickers_sale_status'] = $this->config->get('topstickers_sale_status');
		}


		// BESTSELLER sticker
		if (isset($this->request->post['topstickers_bestseller_text'])) {
			$data['topstickers_bestseller_text'] = $this->request->post['topstickers_bestseller_text'];
		} elseif ($this->config->get('topstickers_bestseller_text')) {
			$data['topstickers_bestseller_text'] = $this->config->get('topstickers_bestseller_text');
		} else {
			foreach ($data['languages'] as $language) {
				$data['topstickers_bestseller_text'][$language['language_id']] = $this->language->get('default_bestseller_text');
			}
		}

		if (isset($this->request->post['topstickers_bestseller_bg'])) {
			$data['topstickers_bestseller_bg'] = $this->request->post['topstickers_bestseller_bg'];
		} elseif ($this->config->get('topstickers_bestseller_bg')) {
			$data['topstickers_bestseller_bg'] = $this->config->get('topstickers_bestseller_bg');
		} else {
			$data['topstickers_bestseller_bg'] = $this->language->get('default_bestseller_bg');
		}

		if (isset($this->request->post['topstickers_bestseller_numbers'])) {
			$data['topstickers_bestseller_numbers'] = $this->request->post['topstickers_bestseller_numbers'];
		} elseif ($this->config->get('topstickers_bestseller_bg')) {
			$data['topstickers_bestseller_numbers'] = $this->config->get('topstickers_bestseller_numbers');
		} else {
			$data['topstickers_bestseller_numbers'] = false;
		}

		if (isset($this->request->post['topstickers_bestseller_status'])) {
			$data['topstickers_bestseller_status'] = $this->request->post['topstickers_bestseller_status'];
		} else {
			$data['topstickers_bestseller_status'] = $this->config->get('topstickers_bestseller_status');
		}


		// NOVELTY sticker
		if (isset($this->request->post['topstickers_novelty_text'])) {
			$data['topstickers_novelty_text'] = $this->request->post['topstickers_novelty_text'];
		} elseif ($this->config->get('topstickers_novelty_text')) {
			$data['topstickers_novelty_text'] = $this->config->get('topstickers_novelty_text');
		} else {
			foreach ($data['languages'] as $language) {
				$data['topstickers_novelty_text'][$language['language_id']] = $this->language->get('default_novelty_text');
			}
		}

		if (isset($this->request->post['topstickers_novelty_bg'])) {
			$data['topstickers_novelty_bg'] = $this->request->post['topstickers_novelty_bg'];
		} elseif ($this->config->get('topstickers_novelty_bg')) {
			$data['topstickers_novelty_bg'] = $this->config->get('topstickers_novelty_bg');
		} else {
			$data['topstickers_novelty_bg'] = $this->language->get('default_novelty_bg');
		}

		if (isset($this->request->post['topstickers_novelty_days'])) {
			$data['topstickers_novelty_days'] = $this->request->post['topstickers_novelty_days'];
		} elseif ($this->config->get('topstickers_novelty_days')) {
			$data['topstickers_novelty_days'] = $this->config->get('topstickers_novelty_days');
		} else {
			$data['topstickers_novelty_days'] = false;
		}

		if (isset($this->request->post['topstickers_novelty_status'])) {
			$data['topstickers_novelty_status'] = $this->request->post['topstickers_novelty_status'];
		} else {
			$data['topstickers_novelty_status'] = $this->config->get('topstickers_novelty_status');
		}


		// LAST sticker
		if (isset($this->request->post['topstickers_last_text'])) {
			$data['topstickers_last_text'] = $this->request->post['topstickers_last_text'];
		} elseif ($this->config->get('topstickers_last_text')) {
			$data['topstickers_last_text'] = $this->config->get('topstickers_last_text');
		} else {
			foreach ($data['languages'] as $language) {
				$data['topstickers_last_text'][$language['language_id']] = $this->language->get('default_last_text');
			}
		}

		if (isset($this->request->post['topstickers_last_bg'])) {
			$data['topstickers_last_bg'] = $this->request->post['topstickers_last_bg'];
		} elseif ($this->config->get('topstickers_last_bg')) {
			$data['topstickers_last_bg'] = $this->config->get('topstickers_last_bg');
		} else {
			$data['topstickers_last_bg'] = $this->language->get('default_last_bg');
		}

		if (isset($this->request->post['topstickers_last_numbers'])) {
			$data['topstickers_last_numbers'] = $this->request->post['topstickers_last_numbers'];
		} elseif ($this->config->get('topstickers_last_numbers')) {
			$data['topstickers_last_numbers'] = $this->config->get('topstickers_last_numbers');
		} else {
			$data['topstickers_last_numbers'] = false;
		}

		if (isset($this->request->post['topstickers_last_status'])) {
			$data['topstickers_last_status'] = $this->request->post['topstickers_last_status'];
		} else {
			$data['topstickers_last_status'] = $this->config->get('topstickers_last_status');
		}


		// FREE SHIPPING sticker
		if (isset($this->request->post['topstickers_freeshipping_text'])) {
			$data['topstickers_freeshipping_text'] = $this->request->post['topstickers_freeshipping_text'];
		} elseif ($this->config->get('topstickers_freeshipping_text')) {
			$data['topstickers_freeshipping_text'] = $this->config->get('topstickers_freeshipping_text');
		} else {
			foreach ($data['languages'] as $language) {
				$data['topstickers_freeshipping_text'][$language['language_id']] = $this->language->get('default_freeshipping_text');
			}
		}

		if (isset($this->request->post['topstickers_freeshipping_bg'])) {
			$data['topstickers_freeshipping_bg'] = $this->request->post['topstickers_freeshipping_bg'];
		} elseif ($this->config->get('topstickers_freeshipping_bg')) {
			$data['topstickers_freeshipping_bg'] = $this->config->get('topstickers_freeshipping_bg');
		} else {
			$data['topstickers_freeshipping_bg'] = $this->language->get('default_freeshipping_bg');
		}

		if (isset($this->request->post['topstickers_freeshipping_price'])) {
			$data['topstickers_freeshipping_price'] = $this->request->post['topstickers_freeshipping_price'];
		} elseif ($this->config->get('topstickers_freeshipping_price')) {
			$data['topstickers_freeshipping_price'] = $this->config->get('topstickers_freeshipping_price');
		} else {
			$data['topstickers_freeshipping_price'] = false;
		}

		if (isset($this->request->post['topstickers_freeshipping_status'])) {
			$data['topstickers_freeshipping_status'] = $this->request->post['topstickers_freeshipping_status'];
		} else {
			$data['topstickers_freeshipping_status'] = $this->config->get('topstickers_freeshipping_status');
		}
		
		
		// CUSTOM stickers
		$custom_topstickers = $this->model_module_topstickers->getTopStickers();
		if (isset($this->request->post['custom_topsticker'])) {
			$custom_topstickers = $this->request->post['custom_topsticker'];
		} elseif (!empty($custom_topstickers)) {
			$custom_topstickers = $this->model_module_topstickers->getTopStickers();
		} else {
			$custom_topstickers = array();
		}

		$data['custom_topstickers'] = array();

		foreach ($custom_topstickers as $custom_topsticker) {
			$data['custom_topstickers'][] = array(
				'topsticker_id'	=> $custom_topsticker['topsticker_id'],
				'name'			=> $custom_topsticker['name'],
				'text' 			=> json_decode($custom_topsticker['text'], true),
				'bg_color'		=> $custom_topsticker['bg_color'],
				'status'		=> $custom_topsticker['status'],
			);
		}		
		
		// var_dump($data['custom_topstickers']);
		
		$data['header'] = $this->load->controller('common/header');
		$data['column_left'] = $this->load->controller('common/column_left');
		$data['footer'] = $this->load->controller('common/footer');

		$this->response->setOutput($this->load->view('module/topstickers.tpl', $data));
	}

	protected function validate() {
		if (!$this->user->hasPermission('modify', 'module/topstickers')) {
			$this->error['warning'] = $this->language->get('error_permission');
		}
		return !$this->error;
	}
}