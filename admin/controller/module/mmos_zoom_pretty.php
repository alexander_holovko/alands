<?php

class ControllerModuleMmosZoomPretty extends Controller {

    private $error = array();

    public function index() {

        $this->load->language('module/mmos_zoom_pretty');

        $this->document->setTitle($this->language->get('heading_title'));

        $this->load->model('setting/setting');

        if (($this->request->server['REQUEST_METHOD'] == 'POST') && $this->validate()) {
            $this->model_setting_setting->editSetting('mmos_zoom_pretty', $this->request->post);

            $this->session->data['success'] = $this->language->get('text_success');

            $this->response->redirect($this->url->link('module/mmos_zoom_pretty', 'token=' . $this->session->data['token'], 'SSL'));
        }

        $this->document->addStyle('view/javascript/colorpicker/css/colorpicker.css');
        $this->document->addScript('view/javascript/colorpicker/js/bootstrap-colorpicker.js');

        $data['heading_title'] = $this->language->get('heading_title1');
        $data['MMOS_version'] = '4.2';
        $data['MMOS_code_id'] = 'MMOSOC112';
        $data['tab_setting'] = $this->language->get('tab_setting');
        $data['tab_support'] = $this->language->get('tab_support');

        $data['text_show_title_cloud_zoom'] = $this->language->get('text_show_title_cloud_zoom');
        $data['text_border_cloud_zoom'] = $this->language->get('text_border_cloud_zoom');
        $data['color_border_cloud_zoom'] = $this->language->get('color_border_cloud_zoom');
        $data['text_status'] = $this->language->get('text_status');
        $data['text_resize_cloud_zoom'] = $this->language->get('text_resize_cloud_zoom');
        $data['text_autoplay_slideshow'] = $this->language->get('text_autoplay_slideshow');
        $data['text_opacity'] = $this->language->get('text_opacity');
        $data['text_show_title'] = $this->language->get('text_show_title');
        $data['text_theme_popup'] = $this->language->get('text_theme_popup');
        $data['text_speed_popup'] = $this->language->get('text_speed_popup');
        $data['text_resize_popup'] = $this->language->get('text_resize_popup');
        $data['text_slideshow'] = $this->language->get('text_slideshow');
        $data['text_show_popup_button'] = $this->language->get('text_show_popup_button');
        $data['text_social_button'] = $this->language->get('text_social_button');
        $data['text_carousel'] = $this->language->get('text_carousel');
        $data['text_edit_module'] = $this->language->get('text_edit_module');
        $data['text_enabled'] = $this->language->get('text_enabled');
        $data['text_disabled'] = $this->language->get('text_disabled');
        $data['text_item_carousel'] = $this->language->get('text_item_carousel');
        $data['help_slideshow'] = $this->language->get('help_slideshow');
        $data['help_opacity'] = $this->language->get('help_opacity');
        $data['help_border_cloud_zoom'] = $this->language->get('help_border_cloud_zoom');
        $data['help_item_carousel'] = $this->language->get('help_item_carousel');
        $data['text_nav_but_color'] = $this->language->get('text_nav_but_color');

        $data['button_save'] = $this->language->get('button_save');
        $data['button_cancel'] = $this->language->get('button_cancel');

        if (isset($this->error['warning'])) {
            $data['error_warning'] = $this->error['warning'];
        } else {
            $data['error_warning'] = '';
        }

        $data['breadcrumbs'] = array();

        $data['breadcrumbs'][] = array(
            'text' => $this->language->get('text_home'),
            'href' => $this->url->link('common/dashboard', 'token=' . $this->session->data['token'], 'SSL')
        );

        $data['breadcrumbs'][] = array(
            'text' => $this->language->get('text_module'),
            'href' => $this->url->link('extension/module', 'token=' . $this->session->data['token'], 'SSL')
        );

        $data['breadcrumbs'][] = array(
            'text' => $this->language->get('heading_title1'),
            'href' => $this->url->link('module/mmos_zoom_pretty', 'token=' . $this->session->data['token'], 'SSL')
        );

        if (isset($this->request->post['mmos_zoom_pretty'])) {
            $data['mmos_zoom_pretty'] = $this->request->post['mmos_zoom_pretty'];
        } else {
            $data['mmos_zoom_pretty'] = $this->config->get('mmos_zoom_pretty');
        }
        $data['action'] = $this->url->link('module/mmos_zoom_pretty', 'token=' . $this->session->data['token'], 'SSL');

        $data['cancel'] = $this->url->link('extension/module', 'token=' . $this->session->data['token'], 'SSL');

        $data['token'] = $this->session->data['token'];

        $data['header'] = $this->load->controller('common/header');
        $data['column_left'] = $this->load->controller('common/column_left');
        $data['footer'] = $this->load->controller('common/footer');

        $this->response->setOutput($this->load->view('module/mmos_zoom_pretty.tpl', $data));
    }

    private function validate() {
        if (!$this->user->hasPermission('modify', 'module/mmos_zoom_pretty')) {
            $this->error['warning'] = $this->language->get('error_permission');
        }

        return !$this->error;
    }

    public function uninstall() {
        if ($this->user->hasPermission('modify', 'extension/module')) {
            $this->load->model('setting/setting');
            $this->model_setting_setting->deleteSetting('mmos_zoom_pretty');
            $this->vqmod_protect();
        }
    }

    public function install() {

        if ($this->user->hasPermission('modify', 'extension/module')) {
            $initial = array(
                'mmos_zoom_pretty' => array(
                    'status' => 0,
                    'theme_popup' => 'pp_default',
                    'speed_popup' => 'normal',
                    'title_popup' => 0,
                    'autoplay' => 1,
                    'resize_popup' => 1,
                    'speed_slide' => 5000,
                    'opacity' => 0.8,
                    'popup_button' => 1,
                    'social_button' => 1,
                    'title_cloudzoom' => 0,
                    'border_cloudzoom' => 0,
                    'nav_but_color' => '#ed7e4f',
                    'color_border_cloudzoom' => '#ffffff',
                    'zoom_size' => 500,
                    'carousel' => 1,
                    'item_carousel' => 4
                )
            );
            $this->load->model('setting/setting');
            $this->model_setting_setting->editSetting('mmos_zoom_pretty', $initial);
            $this->vqmod_protect(1);
            $this->response->redirect($this->url->link('module/mmos_zoom_pretty', 'token=' . $this->session->data['token'], 'SSL'));
        }
    }

    protected function vqmod_protect($action = 0) {
        // action 1 =  install; 0: uninstall
        $vqmod_file = 'MMOSolution_cloud_zoom_pretty_photos.xml';
        if ($this->user->hasPermission('modify', 'extension/module')) {
            $MMOS_ROOT_DIR = substr(DIR_APPLICATION, 0, strrpos(DIR_APPLICATION, '/', -2)) . '/vqmod/xml/';
            if ($action == 1) {
                if (is_file($MMOS_ROOT_DIR . $vqmod_file . '_mmosolution')) {
                    @rename($MMOS_ROOT_DIR . $vqmod_file . '_mmosolution', $MMOS_ROOT_DIR . $vqmod_file);
                }
            } else {
                if (is_file($MMOS_ROOT_DIR . $vqmod_file)) {
                    @rename($MMOS_ROOT_DIR . $vqmod_file, $MMOS_ROOT_DIR . $vqmod_file . '_mmosolution');
                }
            }
        }
    }

}

?>