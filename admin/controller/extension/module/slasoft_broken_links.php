<?php
class ControllerExtensionModuleSlasoftBrokenLinks extends Controller {
	private $error = array();
	private $data;
	private $version = '3.0';
    public function __construct($registry) {
        parent::__construct($registry);
        $this->_moduleName = "SlaSoft Broken Links";
        $this->_moduleSysName = "slasoft_broken_links";
    }

	public function index() {
		$this->language->load('extension/module/' . $this->_moduleSysName);
		$this->data = $this->language->all();
		$this->document->setTitle($this->language->get('heading_title_raw'));		
				
		if (($this->request->server['REQUEST_METHOD'] == 'POST') && $this->validate()) {
			$this->load->model('setting/setting');
			$this->model_setting_setting->editSetting($this->_moduleSysName, $this->request->post);
			$this->session->data['success'] = $this->language->get('text_success');
						
            if( isset($this->request->get['apply'])) {
                $this->response->redirect($this->url->link('extension/module/' . $this->_moduleSysName, 'user_token=' . $this->session->data['user_token'], true));
            } else {
                $this->response->redirect($this->url->link('extension/extension/', 'type=module&user_token=' . $this->session->data['user_token'], true));
            }
		}

        if (isset($this->error['warning'])) {
            $this->data['error_warning'] = $this->error['warning'];
        } else {
            $this->data['error_warning'] = '';
        }

		$this->data['action'] = $this->url->link('extension/module/' . $this->_moduleSysName, 'user_token=' . $this->session->data['user_token'], true);

		$this->data['cancel'] = $this->url->link('marketplace/extension', 'type=module&user_token=' . $this->session->data['user_token'], true);        $this->data['clear'] = $this->url->link('extension/module/' . $this->_moduleSysName . '/clear', 'user_token=' . $this->session->data['user_token'], true);
        $this->data['save']   = $this->url->link('extension/module/' . $this->_moduleSysName, 'user_token=' . $this->session->data['user_token'] . "&apply=1", true);
        $this->data['save_and_quit'] = $this->url->link('extension/extension', 'type=module&user_token=' . $this->session->data['user_token'], true);
		$this->data['view_log'] = $this->url->link('extension/module/' . $this->_moduleSysName . '/view_log', 'user_token=' . $this->session->data['user_token'], true);

		if (isset($this->session->data['success'])) {
			$this->data['success'] = $this->session->data['success'];
			unset($this->session->data['success']);
		}
		
  		$this->data['breadcrumbs'] = array();

   		$this->data['breadcrumbs'][] = array(
       		'text'      => $this->language->get('text_home'),
			'href'      => $this->url->link('common/dashboard', 'user_token=' . $this->session->data['user_token'], true),
   		);

   		$this->data['breadcrumbs'][] = array(
       		'text'      => $this->language->get('text_module'),
			'href'      => $this->url->link('marketplace/extension', 'type=module&user_token=' . $this->session->data['user_token'], true)
   		);
		
   		$this->data['breadcrumbs'][] = array(
       		'text'      => $this->language->get('heading_title'),
			'href'      => $this->url->link('extension/module/' . $this->_moduleSysName, 'user_token=' . $this->session->data['user_token'], true),
   		);


		if (isset($this->request->post[$this->_moduleSysName . '_enable'])) {
			$this->data[$this->_moduleSysName . '_enable'] = $this->request->post[$this->_moduleSysName . '_enable'];
		} else {
			$this->data[$this->_moduleSysName . '_enable'] = $this->config->get($this->_moduleSysName . '_enable');
		}

		$this->data['module_name'] = $this->_moduleSysName;
		$this->data['enabled_disabled'] = array(
							0 => $this->language->get('text_disabled'),
							1 => $this->language->get('text_enabled')
		);
		$this->data['user_token'] = $this->session->data['user_token'];
		
		$this->data['header'] = $this->load->controller('common/header');
		$this->data['column_left'] = $this->load->controller('common/column_left');
		$this->data['footer'] = $this->load->controller('common/footer');
		$this->response->setOutput($this->load->view('extension/module/' . $this->_moduleSysName, $this->data));
	}

	protected function validate() {
		if (!$this->user->hasPermission('modify', 'extension/module/' . $this->_moduleSysName)) {
			$this->error['warning'] = $this->language->get('error_permission');
		}
		
		if (!$this->error) {
			return true;
		} else {
			return false;
		}	
	}

	protected function validate_() {
		$this->load->model('extension/module/' . $this->_moduleSysName);
		$this->registry->get('model_extension_module_' . $this->_moduleSysName )->checkTable();

		if (!$this->user->hasPermission('access', 'extension/module/' . $this->_moduleSysName)) {
			$this->error['warning'] = $this->language->get('error_permission_view');
		}
		
		if (!$this->error) {
			return true;
		} else {
			return false;
		}	
	}

    public function view_log() {
		$this->language->load('extension/module/' . $this->_moduleSysName);
		$this->data = $this->language->all();
		$this->document->setTitle($this->language->get('heading_log'));		
		
		if ($this->validate_()) {

			$this->data['user_token'] = $this->session->data['user_token'];
			$this->data['clear'] = $this->url->link('extension/module/' . $this->_moduleSysName . '/clear', 'user_token=' . $this->session->data['user_token'], true);
			$this->data['settings'] = $this->url->link('extension/module/' . $this->_moduleSysName , 'user_token=' . $this->session->data['user_token'], true);
			$this->data['export'] 	 = $this->url->link('extension/module/' . $this->_moduleSysName . '/export', 'user_token=' . $this->session->data['user_token'],true);
			$this->data['delete'] 	 = $this->url->link('extension/module/' . $this->_moduleSysName . '/not_foundDelete', 'user_token=' . $this->session->data['user_token'],true);
			$this->data['action'] 	 = $this->url->link('extension/module/' . $this->_moduleSysName . '/view_log', 'user_token=' . $this->session->data['user_token'], true);
			$this->data['breadcrumbs'] = array();
	
			$this->data['breadcrumbs'][] = array(
				'text'      => $this->language->get('text_home'),
				'href'      => $this->url->link('common/dashboard', 'user_token=' . $this->session->data['user_token'], true),
			);
			
			$this->data['breadcrumbs'][] = array(
				'text'      => $this->language->get('text_module'),
				'href'      => $this->url->link('marketplace/extension', 'type=module&user_token=' . $this->session->data['user_token'], true)
			);

			$this->data['breadcrumbs'][] = array(
				'text'      => $this->language->get('text_view_log'),
				'href'      => $this->url->link('extension/module/' . $this->_moduleSysName, 'user_token=' . $this->session->data['user_token'], true)
			);

			if (isset($this->request->get['page'])) {
				$page = $this->request->get['page'];
			} else {
				$page = 1;
			}

			$filter = array();
			if (isset($this->request->get['filter'])) {
				$filter = $this->request->get['filter'];
			}
			if (isset($this->request->post['filter'])) {
				$filter = $this->request->post['filter'];
			}
			
			$limit = ($this->config->get('config_admin_limit'))?$this->config->get('config_admin_limit'):50;
			$filer_data = array(
				'limit' => $limit,
				'page' => $page,
				'filter' => $filter,
				'start'  => ($page - 1) * $limit,
			);
			$this->load->model('extension/module/' . $this->_moduleSysName);
			
			$total = $this->registry->get('model_extension_module_' . $this->_moduleSysName )->getTotalRecords($filer_data);
				
			$results = $this->registry->get('model_extension_module_' . $this->_moduleSysName )->getRecords($filer_data);
			$this->data['results'] = array();
            include(DIR_SYSTEM."vendor/geoip/geoip.inc");
            $gi = geoip_open(DIR_SYSTEM."vendor/geoip/GeoIP.dat", GEOIP_STANDARD);

			foreach ($results as $result) {
				$country = '';
				$flag = false;
				$flag = strtolower(geoip_country_code_by_addr($gi, $result['ip']));
				$country = geoip_country_name_by_addr($gi, $result['ip']);
				if (file_exists(DIR_APPLICATION.'view/image/flags/' . $flag . '.png')) 
					$flag = 'view/image/flags/' . $flag . '.png';
				else {
					$flag = false;
				}

				$ip = array(
					'ip'      => $result['ip'],
					'country' => $country,
					'flag'    => $flag,
				);
				$referer = strip_tags($result['referer']);
				if (mb_strlen($result['referer'], 'utf-8') > 120) {
					$length = mb_strlen($result['referer'], 'utf-8');
					$short_referer = mb_substr($result['referer'], 0, 60, 'utf-8') .' ... ' . mb_substr($result['referer'], ($length - 60/2), $length, 'utf-8');
				} else {
					$short_referer = $result['referer'];
				}

				$this->data['results'][] = array(
					'notfound_id'   => $result['notfound_id'],
					'date_record'   => $result['date_record'],
					'ip'            => $ip,
					'request_uri'   => $result['request_uri'],
					'browser'       => $result['browser'],
					'short_referer' => $short_referer,
					'href'          => ($short_referer == 'Referer not detected' || $short_referer == '')?'':$result['referer']
				);
			}
			$url = '';
			if ($filter) {
				$url_filter = http_build_query($filter, '', '&');
				$url .= $url_filter;
			}
	
			if (isset($this->request->get['sort'])) {
				$url .= '&sort=' . $this->request->get['sort'];
			}
	
			if (isset($this->request->get['order'])) {
				$url .= '&order=' . $this->request->get['order'];
			}

			if (isset($this->error['warning'])) {
				$this->data['error_warning'] = $this->error['warning'];
			} else {
				$this->data['error_warning'] = '';
			}
			if (isset($this->request->post['selected'])) {
				$this->data['selected'] = $this->request->post['selected'];
			} else {
				$this->data['selected'] = array();;
			}
			$empty_filter= array(
				'browser' =>'',
				'request' => '',
				'referer' => '',
			);

			$this->data['filter'] = array_merge($empty_filter,$filter);
			$pagination = new Pagination();
			$pagination->total = $total;
			$pagination->page = $page;
			$pagination->limit = $limit;
			$pagination->text = $this->language->get('text_pagination');
			$pagination->url = $this->url->link('extension/module/'. $this->_moduleSysName . '/view_log', 'user_token=' . $this->session->data['user_token'] . $url . '&page={page}', true);
			$this->data['pagination'] = $pagination->render();
			$this->data['result'] = sprintf($this->language->get('text_pagination'), ($total) ? (($page - 1) * $limit) + 1 : 0, ((($page - 1) * $limit) > ($total - $limit)) ? $total : ((($page - 1) * $limit) + $limit), $total, ceil($total / $limit));
		
			$this->data['header'] = $this->load->controller('common/header');
			$this->data['column_left'] = $this->load->controller('common/column_left');
			$this->data['footer'] = $this->load->controller('common/footer');
			$this->response->setOutput($this->load->view('tool/' . $this->_moduleSysName, $this->data));
		} else {
			$this->response->redirect($this->url->link('extension/module/' . $this->_moduleSysName, 'user_token=' . $this->session->data['user_token'], true));
		}
	}
	
    public function clear() {
        $this->load->model('extension/module/' . $this->_moduleSysName);
        $this->registry->get("model_extension_module_" . $this->_moduleSysName )->clear();
		$this->response->redirect($this->url->link('extension/module/' . $this->_moduleSysName . '/view_log', 'user_token=' . $this->session->data['user_token'], true));
    }    

    public function not_foundDelete() {
        $this->load->model('extension/module/' . $this->_moduleSysName);
        $this->registry->get("model_extension_module_" . $this->_moduleSysName )->not_foundDelete();
		$this->response->redirect($this->url->link('extension/module/' . $this->_moduleSysName . '/view_log', 'user_token=' . $this->session->data['user_token'], true));
    }    

	public function export() {
		if ($this->request->server['REQUEST_METHOD'] == 'POST' && $this->validate_()) {
			$this->load->model('extension/module/' . $this->_moduleSysName);
			
			$total = $this->registry->get('model_extension_module_' . $this->_moduleSysName )->getTotalRecords();
			$filer_data = array(
				'limit' => $total,
				'page' => 0,
				'start'  => 0,
			);
				
			$this->data['results'] = $this->registry->get('model_extension_module_' . $this->_moduleSysName )->getRecords($filer_data);
			
			header("Content-Type: text/csv");
			header("Content-Disposition: attachment; filename=not_found-".date('d-m-Y').".csv");
			header("Cache-Control: no-cache, no-store, must-revalidate"); // HTTP 1.1
			header("Pragma: no-cache"); // HTTP 1.0
			header("Expires: 0"); // Proxies
		 	$output = fopen("php://output", "w"); 
			foreach($this->data['results']as $csv_data) {
				$csv = array(
					'ip'          => $csv_data['ip'],
					'browser'     => $csv_data['browser'],
					'request_uri' => $csv_data['request_uri'],
					'referer'     => $csv_data['referer'],
					'date_record' => $csv_data['date_record'],
				);
				fputcsv($output, $csv, ';', '"'); // here you can change delimiter/enclosure
			}
			fclose($output);
		} 
	}

    public function install(){
        $this->load->model('extension/module/' . $this->_moduleSysName);
        $this->registry->get('model_extension_module_' . $this->_moduleSysName)->install();
		$events = $this->getEvents();
		$this->load->model('setting/event');
		foreach ($events as $code=>$value) {
			$this->model_setting_event->deleteEventByCode($code);
			$this->model_setting_event->addEvent($code, $value['trigger'], $value['action'], 1);
		}		
   }

    public function uninstall(){
        $this->load->model('extension/module/' . $this->_moduleSysName);
        $this->registry->get('model_extension_module_' . $this->_moduleSysName)->uninstall();
		$events = $this->getEvents();
		$this->load->model('setting/event');
		foreach ($events as $code=>$value) {
			$this->model_setting_event->deleteEventByCode($code);
		}
    }
	protected function getEvents() {
		$events = array(
			'slasoft_broken_links' => array(
				'trigger' => 'catalog/view/error/not_found/before',
				'action'  => 'extension/event/slasoft_broken_links',
			),
		);
		return $events;
	}

}