<?php echo $header; ?><?php echo $column_left; ?>
<div id="content">
  <div class="page-header">
    <div class="container-fluid">
      <div class="pull-right">
        <button type="submit" form="form-setting" data-toggle="tooltip" title="<?php echo $lng['button_save']; ?>" class="btn btn-primary"><i class="fa fa-save"></i></button>
        <a href="<?php echo $cancel; ?>" data-toggle="tooltip" title="<?php echo $lng['button_cancel']; ?>" class="btn btn-default"><i class="fa fa-reply"></i></a></div>
      <h1><?php echo $heading_title; ?></h1>
      <ul class="breadcrumb">
        <?php foreach ($breadcrumbs as $breadcrumb) { ?>
        <li><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a></li>
        <?php } ?>
      </ul>
    </div>
  </div>
  <div class="container-fluid">
    <?php if ($warning) { ?>
    <div class="alert alert-danger"><i class="fa fa-exclamation-circle"></i> <?php echo $error_warning; ?>
      <button type="button" class="close" data-dismiss="alert">&times;</button>
    </div>
    <?php } ?>
    <?php if ($success) { ?>
    <div class="alert alert-success"><i class="fa fa-check-circle"></i> <?php echo $success; ?>
      <button type="button" class="close" data-dismiss="alert">&times;</button>
    </div>
    <?php } ?>
    <div class="panel panel-default">
      <div class="panel-heading">
        <h3 class="panel-title"><i class="fa fa-pencil"></i> <?php echo $lng['text_edit']; ?></h3>
      </div>
      <div class="panel-body">
        <ul class="nav nav-tabs"> 
          <li class="<?php if ($tab == 'setting') echo 'active'; ?>"><a href="#tab-setting" data-toggle="tab"><?php echo $lng['tab_setting']; ?></a></li>
        </ul>
        <div class="tab-content">
          <div class="tab-pane <?php if ($tab == 'setting') echo 'active'; ?>" id="tab-setting">
            <div class="alert alert-info"><i class="fa fa-info-circle"></i> <?php echo $text_layout; ?>
              <button type="button" class="close" data-dismiss="alert">&times;</button>
            </div>
            <form action="<?php echo $action; ?>" method="post" enctype="multipart/form-data" id="form-setting" class="form-horizontal">
              <input type="hidden" name="deluxe_voucher_installed" value="<?php echo $deluxe_voucher_installed; ?>" />
              <div class="form-group">
                <label class="col-sm-2 control-label" for="input-status"><?php echo $lng['status']; ?></label>
                <div class="col-sm-4">
                  <select name="deluxe_voucher_status" id="input-status" class="form-control">
                    <option value="1" selected="selected"><?php echo $lng['text_enabled']; ?></option>
                    <option <?php if (!$deluxe_voucher_status) echo 'selected'; ?> value="0"><?php echo $lng['text_disabled']; ?></option>
                  </select>
                </div>
              </div>
              <div class="form-group">
                <label class="col-sm-2 control-label"><?php echo $lng['price_list']; ?></label>
                <div class="col-sm-4"><input type="text" name="deluxe_voucher_prices" value="<?php echo $deluxe_voucher_prices; ?>" class="form-control" /></div>
              </div>
              <div class="form-group">
                <label class="col-sm-2 control-label"><?php echo $lng['default_price']; ?></label>
                <div class="col-sm-2"><input type="text" name="deluxe_voucher_default_price" value="<?php echo $deluxe_voucher_default_price; ?>" class="form-control" /></div>
              </div>
              <div class="form-group">
                <label class="col-sm-2 control-label"><?php echo $lng['hide_label']; ?></label>
                <div class="col-sm-10 checkbox"><label><input type="checkbox" name="deluxe_voucher_hide_label" value="1" <?php if ($deluxe_voucher_hide_label) echo 'checked'; ?> /></label></div>
              </div>
              <div class="form-group">
                <label class="col-sm-2 control-label"><?php echo $lng['hide_image']; ?></label>
                <div class="col-sm-10 checkbox"><label><input type="checkbox" name="deluxe_voucher_hide_image" value="1" <?php if ($deluxe_voucher_hide_image) echo 'checked'; ?> /></label></div>
              </div>
              <div class="form-group">
                <label class="col-sm-2 control-label"><?php echo $lng['image_size']; ?></label>
                <div class="col-sm-2"><?php echo $lng['width']?> <input type="number" name="deluxe_voucher_width" value="<?php echo $deluxe_voucher_width; ?>" class="form-control" /></div>
                <div class="col-sm-2"><?php echo $lng['height']?> <input type="number" name="deluxe_voucher_height" value="<?php echo $deluxe_voucher_height; ?>" class="form-control" /></div>
              </div>
              <div class="form-group">
                <label class="col-sm-2 control-label"><?php echo $lng['hide_date']; ?></label>
                <div class="col-sm-10 checkbox"><label><input type="checkbox" name="deluxe_voucher_hide_date" value="1" <?php if ($deluxe_voucher_hide_date) echo 'checked'; ?> /></label></div>
              </div>
              <div class="form-group">
                <label class="col-sm-2 control-label"><?php echo $lng['max_day']; ?></label>
                <div class="col-sm-2"><input type="number" name="deluxe_voucher_max_day" value="<?php echo $deluxe_voucher_max_day; ?>" class="form-control" /></div>
              </div>
              <div class="form-group">
                <label class="col-sm-2 control-label"><?php echo $lng['cron']; ?></label>
                <div class="col-sm-10"><input type="text" value="<?php echo $cron; ?>" class="form-control" readonly onclick="$(this).select();" /></div>
              </div>
            </form>
          </div><!-- tab-setting -->
        </div>
      </div>
    </div>
    <div class="row" id="support"><a href="mailto:trile7@gmail.com?Subject=<?php echo urlencode($heading_title); ?>">Support Email</a></div>
    <div class="row" id="upgrade">Upgrade:
      <ol>
        <li><a href="http://tlecoding.gurleegirl.com/deluxe_voucher" target="_blank">Check for new upgrade</a></li>
        <li>Download, unzip, and upload file to opencart installation root</li>
        <li><a href="index.php?route=extension/module/deluxe_voucher/install&token=<?php echo $token; ?>&redirect=1">Complete upgrade process</a></li>
      </ol>
      <a href="http://tlecoding.gurleegirl.com" target="_blank">My other extensions</a>
    </div>
  </div>
</div>
<?php echo $footer; ?>
<script type="text/javascript">
function upgrade() {
  $("#upgrade").find(".success, .warning").remove();
  $.get("index.php?route=extension/module/deluxe_voucher/install&token=<?php echo $token; ?>", function(html) {
    $("#upgrade").append(html);
  });
} //upgrade end
</script>
<?php echo $footer; ?>
