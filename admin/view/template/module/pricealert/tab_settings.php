<div class="container-fluid">
	<div class="row">
      <div class="col-xs-3">
        <h5><strong>Уведомлять клиента когда:</strong></h5>
        <span class="help"><i class="fa fa-info-circle"></i>&nbsp;Когда уведомлять клиента?</span>
      </div>
      <div class="col-xs-3">
        <select name="<?php echo $moduleName; ?>[Alert]" class="form-control">
            <option value="default" <?php echo ((isset($moduleData['Alert']) && $moduleData['Alert'] == 'default')) ? 'selected=selected' : '' ?>>Меняеться цена</option>
           <option value="lower" <?php echo ((isset($moduleData['Alert']) && $moduleData['Alert'] == 'lower')) ? 'selected=selected' : '' ?>>Товар подешевел</option>
           <option value="higher" <?php echo ((isset($moduleData['Alert']) && $moduleData['Alert'] == 'higher')) ? 'selected=selected' : '' ?>>Товар подорожал</option>
        </select>
      </div>
    </div>
	<hr />
	<div class="row">
      <div class="col-xs-3">
        <h5><strong>Уведомление Админа на Email:</strong></h5>
        <span class="help"><i class="fa fa-info-circle"></i>&nbsp;Когда уведомлять Админа?</span>
      </div>
      <div class="col-xs-3">
        <select name="<?php echo $moduleName; ?>[Notifications]" class="form-control">
            <option value="yes" <?php echo ((isset($moduleData['Notifications']) && $moduleData['Notifications'] == 'yes')) ? 'selected=selected' : '' ?>>Включено</option>
           <option value="no" <?php echo ((isset($moduleData['Notifications']) && $moduleData['Notifications'] == 'no')) ? 'selected=selected' : '' ?>>Отключено</option>
        </select>
      </div>
    </div>
	<hr />
	<div class="row">
      <div class="col-xs-3">
        <h5><strong>Ширина окна:</strong></h5>
        <span class="help"><i class="fa fa-info-circle"></i>&nbsp;В пикселях</span>
      </div>
      <div class="col-xs-3">
      	<div class="input-group">
          <input type="text" name="<?php echo $moduleName; ?>[PopupWidth]" class="form-control" value="<?php echo (isset($moduleData['PopupWidth'])) ? $moduleData['PopupWidth'] : '250' ?>" />
          <span class="input-group-addon">px</span>
        </div>
      </div>
    </div>
	<hr />
	<div class="row">
      <div class="col-xs-3">
        <h5><strong>Внешний вид:</strong></h5>
        <span class="help"><i class="fa fa-info-circle"></i>&nbsp;Используйте коды:<br /><br />{name_field} - Поле Имя<br />{email_field} - Поле Email <br />{submit_button} - Кнопка отправить</span>
      </div>
      <div class="col-xs-7">
      	<?php foreach ($languages as $language) { ?>
    		<img src="view/image/flags/<?php echo $language['image']; ?>" style="float:left;position:absolute;margin-left:-20px;" title="<?php echo $language['name']; ?>" />
  			Заголовок окна: <input name="<?php echo $moduleName; ?>[CustomTitle][<?php echo $language['language_id']; ?>]" class="form-control" type="text" value="<?php echo (isset($moduleData['CustomTitle'][$language['language_id']])) ? $moduleData['CustomTitle'][$language['language_id']] : 'Следить за ценой!' ?>" />
            <textarea id="description_<?php echo $language['language_id']; ?>" name="<?php echo $moduleName; ?>[CustomText][<?php echo $language['language_id']; ?>]" class="form-control"><?php echo (isset($moduleData['CustomText'][$language['language_id']])) ? $moduleData['CustomText'][$language['language_id']] : '<p align="left"><span style="line-height: 1.6em;">Вам будет приходить уведомление когда цена товара поменяеться!</span></p>
<p align="left">Имя: {name_field}</p><p align="left">Email: {email_field}</p><p align="left">{submit_button}</p>' ?></textarea><br />
 		<?php } ?>
      </div>
    </div>
	<hr />
	<div class="row">
      <div class="col-xs-3">
        <h5><strong>Текст сообщения Email:</strong></h5>
        <span class="help"><i class="fa fa-info-circle"></i>&nbsp;Используйте коды:<br /><br />{c_name} - Имя клиента<br />{p_name} - Назавние товара<br />{p_image} - Фото товара<br />{p_link} - Ссылка на товар</span>
      </div>
      <div class="col-xs-7">
      	<?php foreach ($languages as $language) { ?>
    		<img src="view/image/flags/<?php echo $language['image']; ?>" style="float:left;position:absolute;margin-left:-20px;" title="<?php echo $language['name']; ?>" />
  			Заголовок Email: <input name="<?php echo $moduleName; ?>[EmailSubject][<?php echo $language['language_id']; ?>]" class="form-control" type="text" value="<?php echo (isset($moduleData['EmailSubject'][$language['language_id']])) ? $moduleData['EmailSubject'][$language['language_id']] : 'Новая цена товара!' ?>" />
            <textarea id="email_text_<?php echo $language['language_id']; ?>" name="<?php echo $moduleName; ?>[EmailText][<?php echo $language['language_id']; ?>]" class="form-control"><?php echo (isset($moduleData['EmailText'][$language['language_id']])) ? $moduleData['EmailText'][$language['language_id']] : '<p align="center"><b>Здравствуйте, {c_name}!</b></p><p align="center">Мы очень рады Вам сообщить, что в товара  {p_name}&nbsp; поменялась цена!</p><p align="center">{p_image}</p><p align="center"><strong>Проверить цену можно на &nbsp;<a href="http://{p_link}" target="_blank">странице товара</a>!</strong></p>' ?></textarea><br />
		<?php } ?>
      </div>
    </div>
	<hr />
    <div class="row">
      <div class="col-xs-3">
        <h5><strong>Ваш CSS код:</strong></h5>
        <span class="help"><i class="fa fa-info-circle"></i>&nbsp;Ваши стили</span>
      </div>
      <div class="col-xs-3">
      	<textarea name="<?php echo $moduleName; ?>[CustomCSS]" placeholder="Введите ваш CSS тут... ..." class="form-control"><?php echo (isset($moduleData['CustomCSS'])) ? $moduleData['CustomCSS'] : '' ?></textarea>
      </div>
    </div>
</div>
<script type="text/javascript"><!--
<?php foreach ($languages as $language) { ?>
	$('#description_<?php echo $language['language_id']; ?>').summernote({
		height: 250
	});
	$('#email_text_<?php echo $language['language_id']; ?>').summernote({
		height: 250
	});
<?php } ?>
//--></script>