<div id="archiveWrapper<?php echo $store['store_id']; ?>"> </div>
<script>
// Show date from the module
$(document).ready(function(){
	$.ajax({
		url: "index.php?route=module/<?php echo $moduleNameSmall; ?>/getarchive&token=<?php echo $token; ?>&page=1&store_id=<?php echo $store['store_id']; ?>",
		type: 'get',
		dataType: 'html',
		success: function(data) {		
			$("#archiveWrapper<?php echo $store['store_id']; ?>").html(data);
		}
	});
});
// Remove all customers from the archive
function removeAllArchive() {      
	var r=confirm("Удалить всех?");
	if (r==true) {
		$.ajax({
			url: 'index.php?route=module/<?php echo $moduleNameSmall; ?>/removeallarchive&token=<?php echo $token; ?>&store_id=<?php echo $store['store_id']; ?>',
			type: 'post',
			data: {'remove': r},
			success: function(response) {
				location.reload();
			}
		});
	}
}
// Remove single customer
function removeCustomer(pricealertID) {      
	var r=confirm("Are you sure you want to remove the customer?");
	if (r==true) {
		$.ajax({
			url: 'index.php?route=module/<?php echo $moduleNameSmall; ?>/removecustomer&token=<?php echo $token; ?>&store_id=<?php echo $store['store_id']; ?>',
			type: 'post',
			data: {'pricealert_id': pricealertID},
			success: function(response) {
				location.reload();
			}
		});
	}
}
</script>