<table id="ordersWrapper<?php echo $store_id; ?>" class="table table-bordered table-hover" width="100%">
    <thead>
        <tr class="table-header">
            <th width="25%">Email клиента</th>
            <th width="15%">Клиент</th>
            <th width="25%">Товар</th>
            <th width="10%">Дата</th>
            <th width="5%">Действия</th>
        </tr>
    </thead>
    <tbody>
        <?php foreach($sources as $source) { ?>
            <tr>
                <td>
                	<?php echo $source['customer_email']; ?>
                </td>
                <td>
                	<?php echo $source['customer_name']; ?>
                </td>
                <td>
                	<a href="<?php echo '../index.php?route=product/product&product_id='.$source['product_id']; ?>" target="_blank"><strong><?php echo $source['product_name']; ?></strong></a>
                </td>
                <td>
                	<?php echo $source['date_created']; ?>
                </td>
                <td>
                	<a onclick="removeCustomer('<?php echo $source['pricealert_id']; ?>')" class="btn btn-xs btn-danger"><i class="fa fa-times"></i> Удалить</a>
                </td>
			</tr>
        <?php } ?>
    </tbody>
	<tfoot><tr><td colspan="6">
    	<br />
    	<div class="row">
          <div class="col-sm-6 text-left"><?php echo $pagination; ?></div>
          <div class="col-sm-6 text-right"><?php echo $results; ?></div>
        </div>
    </td></tr></tfoot>
</table>
<div style="float:right;padding: 5px;">
	<a onclick="removeAllArchive()" class="btn btn-sm btn-info"><i class="fa fa-trash"></i>&nbsp;&nbsp;Удалить всех</a>
</div>
<script>
$(document).ready(function(){
	$('#ordersWrapper<?php echo $store_id; ?> .pagination a').click(function(e){
		e.preventDefault();
		$.ajax({
			url: this.href,
			type: 'get',
			dataType: 'html',
			success: function(data) {				
				$("#ordersWrapper<?php echo $store_id; ?>").html(data);
			}
		});
	});		 
});
</script>