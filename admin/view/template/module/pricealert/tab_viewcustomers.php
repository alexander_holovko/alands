<div id="ordersWrapper<?php echo $store['store_id']; ?>"> </div>
<script>
// Show data from the module
$(document).ready(function(){
	$.ajax({
		url: "index.php?route=module/<?php echo $moduleNameSmall; ?>/getcustomers&token=<?php echo $token; ?>&page=1&store_id=<?php echo $store['store_id']; ?>",
		type: 'get',
		dataType: 'html',
		success: function(data) {		
			$("#ordersWrapper<?php echo $store['store_id']; ?>").html(data);
		}
	});
});
// Remove all customers from the waiting list
function removeAll() {      
	var r=confirm("Удалить записи?");
	if (r==true) {
		$.ajax({
			url: 'index.php?route=module/<?php echo $moduleNameSmall; ?>/removeallcustomers&token=<?php echo $token; ?>&store_id=<?php echo $store['store_id']; ?>',
			type: 'post',
			data: {'remove': r},
			success: function(response) {
				location.reload();
			}
		});
	}
}
</script>
