<div class="container-fluid">
	<div class="row">
  		<div class="col-md-4">
    		<div class="box-heading">
      			<h3><i class="fa fa-user"></i>&nbsp;Ваша лицензия</h3>
    		</div>
			<?php if (empty($moduleData['LicensedOn'])): ?>
    			<div class="licenseAlerts"></div>
    			<div class="licenseDiv"></div>
                <table class="table notLicensedTable">
                	<tr>
                    	<td colspan="2">
                            <div class="form-group">
                                <label for="moduleLicense">Введите код лицензии (Ваш код лицензии: 5743-2332-8142-2790)</label>
                                <input type="text" class="licenseCodeBox form-control" placeholder="Код выше" name="<?php echo $moduleName; ?>[LicenseCode]" id="moduleLicense" value="<?php echo !empty($moduleData['LicenseCode']) ? $moduleData['LicenseCode'] : ''?>" />
                            </div>
                            <button type="button" class="btn btn-success btnActivateLicense"><i class="fa fa-check"></i></i>&nbsp;Активировать</button>
                        	
                  		</td>
                	</tr>
              	</table>
				<?php 
                    $hostname = (!empty($_SERVER['HTTP_HOST'])) ? $_SERVER['HTTP_HOST'] : '' ;
                    $hostname = (strstr($hostname,'http://') === false) ? 'http://'.$hostname: $hostname;
                ?>
				<script type="text/javascript">
                var domain='<?php echo base64_encode($hostname); ?>';
                var domainraw='<?php echo $hostname; ?>';
                var timenow=<?php echo time(); ?>;
                var MID = 'FYED7GDVA1';
                </script>
                <script type="text/javascript" src="view/javascript/val.js"></script>
    		<?php endif; ?>
    
			<?php if (!empty($moduleData['LicensedOn'])): ?>
    			<input name="cHRpbWl6YXRpb24ef4fe" type="hidden" value="<?php echo base64_encode(json_encode($moduleData['License'])); ?>" />
    			<input name="OaXRyb1BhY2sgLSBDb21" type="hidden" value="<?php echo $moduleData['LicensedOn']; ?>" />
    			<table class="table licensedTable">
                    <tr>
                    	<td>License Holder</td>
                    	<td><?php echo $moduleData['License']['customerName']; ?></td>
                    </tr>
                	<tr>
                		<td>Registered domains</td>
                		<td>
                    		<ul class="registeredDomains">
                    			<?php foreach ($moduleData['License']['licenseDomainsUsed'] as $domain): ?>
                        			<li><i class="fa fa-check"></i>&nbsp;<?php echo $domain; ?></li>
                    			<?php endforeach; ?>
                    		</ul>
                		</td>
                	</tr>
                	<tr>
                		<td>License Expires on</td>
                		<td><?php echo date("F j, Y",strtotime($moduleData['License']['licenseExpireDate'])); ?></td>
                	</tr>
                	<tr>
                    	<td colspan="2" style="text-align:center;background-color:#EAF7D9;">VALID LICENSE</td>
                	</tr>
				</table>
    		<?php endif; ?>
  		</div>
  
		<div class="col-md-8">
    		
		</div>
	</div>
</div>