<div class="well">
	<div class="row">
		<div class="col-sm-4">
			<label class="control-label" for="gifts-history-filter_order_id"><?php echo $entry_order_number; ?></label>
			<input type="text" name="filter_order_id" value="<?php echo $filter_order_id; ?>" placeholder="<?php echo $entry_order_number; ?>" id="gifts-history-filter_order_id" class="form-control" />
		</div>
		<div class="col-sm-4">
			<label class="control-label" for="gifts-history-filter_name"><?php echo $entry_name; ?></label>
			<input type="text" name="filter_name" value="<?php echo $filter_name; ?>" placeholder="<?php echo $entry_name; ?>" id="gifts-history-filter_name" class="form-control" />
		</div>
		<div class="col-sm-4">
			<label class="control-label" for="gifts-history-filter_customer"><?php echo $entry_customer; ?></label>
			<input type="text" name="filter_customer" value="<?php echo $filter_customer; ?>" placeholder="<?php echo $entry_customer; ?>" id="gifts-history-filter_customer" class="form-control" />
		</div>
	</div>
	<div class="row" style="padding-top: 15px;">
		<div class="col-sm-2 col-sm-offset-10">
			<button type="button" id="button-gifts-history-filter" class="btn btn-primary pull-right"><i class="fa fa-search"></i> <?php echo $button_filter; ?></button>
		</div>
	</div>
</div>
<div class="panel panel-default">
	<div class="panel-heading clearfix">
		<h3 class="panel-title pull-left" style="padding-top: 9px;"><i class="fa fa-list"></i> <?php echo $text_gifts_history; ?></h3>
		<div class="pull-right">
			<button type="button" title="<?php echo $button_delete; ?>" class="btn btn-danger" data-toggle="tooltip" id="button-delete-gifts-history" onclick="confirm('<?php echo $text_confirm; ?>') ? deleteData(this, 'giftHistory') : false;" disabled="disabled"><i class="fa fa-trash-o"></i></button>
		</div>
	</div>
	<div class="panel-body">
		<div class="table-responsive">
			<table class="table table-bordered table-hover">
				<thead>
					<tr>
						<td style="width: 1px;" class="text-center"><input type="checkbox" onclick="$('input[name*=\'selected\']').prop('checked', this.checked).trigger('change');" /></td>
						<td class="text-center">
						<?php if ($sort == 'order_id') { ?>
							<a href="<?php echo $sort_order_id; ?>" class="<?php echo strtolower($order); ?>"><?php echo $column_order_id; ?></a>
						<?php } else { ?>
							<a href="<?php echo $sort_order_id; ?>"><?php echo $column_order_id; ?></a>
						<?php } ?>	
						</td>
						<td class="text-center">
						<?php if ($sort == 'name') { ?>
							<a href="<?php echo $sort_name; ?>" class="<?php echo strtolower($order); ?>"><?php echo $column_name; ?></a>
						<?php } else { ?>
							<a href="<?php echo $sort_name; ?>"><?php echo $column_name; ?></a>
						<?php } ?>	
						</td>
						<td class="text-center">
						<?php if ($sort == 'customer') { ?>
							<a href="<?php echo $sort_customer; ?>" class="<?php echo strtolower($order); ?>"><?php echo $column_customer; ?></a>
						<?php } else { ?>
							<a href="<?php echo $sort_customer; ?>"><?php echo $column_customer; ?></a>
						<?php } ?>
						</td>
						<td class="text-center">
						<?php if ($sort == 'total') { ?>
							<a href="<?php echo $sort_total; ?>" class="<?php echo strtolower($order); ?>"><?php echo $column_total; ?></a>
						<?php } else { ?>
							<a href="<?php echo $sort_total; ?>"><?php echo $column_total; ?></a>
						<?php } ?>
						</td>
						<td class="text-center">
						<?php if ($sort == 'datetime_added') { ?>
							<a href="<?php echo $sort_datetime_added; ?>" class="<?php echo strtolower($order); ?>"><?php echo $column_datetime_added; ?></a>
						<?php } else { ?>
							<a href="<?php echo $sort_datetime_added; ?>"><?php echo $column_datetime_added; ?></a>
						<?php } ?>
						</td>
						<td class="text-center"><?php echo $column_action; ?>
						</td>
					</tr>
				</thead>
				<tbody>
				<?php if ($histories) { ?>
		    		<?php foreach ($histories as $history) { ?>
		    		<?php if (!$history['status']) { ?>
		    		<tr class="danger">
		    		<?php } else { ?>
		    		<tr>
		    		<?php } ?>
		    			<td class="text-center">
							<input type="checkbox" name="selected[]" value="<?php echo $history['gift_history_id']; ?>" />
						</td>
		    			<td class="text-center">
		    			<?php if ($history['order_id']) { ?>
							<a href="index.php?route=sale/order/info&token=<?php echo $token; ?>&order_id=<?php echo $history['order_id']; ?>" target="_blank"><?php echo $history['order_id']; ?></a>
						<?php } ?>
		    			</td>
		    			<td class="text-center">
							<a href="<?php echo $gift_edit; ?>&gift_id=<?php echo $history['gift_id']; ?>"><?php echo $history['name']; ?></a>
		    			<td class="text-center">
		    			<?php if ($history['customer_id']) { ?>
							<a href="index.php?route=customer/customer/edit&token=<?php echo $token; ?>&customer_id=<?php echo $history['customer_id']; ?>" target="_blank"><?php echo $history['customer']; ?></a>
						<?php } else { ?>
							<?php echo $history['customer']; ?>
						<?php } ?>
		    			</td>
		    			<td class="text-center"><?php echo $history['total']; ?></td>
		    			<td class="text-center"><?php echo $history['datetime_added']; ?></td>
		    			<td class="text-center">
		    				<button type="button" onclick="confirm('<?php echo $text_confirm; ?>') ? deleteData(this, 'giftHistory') : false;" class="btn btn-danger" data-toggle="tooltip" title="<?php echo $button_delete; ?>"><i class="fa fa-trash-o" aria-hidden="true"></i></button>
		    			</td>
		    		</tr>	
		    		<?php } ?>
		    	<?php } else { ?>
		    		<tr>
		        		<td class="text-center" colspan="7"><?php echo $text_no_results; ?></td>
		        	</tr>
		      	<?php } ?>
				</tbody>
			</table>
		</div>
		<div class="row">
			<div class="col-sm-6 text-left"><?php echo $pagination; ?></div>
			<div class="col-sm-6 text-right"><?php echo $results; ?></div>
		</div>
	</div>
</div>
<script type="text/javascript"><!--
$(function() {
	$('#gifts-history-filter_order_id').autocomplete( {
		'source': function(request, response) {
			$.ajax( {
				url: 'index.php?route=module/membership/autocomplete&token=<?php echo $token; ?>',
				dataType: 'json',
				type: 'POST',
				data: 'filter_order_id=' +  encodeURIComponent(request) + '&giftHistory=1',
				beforeSend: function () {},
				complete: function () {},
				success: function(json) {
					response($.map(json, function(item) {
						return {
							label: '<i class="fa fa-shopping-cart" aria-hidden="true"></i> ' + item['order_id'],
							value: item['order_id']
						}
					} ));
				}
			} );
		},
		'select': function(item) {
			$(this).val(item['value']);
		}
	} );
	
	$('#gifts-history-filter_name').autocomplete( {
		'source': function(request, response) {
			$.ajax( {
				url: 'index.php?route=module/membership/autocomplete&token=<?php echo $token; ?>',
				dataType: 'json',
				type: 'POST',
				data: 'filter_gift_name=' +  encodeURIComponent(request) + '&giftHistory=1',
				beforeSend: function () {},
				complete: function () {},
				success: function(json) {
					response($.map(json, function(item) {
						return {
							label: item['name'],
							value: item['name']
						}
					} ));
				}
			} );
		},
		'select': function(item) {
			$(this).val(item['value']);
		}
	} );
	
	$('#gifts-history-filter_customer').autocomplete( {
		'source': function(request, response) {
			$.ajax( {
				url: 'index.php?route=module/membership/autocomplete&token=<?php echo $token; ?>',
				dataType: 'json',
				type: 'POST',
				data: 'filter_customer=' +  encodeURIComponent(request) + '&giftHistory=1',
				beforeSend: function () {},
				complete: function () {},
				success: function(json) {
					response($.map(json, function(item) {
						return {
							label: '<i class="fa fa-user" aria-hidden="true"></i> ' + item['customer'],
							value: item['customer']
						}
					} ));
				}
			} );
		},
		'select': function(item) {
			$(this).val(item['value']);
		}
	} );

	$('#button-gifts-history-filter').on('click', function() {
		var 
			url = 'index.php?route=module/membership/getGiftsHistory&token=<?php echo $token; ?>',
			filter_order_id = $('#gifts-history-filter_order_id').val(),
			filter_name = $('#gifts-history-filter_name').val(),
			filter_customer = $('#gifts-history-filter_customer').val();

		if (filter_order_id) {
			url += '&filter_order_id=' + filter_order_id;
		}
		
		if (filter_name) {
			url += '&filter_name=' + encodeURIComponent(filter_name);
		}
		
		if (filter_customer) {
			url += '&filter_customer=' + encodeURIComponent(filter_customer);
		}

		$('#gifts-history').load(url);
	} );
} );	
//--></script>