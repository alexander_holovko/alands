<div class="row">
	<div class="col-sm-11">
		<h3 style="padding-top: 8px;"><?php echo $text_form; ?></h3>
	</div>
	<div class="col-sm-1">
		<button type="button" onclick="saveCard();" class="btn btn-primary btn-block"><i class="fa fa-save"></i> <?php echo $button_save; ?></button>
	</div>
</div>
<hr>
<ul class="nav nav-tabs">
	<li class="active"><a href="#card-main-settings" data-toggle="tab"><?php echo $text_main; ?></a></li>
	<?php if ($card_id) { ?>
	<li><a href="#card-history" data-toggle="tab"><?php echo $text_history; ?></a></li>
	<?php } ?>
</ul>
<div class="tab-content">
	<div class="tab-pane active" id="card-main-settings">
		<div class="form-group">
			<label class="col-sm-2 control-label" for="card-owner_status-enabled"><?php echo $entry_card_status; ?></label>
		    <div class="col-sm-4">
		      	<div class="radio-switch">
		      	<?php if ($status) { ?>
					<input type="radio" name="status" value="0" id="card-owner_status-disabled">
	                <label class="col-sm-4" for="card-owner_status-enabled"><?php echo $text_disabled; ?></label>
		      		<input type="radio" name="status" value="1" id="card-owner_status-enabled" checked>
	                <label class="col-sm-4" for="card-owner_status-disabled"><?php echo $text_enabled; ?></label>
				<?php } else { ?>
	                <input type="radio" name="status" value="0" id="card-owner_status-disabled" checked>
	                <label class="col-sm-4" for="card-owner_status-enabled"><?php echo $text_disabled; ?></label>
	                <input type="radio" name="status" value="1" id="card-owner_status-enabled">
	                <label class="col-sm-4" for="card-owner_status-disabled"><?php echo $text_enabled; ?></label>  
				<?php } ?>
		      	</div>
		    </div>
		</div>
	    <div class="form-group required">
	    	<label class="col-sm-2 control-label" for="cardowner"><span data-toggle="tooltip" title="<?php echo $help_cardowner; ?>"><?php echo $entry_cardowner; ?></span></label>
	    	<div class="col-sm-10">
	    		<input type="text" name="cardowner" value="<?php echo $cardowner['name']; ?>" placeholder="<?php echo $entry_cardowner; ?>" id="cardowner" class="form-control" />
	    		<input type="hidden" name="customer_id" value="<?php echo $cardowner['customer_id']; ?>" id="customer_id" />
	    		<?php if ($error_cardowner) { ?>
	    		<div class="text-danger"><?php echo $error_cardowner; ?></div>
	    		<?php } ?>
	    	</div>
	    </div>
	    <div class="form-group">
	    	<label class="col-sm-2 control-label" for="telephone"><span data-toggle="tooltip" title="<?php echo $help_telephone; ?>"><?php echo $entry_telephone; ?></span></label>
	    	<div class="col-sm-10">
	    		<input type="text" name="telephone" value="<?php echo $telephone; ?>" placeholder="<?php echo $entry_telephone; ?>" id="telephone" class="form-control" />
	    	</div>
	    </div>
	    <div class="form-group">
	    	<label class="col-sm-2 control-label" for="email"><span data-toggle="tooltip" title="<?php echo $help_email; ?>"><?php echo $entry_email; ?></span></label>
	    	<div class="col-sm-10">
	    		<input type="text" name="email" value="<?php echo $email; ?>" placeholder="<?php echo $entry_email; ?>" id="email" class="form-control" />
	    		<?php if ($error_email) { ?>
		    	<div class="text-danger"><?php echo $error_email; ?></div>
		    	<?php } ?>
	    	</div>
	    </div>
	    <div class="form-group required">
	    	<label class="col-sm-2 control-label" for="card-code"><span data-toggle="tooltip" title="<?php echo $help_code; ?>"><?php echo $entry_code; ?></span></label>
	    	<div class="col-sm-10">
	    		<div class="input-group">
	    			<input type="text" name="code" value="<?php echo $code; ?>" placeholder="<?php echo $entry_code; ?>" id="card-code" class="form-control" />
	    			<span class="input-group-btn">
	    				<button id="generate-code" onclick="generateCardCode();" type="button" class="btn btn-info" data-toggle="tooltip" title="<?php echo $button_generate; ?>"><i class="fa fa-cog"></i></button>
	    			</span>
	    		</div>
	    		<?php if ($error_code) { ?>
	    		<div class="text-danger"><?php echo $error_code; ?></div>
	    		<?php } ?>	
	    	</div>
	    </div>
	    <div class="form-group">
	    	<label class="col-sm-2 control-label" for="card-date_start"><?php echo $entry_start; ?></label>
	    	<div class="col-sm-4">
	    		<div class="input-group date">
	    			<input type="text" name="date_start" value="<?php echo $date_start; ?>" placeholder="<?php echo $entry_start; ?>" data-date-format="YYYY-MM-DD" id="card-date_start" class="form-control" />
	    			<span class="input-group-btn">
	    				<button type="button" class="btn btn-default"><i class="fa fa-calendar"></i></button>
	    			</span>
	    		</div>
	    	</div>
	    	<label class="col-sm-2 control-label" for="card-date_expiry"><?php echo $entry_expiry; ?></label>
	    	<div class="col-sm-4">
	    		<div class="input-group date">
	    			<input type="text" name="date_expiry" value="<?php echo $date_expiry; ?>" placeholder="<?php echo $entry_expiry; ?>" data-date-format="YYYY-MM-DD" id="card-date_expiry" class="form-control" />
	    			<span class="input-group-btn">
	    				<button type="button" class="btn btn-default"><i class="fa fa-calendar"></i></button>
	    			</span>
	    		</div>
	    	</div>
	    </div>
		<div class="panel panel-default">
			<div class="panel-heading">
				<h3 class="panel-title"><i class="fa fa-table"></i> <?php echo $text_discount_table; ?></h3>
			</div>
			<div class="panel-body">
				<div class="table-responsive">
					<table class="table table-bordered table-hover" id="table-card-discounts">
					    <thead>
					        <tr>
					            <td class="text-center"><?php echo $column_discount_base; ?></td>
					            <td class="text-center"><?php echo $column_discount_limit; ?></td>
					            <td class="text-center"><?php echo $column_discount_value; ?></td>
					            <td class="text-center"><?php echo $column_discount_type; ?></td>
					            <td class="text-center"><?php echo $column_action; ?></td>
					        </tr>
					    </thead>
					    <tbody>
					    <?php if (is_array($discount)) { $count = 0; ?>
							<?php foreach ($discount as $discount) { ?>
							<tr>
								<td>
								    <select name="discount[<?php echo $count; ?>][base]" id="discount-base" class="form-control">
								    <?php foreach ($discount_base as $v => $n) { ?>
									    <?php if ($v == $discount['base']) { ?>
									    <option value="<?php echo $v; ?>" selected="selected"><?php echo $n; ?></option>
									    <?php } else { ?>
									    <option value="<?php echo $v; ?>"><?php echo $n; ?></option>
									    <?php } ?>
									<?php } ?>
								    </select>
								</td>
								<td>
								    <input type="text" name="discount[<?php echo $count; ?>][limit]" value="<?php echo $discount['limit']; ?>" placeholder="<?php echo $column_discount_limit; ?>" id="discount-limit" class="form-control" />
								</td>
								<td>
								    <input type="text" name="discount[<?php echo $count; ?>][value]" value="<?php echo $discount['value']; ?>" placeholder="<?php echo $column_discount_value; ?>" id="discount-value" class="form-control" />
								</td>
								<td>
								    <select name="discount[<?php echo $count; ?>][type]" id="discount-type" class="form-control">
								    <?php foreach ($discount_type as $v => $n) { ?>
								        <?php if ($v == $discount['type']) { ?>
								        <option value="<?php echo $v; ?>" selected="selected"><?php echo $n; ?></option>
								        <?php } else { ?>
								        <option value="<?php echo $v; ?>"><?php echo $n; ?></option>
								        <?php } ?>
								    <?php } ?>
								    </select>
								</td>
								<td class="text-center">
								    <button type="button" onclick="$(this).parents('tr').remove()" class="btn btn-danger"><i class="fa fa-minus-circle"></i></button>
								</td>
							</tr>
							<?php $count++; } ?>
						<?php } ?>
						</tbody>
						<tfoot>
							<tr>
								<td colspan="4"></td>
								<td class="text-center">
								    <button type="button" onclick="addDiscount('table-card-discounts', 'discount');" class="btn btn-primary"><i class="fa fa-plus-circle"></i></button>
								</td>
							</tr>
						</tfoot>
					</table>
				</div>
			</div>
		</div>
		<div class="form-group">
			<label class="col-sm-2 control-label" for="card-category"><?php echo $entry_categories; ?></label>
			<div class="col-sm-10">
				<input type="text" name="category" value="" placeholder="<?php echo $entry_categories; ?>" id="card-category" class="form-control" />
				<div id="card-categories" class="well well-sm" style="height: 150px; overflow: auto;">
				<?php if (is_array($categories)) { ?>
					<?php foreach ($categories as $category) { ?>
					<div id="card-categories-<?php echo $category['category_id']; ?>"><i class="fa fa-minus-circle"></i> <?php echo $category['name']; ?>
						<input type="hidden" name="categories[]" value="<?php echo $category['category_id']; ?>" />
					</div>
					<?php } ?>
				<?php } ?>
				</div>
			</div>
		</div>
		<div class="form-group">
			<label class="col-sm-2 control-label" for="card-product"><?php echo $entry_products; ?></label>
			<div class="col-sm-10">
				<input type="text" name="product" value="" placeholder="<?php echo $entry_products; ?>" id="card-product" class="form-control" />
				<div id="card-products" class="well well-sm" style="height: 150px; overflow: auto;">
				<?php if (is_array($products)) { ?>
					<?php foreach ($products as $product) { ?>
					<div id="card-products-<?php echo $product['product_id']; ?>"><i class="fa fa-minus-circle"></i> <?php echo $product['name']; ?>
						<input type="hidden" name="products[]" value="<?php echo $product['product_id']; ?>" />
					</div>
					<?php } ?>
				<?php } ?>
				</div>
			</div>
		</div>
		<div class="form-group">
			<label class="col-sm-2 control-label" for="card-logged-enabled"><?php echo $entry_logged; ?></label>
			<div class="col-sm-2">
		      	<div class="radio-switch">
		      	<?php if ($logged) { ?>
					<input type="radio" name="logged" value="0" id="card-logged-disabled">
	                <label class="col-sm-4" for="card-logged-enabled"><?php echo $text_no; ?></label>
		      		<input type="radio" name="logged" value="1" id="card-logged-enabled" checked>
	                <label class="col-sm-4" for="card-logged-disabled"><?php echo $text_yes; ?></label>
				<?php } else { ?>
	                <input type="radio" name="logged" value="0" id="card-logged-disabled" checked>
	                <label class="col-sm-4" for="card-logged-enabled"><?php echo $text_no; ?></label>
	                <input type="radio" name="logged" value="1" id="card-logged-enabled">
	                <label class="col-sm-4" for="card-logged-disabled"><?php echo $text_yes; ?></label>  
				<?php } ?>
		      	</div>
		    </div>
		</div>
		<div class="form-group">    
			<label class="col-sm-2 control-label" for="card-free_shipping-enabled"><?php echo $entry_free_shipping; ?></label>
			<div class="col-sm-2">
		      	<div class="radio-switch">
		      	<?php if ($free_shipping) { ?>
					<input type="radio" name="free_shipping" value="0" id="card-free_shipping-disabled">
	                <label class="col-sm-4" for="card-free_shipping-enabled"><?php echo $text_no; ?></label>
		      		<input type="radio" name="free_shipping" value="1" id="card-free_shipping-enabled" checked>
	                <label class="col-sm-4" for="card-free_shipping-disabled"><?php echo $text_yes; ?></label>
				<?php } else { ?>
	                <input type="radio" name="free_shipping" value="0" id="card-free_shipping-disabled" checked>
	                <label class="col-sm-4" for="card-free_shipping-enabled"><?php echo $text_no; ?></label>
	                <input type="radio" name="free_shipping" value="1" id="card-free_shipping-enabled">
	                <label class="col-sm-4" for="card-free_shipping-disabled"><?php echo $text_yes; ?></label>  
				<?php } ?>
		      	</div>
		    </div>
		</div>
		<div class="form-group">
			<label class="col-sm-2 control-label" for="card-minimum_order_amount"><?php echo $entry_minimum_order_amount; ?></label>
			<div class="col-sm-10">
				<div class="input-group">
					<input type="text" name="minimum_order_amount" value="<?php echo $minimum_order_amount; ?>" placeholder="<?php echo $entry_minimum_order_amount; ?>" id="card-minimum_order_amount" class="form-control" />
					<span class="input-group-addon"><?php echo $currency_symbol; ?></span>
				</div>
			</div>
		</div>
		<div class="form-group">
			<label class="col-sm-2 control-label" for="card-maximum_order_amount"><?php echo $entry_maximum_order_amount; ?></label>
			<div class="col-sm-10">
				<div class="input-group">
					<input type="text" name="maximum_order_amount" value="<?php echo $maximum_order_amount; ?>" placeholder="<?php echo $entry_maximum_order_amount; ?>" id="card-maximum_order_amount" class="form-control" />
					<span class="input-group-addon"><?php echo $currency_symbol; ?></span>
				</div>
			</div>
		</div>
		<div class="form-group">
			<label class="col-sm-2 control-label" for="card-uses_total"><?php echo $entry_uses_total; ?></label>
			<div class="col-sm-10">
				<input type="text" name="uses_total" value="<?php echo $uses_total; ?>" placeholder="<?php echo $entry_uses_total; ?>" id="card-uses_total" class="form-control" />
			</div>
		</div>
		<div class="form-group">
			<label class="col-sm-2 control-label" for="card-uses_customer"><?php echo $entry_uses_customer; ?></label>
			<div class="col-sm-10">
				<input type="text" name="uses_customer" value="<?php echo $uses_customer; ?>" placeholder="<?php echo $entry_uses_customer; ?>" id="card-uses_customer" class="form-control" />
			</div>
		</div>
	</div>
    <?php if ($card_id) { ?>
    <div class="tab-pane" id="card-history"></div>
    <?php } ?>
</div>
<script type="text/javascript"><!--
function saveCard() {
	var 
		$container = $('#card-form'),
		post_data = $container.find('input, select, textarea').serialize();
	
	$.ajax( {
		url: '<?php echo $action; ?>'.replace(/&amp;/g, '&'),
		type: 'POST',
		data: post_data,
		dataType: 'html',
		success: function (data, textStatus, jqXHR) {
			if (data.match(/button\-cards\-list\-filter/i)) {
				$('#menu-cards-list').tab('show');
				$('#cards-list').html(data);
			} else {
				$container.html(data);
			}
		}
	} );
}

function generateCardCode() {
	$.ajax( {
		url: 'index.php?route=module/membership/getCardCode&token=<?php echo $token; ?>',
		type: 'POST',
		dataType: 'json',
		beforeSend: function () {
			$('#generate-code > i').addClass('fa-spin');
		},
		complete: function () {
			$('#generate-code > i').removeClass('fa-spin');
		},
		success: function (json) {
			if (json['code_list']) {
				$('#card-code').val(json['code_list'][0]);
			}
		}
	} );
}

$(function() {
	$('.date').datetimepicker( {pickTime: false} );
	
	// Cardowner
	$('#cardowner').autocomplete( {
		'source': function(request, response) {
			$.ajax( {
				url: 'index.php?route=module/membership/autocomplete&token=<?php echo $token; ?>',
				type: 'POST',
				data: 'cardowner=' +  encodeURIComponent(request),
				dataType: 'json',
				beforeSend: function () {},
				complete: function () {},
				success: function(json) {
					response($.map(json, function(item) {
						return {
							label: '<i class="fa fa-user" aria-hidden="true"></i> ' + item['cardowner'] + ' <i class="fa fa-phone" aria-hidden="true"></i> ' + item['telephone'] + ' <i class="fa fa-envelope" aria-hidden="true"></i> ' + item['email'],
							value: item['customer_id'],
							cardowner: item['cardowner'],
							telephone: item['telephone'],
							email: item['email']
						}
					} ) );
				}
			} );
		},
		'select': function(item) {
			$(this).val(item['cardowner']);
			$('#customer_id').val(item['value']);
			$('#telephone').val(item['telephone']);
			$('#email').val(item['email']);
		}
	} );
	
	$('#cardowner').on('change', function() {
		$('#customer_id').val('');
	} );

    // Categories
    $('#card-category').autocomplete( {
        'source': function(request, response) {
            $.ajax( {
                url: 'index.php?route=catalog/category/autocomplete&token=<?php echo $token; ?>&filter_name=' +  encodeURIComponent(request),
                dataType: 'json',
                beforeSend: function () {},
                complete: function () {},
                success: function(json) {
                    response($.map(json, function(item) {
                        return {
                            label: item['name'],
                            value: item['category_id']
                        }
                    } ) );
                }
            } );
        },
        'select': function(item) {
            this.value = '';

            $('#card-categories-' + item['value']).remove();

            $('#card-categories').append('<div id="card-categories-' + item['value'] + '"><i class="fa fa-minus-circle"></i> ' + item['label'] + '<input type="hidden" name="categories[]" value="' + item['value'] + '" /></div>');
        }
    } );

    $('#card-categories').delegate('.fa-minus-circle', 'click', function() {
        $(this).parent().remove();
    } );
	
	// Products
	$('#card-product').autocomplete( {
		'source': function(request, response) {
			$.ajax( {
				url: 'index.php?route=catalog/product/autocomplete&token=<?php echo $token; ?>&filter_name=' +  encodeURIComponent(request),
				dataType: 'json',
				beforeSend: function () {},
				complete: function () {},			
				success: function(json) {
					response($.map(json, function(item) {
						return {
							label: item['name'],
							value: item['product_id']
						}
					} ) );
				}
			} );
		},
		'select': function(item) {
			this.value = '';
			
			$('#card-products-' + item['value']).remove();
			
			$('#card-products').append('<div id="card-products-' + item['value'] + '"><i class="fa fa-minus-circle"></i> ' + item['label'] + '<input type="hidden" name="products[]" value="' + item['value'] + '" /></div>');	
		}
	} );

	$('#card-products').delegate('.fa-minus-circle', 'click', function() {
		$(this).parent().remove();
	} );
} );

//--></script>
<?php if ($card_id) { ?>
<script type="text/javascript"><!--			
$('#card-history').load('index.php?route=module/membership/getCardHistory&token=<?php echo $token; ?>&card_id=<?php echo $card_id; ?>');
//--></script>
<?php } ?>