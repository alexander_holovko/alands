<?php echo $header; ?><?php echo $column_left; ?>
<div id="content">
	<div class="page-header">
		<div class="container-fluid">
	<div class="pull-right">
	  <button type="submit" form="form-banner" data-toggle="tooltip" title="<?php echo $button_save; ?>" class="btn btn-primary"><i class="fa fa-save"></i></button>
	  <a href="<?php echo $cancel; ?>" data-toggle="tooltip" title="<?php echo $button_cancel; ?>" class="btn btn-default"><i class="fa fa-reply"></i></a>
	</div>
	<h1><?php echo $heading_title; ?></h1>
	  <ul class="breadcrumb">
	    <?php foreach ($breadcrumbs as $breadcrumb) { ?>
		<li><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a></li>
	    <?php } ?>
	  </ul>
		</div>
	</div>
	<div class="container-fluid">
	<?php if ($error_warning) { ?>
	<div class="alert alert-danger"><i class="fa fa-exclamation-circle"></i> <?php echo $error_warning; ?>
		<button type="button" class="close" data-dismiss="alert">&times;</button>
	</div>
	<?php } ?>
	<div class="panel panel-default">
	<div class="panel-heading">
	  <h3 class="panel-title"><i class="fa fa-pencil"></i> <?php echo $text_edit; ?></h3>
	</div>
	<div class="panel-body">
	  <form action="<?php echo $action; ?>" method="post" enctype="multipart/form-data" id="form-banner" class="form-horizontal">
		<ul class="nav nav-tabs">            
			<li class="active"><a href="#tab-general" data-toggle="tab"><?php echo $tab_general; ?></a></li>
			<li><a href="#tab-support" data-toggle="tab"><?php echo $tab_support; ?></a></li>
		</ul>
		<div class="tab-content">
			<div id="tab-general" class="tab-pane active">
				<div class="form-group">
					<label class="col-sm-2 control-label" for="input-name">
						<?php echo $entry_name; ?>
					</label>
					<div class="col-sm-10">
						<input type="text" name="name" value="<?php echo $name; ?>" placeholder="<?php echo $entry_name; ?>" id="input-name" class="form-control" />
						<?php if ($error_name) { ?>
							<div class="text-danger"><?php echo $error_name; ?></div>
						<?php } ?>
					</div>
				</div>
				<div class="form-group">
					<label class="col-sm-2 control-label" for="input-name">
						<span data-toggle="tooltip" title="<?php echo $help_name_block; ?>"><?php echo $entry_name_block; ?></span>
					</label>
					<div class="col-sm-10">
						<?php foreach ($languages as $language) { ?>
							<div class="input-group">
								<span class="input-group-addon"><img src="<?php echo $language['flag_img']; ?>" title="<?php echo $language['name']; ?>" /></span>
								<input type="text" name="name_block[<?php echo $language['language_id']; ?>]" value="<?php echo isset($name_block[$language['language_id']]) ? $name_block[$language['language_id']] : ''; ?>" placeholder="<?php echo $entry_name_block; ?>" class="form-control" />
							</div>
						<?php } ?>
					</div>
				</div>
		<div class="form-group">
		<label class="col-sm-2 control-label" for="input-infinite"><?php echo $entry_infinite; ?></label>
		<div class="col-sm-2">
			<select name="infinite" id="input-infinite" class="form-control">
				<?php if ($infinite) { ?>
					<option value="1" selected="selected"><?php echo $text_yes; ?></option>
					<option value="0"><?php echo $text_no; ?></option>
				<?php } else { ?>
					<option value="1"><?php echo $text_yes; ?></option>
					<option value="0" selected="selected"><?php echo $text_no; ?></option>
				<?php } ?>
			</select>
		</div>
		<label class="col-sm-2 control-label" for="input-scroll_auto"><?php echo $entry_scroll_auto; ?></label>
		<div class="col-sm-2">
			<select name="scroll_auto" id="input-scroll_auto" class="form-control">
				<?php if ($scroll_auto) { ?>
					<option value="1" selected="selected"><?php echo $text_yes; ?></option>
					<option value="0"><?php echo $text_no; ?></option>
				<?php } else { ?>
					<option value="1"><?php echo $text_yes; ?></option>
					<option value="0" selected="selected"><?php echo $text_no; ?></option>
				<?php } ?>
			</select>
		</div>
		<label class="col-sm-2 control-label" for="input-scroll-pause"><?php echo $entry_scroll_pause; ?></label>
		<div class="col-sm-2">
			<select name="scroll_pause" id="input-scroll-pause" class="form-control">
				<?php if ($scroll_pause == 'true') { ?>
					<option value="true" selected="selected"><?php echo $text_yes; ?></option>
					<option value="false"><?php echo $text_no; ?></option>
				<?php } else { ?>
					<option value="true"><?php echo $text_yes; ?></option>
					<option value="false" selected="selected"><?php echo $text_no; ?></option>
				<?php } ?>
			</select>
		</div>
	    </div>
		<div class="form-group">
		<label class="col-sm-3 control-label" for="input-animation_speed"><?php echo $entry_animation_speed; ?></label>
		<div class="col-sm-3">
		  <input type="text" name="animation_speed" value="<?php echo $animation_speed; ?>" id="input-animation_speed" class="form-control" />
		  <?php if ($error_animation_speed) { ?>
		    <div class="text-danger"><?php echo $error_animation_speed; ?></div>
		  <?php } ?>
		</div>
		<label class="col-sm-3 control-label" for="input-interval"><?php echo $entry_interval; ?></label>
		<div class="col-sm-3">
		  <input type="text" name="interval" value="<?php echo $interval; ?>" id="input-interval" class="form-control" />
		  <?php if ($error_interval) { ?>
		    <div class="text-danger"><?php echo $error_interval; ?></div>
		  <?php } ?>
		</div>
	    </div>
		<div class="form-group">
		<label class="col-sm-3 control-label" for="input-scroll-limit"><?php echo $entry_scroll_limit; ?></label>
		<div class="col-sm-3">
			<input type="text" name="scroll_limit" value="<?php echo $scroll_limit; ?>" id="input-scroll-limit" class="form-control" />
			<?php if ($error_scroll_limit) { ?>
				<div class="text-danger"><?php echo $error_scroll_limit; ?></div>
			<?php } ?>
		</div>
		<label class="col-sm-3 control-label" for="input-scroll"><?php echo $entry_scroll; ?></label>
		<div class="col-sm-3">
			<input type="text" name="scroll" value="<?php echo $scroll; ?>" placeholder="<?php echo $entry_scroll; ?>" id="input-scroll" class="form-control" />
			<?php if ($error_scroll) { ?>
				<div class="text-danger"><?php echo $error_scroll; ?></div>
			<?php } ?>
		</div>
	    </div>
		<div class="form-group">
		<label class="col-sm-12 control-label" for="input-image"><center><?php echo $entry_image; ?></center></label>
	    </div>
		<div class="form-group">
		<label class="col-sm-2 control-label" for="input-width"><?php echo $entry_width; ?></label>
		<div class="col-sm-4">
			<input type="text" name="width" value="<?php echo $width; ?>" id="input-width" class="form-control" />
			<?php if ($error_width) { ?>
				<div class="text-danger"><?php echo $error_width; ?></div>
			<?php } ?>
		</div>
		<label class="col-sm-2 control-label" for="input-height"><?php echo $entry_height; ?></label>
		<div class="col-sm-4">
			<input type="text" name="height" value="<?php echo $height; ?>" id="input-height" class="form-control" />
				<?php if ($error_height) { ?>
					<div class="text-danger"><?php echo $error_height; ?></div>
				<?php } ?>
		</div>
	    </div>
		<div class="form-group">
		<label class="col-sm-2 control-label" for="input-status"><?php echo $entry_status; ?></label>
		<div class="col-sm-10">
			<select name="status" id="input-status" class="form-control">
				<?php if ($status) { ?>
					<option value="1" selected="selected"><?php echo $text_enabled; ?></option>
					<option value="0"><?php echo $text_disabled; ?></option>
				<?php } else { ?>
					<option value="1"><?php echo $text_enabled; ?></option>
					<option value="0" selected="selected"><?php echo $text_disabled; ?></option>
				<?php } ?>
			</select>
		</div>
	    </div>
			</div>
			<div id="tab-support" class="tab-pane">
				<?php echo $support_text; ?>
			</div>
		</div>
	</form>
	</div>
		</div>
	</div>
</div>
<?php echo $footer; ?>
