<?php echo $header; ?><?php echo $column_left; ?>
<div id="content">
	<div class="page-header">
		<div class="container-fluid">
      		<ul class="breadcrumb">
		        <?php foreach ($breadcrumbs as $breadcrumb) { ?>
		        	<li><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a></li>
		        <?php } ?>
      		</ul>
   		</div>
  	</div>
  	<div class="container-fluid">
  		<?php if ($error_warning) { ?>
  		<div class="alert alert-danger">
  			<i class="fa fa-exclamation-circle"></i> <?php echo $error_warning; ?>
  			<button type="button" class="close" data-dismiss="alert">&times;</button>
  		</div>
   	 	<?php } ?>
   	 	<?php if ($success) { ?>
	    <div class="alert alert-success"><i class="fa fa-check-circle"></i> <?php echo $success; ?>
	    	<button type="button" class="close" data-dismiss="alert">&times;</button>
	    </div>
	    <?php } 	?>

        <nav class="navbar navbar-default">
            <div class="container-fluid">
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar-collapse-1" aria-expanded="false">
                        <span class="sr-only"><?php echo $text_toggle_navigation; ?></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                      </button>
                    <a class="navbar-brand">
                        <i class="fa fa-users" aria-hidden="true"></i> <?php echo $heading_title; ?> v. <?php echo $v; ?>
                    </a>
                </div>
                <div class="collapse navbar-collapse" id="navbar-collapse-1">
                    <ul class="nav navbar-nav">
                    <?php if ($license) { ?>
                        <li class="dropdown<?php if ($license) { ?> active<?php } ?>">
                        	<a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"><i class="fa fa-cog"></i> <?php echo $text_settings; ?><span class="caret"></span></a>
                            <ul class="dropdown-menu">
                                <li<?php if ($license) { ?> class="active"<?php } ?>><a href="#common-settings" data-toggle="tab"><?php echo $text_common; ?></a></li>
                                <li><a href="#email-notification-settings" data-toggle="tab"><?php echo $text_email_notifications; ?></a></li>
                            </ul>
                        </li>
                        <li class="dropdown">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"><i class="fa fa-credit-card"></i> <?php echo $text_club_cards; ?><span class="caret"></span></a>
                            <ul class="dropdown-menu">
                                <li><a href="#cards-list" id="menu-cards-list" data-loadurl="<?php echo $cards_list; ?>" data-toggle="tab"><?php echo $text_lists; ?></a></li>
                                <li><a href="#card-form" id="menu-card-form" data-loadurl="<?php echo $card_add; ?>" data-toggle="tab"><?php echo $text_add; ?></a></li>
                                <li><a href="#cards-generator" id="menu-card-generator" data-loadurl="<?php echo $cards_generator; ?>" data-toggle="tab"><?php echo $text_generator; ?></a></li>
                                <li><a href="#cards-history" id="menu-cards-history" data-loadurl="<?php echo $cards_history; ?>" data-toggle="tab"><?php echo $text_general_history; ?></a></li>
                                <li role="separator" class="divider"></li>
                                <li><a href="#card-settings" data-toggle="tab"><?php echo $text_settings; ?></a></li>
                                <li role="separator" class="divider"></li>
                                <li><a href="#default-card-settings" data-toggle="tab"><?php echo $text_default_card_settings; ?></a></li>
                            </ul>
                        </li>
                        <li class="dropdown">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"><i class="fa fa-gift" aria-hidden="true"></i> <?php echo $text_gifts; ?><span class="caret"></span></a>
                            <ul class="dropdown-menu">
                                <li><a href="#gifts-list" id="menu-gifts-list" data-loadurl="<?php echo $gifts_list; ?>" data-toggle="tab"><?php echo $text_lists; ?></a></li>
                                <li><a href="#gift-form" id="menu-gift-form" data-loadurl="<?php echo $gift_add; ?>" data-toggle="tab"><?php echo $text_add; ?></a></li>
                                <li><a href="#gifts-history" id="menu-gifts-history" data-loadurl="<?php echo $gifts_history; ?>" data-toggle="tab"><?php echo $text_general_history; ?></a></li>
                                <li role="separator" class="divider"></li>
                                <li><a href="#gift-settings" data-toggle="tab"><?php echo $text_settings; ?></a></li>
                            </ul>
                        </li>
                    <?php } ?>    
                        <li class="dropdown">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"><i class="fa fa-life-ring"></i> <?php echo $text_support; ?><span class="caret"></span></a>
                            <ul class="dropdown-menu">
                                <li<?php if (!$license) { ?> class="active"<?php } ?>><a href="#support" data-toggle="tab"><?php echo $text_contacts; ?></a></li>
                                <li><a href="<?php echo $instruction_href; ?>" target="_blank"><?php echo $text_instruction; ?></a></li>
                            </ul>
                        </li>
                    </ul>
					<ul class="nav navbar-nav navbar-right" style="padding: 7.5px;">
						<div class="btn-group">
							<button type="button" onclick="save();" data-toggle="tooltip" title="<?php echo $button_save; ?>" class="btn btn-primary"><i class="fa fa-save"></i></button>
							<button type="button" class="btn btn-primary dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
								<span class="caret"></span>
								<span class="sr-only"></span>
							</button>
							<ul class="dropdown-menu dropdown-menu-right">
								<li><a onclick="$('#form-membership').submit();" style="cursor: pointer;"><i class="fa fa-sign-out fa-fw" aria-hidden="true"></i> <?php echo $button_save_and_exit; ?></a></li>
								<li><a onclick="settings('basic')" style="cursor: pointer;"><i class="fa fa-cloud-download fa-fw" aria-hidden="true"></i> <?php echo $button_download_basic_settings; ?></a></li>
								<li><a onclick="settings('import')" style="cursor: pointer;"><i class="fa fa-download fa-fw" aria-hidden="true"></i> <?php echo $button_import_settings; ?></a></li>
								<li><a onclick="settings('export');" style="cursor: pointer;"><i class="fa fa-upload fa-fw" aria-hidden="true"></i> <?php echo $button_export_settings; ?></a></li>
							</ul>
						</div>
						<a href="<?php echo $cancel; ?>" data-toggle="tooltip" title="<?php echo $button_cancel; ?>" class="btn btn-danger" role="button"><i class="fa fa-reply"></i></a>
            	    </ul>
                </div>
            </div>
        </nav>
   	 	<div class="panel panel-default no-upper-border">
      		<div class="panel-body">
      			<form action="<?php echo $save_and_exit; ?>" method="post" enctype="multipart/form-data" id="form-membership" class="form-horizontal">
	      			<div class="tab-content">
	      				<?php if ($license) { ?>
	      				<div class="tab-pane fade<?php if ($license) { ?> in active<?php } ?>" id="common-settings">
	      					<a href="" id="helper-ajax-menu" style="display: none;"></a>
	      					<div class="form-group">
	      						<label class="col-sm-2 control-label" for="status-enabled"><span data-toggle="tooltip" title="<?php echo $help_status; ?>"><?php echo $entry_status; ?></span></label>
	      						<div class="col-sm-4">
	      							<div class="radio-switch">
	      							<?php if (empty($membership['status'])) { ?>
						                <input type="radio" name="membership[status]" value="0" id="status-disabled" checked>
                                        <label class="col-sm-4" for="status-enabled"><?php echo $text_disabled; ?></label>
	      								<input type="radio" name="membership[status]" value="1" id="status-enabled">
                                        <label class="col-sm-4" for="status-disabled"><?php echo $text_enabled; ?></label>
						                <?php } else { ?>
                                        <input type="radio" name="membership[status]" value="0" id="status-disabled">
                                        <label class="col-sm-4" for="status-enabled"><?php echo $text_disabled; ?></label>
                                        <input type="radio" name="membership[status]" value="1" id="status-enabled" checked>
                                        <label class="col-sm-4" for="status-disabled"><?php echo $text_enabled; ?></label>  
						            <?php } ?>
	      							</div>
	      						</div>
	      					</div>
					        <div class="form-group">
	      						<label class="col-sm-2 control-label" for="condition"><span data-toggle="tooltip" title="<?php echo $help_condition; ?>"><?php echo $entry_condition; ?></span></label>
	      						<div class="col-sm-10">
	      							<select name="membership[condition]" id="condition" class="form-control">
	      								<option value="0"><?php echo $text_none; ?></option>
	      								<?php foreach ($informations as $information) { ?>
	      									<?php if (isset($membership['condition']) && $information['information_id'] == $membership['condition']) { ?>
	      								<option value="<?php echo $information['information_id']; ?>" selected="selected"><?php echo $information['title']; ?></option>
	      									<?php } else { ?>
	      								<option value="<?php echo $information['information_id']; ?>"><?php echo $information['title']; ?></option>
	      									<?php } ?>
	      								<?php } ?>
	      							</select>
	      						</div>
	      					</div>
	      					<div class="form-group">
                  				<label class="col-sm-2 control-label" for="customer_group_id"><span data-toggle="tooltip" title="<?php echo $help_customer_group; ?>"><?php echo $entry_customer_group; ?></span></label>
                  				<div class="col-sm-10">
                    				<select name="membership[customer_group_id]" id="customer_group_id" class="form-control">
                    				<option value="0"><?php echo $text_none; ?></option>
                      				<?php foreach ($customer_groups as $customer_group) { ?>
                      					<?php if (isset($membership['customer_group_id']) && $customer_group['customer_group_id'] == $membership['customer_group_id']) { ?>
                      					<option value="<?php echo $customer_group['customer_group_id']; ?>" selected="selected"><?php echo $customer_group['name']; ?></option>
                      					<?php } else { ?>
                      					<option value="<?php echo $customer_group['customer_group_id']; ?>"><?php echo $customer_group['name']; ?></option>
                      					<?php } ?>
                      				<?php } ?>
                    				</select>
                  				</div>
							</div>
							<div class="form-group">
                  				<label class="col-sm-2 control-label" for="auto_transfer_to_group-enabled"><span data-toggle="tooltip" title="<?php echo $help_auto_transfer_to_group; ?>"><?php echo $entry_auto_transfer_to_group; ?></span></label>
	      						<div class="col-sm-2">
	      							<div class="radio-switch">
	      							<?php if (empty($membership['auto_transfer_to_group'])) { ?>
						                <input type="radio" name="membership[auto_transfer_to_group]" value="0" id="auto_transfer_to_group-disabled" checked>
                                        <label class="col-sm-4" for="auto_transfer_to_group-enabled"><?php echo $text_no; ?></label>
	      								<input type="radio" name="membership[auto_transfer_to_group]" value="1" id="auto_transfer_to_group-enabled">
                                        <label class="col-sm-4" for="auto_transfer_to_group-disabled"><?php echo $text_yes; ?></label>
						                <?php } else { ?>
                                        <input type="radio" name="membership[auto_transfer_to_group]" value="0" id="auto_transfer_to_group-disabled">
                                        <label class="col-sm-4" for="auto_transfer_to_group-enabled"><?php echo $text_no; ?></label>
                                        <input type="radio" name="membership[auto_transfer_to_group]" value="1" id="auto_transfer_to_group-enabled" checked>
                                        <label class="col-sm-4" for="auto_transfer_to_group-disabled"><?php echo $text_yes; ?></label>  
						            <?php } ?>
	      							</div>
	      						</div>
                			</div>
						</div>
						<div class="tab-pane fade" id="email-notification-settings">
							<a class="btn btn-default pull-right" role="button" data-toggle="collapse" href="#message-macros-collapse" aria-expanded="false" aria-controls="message-macros-collapse"><?php echo $text_message_template_macros; ?> <i class="fa fa-angle-down" aria-hidden="true"></i></a>
							<div class="clearfix"></div>
					        <div id="message-macros-collapse" class="collapse">
					        	<div class="panel panel-default">
					        		<div class="panel-body">
					        			<div class="alert alert-info" role="alert">
											<button type="button" class="close" data-dismiss="alert">&times;</button>
									  		<p><i class="fa fa-info-circle" aria-hidden="true"></i> <?php echo $text_about_macros; ?></p>
										</div>
						        		<div class="col-sm-3">
						        			<?php echo $text_card_template_macros; ?>
						        		</div>
						        		<div class="col-sm-3">
						        			<?php echo $text_card_hisory_template; ?>
						        		</div>
						        		<div class="col-sm-3">
						        			<?php echo $text_gift_template_macros; ?>
						        		</div>
						        		<div class="col-sm-3">
						        			<?php echo $text_customer_template_macros; ?>
						        		</div>
					        		</div>
					        	</div>
					        </div>
							<legend><?php echo $text_club_cards; ?></legend>
							<div class="form-group">
	      						<label class="col-sm-2 control-label" for="notification_customer_creation-enabled"><span data-toggle="tooltip" title="<?php echo $help_notification_customer_creation; ?>"><?php echo $entry_notification_customer_creation; ?></span></label>
	      						<div class="col-sm-1">
	      							<div class="radio-switch">
	      							<?php if (empty($membership_notification['customer_creation'])) { ?>
						                <input type="radio" name="membership_notification[customer_creation]" value="0" id="notification_customer_creation-disabled" checked>
                                        <label class="col-sm-4" for="notification_customer_creation-enabled"><?php echo $text_no; ?></label>
	      								<input type="radio" name="membership_notification[customer_creation]" value="1" id="notification_customer_creation-enabled">
                                        <label class="col-sm-4" for="notification_customer_creation-disabled"><?php echo $text_yes; ?></label>
						                <?php } else { ?>
                                        <input type="radio" name="membership_notification[customer_creation]" value="0" id="notification_customer_creation-disabled">
                                        <label class="col-sm-4" for="notification_customer_creation-enabled"><?php echo $text_no; ?></label>
                                        <input type="radio" name="membership_notification[customer_creation]" value="1" id="notification_customer_creation-enabled" checked>
                                        <label class="col-sm-4" for="notification_customer_creation-disabled"><?php echo $text_yes; ?></label>  
						            <?php } ?>
	      							</div>
	      						</div>
	      						<label class="col-sm-1 control-label" for="notification_customer_creation_template"><?php echo $entry_message_template; ?></label>
								<div class="col-sm-8">
									<ul class="nav nav-tabs" role="tablist">
									<?php foreach ($languages as $language) { ?>
										<li<?php echo ($language_id == $language['language_id']) ? ' class="active"' : '' ?>><a href="#notification_customer_creation_template_<?php echo $language['language_id']; ?>" aria-controls="notification_customer_creation_template_<?php echo $language['language_id']; ?>" role="tab" data-toggle="tab"><img src="<?php echo $language_flag[$language['language_id']]; ?>" title="<?php echo $language['name']; ?>" /> <?php echo $language['name']; ?></a></li>
									<?php } ?>	
									</ul>
									<div class="tab-content">
									<?php foreach ($languages as $language) { ?>
										<div role="tabpanel" class="tab-pane<?php echo ($language_id == $language['language_id']) ? ' active' : '' ?>" id="notification_customer_creation_template_<?php echo $language['language_id']; ?>">	
											<textarea name="membership_notification[customer_creation_template][<?php echo $language['language_id']; ?>]" placeholder="<?php echo $entry_message_template; ?>" class="form-control summernote"><?php isset($membership_notification['customer_creation_template'][$language['language_id']]) && print($membership_notification['customer_creation_template'][$language['language_id']]); ?></textarea>
										</div>
									<?php } ?>
									</div>
								</div>
	      					</div>
							<div class="form-group">
	      						<label class="col-sm-2 control-label" for="notification_card_issuance-enabled"><span data-toggle="tooltip" title="<?php echo $help_notification_card_issuance; ?>"><?php echo $entry_notification_card_issuance; ?></span></label>
	      						<div class="col-sm-1">
	      							<div class="radio-switch">
	      							<?php if (empty($membership_notification['card_issuance'])) { ?>
						                <input type="radio" name="membership_notification[card_issuance]" value="0" id="notification_card_issuance-disabled" checked>
                                        <label class="col-sm-4" for="notification_card_issuance-enabled"><?php echo $text_no; ?></label>
	      								<input type="radio" name="membership_notification[card_issuance]" value="1" id="notification_card_issuance-enabled">
                                        <label class="col-sm-4" for="notification_card_issuance-disabled"><?php echo $text_yes; ?></label>
						                <?php } else { ?>
                                        <input type="radio" name="membership_notification[card_issuance]" value="0" id="notification_card_issuance-disabled">
                                        <label class="col-sm-4" for="notification_card_issuance-enabled"><?php echo $text_no; ?></label>
                                        <input type="radio" name="membership_notification[card_issuance]" value="1" id="notification_card_issuance-enabled" checked>
                                        <label class="col-sm-4" for="notification_card_issuance-disabled"><?php echo $text_yes; ?></label>  
						            <?php } ?>
	      							</div>
	      						</div>
	      						<label class="col-sm-1 control-label" for="notification_card_issuance_template"><?php echo $entry_message_template; ?></label>
								<div class="col-sm-8">
									<ul class="nav nav-tabs" role="tablist">
									<?php foreach ($languages as $language) { ?>
										<li<?php echo ($language_id == $language['language_id']) ? ' class="active"' : '' ?>><a href="#notification_card_issuance_template_<?php echo $language['language_id']; ?>" aria-controls="notification_card_issuance_template_<?php echo $language['language_id']; ?>" role="tab" data-toggle="tab"><img src="<?php echo $language_flag[$language['language_id']]; ?>" title="<?php echo $language['name']; ?>" /> <?php echo $language['name']; ?></a></li>
									<?php } ?>	
									</ul>
									<div class="tab-content">
									<?php foreach ($languages as $language) { ?>
										<div role="tabpanel" class="tab-pane<?php echo ($language_id == $language['language_id']) ? ' active' : '' ?>" id="notification_card_issuance_template_<?php echo $language['language_id']; ?>">	
											<textarea name="membership_notification[card_issuance_template][<?php echo $language['language_id']; ?>]" placeholder="<?php echo $entry_message_template; ?>" class="form-control summernote"><?php isset($membership_notification['card_issuance_template'][$language['language_id']]) && print($membership_notification['card_issuance_template'][$language['language_id']]); ?></textarea>
										</div>
									<?php } ?>
									</div>
								</div>
	      					</div>
	      					<div class="form-group">
	      						<label class="col-sm-2 control-label" for="notification_card_balance_change-enabled"><span data-toggle="tooltip" title="<?php echo $help_notification_card_balance_change; ?>"><?php echo $entry_notification_card_balance_change; ?></span></label>
	      						<div class="col-sm-1">
	      							<div class="radio-switch">
	      							<?php if (empty($membership_notification['card_balance_change'])) { ?>
						                <input type="radio" name="membership_notification[card_balance_change]" value="0" id="notification_card_balance_change-disabled" checked>
                                        <label class="col-sm-4" for="notification_card_balance_change-enabled"><?php echo $text_no; ?></label>
	      								<input type="radio" name="membership_notification[card_balance_change]" value="1" id="notification_card_balance_change-enabled">
                                        <label class="col-sm-4" for="notification_card_balance_change-disabled"><?php echo $text_yes; ?></label>
						                <?php } else { ?>
                                        <input type="radio" name="membership_notification[card_balance_change]" value="0" id="notification_card_balance_change-disabled">
                                        <label class="col-sm-4" for="notification_card_balance_change-enabled"><?php echo $text_no; ?></label>
                                        <input type="radio" name="membership_notification[card_balance_change]" value="1" id="notification_card_balance_change-enabled" checked>
                                        <label class="col-sm-4" for="notification_card_balance_change-disabled"><?php echo $text_yes; ?></label>  
						            <?php } ?>
	      							</div>
	      						</div>
	      						<label class="col-sm-1 control-label" for="notification_card_balance_change_template"><?php echo $entry_message_template; ?></label>
								<div class="col-sm-8">
									<ul class="nav nav-tabs" role="tablist">
									<?php foreach ($languages as $language) { ?>
										<li<?php echo ($language_id == $language['language_id']) ? ' class="active"' : '' ?>><a href="#notification_card_balance_change_template_<?php echo $language['language_id']; ?>" aria-controls="notification_card_balance_change_template_<?php echo $language['language_id']; ?>" role="tab" data-toggle="tab"><img src="<?php echo $language_flag[$language['language_id']] ?>" title="<?php echo $language['name']; ?>" /> <?php echo $language['name']; ?></a></li>
									<?php } ?>	
									</ul>
									<div class="tab-content">
									<?php foreach ($languages as $language) { ?>
										<div role="tabpanel" class="tab-pane<?php echo ($language_id == $language['language_id']) ? ' active' : '' ?>" id="notification_card_balance_change_template_<?php echo $language['language_id']; ?>">	
											<textarea name="membership_notification[card_balance_change_template][<?php echo $language['language_id']; ?>]" placeholder="<?php echo $entry_message_template; ?>" class="form-control summernote"><?php isset($membership_notification['card_balance_change_template'][$language['language_id']]) && print($membership_notification['card_balance_change_template'][$language['language_id']]); ?></textarea>
										</div>
									<?php } ?>
									</div>
								</div>
	      					</div>
							<legend><?php echo $text_gifts; ?></legend>
							<div class="form-group">
	      						<label class="col-sm-2 control-label" for="notification_gift_confirm-enabled"><span data-toggle="tooltip" title="<?php echo $help_notification_gift_confirm; ?>"><?php echo $entry_notification_gift_confirm; ?></span></label>
	      						<div class="col-sm-1">
	      							<div class="radio-switch">
	      							<?php if (empty($membership_notification['gift_confirm'])) { ?>
						                <input type="radio" name="membership_notification[gift_confirm]" value="0" id="notification_gift_confirm-disabled" checked>
                                        <label class="col-sm-4" for="notification_gift_confirm-enabled"><?php echo $text_no; ?></label>
	      								<input type="radio" name="membership_notification[gift_confirm]" value="1" id="notification_gift_confirm-enabled">
                                        <label class="col-sm-4" for="notification_gift_confirm-disabled"><?php echo $text_yes; ?></label>
						                <?php } else { ?>
                                        <input type="radio" name="membership_notification[gift_confirm]" value="0" id="notification_gift_confirm-disabled">
                                        <label class="col-sm-4" for="notification_gift_confirm-enabled"><?php echo $text_no; ?></label>
                                        <input type="radio" name="membership_notification[gift_confirm]" value="1" id="notification_gift_confirm-enabled" checked>
                                        <label class="col-sm-4" for="notification_gift_confirm-disabled"><?php echo $text_yes; ?></label>  
						            <?php } ?>
	      							</div>
	      						</div>
	      						<label class="col-sm-1 control-label" for="notification_gift_confirm_template"><?php echo $entry_message_template; ?></label>
								<div class="col-sm-8">
									<ul class="nav nav-tabs" role="tablist">
									<?php foreach ($languages as $language) { ?>
										<li<?php echo ($language_id == $language['language_id']) ? ' class="active"' : '' ?>><a href="#notification_gift_confirm_template_<?php echo $language['language_id']; ?>" aria-controls="notification_gift_confirm_template_<?php echo $language['language_id']; ?>" role="tab" data-toggle="tab"><img src="<?php echo $language_flag[$language['language_id']] ?>" title="<?php echo $language['name']; ?>" /> <?php echo $language['name']; ?></a></li>
									<?php } ?>	
									</ul>
									<div class="tab-content">
									<?php foreach ($languages as $language) { ?>
										<div role="tabpanel" class="tab-pane<?php echo ($language_id == $language['language_id']) ? ' active' : '' ?>" id="notification_gift_confirm_template_<?php echo $language['language_id']; ?>">	
											<textarea name="membership_notification[gift_confirm_template][<?php echo $language['language_id']; ?>]" placeholder="<?php echo $entry_message_template; ?>" class="form-control summernote"><?php isset($membership_notification['gift_confirm_template'][$language['language_id']]) && print($membership_notification['gift_confirm_template'][$language['language_id']]); ?></textarea>
										</div>
									<?php } ?>
									</div>
								</div>
	      					</div>
						</div>
						<div class="tab-pane fade" id="cards-list"></div>
						<div class="tab-pane fade" id="card-form"></div>
						<div class="tab-pane fade" id="cards-generator"></div>
						<div class="tab-pane fade" id="cards-history"></div>
                        <div class="tab-pane fade" id="card-settings">
                            <div class="form-group">
	      						<label class="col-sm-2 control-label" for="card_status-enabled"><span data-toggle="tooltip" title="<?php echo $help_status_total; ?>"><?php echo $entry_status_total; ?></span></label>
	      						<div class="col-sm-4">
	      							<div class="radio-switch">
	      							<?php if ($membership_card_status) { ?>
						                <input type="radio" name="membership_card_status" value="0" id="card_status-disabled">
                                        <label class="col-sm-4" for="card_status-enabled"><?php echo $text_disabled; ?></label>
	      								<input type="radio" name="membership_card_status" value="1" id="card_status-enabled" checked>
                                        <label class="col-sm-4" for="card_status-disabled"><?php echo $text_enabled; ?></label>
						                <?php } else { ?>
                                        <input type="radio" name="membership_card_status" value="0" id="card_status-disabled" checked>
                                        <label class="col-sm-4" for="card_status-enabled"><?php echo $text_disabled; ?></label>
                                        <input type="radio" name="membership_card_status" value="1" id="card_status-enabled">
                                        <label class="col-sm-4" for="card_status-disabled"><?php echo $text_enabled; ?></label>  
						            <?php } ?>
	      							</div>
	      						</div>
	      					</div>
	      					<hr />
							<div class="col-sm-10 col-sm-offset-2">
								<ul class="nav nav-tabs" role="tablist">
								<?php foreach ($languages as $language) { ?>
									<li<?php echo ($language_id == $language['language_id']) ? ' class="active"' : '' ?>><a href="#card_<?php echo $language['language_id']; ?>" aria-controls="card_<?php echo $language['language_id']; ?>" role="tab" data-toggle="tab"><img src="<?php echo $language_flag[$language['language_id']] ?>" title="<?php echo $language['name']; ?>" /> <?php echo $language['name']; ?></a></li>
								<?php } ?>	
								</ul>		
							</div>
							<div class="tab-content">
							<?php foreach ($languages as $language) { ?>
								<div role="tabpanel" class="tab-pane<?php echo ($language_id == $language['language_id']) ? ' active' : '' ?>" id="card_<?php echo $language['language_id']; ?>">	
									<div class="form-group">
										<label class="col-sm-2 control-label" for="card_name_<?php echo $language['language_id'] ?>"><span data-toggle="tooltip" title="<?php echo $help_name_total; ?>"><?php echo $entry_name_total; ?></span></label>
										<div class="col-sm-10">
											<input type="text" name="membership_card[name][<?php echo $language['language_id']; ?>]" value="<?php isset($membership_card['name'][$language['language_id']]) && print($membership_card['name'][$language['language_id']]); ?>" placeholder="<?php echo $entry_name_total; ?>" id="card_name_<?php echo $language['language_id'] ?>" class="form-control" />
										</div>
									</div>
									<div class="form-group">	
			      						<label class="col-sm-2 control-label" for="nearest_discount_description_<?php echo $language['language_id'] ?>"><span data-toggle="tooltip" title="<?php echo $help_nearest_discount_description; ?>"><?php echo $entry_nearest_discount_description; ?></span></label>
										<div class="col-sm-10">
											<textarea name="membership_card[nearest_discount_description][<?php echo $language['language_id']; ?>]" placeholder="<?php echo $entry_nearest_discount_description; ?>" class="form-control summernote"><?php isset($membership_card['nearest_discount_description'][$language['language_id']]) && print($membership_card['nearest_discount_description'][$language['language_id']]); ?></textarea>
										</div> 
								    </div>
								</div>
							<?php } ?>
							</div>
							<hr />
							<div class="form-group">
						    	<label class="col-sm-2 control-label" for="nearest_discount-enabled"><span data-toggle="tooltip" title="<?php echo $help_nearest_discount; ?>"><?php echo $entry_nearest_discount; ?></span></label>
	      						<div class="col-sm-2">
	      							<div class="radio-switch">
	      							<?php if (empty($membership_card['nearest_discount'])) { ?>
						                <input type="radio" name="membership_card[nearest_discount]" value="0" id="nearest_discount-disabled" checked>
                                        <label class="col-sm-4" for="nearest_discount-enabled"><?php echo $text_no; ?></label>
	      								<input type="radio" name="membership_card[nearest_discount]" value="1" id="nearest_discount-enabled">
                                        <label class="col-sm-4" for="nearest_discount-disabled"><?php echo $text_yes; ?></label>
						                <?php } else { ?>
                                        <input type="radio" name="membership_card[nearest_discount]" value="0" id="nearest_discount-disabled">
                                        <label class="col-sm-4" for="nearest_discount-enabled"><?php echo $text_no; ?></label>
                                        <input type="radio" name="membership_card[nearest_discount]" value="1" id="nearest_discount-enabled" checked>
                                        <label class="col-sm-4" for="nearest_discount-disabled"><?php echo $text_yes; ?></label>  
						            <?php } ?>
	      							</div>
	      						</div>
	      					</div>
                            <div class="form-group required">
								<label class="col-sm-2 control-label" for="code_characters"><span data-toggle="tooltip" title="<?php echo $help_code_characters; ?>"><?php echo $entry_code_characters; ?></span></label>
								<div class="col-sm-10">
									<div class="well well-sm" style="height: 135px; overflow: auto;">
									<?php foreach ($code_characters as $k => $v) { ?>
										<div class="checkbox">
											<label>
										<?php if (!empty($membership_card['code_characters']) && is_array($membership_card['code_characters']) && in_array($k, $membership_card['code_characters'])) { ?>
												<input type="checkbox" name="membership_card[code_characters][]" value="<?php echo $k; ?>" id="code_characters-<?php echo $k; ?>" checked="checked" /> <?php echo $v; ?>
										<?php } else { ?>
												<input type="checkbox" name="membership_card[code_characters][]" value="<?php echo $k; ?>" id="code_characters-<?php echo $k; ?>" /> <?php echo $v; ?>
										<?php } ?>
											</label>
										</div>
									<?php } ?>
									</div>
									<?php if ($error_code_characters) { ?>
									<div class="text-danger"><?php echo $error_code_characters; ?></div>
									<?php } ?>
								</div>
							</div>
							<div class="form-group">
                                <label class="col-sm-2 control-label" for="code_prefix"><span data-toggle="tooltip" title="<?php echo $help_code_prefix; ?>"><?php echo $entry_code_prefix; ?></span></label>
								<div class="col-sm-10">
					                <input type="text" name="membership_card[code_prefix]" value="<?php isset($membership_card['code_prefix']) && print($membership_card['code_prefix']); ?>" placeholder="<?php echo $entry_code_prefix; ?>" id="code_prefix" class="form-control" />
					            </div>
							</div>
							<div class="form-group required">
								<label class="col-sm-2 control-label" for="code_length"><span data-toggle="tooltip" title="<?php echo $help_code_length; ?>"><?php echo $entry_code_length; ?></span></label>
								<div class="col-sm-10">
					                <input type="text" name="membership_card[code_length]" value="<?php isset($membership_card['code_length']) && print($membership_card['code_length']); ?>" placeholder="<?php echo $entry_code_length; ?>" id="code_length" class="form-control" />
									<?php if ($error_code_length) { ?>
									<div class="text-danger"><?php echo $error_code_length; ?></div>
									<?php } ?>
					            </div>
					        </div>
					        <div class="form-group">
					        	<label class="col-sm-2 control-label" for="order_expiry"><span data-toggle="tooltip" title="<?php echo $help_order_expiry; ?>"><?php echo $entry_order_expiry; ?></span></label>
						        <div class="col-sm-5">
						        	<select name="membership_card[order_expiry][type]" id="order_expiry_type" class="form-control">
						            <?php foreach ($time as $k => $v) { ?>
						            	<?php if (isset($membership_card['order_expiry']['type']) && $k == $membership_card['order_expiry']['type']) { ?>
						            	<option value="<?php echo $k; ?>" selected="selected"><?php echo $v; ?></option>
						            	<?php } else { ?>
						            	<option value="<?php echo $k; ?>"><?php echo $v; ?></option>
						            	<?php } ?>
						            <?php } ?>
						            </select>
						        </div>
						        <div class="col-sm-5">		
						            <input type="text" name="membership_card[order_expiry][value]" value="<?php isset($membership_card['order_expiry']['value']) && print($membership_card['order_expiry']['value']); ?>" placeholder="<?php echo $entry_order_expiry; ?>" id="order_expiry" class="form-control" />
						        </div>
					        </div>
                            <div class="form-group required">
                                <label class="col-sm-2 control-label" for="accounted_orders_balance"><span data-toggle="tooltip" title="<?php echo $help_accounted_orders_balance; ?>"><?php echo $entry_accounted_orders_balance; ?></span></label>
	      						<div class="col-sm-10">
	      							<div class="well well-sm" style="height: 135px; overflow: auto;">
									<?php foreach ($order_types_b as $k => $v) { ?>
										<div class="checkbox">
											<label>
										<?php if (!empty($membership_card['accounted_orders_balance']) && is_array($membership_card['accounted_orders_balance']) && in_array($k, $membership_card['accounted_orders_balance'])) { ?>
												<input type="checkbox" name="membership_card[accounted_orders_balance][]" value="<?php echo $k; ?>" id="accounted_orders_balance-<?php echo $k; ?>" checked="checked" /> <?php echo $v; ?>
										<?php } else { ?>
												<input type="checkbox" name="membership_card[accounted_orders_balance][]" value="<?php echo $k; ?>" id="accounted_orders_balance-<?php echo $k; ?>" /> <?php echo $v; ?>
										<?php } ?>
											</label>
										</div>
									<?php } ?>
									</div>
									<?php if ($error_accounted_orders_balance) { ?>
									<div class="text-danger"><?php echo $error_accounted_orders_balance; ?></div>
									<?php } ?>
	      						</div>
							</div>
							<div class="form-group required">
                                <label class="col-sm-2 control-label" for="accounted_orders_request"><span data-toggle="tooltip" title="<?php echo $help_accounted_orders_request; ?>"><?php echo $entry_accounted_orders_request; ?></span></label>
	      						<div class="col-sm-10">
	      							<div class="well well-sm" style="height: 135px; overflow: auto;">
									<?php foreach ($order_types_r as $k => $v) { ?>
										<div class="checkbox">
											<label>
										<?php if (!empty($membership_card['accounted_orders_request']) && is_array($membership_card['accounted_orders_request']) && in_array($k, $membership_card['accounted_orders_request'])) { ?>
												<input type="checkbox" name="membership_card[accounted_orders_request][]" value="<?php echo $k; ?>" id="accounted_orders_request-<?php echo $k; ?>" checked="checked" /> <?php echo $v; ?>
										<?php } else { ?>
												<input type="checkbox" name="membership_card[accounted_orders_request][]" value="<?php echo $k; ?>" id="accounted_orders_request-<?php echo $k; ?>" /> <?php echo $v; ?>
										<?php } ?>
											</label>
										</div>
									<?php } ?>
									</div>
									<?php if ($error_accounted_orders_request) { ?>
									<div class="text-danger"><?php echo $error_accounted_orders_request; ?></div>
									<?php } ?>
	      						</div>
	      					</div>
                            <div class="form-group">	
                                <label class="col-sm-2 control-label" for="request_threshold"><span data-toggle="tooltip" title="<?php echo $help_request_threshold; ?>"><?php echo $entry_threshold; ?></span></label>
				                <div class="col-sm-10">
				                	<div class="input-group">
				                		<input type="text" name="membership_card[request_threshold]" value="<?php isset($membership_card['request_threshold']) && print($membership_card['request_threshold']); ?>" placeholder="<?php echo $entry_threshold; ?>" id="request_threshold" class="form-control" />
				                		<span class="input-group-addon"><?php echo $currency_symbol; ?></span>
	    							</div>
				                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-2 control-label" for="auto_issuance_card"><span data-toggle="tooltip" title="<?php echo $help_auto_issuance_card; ?>"><?php echo $entry_auto_issuance_card; ?></span></label>
	      						<div class="col-sm-10">
	      							<select name="membership_card[auto_issuance_card]" id="auto_issuance_card" class="form-control">
	      								<?php foreach ($auto_issuance_types as $k => $v) { ?>
	      									<?php if (isset($membership_card['auto_issuance_card']) && $k == $membership_card['auto_issuance_card']) { ?>
	      								<option value="<?php echo $k; ?>" selected="selected"><?php echo $v; ?></option>
	      									<?php } else { ?>
	      								<option value="<?php echo $k; ?>"><?php echo $v; ?></option>
	      									<?php } ?>
	      								<?php } ?>
	      							</select>
	      						</div>
                            </div>
                            <div class="form-group">
                            	<label class="col-sm-2 control-label" for="logged_card_request-enabled"><span data-toggle="tooltip" title="<?php echo $help_logged_card_request; ?>"><?php echo $entry_logged_card_request; ?></span></label>
	      						<div class="col-sm-2">
	      							<div class="radio-switch">
	      							<?php if (empty($membership_card['logged_card_request'])) { ?>
						                <input type="radio" name="membership_card[logged_card_request]" value="0" id="logged_card_request-disabled" checked>
                                        <label class="col-sm-4" for="logged_card_request-enabled"><?php echo $text_no; ?></label>
	      								<input type="radio" name="membership_card[logged_card_request]" value="1" id="logged_card_request-enabled">
                                        <label class="col-sm-4" for="logged_card_request-disabled"><?php echo $text_yes; ?></label>
						                <?php } else { ?>
                                        <input type="radio" name="membership_card[logged_card_request]" value="0" id="logged_card_request-disabled">
                                        <label class="col-sm-4" for="logged_card_request-enabled"><?php echo $text_no; ?></label>
                                        <input type="radio" name="membership_card[logged_card_request]" value="1" id="logged_card_request-enabled" checked>
                                        <label class="col-sm-4" for="logged_card_request-disabled"><?php echo $text_yes; ?></label>  
						            <?php } ?>
	      							</div>
	      						</div>  
                            </div>
                            <div class="form-group">
                            	<label class="col-sm-2 control-label" for="card_request_discount-enabled"><span data-toggle="tooltip" title="<?php echo $help_card_request_discount; ?>"><?php echo $entry_card_request_discount; ?></span></label>
	      						<div class="col-sm-2">
	      							<div class="radio-switch">
	      							<?php if (empty($membership_card['request_discount'])) { ?>
						                <input type="radio" name="membership_card[request_discount]" value="0" id="card_request_discount-disabled" checked>
                                        <label class="col-sm-4" for="card_request_discount-enabled"><?php echo $text_no; ?></label>
	      								<input type="radio" name="membership_card[request_discount]" value="1" id="card_request_discount-enabled">
                                        <label class="col-sm-4" for="card_request_discount-disabled"><?php echo $text_yes; ?></label>
						                <?php } else { ?>
                                        <input type="radio" name="membership_card[request_discount]" value="0" id="card_request_discount-disabled">
                                        <label class="col-sm-4" for="card_request_discount-enabled"><?php echo $text_no; ?></label>
                                        <input type="radio" name="membership_card[request_discount]" value="1" id="card_request_discount-enabled" checked>
                                        <label class="col-sm-4" for="card_request_discount-disabled"><?php echo $text_yes; ?></label>  
						            <?php } ?>
	      							</div>
	      						</div>
	      						<label class="col-sm-2 control-label" for="first_discount_type"><span data-toggle="tooltip" title="<?php echo $help_first_discount_type; ?>"><?php echo $entry_first_discount_type; ?></span></label>
	      						<div class="col-sm-2">
	      							<select name="membership_card[first_discount_type]" id="first_discount_type" class="form-control">
	      								<?php foreach ($first_discount_type as $k => $v) { ?>
	      									<?php if (isset($membership_card['first_discount_type']) && $k == $membership_card['first_discount_type']) { ?>
	      								<option value="<?php echo $k; ?>" selected="selected"><?php echo $v; ?></option>
	      									<?php } else { ?>
	      								<option value="<?php echo $k; ?>"><?php echo $v; ?></option>
	      									<?php } ?>
	      								<?php } ?>
	      							</select>
	      						</div>
	      						<label class="col-sm-2 control-label" for="first_discount_value"><span data-toggle="tooltip" title="<?php echo $help_first_discount_value; ?>"><?php echo $entry_first_discount_value; ?></span></label>
				                <div class="col-sm-2">
				                	<input type="text" name="membership_card[first_discount_value]" value="<?php isset($membership_card['first_discount_value']) && print($membership_card['first_discount_value']); ?>" placeholder="<?php echo $entry_first_discount_value; ?>" id="first_discount_value" class="form-control" />

				                </div>     
				            </div>
						    <div class="form-group">
						    	<label class="col-sm-2 control-label" for="logged_auto_discount-enabled"><span data-toggle="tooltip" title="<?php echo $help_logged_auto_discount; ?>"><?php echo $entry_logged_auto_discount; ?></span></label>
	      						<div class="col-sm-2">
	      							<div class="radio-switch">
	      							<?php if (empty($membership_card['logged_auto_discount'])) { ?>
						                <input type="radio" name="membership_card[logged_auto_discount]" value="0" id="logged_auto_discount-disabled" checked>
                                        <label class="col-sm-4" for="logged_auto_discount-enabled"><?php echo $text_no; ?></label>
	      								<input type="radio" name="membership_card[logged_auto_discount]" value="1" id="logged_auto_discount-enabled">
                                        <label class="col-sm-4" for="logged_auto_discount-disabled"><?php echo $text_yes; ?></label>
						                <?php } else { ?>
                                        <input type="radio" name="membership_card[logged_auto_discount]" value="0" id="logged_auto_discount-disabled">
                                        <label class="col-sm-4" for="logged_auto_discount-enabled"><?php echo $text_no; ?></label>
                                        <input type="radio" name="membership_card[logged_auto_discount]" value="1" id="logged_auto_discount-enabled" checked>
                                        <label class="col-sm-4" for="logged_auto_discount-disabled"><?php echo $text_yes; ?></label>  
						            <?php } ?>
	      							</div>
	      						</div> 
						    </div>
                            <div class="form-group">
				                <label class="col-sm-2 control-label" for="minimum_cost_goods"><span data-toggle="tooltip" title="<?php echo $help_minimum_cost_goods; ?>"><?php echo $entry_minimum_cost_goods; ?></span></label>
				                <div class="col-sm-10">
				                	<div class="input-group">
				                		<input type="text" name="membership_card[minimum_cost_goods]" value="<?php isset($membership_card['minimum_cost_goods'])&& print($membership_card['minimum_cost_goods']); ?>" placeholder="<?php echo $entry_minimum_cost_goods; ?>" id="minimum_cost_goods" class="form-control" />
				                		<span class="input-group-addon"><?php echo $currency_symbol; ?></span>
				                	</div>	
				                </div>
                            </div>
                            <div class="form-group">
				            	<label class="col-sm-2 control-label" for="maximum_card_discount"><span data-toggle="tooltip" title="<?php echo $help_maximum_card_discount; ?>"><?php echo $entry_maximum_discount; ?></span></label>
				            	<div class="col-sm-10">
					            	<div class="input-group">
				              			<input type="text" name="membership_card[maximum_card_discount]" value="<?php isset($membership_card['maximum_card_discount']) && print($membership_card['maximum_card_discount']); ?>" placeholder="<?php echo $entry_maximum_discount; ?>" id="maximum_card_discount" class="form-control" />
				              			<span class="input-group-addon"><?php echo $text_pct; ?></span>
				              		</div>
			              		</div>
				          	</div>
						</div>
                        <div class="tab-pane fade" id="default-card-settings">
	      					<div class="form-group">
	      						<label class="col-sm-2 control-label" for="default-card-status-enabled"><span data-toggle="tooltip" title="<?php echo $help_card_status; ?>"><?php echo $entry_card_status; ?></span></label>
	      						<div class="col-sm-4">
	      							<div class="radio-switch">
	      							<?php if (empty($membership_default_card['status'])) { ?>
						                <input type="radio" name="membership_default_card[status]" value="0" id="default-card-status-disabled" checked>
                                        <label class="col-sm-4" for="default-card-status-enabled"><?php echo $text_disabled; ?></label>
	      								<input type="radio" name="membership_default_card[status]" value="1" id="default-card-status-enabled">
                                        <label class="col-sm-4" for="default-card-status-disabled"><?php echo $text_enabled; ?></label>
						                <?php } else { ?>
                                        <input type="radio" name="membership_default_card[status]" value="0" id="default-card-status-disabled">
                                        <label class="col-sm-4" for="default-card-status-enabled"><?php echo $text_disabled; ?></label>
                                        <input type="radio" name="membership_default_card[status]" value="1" id="default-card-status-enabled" checked>
                                        <label class="col-sm-4" for="default-card-status-disabled"><?php echo $text_enabled; ?></label>  
						            <?php } ?>
	      							</div>
	      						</div>
	      					</div>
				            <div class="form-group">
				            	<label class="col-sm-2 control-label" for="default-card-date_start"><span data-toggle="tooltip" title="<?php echo $help_card_start; ?>"><?php echo $entry_start; ?></span></label>
	    						<div class="col-sm-4">
	    							<div class="input-group">
	    								<span class="input-group-btn" style="width: 50%;">
	    									<select name="membership_default_card[date_start][type]" class="form-control">
	    									<?php foreach ($time as $k => $v) { ?>
	    										<?php if (isset($membership_default_card['date_start']['type']) && $k == $membership_default_card['date_start']['type']) { ?>
	    										<option value="<?php echo $k; ?>" selected="selected"><?php echo $v; ?></option>
	    										<?php } else { ?>
								                <option value="<?php echo $k; ?>"><?php echo $v; ?></option>
								                <?php } ?>
								            <?php } ?>
								            </select>
								        </span>
								     	<input type="text" name="membership_default_card[date_start][value]" value="<?php isset($membership_default_card['date_start']['value']) && print($membership_default_card['date_start']['value']); ?>" placeholder="<?php echo $entry_start; ?>" id="default-card-date_start" class="form-control" />
								    </div>
								</div>
						        <label class="col-sm-2 control-label" for="default-card-date_expiry"><span data-toggle="tooltip" title="<?php echo $help_card_expiry; ?>"><?php echo $entry_expiry; ?></span></label>
						        <div class="col-sm-4">
						        	<div class="input-group">
						        		<span class="input-group-btn" style="width: 50%;">
						        			<select name="membership_default_card[date_expiry][type]" class="form-control">
						            		<?php foreach ($time as $k => $v) { ?>
						            			<?php if (isset($membership_default_card['date_expiry']['type']) && $k == $membership_default_card['date_expiry']['type']) { ?>
						            			<option value="<?php echo $k; ?>" selected="selected"><?php echo $v; ?></option>
						            			<?php } else { ?>
						            			<option value="<?php echo $k; ?>"><?php echo $v; ?></option>
						            			<?php } ?>
						            		<?php } ?>
						            		</select>
						            	</span>
						            	<input type="text" name="membership_default_card[date_expiry][value]" value="<?php isset($membership_default_card['date_expiry']['value']) && print($membership_default_card['date_expiry']['value']); ?>" placeholder="<?php echo $entry_expiry; ?>" id="default-card-date_expiry" class="form-control" />
						            </div>
						        </div>
						    </div>
					        <div class="panel panel-default">
					        	<div class="panel-heading">
					            	<h3 class="panel-title"><i class="fa fa-table"></i> <?php echo $text_discount_table; ?></h3>
					            </div>
					            <div class="panel-body">
					            	<div class="table-responsive">
					            		<table class="table table-bordered table-hover" id="table-discounts">
					            			<thead>
					            				<tr>
					            					<td class="text-center"><span data-toggle="tooltip" title="<?php echo $help_discount_base; ?>"><?php echo $column_discount_base; ?></span></td>
					            					<td class="text-center"><span data-toggle="tooltip" title="<?php echo $help_discount_limit; ?>"><?php echo $column_discount_limit; ?></span></td>
					            					<td class="text-center"><span data-toggle="tooltip" title="<?php echo $help_discount_value; ?>"><?php echo $column_discount_value; ?></span></td>
					            					<td class="text-center"><span data-toggle="tooltip" title="<?php echo $help_discount_type; ?>"><?php echo $column_discount_type; ?></span></td>
					            					<td class="text-center"><?php echo $column_action; ?></td>
					            				</tr>
					            			</thead>
					            			<tbody>
					            			<?php if (!empty($membership_default_card['discount']) && is_array($membership_default_card['discount'])) { $count = 0; ?>
								                <?php foreach ($membership_default_card['discount'] as $discount) { ?>
								                <tr>
								                	<td>
								                		<select name="membership_default_card[discount][<?php echo $count; ?>][base]" class="form-control">
								                		<?php foreach ($discount_base as $v => $n) { ?>
								                			<?php if ($v == $discount['base']) { ?>
								                			<option value="<?php echo $v; ?>" selected="selected"><?php echo $n; ?></option>
								                			<?php } else { ?>
								                			<option value="<?php echo $v; ?>"><?php echo $n; ?></option>
								                			<?php } ?>
								                		<?php } ?>
								                		</select>
								                	</td>
								                	<td>
								                		<input type="text" name="membership_default_card[discount][<?php echo $count; ?>][limit]" value="<?php echo $discount['limit']; ?>" placeholder="<?php echo $column_discount_limit; ?>" class="form-control" />
								                	</td>
								                	<td>
								                		<input type="text" name="membership_default_card[discount][<?php echo $count; ?>][value]" value="<?php echo $discount['value']; ?>" placeholder="<?php echo $column_discount_value; ?>" class="form-control" />
								                	</td>
								                	<td>
								                		<select name="membership_default_card[discount][<?php echo $count; ?>][type]" class="form-control">
								                		<?php foreach ($discount_type as $v => $n) { ?>
								                			<?php if ($v == $discount['type']) { ?>
								                			<option value="<?php echo $v; ?>" selected="selected"><?php echo $n; ?></option>
								                			<?php } else { ?>
								                			<option value="<?php echo $v; ?>"><?php echo $n; ?></option>
								                			<?php } ?>
								                		<?php } ?>
								                		</select>
								                	</td>
								                	<td class="text-center">
								                		<button type="button" onclick="$(this).parents('tr').remove()" class="btn btn-danger"><i class="fa fa-minus-circle"></i></button>
								                	</td>
								                </tr>
								                <?php $count++; } ?>
								            <?php } ?>
								            </tbody>
								            <tfoot>
								                <tr>
								                	<td colspan="4"></td>
								                	<td class="text-center">
								                		<button type="button" onclick="addDiscount('table-discounts', 'membership_default_card[discount]')" class="btn btn-primary"><i class="fa fa-plus-circle"></i></button>
								                	</td>
								                </tr>
								            </tfoot>
								        </table>
								    </div>
								</div>
							</div>
						    <div class="form-group">
						    	<label class="col-sm-2 control-label" for="default-card-category"><span data-toggle="tooltip" title="<?php echo $help_card_categories; ?>"><?php echo $entry_categories; ?></span></label>
						    	<div class="col-sm-10">
						    		<input type="text" name="category" value="" placeholder="<?php echo $entry_categories; ?>" id="default-card-category" class="form-control" />
						        	<div id="default-card-categories" class="well well-sm" style="height: 150px; overflow: auto;">
						        	<?php if (!empty($membership_default_card['categories']) && is_array($membership_default_card['categories'])) { ?>
						        		<?php foreach ($categories as $category) { ?>
						        		<div id="default-card-categories-<?php echo $category['category_id']; ?>"><i class="fa fa-minus-circle"></i> <?php echo $category['name']; ?>
						        			<input type="hidden" name="membership_default_card[categories]" value="<?php echo $category['category_id']; ?>" />
						        		</div>
						        		<?php } ?>
						        	<?php } ?>
						        	</div>
						        </div>
						    </div>
						    <div class="form-group">
						    	<label class="col-sm-2 control-label" for="default-card-product"><span data-toggle="tooltip" title="<?php echo $help_card_products; ?>"><?php echo $entry_products; ?></span></label>
						    	<div class="col-sm-10">
						    		<input type="text" name="product" value="" placeholder="<?php echo $entry_products; ?>" id="default-card-product" class="form-control" />
						    		<div id="default-card-products" class="well well-sm" style="height: 150px; overflow: auto;">
						    		<?php if (!empty($membership_default_card['products']) && is_array($membership_default_card['products'])) { ?>
						    			<?php foreach ($products as $product) { ?>
						        		<div id="default-card-products-<?php echo $product['product_id']; ?>"><i class="fa fa-minus-circle"></i> <?php echo $product['name']; ?>
						        			<input type="hidden" name="membership_default_card[products][]" value="<?php echo $product['product_id']; ?>" />
						        		</div>
						        		<?php } ?>
						        	<?php } ?>
						        	</div>
						        </div>
						    </div>
						    <div class="form-group">
						    	<label class="col-sm-2 control-label" for="default-card-logged-enabled"><span data-toggle="tooltip" title="<?php echo $help_logged; ?>"><?php echo $entry_logged; ?></span></label>
	      						<div class="col-sm-2">
	      							<div class="radio-switch">
	      							<?php if (empty($membership_default_card['logged'])) { ?>
						                <input type="radio" name="membership_default_card[logged]" value="0" id="default-card-logged-disabled" checked>
                                        <label class="col-sm-4" for="default-card-logged-enabled"><?php echo $text_no; ?></label>
	      								<input type="radio" name="membership_default_card[logged]" value="1" id="default-card-logged-enabled">
                                        <label class="col-sm-4" for="default-card-logged-disabled"><?php echo $text_yes; ?></label>
						                <?php } else { ?>
                                        <input type="radio" name="membership_default_card[logged]" value="0" id="default-card-logged-disabled">
                                        <label class="col-sm-4" for="default-card-logged-enabled"><?php echo $text_no; ?></label>
                                        <input type="radio" name="membership_default_card[logged]" value="1" id="default-card-logged-enabled" checked>
                                        <label class="col-sm-4" for="default-card-logged-disabled"><?php echo $text_yes; ?></label>  
						            <?php } ?>
	      							</div>
	      						</div> 
						    </div>
						    <div class="form-group">
						        <label class="col-sm-2 control-label" for="default-card-free_shipping-enabled"><span data-toggle="tooltip" title="<?php echo $help_free_shipping; ?>"><?php echo $entry_free_shipping; ?></span></label>
						        <div class="col-sm-2">
	      							<div class="radio-switch">
	      							<?php if (empty($membership_default_card['free_shipping'])) { ?>
						                <input type="radio" name="membership_default_card[free_shipping]" value="0" id="default-card-free_shipping-disabled" checked>
                                        <label class="col-sm-4" for="default-card-free_shipping-enabled"><?php echo $text_no; ?></label>
	      								<input type="radio" name="membership_default_card[free_shipping]" value="1" id="default-card-free_shipping-enabled">
                                        <label class="col-sm-4" for="default-card-free_shipping-disabled"><?php echo $text_yes; ?></label>
						                <?php } else { ?>
                                        <input type="radio" name="membership_default_card[free_shipping]" value="0" id="default-card-free_shipping-disabled">
                                        <label class="col-sm-4" for="default-card-free_shipping-enabled"><?php echo $text_no; ?></label>
                                        <input type="radio" name="membership_default_card[free_shipping]" value="1" id="default-card-free_shipping-enabled" checked>
                                        <label class="col-sm-4" for="default-card-free_shipping-disabled"><?php echo $text_yes; ?></label>  
						            <?php } ?>
	      							</div>
	      						</div>
						    </div>
						    <div class="form-group">
						    	<label class="col-sm-2 control-label" for="default-card-minimum_order_amount"><span data-toggle="tooltip" title="<?php echo $help_minimum_order_amount; ?>"><?php echo $entry_minimum_order_amount; ?></span></label>
						    	<div class="col-sm-10">
						    		<div class="input-group">
						    			<input type="text" name="membership_default_card[minimum_order_amount]" value="<?php isset($membership_default_card['minimum_order_amount']) && print($membership_default_card['minimum_order_amount']); ?>" placeholder="<?php echo $entry_minimum_order_amount; ?>" id="default-card-minimum_order_amount" class="form-control" />
						    			<span class="input-group-addon"><?php echo $currency_symbol; ?></span>
									</div>
						    	</div>
						    </div>
						    <div class="form-group">
						    	<label class="col-sm-2 control-label" for="default-card-maximum_order_amount"><span data-toggle="tooltip" title="<?php echo $help_maximum_order_amount; ?>"><?php echo $entry_maximum_order_amount; ?></span></label>
						    	<div class="col-sm-10">
						    		<div class="input-group">
						    			<input type="text" name="membership_default_card[maximum_order_amount]" value="<?php isset($membership_default_card['maximum_order_amount']) && print($membership_default_card['maximum_order_amount']); ?>" placeholder="<?php echo $entry_maximum_order_amount; ?>" id="default-card-maximum_order_amount" class="form-control" />
						    			<span class="input-group-addon"><?php echo $currency_symbol; ?></span>
									</div>
						    	</div>
						    </div>
						    <div class="form-group">
						    	<label class="col-sm-2 control-label" for="default-card-uses_total"><span data-toggle="tooltip" title="<?php echo $help_uses_total; ?>"><?php echo $entry_uses_total; ?></span></label>
						    	<div class="col-sm-10">
						    		<input type="text" name="membership_default_card[uses_total]" value="<?php isset($membership_default_card['uses_total']) && print($membership_default_card['uses_total']); ?>" placeholder="<?php echo $entry_uses_total; ?>" id="default-card-uses_total" class="form-control" />
						    	</div>
						    </div>
						    <div class="form-group">
						    	<label class="col-sm-2 control-label" for="default-card-uses_customer"><span data-toggle="tooltip" title="<?php echo $help_uses_customer; ?>"><?php echo $entry_uses_customer; ?></span></label>
						        <div class="col-sm-10">
						        	<input type="text" name="membership_default_card[uses_customer]" value="<?php isset($membership_default_card['uses_customer']) && print($membership_default_card['uses_customer']); ?>" placeholder="<?php echo $entry_uses_customer; ?>" id="default-card-uses_customer" class="form-control" />
						        </div>
						    </div>
						</div>
						<div class="tab-pane fade" id="gifts-list"></div>
						<div class="tab-pane fade" id="gift-form"></div>
						<div class="tab-pane fade" id="gifts-history"></div>
						<div class="tab-pane fade" id="gift-settings">
                            <div class="form-group">
	      						<label class="col-sm-2 control-label" for="gift_status-enabled"><span data-toggle="tooltip" title="<?php echo $help_status_total; ?>"><?php echo $entry_status_total; ?></span></label>
	      						<div class="col-sm-4">
	      							<div class="radio-switch">
	      							<?php if ($membership_gift_status) { ?>
						                <input type="radio" name="membership_gift_status" value="0" id="gift_status-disabled">
                                        <label class="col-sm-4" for="gift_status-enabled"><?php echo $text_disabled; ?></label>
	      								<input type="radio" name="membership_gift_status" value="1" id="gift_status-enabled" checked>
                                        <label class="col-sm-4" for="gift_status-disabled"><?php echo $text_enabled; ?></label>
						                <?php } else { ?>
                                        <input type="radio" name="membership_gift_status" value="0" id="gift_status-disabled" checked>
                                        <label class="col-sm-4" for="gift_status-enabled"><?php echo $text_disabled; ?></label>
                                        <input type="radio" name="membership_gift_status" value="1" id="gift_status-enabled">
                                        <label class="col-sm-4" for="gift_status-disabled"><?php echo $text_enabled; ?></label>  
						            <?php } ?>
	      							</div>
	      						</div>
	      					</div>
	      					<hr />
							<div class="col-sm-10 col-sm-offset-2">
								<ul class="nav nav-tabs" role="tablist">
								<?php foreach ($languages as $language) { ?>
									<li<?php echo ($language_id == $language['language_id']) ? ' class="active"' : '' ?>><a href="#gift_<?php echo $language['language_id']; ?>" aria-controls="gift_<?php echo $language['language_id']; ?>" role="tab" data-toggle="tab"><img src="<?php echo $language_flag[$language['language_id']] ?>" title="<?php echo $language['name']; ?>" /> <?php echo $language['name']; ?></a></li>
								<?php } ?>	
								</ul>		
							</div>
							<div class="tab-content">
							<?php foreach ($languages as $language) { ?>
								<div role="tabpanel" class="tab-pane<?php echo ($language_id == $language['language_id']) ? ' active' : '' ?>" id="gift_<?php echo $language['language_id']; ?>">	
									<div class="form-group">
										<label class="col-sm-2 control-label" for="gift_name_<?php echo $language['language_id'] ?>"><span data-toggle="tooltip" title="<?php echo $help_name_total; ?>"><?php echo $entry_name_total; ?></span></label>
										<div class="col-sm-10">
											<input type="text" name="membership_gift[name][<?php echo $language['language_id']; ?>]" value="<?php isset($membership_gift['name'][$language['language_id']]) && print($membership_gift['name'][$language['language_id']]); ?>" placeholder="<?php echo $entry_name_total; ?>" id="gift_name_<?php echo $language['language_id'] ?>" class="form-control" />
										</div>
									</div>
									<div class="form-group">	
			      						<label class="col-sm-2 control-label" for="nearest_gift_description_<?php echo $language['language_id'] ?>"><span data-toggle="tooltip" title="<?php echo $help_nearest_gift_description; ?>"><?php echo $entry_nearest_gift_description; ?></span></label>
										<div class="col-sm-10">
											<textarea name="membership_gift[nearest_gift_description][<?php echo $language['language_id']; ?>]" placeholder="<?php echo $entry_nearest_gift_description; ?>" class="form-control summernote"><?php isset($membership_gift['nearest_gift_description'][$language['language_id']]) && print($membership_gift['nearest_gift_description'][$language['language_id']]); ?></textarea>
										</div> 
								    </div>
								</div>
							<?php } ?>
							</div>
							<hr />
						    <div class="form-group">
						    	<label class="col-sm-2 control-label" for="nearest_gift-enabled"><span data-toggle="tooltip" title="<?php echo $help_nearest_gift; ?>"><?php echo $entry_nearest_gift; ?></span></label>
	      						<div class="col-sm-2">
	      							<div class="radio-switch">
	      							<?php if (empty($membership_gift['nearest_gift'])) { ?>
						                <input type="radio" name="membership_gift[nearest_gift]" value="0" id="nearest_gift-disabled" checked>
                                        <label class="col-sm-4" for="nearest_gift-enabled"><?php echo $text_no; ?></label>
	      								<input type="radio" name="membership_gift[nearest_gift]" value="1" id="nearest_gift-enabled">
                                        <label class="col-sm-4" for="nearest_gift-disabled"><?php echo $text_yes; ?></label>
						                <?php } else { ?>
                                        <input type="radio" name="membership_gift[nearest_gift]" value="0" id="nearest_gift-disabled">
                                        <label class="col-sm-4" for="nearest_gift-enabled"><?php echo $text_no; ?></label>
                                        <input type="radio" name="membership_gift[nearest_gift]" value="1" id="nearest_gift-enabled" checked>
                                        <label class="col-sm-4" for="nearest_gift-disabled"><?php echo $text_yes; ?></label>  
						            <?php } ?>
	      							</div>
	      						</div>
	      					</div> 
                            <div class="form-group">
                                <label class="col-sm-2 control-label" for="gift_name_action"><span data-toggle="tooltip" title="<?php echo $help_gift_name_action; ?>"><?php echo $entry_gift_name_action; ?></span></label>
	      						<div class="col-sm-10">
	      							<select name="membership_gift[name_action]" id="gift_name_action" class="form-control">
	      								<?php foreach ($gift_name_actions as $k => $v) { ?>
	      									<?php if (isset($membership_gift['name_action']) && $k == $membership_gift['name_action']) { ?>
	      								<option value="<?php echo $k; ?>" selected="selected"><?php echo $v; ?></option>
	      									<?php } else { ?>
	      								<option value="<?php echo $k; ?>"><?php echo $v; ?></option>
	      									<?php } ?>
	      								<?php } ?>
	      							</select>
	      						</div>
						    </div>
						    <div class="form-group required">
                                <label class="col-sm-2 control-label" for="gift_place_of_display"><span data-toggle="tooltip" title="<?php echo $help_gift_place_of_display; ?>"><?php echo $entry_gift_place_of_display; ?></span></label>
	      						<div class="col-sm-10">
	      							<div class="well well-sm" style="height: 80px; overflow: auto;">
									<?php foreach ($places_of_display as $k => $v) { ?>
										<div class="checkbox">
											<label>
										<?php if (!empty($membership_gift['place_of_display']) && is_array($membership_gift['place_of_display']) && in_array($k, $membership_gift['place_of_display'])) { ?>
												<input type="checkbox" name="membership_gift[place_of_display][]" value="<?php echo $k; ?>" id="place_of_display-<?php echo $k; ?>" checked="checked" /> <?php echo $v; ?>
										<?php } else { ?>
												<input type="checkbox" name="membership_gift[place_of_display][]" value="<?php echo $k; ?>" id="place_of_display-<?php echo $k; ?>" /> <?php echo $v; ?>
										<?php } ?>
											</label>
										</div>
									<?php } ?>
									</div>
									<?php if ($error_place_of_display) { ?>
									<div class="text-danger"><?php echo $error_place_of_display; ?></div>
									<?php } ?>
	      						</div>
                            </div>
                            <div class="form-group required">
                                <label class="col-sm-2 control-label" for="gift_giving_mode"><span data-toggle="tooltip" title="<?php echo $help_gift_giving_mode; ?>"><?php echo $entry_gift_giving_mode; ?></span></label>
	      						<div class="col-sm-10">
	      							<div class="well well-sm" style="height: 135px; overflow: auto;">
									<?php foreach ($gift_giving_modes as $k => $v) { ?>
										<div class="checkbox">
											<label>
										<?php if (!empty($membership_gift['giving_mode']) && is_array($membership_gift['giving_mode']) && in_array($k, $membership_gift['giving_mode'])) { ?>
												<input type="checkbox" name="membership_gift[giving_mode][]" value="<?php echo $k; ?>" id="giving_mode-<?php echo $k; ?>" checked="checked" /> <?php echo $v; ?>
										<?php } else { ?>
												<input type="checkbox" name="membership_gift[giving_mode][]" value="<?php echo $k; ?>" id="giving_mode-<?php echo $k; ?>" /> <?php echo $v; ?>
										<?php } ?>
											</label>
										</div>
									<?php } ?>
									</div>
									<?php if ($error_giving_mode) { ?>
									<div class="text-danger"><?php echo $error_giving_mode; ?></div>
									<?php } ?>
	      						</div>
                            </div>
						</div>
						<?php } ?>
						<div class="tab-pane fade<?php if (!$license) { ?> in active<?php } ?>" id="support">
          					<?php echo $support; ?>			
          				</div>
          			</div>
          		</form>
				<form action="<?php echo $action_settings; ?>&type=import" method="post" enctype="multipart/form-data" style="display: none;">
					<input type="file" name="import" accept="text/plain" id="input-import-settings" onchange="this.form.submit();">
				</form>
      		</div>
    	</div>
  	</div>
</div>
<link href="https://cdnjs.cloudflare.com/ajax/libs/summernote/0.8.2/summernote.css" rel="stylesheet">
<script src="https://cdnjs.cloudflare.com/ajax/libs/summernote/0.8.2/summernote.js"></script>
<script type="text/javascript"><!--
function save() {
	$.ajax( {
        url: 'index.php?route=module/membership&token=<?php echo $token; ?>',
        type: 'POST',
        data: $('#common-settings, #email-notification-settings, #card-settings, #default-card-settings, #gift-settings, #support').find('input, select, textarea').serialize(),
		dataType: 'HTML',
		success: function (data, textStatus, jqXHR) {
            var $data = $(data);

            if ($data.find('div.alert-danger').length) {
                $('.container-fluid:eq(1)').prepend($data.find('div.alert-danger'));
            }

            if ($data.find('div.text-danger').length) {
                $data.find('div.text-danger').each(function(i, el) {
                    var id = '#' + $(el).parents('div[class^="col-sm"]:first').find('input, select, textarea, radio').attr('id');

                    $(id).parents('div[class^="col-sm"]:first').append(el);
                    $(id).parents('div.form-group').addClass('has-error');
                } );
            }

            if ($data.find('div.alert-success').length) {
                $('.container-fluid:eq(1)').prepend($data.find('div.alert-success'));
            }
		}
	} );
}

function settings(type) {
    if (!confirm('<?php echo $text_confirm; ?>')) {
        return false;
    }

	if (type == 'basic') {
		$.ajax( {
			url: 'index.php?route=module/membership/extensionSettings&token=<?php echo $token; ?>&type=' + type,
			type: 'GET',
			dataType: 'json',
			success: function (json) {
				if (json['success']) {
					$('.container-fluid:eq(1)').prepend('<div class="alert alert-success"><i class="fa fa-check-circle"></i> ' + json['success'] + '<button type="button" class="close" data-dismiss="alert">&times;</button></div>');

					setTimeout(function() {
							location.reload();
						},
						2000
					);
				}

				if(json['error']) {
					$('.container-fluid:eq(1)').prepend('<div class="alert alert-danger"><i class="fa fa-exclamation-circle"></i> ' + json['error'] + '<button type="button" class="close" data-dismiss="alert">&times;</button></div>');
				}

				$('html, body').animate({ scrollTop: 0 }, 'slow');
			}
		} );
	} else if (type == 'export') {
		location.href = 'index.php?route=module/membership/extensionSettings&token=<?php echo $token; ?>&type=' + type;
	} else if (type == 'import') {
		$('#input-import-settings').trigger('click');
	}
}

function formHandler(element) {
	switch (element.id) {
        case 'code_characters-digits':
        case 'code_characters-digits_luhn':
        case 'code_characters-upper_case':
        case 'code_characters-lower_case':
            var
                cch_digits = $('#code_characters-digits:checked').length,
                cch_digits_luhn = $('#code_characters-digits_luhn:checked').length,
                cch_upper_case = $('#code_characters-upper_case:checked').length,
                cch_lower_case = $('#code_characters-lower_case:checked').length;

            if (cch_digits || cch_upper_case || cch_lower_case) {
                $('#code_characters-digits_luhn').attr('disabled', true);
                $('#code_characters-digits, #code_characters-upper_case, #code_characters-lower_case').removeAttr('disabled');
            } else if (cch_digits_luhn) {
                $('#code_characters-digits_luhn').removeAttr('disabled');
                $('#code_characters-digits, #code_characters-upper_case, #code_characters-lower_case').attr('disabled', true);
            } else {
                $('#code_characters-digits, #code_characters-upper_case, #code_characters-lower_case, #code_characters-digits_luhn').removeAttr('disabled');
            }

            break;

        case 'accounted_orders_balance-membership_card_orders_total':
        case 'accounted_orders_balance-membership_card_orders_discount':
        case 'accounted_orders_balance-completed_orders':
            var
				aob_membership_card_orders_total = $('#accounted_orders_balance-membership_card_orders_total:checked').length,
                aob_membership_card_orders_discount = $('#accounted_orders_balance-membership_card_orders_discount:checked').length,
                aob_completed_orders = $('#accounted_orders_balance-completed_orders:checked').length;

            if (aob_membership_card_orders_total || aob_membership_card_orders_discount) {
                $('#accounted_orders_balance-completed_orders').attr('disabled', true);
                $('#accounted_orders_balance-membership_card_orders_total, #accounted_orders_balance-membership_card_orders_discount').removeAttr('disabled');
            } else if (aob_completed_orders) {
                $('#accounted_orders_balance-completed_orders').removeAttr('disabled');
                $('#accounted_orders_balance-membership_card_orders_total, #accounted_orders_balance-membership_card_orders_discount').attr('disabled', true);
            } else {
                $('#accounted_orders_balance-membership_card_orders_total, #accounted_orders_balance-membership_card_orders_discount, #accounted_orders_balance-completed_orders').removeAttr('disabled');
            }

            break;

		case 'card_request_discount-enabled':
		case 'card_request_discount-disabled':
			if (+element.value) {
				$(element).parent().parent().nextAll().fadeIn();
			} else {
				$(element).parent().parent().nextAll().fadeOut();
			}

			break;
	}
}

function addDiscount(id, name) {
	var number = $('#' + id + ' tbody tr').length;
	var row = '<tr>';
		
	row += '<td><select name="' + name + '[' + number + '][base]" class="form-control"><?php foreach ($discount_base as $v => $n) { ?><option value="<?php echo $v; ?>"><?php echo $n; ?></option><?php } ?></select></td>';
	row += '<td><input type="text" name="' + name + '[' + number + '][limit]" value="" placeholder="<?php echo $column_discount_limit; ?>" class="form-control" /></td>';
	row += '<td><input type="text" name="' + name + '[' + number + '][value]" value="" placeholder="<?php echo $column_discount_value; ?>" class="form-control" /></td>';
	row += '<td><select name="' + name + '[' + number + '][type]" class="form-control"><?php foreach ($discount_type as $v => $n) { ?><option value="<?php echo $v; ?>"><?php echo $n; ?></option><?php } ?></select></td>';
	row += '<td class="text-center"><button type="button" onclick="$(this).parents(\'tr\').remove()" class="btn btn-danger"><i class="fa fa-minus-circle"></i></button></td>';
	row += '</tr>';
		
	$('#' + id + ' tbody').append(row);
}

function deleteData(self, data_type) {
	var post_data;

	if (self.id.match(/button\-delete/i)) {
		post_data = $('input[name^="selected"]:checked').serialize();
	} else {
		post_data = 'selected[]=' + $(self).parents('tr').find('input[name^="selected"]').attr('value');
	}

	$.ajax( {
		url: 'index.php?route=module/membership/deleteData&data_type=' + data_type + '&token=<?php echo $token; ?>',
		type: 'POST',
		data: post_data,
		dataType: 'json',
		success: function(json) {
			if (json['success'] && json['deleted'] instanceof Array) {
				for(var i in json['deleted']) {
					$('input[value="' + json['deleted'][i] + '"]').parents('tr').fadeOut('slow');
				}
				
				$('.container-fluid:eq(1)').prepend('<div class="alert alert-success"><i class="fa fa-check-circle"></i> ' + json['success'] + '<button type="button" class="close" data-dismiss="alert">&times;</button></div>');
			}
		}
	} );
}

function changeStatus(self, data_type, change_type) {
	if (change_type === undefined) {
    	change_type = '';
  	}

	$.ajax( {
		url: 'index.php?route=module/membership/changeStatus&data_type=' + data_type + '&token=<?php echo $token; ?>',
		type: 'POST',
		data: 'id=' + parseInt($(self).parents('tr').find('input[name^="selected"]').attr('value')) + '&change_type=' + change_type,
		dataType: 'json',
		success: function(json) {
			if (typeof json['new_status_id'] != 'undefined') {
				switch (json['new_status_id']) {
					case 0:
						$(self).parents('tr').find('span').html(json['new_status_text']).removeClass().addClass('label label-danger');
						
						break;
					case 1:
						$(self).parents('tr').find('span').html(json['new_status_text']).removeClass().addClass('label label-success');
						
						break;
					case 2:
						$(self).parents('tr').find('span').html(json['new_status_text']).removeClass().addClass('label label-warning');
						
						break;		
				}
			}
		}
	} );
}

$(function() {
	if (window.location.hash) {
		var url_parts = window.location.href.replace('#', '&').split('&');

		$('#helper-ajax-menu').attr('href', url_parts[0] + '/' + url_parts[2] + '&' + url_parts[3] + '&' + url_parts[1]);
		
		setTimeout(function() { $('#helper-ajax-menu').trigger('click'); }, 100);
	}
	
	$('.summernote').summernote();
	
	$.ajaxSetup( {
		beforeSend: function () {
			$('body').fadeTo('fast', 0.7).prepend('<div class="cssload-loader"><div class="cssload-flipper"><div class="cssload-front"></div><div class="cssload-back"></div></div></div>');
		},
		complete: function () {
			var $alerts = $('.alert-danger, .alert-success');
			
			if ($alerts.length !== 0) {
				setTimeout(function() { $alerts.fadeOut() }, 5000);
			}

			$('body').fadeTo('fast', 1)
			$('div.cssload-loader').remove();
		},
		error: function (jqXHR, textStatus, errorThrown) {
	   		console.log(textStatus);
	    }
	} );

	$('[data-loadurl]').on('click', function (e) {
		$(e.target.hash).load(e.currentTarget.dataset.loadurl);
	} );

    $('input[id^="code_characters"]:checked, input[id^="accounted_orders_balance"]:checked, input[id^="card_request_discount"]:checked').each(function() {
        formHandler(this);
    } );

    $('input[id^="code_characters"], input[id^="accounted_orders_balance"], input[id^="card_request_discount"]').on('change', function(e) {
        formHandler(e.currentTarget);
    } );
	
	$('div.tab-pane').on('click', 'a[href*="index.php"]', function (e) {
		e.preventDefault();
		
		var container_id = $(e.currentTarget).parents('div.tab-pane').attr('id');

		if (e.currentTarget.href.match(/(addCard|editCard)/i)) {
			var 
				$menu_card_form = $('#menu-card-form'),
				loadurl = $menu_card_form.attr('data-loadurl');
				
			$menu_card_form.attr('data-loadurl', e.currentTarget.href).trigger('click').attr('data-loadurl', loadurl);
		} else if (e.currentTarget.href.match(/(addGift|editGift)/i)) {
			var 
				$menu_gift_form = $('#menu-gift-form'),
				loadurl = $menu_gift_form.attr('data-loadurl');
				
			$menu_gift_form.attr('data-loadurl', e.currentTarget.href).trigger('click').attr('data-loadurl', loadurl);
		} else {
			$('#' + container_id).load(e.currentTarget.href);
		}
	} );

	$('div.tab-pane').on('change', 'input[name^="select"]', function() {
		var selected = $('input[name^="selected"]:checked');

		if (selected.length) {
			$('button[id^="button-delete"]').attr('disabled', false);
		} else {
			$('button[id^="button-delete"]').attr('disabled', true);
		}
	} );

    // Categories
    $('#default-card-category').autocomplete({
        'source': function(request, response) {
            $.ajax({
                url: 'index.php?route=catalog/category/autocomplete&token=<?php echo $token; ?>&filter_name=' +  encodeURIComponent(request),
                dataType: 'json',
                beforeSend: function () {},
                complete: function () {},
                success: function(json) {
                    response($.map(json, function(item) {
                        return {
                            label: item['name'],
                            value: item['category_id']
                        }
                    }));
                }
            });
        },
        'select': function(item) {
            this.value = '';

            $('#default-card-categories-' + item['value']).remove();

            $('#default-card-categories').append('<div id="default-card-categories-' + item['value'] + '"><i class="fa fa-minus-circle"></i> ' + item['label'] + '<input type="hidden" name="membership_default_card[categories][]" value="' + item['value'] + '" /></div>');
        }
    });

    $('#default-card-categories').delegate('.fa-minus-circle', 'click', function() {
        $(this).parent().remove();
    } );

	// Products
	$('#default-card-product').autocomplete({
		'source': function(request, response) {
			$.ajax({
				url: 'index.php?route=catalog/product/autocomplete&token=<?php echo $token; ?>&filter_name=' +  encodeURIComponent(request),
				dataType: 'json',	
				beforeSend: function () {},
				complete: function () {},		
				success: function(json) {
					response($.map(json, function(item) {
						return {
							label: item['name'],
							value: item['product_id']
						}
					}));
				}
			});
		},
		'select': function(item) {
			this.value = '';
			
			$('#default-card-products-' + item['value']).remove();
			
			$('#default-card-products').append('<div id="default-card-products-' + item['value'] + '"><i class="fa fa-minus-circle"></i> ' + item['label'] + '<input type="hidden" name="membership_default_card[products][]" value="' + item['value'] + '" /></div>');	
		}
	});

	$('#default-card-products').delegate('.fa-minus-circle', 'click', function() {
		$(this).parent().remove();
	});
} );
//--></script>
<?php echo $footer; ?>