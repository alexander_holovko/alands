<?php if ($error_warning) { ?>
<div class="alert alert-danger">
  	<i class="fa fa-exclamation-circle"></i> <?php echo $error_warning; ?>
  	<button type="button" class="close" data-dismiss="alert">&times;</button>
</div>
<?php } ?>
<div class="row">
	<div class="col-sm-11">
		<h3 style="padding-top: 8px;"><?php echo $text_form; ?></h3>
	</div>
	<div class="col-sm-1">
		<button type="button" onclick="saveGift();" class="btn btn-primary btn-block"><i class="fa fa-save"></i> <?php echo $button_save; ?></button>
	</div>
</div>
<hr>
<div class="tab-content">
	<div class="form-group">
		<label class="col-sm-2 control-label" for="gift-status-enabled"><?php echo $entry_status; ?></label>
		<div class="col-sm-4">
		    <div class="radio-switch">
		    <?php if ($status) { ?>
				<input type="radio" name="status" value="0" id="gift-status-disabled">
	            <label class="col-sm-4" for="gift-status-enabled"><?php echo $text_disabled; ?></label>
		      	<input type="radio" name="status" value="1" id="gift-status-enabled" checked>
	            <label class="col-sm-4" for="gift-status-disabled"><?php echo $text_enabled; ?></label>
			<?php } else { ?>
	            <input type="radio" name="status" value="0" id="gift-status-disabled" checked>
	            <label class="col-sm-4" for="gift-status-enabled"><?php echo $text_disabled; ?></label>
	            <input type="radio" name="status" value="1" id="gift-status-enabled">
	            <label class="col-sm-4" for="gift-status-disabled"><?php echo $text_enabled; ?></label>  
			<?php } ?>
		    </div>
		</div>
	</div>
	<hr />
	<div class="col-sm-10 col-sm-offset-2">
		<ul class="nav nav-tabs" role="tablist">
		<?php foreach ($languages as $language) { ?>
			<li<?php echo ($language_id == $language['language_id']) ? ' class="active"' : '' ?>><a href="#gift-<?php echo $language['language_id']; ?>" aria-controls="gift-<?php echo $language['language_id']; ?>" role="tab" data-toggle="tab"><img src="<?php echo $language_flag[$language['language_id']] ?>" title="<?php echo $language['name']; ?>" /> <?php echo $language['name']; ?></a></li>
		<?php } ?>	
		</ul>		
	</div>
	<div class="tab-content">
	<?php foreach ($languages as $language) { ?>
		<div role="tabpanel" class="tab-pane<?php echo ($language_id == $language['language_id']) ? ' active' : '' ?>" id="gift-<?php echo $language['language_id']; ?>">	
			<div class="form-group">
				<label class="col-sm-2 control-label" for="gift-name_<?php echo $language['language_id'] ?>"><span data-toggle="tooltip" title="<?php echo $help_gift_name; ?>"><?php echo $entry_name; ?></span></label>
				<div class="col-sm-10">
					<input type="text" name="name[<?php echo $language['language_id']; ?>]" value="<?php echo isset($name[$language['language_id']]) ? $name[$language['language_id']] : ''; ?>" placeholder="<?php echo $entry_name; ?>" id="gift-name_<?php echo $language['language_id'] ?>" class="form-control" />
				</div>
			</div>
		</div>
	<?php } ?>
	</div>
	<hr />
	<div class="form-group">
	    <label class="col-sm-2 control-label" for="gift-threshold"><span data-toggle="tooltip" title="<?php echo $help_gift_threshold; ?>"><?php echo $entry_threshold; ?></span></label>
	    <div class="col-sm-10">
	    	<div class="input-group">
	    		<input type="text" name="threshold" value="<?php echo $threshold; ?>" placeholder="<?php echo $entry_threshold; ?>" id="gift-threshold" class="form-control" />
	    		<span class="input-group-addon"><?php echo $currency_symbol; ?></span>
	    	</div>
	    </div>
	</div>
	<!--
	<div class="form-group">
		<label class="col-sm-2 control-label" for="gift-maximum_cost_gift"><span data-toggle="tooltip" title="<?php echo $help_maximum_cost_gift; ?>"><?php echo $entry_maximum_cost_gift; ?></span></label>
		<div class="col-sm-10">
			<div class="input-group">
				<input type="text" name="maximum_cost" value="<?php echo $maximum_cost; ?>" placeholder="<?php echo $entry_maximum_cost_gift; ?>" id="gift-maximum_cost_gift" class="form-control" />
				<span class="input-group-addon"><?php echo $currency_symbol; ?></span>
			</div>	
		</div>
    </div>
    -->
    <div class="form-group">
		<label class="col-sm-2 control-label" for="gift-giving_quantity"><span data-toggle="tooltip" title="<?php echo $help_giving_quantity; ?>"><?php echo $entry_giving_quantity; ?></span></label>
		<div class="col-sm-10">
			<input type="text" name="giving_quantity" value="<?php echo $giving_quantity; ?>" placeholder="<?php echo $entry_giving_quantity; ?>" id="gift-giving_quantity" class="form-control" />
		</div>
    </div>
	<div class="form-group">
	    <label class="col-sm-2 control-label" for="gift-date_start"><?php echo $entry_start; ?></label>
	    <div class="col-sm-4">
	    	<div class="input-group date">
	    		<input type="text" name="date_start" value="<?php echo $date_start; ?>" placeholder="<?php echo $entry_start; ?>" data-date-format="YYYY-MM-DD" id="gift-date_start" class="form-control" />
	    		<span class="input-group-btn">
	    			<button type="button" class="btn btn-default"><i class="fa fa-calendar"></i></button>
	    		</span>
	    	</div>
	    </div>
	    <label class="col-sm-2 control-label" for="gift-date_expiry"><?php echo $entry_expiry; ?></label>
	    <div class="col-sm-4">
	    	<div class="input-group date">
	    		<input type="text" name="date_expiry" value="<?php echo $date_expiry; ?>" placeholder="<?php echo $entry_expiry; ?>" data-date-format="YYYY-MM-DD" id="gift-date_expiry" class="form-control" />
	    		<span class="input-group-btn">
	    			<button type="button" class="btn btn-default"><i class="fa fa-calendar"></i></button>
	    		</span>
	    	</div>
	    </div>
	</div>
	<div class="form-group">
		<label class="col-sm-2 control-label" for="gift-category"><span data-toggle="tooltip" title="<?php echo $help_gift_categories; ?>"><?php echo $entry_categories; ?></span></label>
		<div class="col-sm-10">
			<input type="text" name="category" value="" placeholder="<?php echo $entry_categories; ?>" id="gift-category" class="form-control" />
			<div id="gift-categories" class="well well-sm" style="height: 150px; overflow: auto;">
			<?php if (is_array($categories)) { ?>
				<?php foreach ($categories as $category) { ?>
				<div id="gift-categories-<?php echo $category['category_id']; ?>"><i class="fa fa-minus-circle"></i> <?php echo $category['name']; ?>
					<input type="hidden" name="categories[]" value="<?php echo $category['category_id']; ?>" />
				</div>
				<?php } ?>
			<?php } ?>
			</div>
		</div>
	</div>
	<div class="form-group">
		<label class="col-sm-2 control-label" for="gift-product"><span data-toggle="tooltip" title="<?php echo $help_gift_products; ?>"><?php echo $entry_products; ?></span></label>
		<div class="col-sm-10">
			<input type="text" name="product" value="" placeholder="<?php echo $entry_products; ?>" id="gift-product" class="form-control" />
			<div id="gift-products" class="well well-sm" style="height: 150px; overflow: auto;">
			<?php if (is_array($products)) { ?>
				<?php foreach ($products as $product) { ?>
				<div id="gift-products-<?php echo $product['product_id']; ?>"><i class="fa fa-minus-circle"></i> <?php echo $product['name']; ?>
					<input type="hidden" name="products[]" value="<?php echo $product['product_id']; ?>" />
				</div>
				<?php } ?>
			<?php } ?>
			</div>
		</div>
	</div>
</div>
<script type="text/javascript"><!--
function saveGift() {
	var 
		$container = $('#gift-form'),
		post_data = $container.find('input, select, textarea').serialize();
	
	$.ajax( {
		url: '<?php echo $action; ?>'.replace(/&amp;/g, '&'),
		type: 'POST',
		data: post_data,
		dataType: 'html',
		success: function (data, textStatus, jqXHR) {
			if (data.match(/button\-gifts\-list\-filter/i)) {
				$('#menu-gifts-list').tab('show');
				$('#gifts-list').html(data);
			} else {
				$container.html(data);
			}
		}
	} );
}

$(function() {
	$('.date').datetimepicker( {pickTime: false} );
	
	// Categories
	$('#gift-category').autocomplete( {
		'source': function(request, response) {
			$.ajax( {
				url: 'index.php?route=catalog/category/autocomplete&token=<?php echo $token; ?>&filter_name=' +  encodeURIComponent(request),
				dataType: 'json',
				beforeSend: function () {},
				complete: function () {},
				success: function(json) {
					response($.map(json, function(item) {
						return {
							label: item['name'],
							value: item['category_id']
						}
					} ) );
				}
			} );
		},
		'select': function(item) {
			this.value = '';
			
			$('#gift-categories-' + item['value']).remove();
			
			$('#gift-categories').append('<div id="gift-categories-' + item['value'] + '"><i class="fa fa-minus-circle"></i> ' + item['label'] + '<input type="hidden" name="categories[]" value="' + item['value'] + '" /></div>');
		}	
	} );

	$('#gift-categories').delegate('.fa-minus-circle', 'click', function() {
		$(this).parent().remove();
	} );
	
	// Products
	$('#gift-product').autocomplete( {
		'source': function(request, response) {
			$.ajax( {
				url: 'index.php?route=catalog/product/autocomplete&token=<?php echo $token; ?>&filter_name=' +  encodeURIComponent(request),
				dataType: 'json',
				beforeSend: function () {},
				complete: function () {},			
				success: function(json) {
					response($.map(json, function(item) {
						return {
							label: item['name'],
							value: item['product_id']
						}
					} ) );
				}
			} );
		},
		'select': function(item) {
			this.value = '';
			
			$('#gift-products-' + item['value']).remove();
			
			$('#gift-products').append('<div id="gift-products-' + item['value'] + '"><i class="fa fa-minus-circle"></i> ' + item['label'] + '<input type="hidden" name="products[]" value="' + item['value'] + '" /></div>');	
		}
	} );

	$('#gift-products').delegate('.fa-minus-circle', 'click', function() {
		$(this).parent().remove();
	} );
} );

//--></script>