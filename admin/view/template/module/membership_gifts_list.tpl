<?php if ($error_warning) { ?>
<div class="alert alert-danger">
  	<i class="fa fa-exclamation-circle"></i> <?php echo $error_warning; ?><button type="button" class="close" data-dismiss="alert">&times;</button>
 </div>
<?php } ?>
<?php if ($success) { ?>
<div class="alert alert-success"><i class="fa fa-check-circle"></i> <?php echo $success; ?>
	<button type="button" class="close" data-dismiss="alert">&times;</button>
</div>
<?php } ?>
<div class="well">
	<div class="row">
		<div class="col-sm-4">
			<label class="control-label" for="gifts-list-filter_name"><?php echo $entry_name; ?></label>
			<input type="text" name="filter_name" value="<?php echo $filter_name; ?>" placeholder="<?php echo $entry_name; ?>" id="gifts-list-filter_name" class="form-control" />
		</div>
	</div>
	<div class="row" style="padding-top: 15px;">
		<div class="col-sm-4 col-sm-offset-8">
			<button type="button" id="button-gifts-list-filter" class="btn btn-primary pull-right"><i class="fa fa-search"></i> <?php echo $button_filter; ?></button>
		</div>
	</div>
</div>
<div class="panel panel-default">
	<div class="panel-heading clearfix">
		<h3 class="panel-title pull-left" style="padding-top: 9px;"><i class="fa fa-list"></i> <?php echo $text_gifts_list; ?></h3>
		<div class="pull-right">
			<a href="<?php echo $add; ?>" id="button-gift-add" data-toggle="tooltip" title="<?php echo $button_add; ?>" class="btn btn-primary"><i class="fa fa-plus"></i></a>
			<button type="button" title="<?php echo $button_delete; ?>" class="btn btn-danger" data-toggle="tooltip" id="button-delete-gifts-list" onclick="confirm('<?php echo $text_confirm; ?>') ? deleteData(this, 'gift') : false;" disabled="disabled"><i class="fa fa-trash-o"></i></button>
		</div>
	</div>
	<div class="panel-body">
		<div class="table-responsive">
			<table class="table table-bordered table-hover">
				<thead>
					<tr>
						<td style="width: 1px;" class="text-center"><input type="checkbox" onclick="$('input[name*=\'selected\']').prop('checked', this.checked).trigger('change');" /></td>
						<td class="text-center">
						<?php if ($sort == 'name') { ?>
							<a href="<?php echo $sort_name; ?>" class="<?php echo strtolower($order); ?>"><?php echo $column_name; ?></a>
						<?php } else { ?>
							<a href="<?php echo $sort_name; ?>"><?php echo $column_name; ?></a>
						<?php } ?>
						</td>
						<td class="text-center">
						<?php if ($sort == 'threshold') { ?>
							<a href="<?php echo $sort_threshold; ?>" class="<?php echo strtolower($order); ?>"><?php echo $column_threshold; ?></a>
						<?php } else { ?>
							<a href="<?php echo $sort_threshold; ?>"><?php echo $column_threshold; ?></a>
						<?php } ?>
						</td>
						<!--
						<td class="text-center">
						<?php if ($sort == 'maximum_cost') { ?>
							<a href="<?php echo $sort_maximum_cost; ?>" class="<?php echo strtolower($order); ?>"><?php echo $column_maximum_cost_gift; ?></a>
						<?php } else { ?>
							<a href="<?php echo $sort_maximum_cost; ?>"><?php echo $column_maximum_cost_gift; ?></a>
						<?php } ?>
						</td>
						-->
						<td class="text-center">
						<?php if ($sort == 'total') { ?>
							<a href="<?php echo $sort_total; ?>" class="<?php echo strtolower($order); ?>"><?php echo $column_giving_quantity; ?></a>
						<?php } else { ?>
							<a href="<?php echo $sort_total; ?>"><?php echo $column_giving_quantity; ?></a>
						<?php } ?>
						</td>
						<td class="text-center">
						<?php if ($sort == 'date_start') { ?>
							<a href="<?php echo $sort_date_start; ?>" class="<?php echo strtolower($order); ?>"><?php echo $column_date_start; ?></a>
						<?php } else { ?>
							<a href="<?php echo $sort_date_start; ?>"><?php echo $column_date_start; ?></a>
						<?php } ?>
						</td>
						<td class="text-center">
						<?php if ($sort == 'date_expiry') { ?>
							<a href="<?php echo $sort_date_expiry; ?>" class="<?php echo strtolower($order); ?>"><?php echo $column_date_expiry; ?></a>
						<?php } else { ?>
							<a href="<?php echo $sort_date_expiry; ?>"><?php echo $column_date_expiry; ?></a>
						<?php } ?>
						</td>
						<td class="text-center">
						<?php if ($sort == 'status') { ?>
							<a href="<?php echo $sort_status; ?>" class="<?php echo strtolower($order); ?>"><?php echo $column_status; ?></a>
						<?php } else { ?>
							<a href="<?php echo $sort_status; ?>"><?php echo $column_status; ?></a>
						<?php } ?>
						</td>
						<td class="text-center"><?php echo $column_action; ?></td>
					</tr>
				</thead>
				<tbody>
				<?php if ($gifts) { ?>
					<?php foreach ($gifts as $gift) { ?>
					<tr>
						<td class="text-center">
							<input type="checkbox" name="selected[]" value="<?php echo $gift['gift_id']; ?>" />
						</td>
						<td><?php echo $gift['name']; ?></td>
						<td class="text-center"><?php echo $gift['threshold']; ?></td>
						<!--<td class="text-center"><?php echo $gift['maximum_cost']; ?></td>-->
						<td class="text-center"><?php echo $gift['giving_quantity'] ? $gift['giving_quantity'] : '∞'; ?> / <?php echo $gift['total']; ?></td>
						<td class="text-center"><?php echo $gift['date_start']; ?></td>
						<td class="text-center"><?php echo $gift['date_expiry']; ?></td>
						<td class="text-center">						
						<?php if ($gift['status'] == 1) { ?>	
							<span class="label label-success" style="cursor: pointer;" onclick="changeStatus(this, 'gift');">
						<?php } else { ?>
							<span class="label label-danger" style="cursor: pointer;" onclick="changeStatus(this, 'gift');">
						<?php } ?>		
							<?php echo $gift['status_text']; ?>
							</span>
						</td>
						<td class="text-center">
							<a href="<?php echo $gift['edit']; ?>" id="button-gift-edit-<?php echo $gift['gift_id']; ?>" title="<?php echo $button_edit; ?>" class="btn btn-primary" data-toggle="tooltip"><i class="fa fa-pencil"></i></a>
							<button type="button" onclick="confirm('<?php echo $text_confirm; ?>') ? deleteData(this, 'gift') : false;" data-toggle="tooltip" data-value="" title="<?php echo $button_delete; ?>" class="btn btn-danger"><i class="fa fa-trash-o"></i></button>
						</td>
					</tr>
					<?php } ?>
				<?php } else { ?>
					<tr>
						<td class="text-center" colspan="9"><?php echo $text_no_results; ?></td>
					</tr>
				<?php } ?>
				</tbody>
			</table>
		</div>
		<div class="row">
			<div class="col-sm-6 text-left"><?php echo $pagination; ?></div>
			<div class="col-sm-6 text-right"><?php echo $results; ?></div>
		</div>
	</div>
</div>
<script type="text/javascript"><!--
$(function() {
	$('#gifts-list-filter_name').autocomplete( {
		'source': function(request, response) {
			$.ajax( {
				url: 'index.php?route=module/membership/autocomplete&token=<?php echo $token; ?>',
				dataType: 'json',
				type: 'POST',
				data: 'filter_gift_name=' +  encodeURIComponent(request),
				beforeSend: function () {},
				complete: function () {},
				success: function(json) {
					response($.map(json, function(item) {
						return {
							label: item['name'],
							value: item['name']
						}
					} ));
				}
			} );
		},
		'select': function(item) {
			$(this).val(item['value']);
		}
	} );	
	
	$('#button-gifts-list-filter').on('click', function() {
		var 
			url = 'index.php?route=module/membership/getGiftsList&token=<?php echo $token; ?>'
			filter_name = $('#gifts-list-filter_name').val();

		if (filter_name) {
			url += '&filter_name=' + encodeURIComponent(filter_name);
		}

		$('#gifts-list').load(url);
	} );
} );	
//--></script>