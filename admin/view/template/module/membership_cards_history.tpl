<div class="well">
	<div class="row">
		<div class="col-sm-3">
			<label class="control-label" for="cards-history-filter_order_id"><?php echo $entry_order_number; ?></label>
			<input type="text" name="filter_order_id" value="<?php echo $filter_order_id; ?>" placeholder="<?php echo $entry_order_number; ?>" id="cards-history-filter_order_id" class="form-control" />
		</div>
		<div class="col-sm-3">
			<label class="control-label" for="cards-history-filter_code"><?php echo $entry_code; ?></label>
			<input type="text" name="filter_code" value="<?php echo $filter_code; ?>" placeholder="<?php echo $entry_code; ?>" id="cards-history-filter_code" class="form-control" />
		</div>
		<div class="col-sm-3">
			<label class="control-label" for="cards-history-filter_cardowner"><?php echo $entry_cardowner; ?></label>
			<input type="text" name="filter_cardowner" value="<?php echo $filter_cardowner; ?>" placeholder="<?php echo $entry_cardowner; ?>" id="cards-history-filter_cardowner" class="form-control" />
		</div>
		<div class="col-sm-3">
			<label class="control-label" for="cards-history-filter_customer"><?php echo $entry_customer; ?></label>
			<input type="text" name="filter_customer" value="<?php echo $filter_customer; ?>" placeholder="<?php echo $entry_customer; ?>" id="cards-history-filter_customer" class="form-control" />
		</div>
	</div>
	<div class="row" style="padding-top: 15px;">
		<div class="col-sm-2 col-sm-offset-10">
			<button type="button" id="button-cards-history-filter" class="btn btn-primary pull-right"><i class="fa fa-search"></i> <?php echo $button_filter; ?></button>
		</div>
	</div>
</div>
<div class="panel panel-default">
	<div class="panel-heading clearfix">
		<h3 class="panel-title pull-left" style="padding-top: 9px;"><i class="fa fa-list"></i> <?php echo $text_cards_history; ?></h3>
		<div class="pull-right">
			<button type="button" title="<?php echo $button_delete; ?>" class="btn btn-danger" data-toggle="tooltip" id="button-delete-cards-history" onclick="confirm('<?php echo $text_confirm; ?>') ? deleteData(this, 'cardHistory') : false;" disabled="disabled"><i class="fa fa-trash-o"></i></button>
		</div>
	</div>
	<div class="panel-body">
		<div class="table-responsive">
			<table class="table table-bordered table-hover">
				<thead>
					<tr>
						<td style="width: 1px;" class="text-center"><input type="checkbox" onclick="$('input[name*=\'selected\']').prop('checked', this.checked).trigger('change');" /></td>
						<td class="text-center">
						<?php if ($sort == 'order_id') { ?>
							<a href="<?php echo $sort_order_id; ?>" class="<?php echo strtolower($order); ?>"><?php echo $column_order_id; ?></a>
						<?php } else { ?>
							<a href="<?php echo $sort_order_id; ?>"><?php echo $column_order_id; ?></a>
						<?php } ?>	
						</td>
						<td class="text-center">
						<?php if ($sort == 'code') { ?>
							<a href="<?php echo $sort_code; ?>" class="<?php echo strtolower($order); ?>"><?php echo $column_code; ?></a>
						<?php } else { ?>
							<a href="<?php echo $sort_code; ?>"><?php echo $column_code; ?></a>
						<?php } ?>	
						</td>
						<td class="text-center">
						<?php if ($sort == 'cardowner') { ?>
							<a href="<?php echo $sort_cardowner; ?>" class="<?php echo strtolower($order); ?>"><?php echo $column_cardowner; ?></a>
						<?php } else { ?>
							<a href="<?php echo $sort_cardowner; ?>"><?php echo $column_cardowner; ?></a>
						<?php } ?>
						</td>
						<td class="text-center">
						<?php if ($sort == 'customer') { ?>
							<a href="<?php echo $sort_customer; ?>" class="<?php echo strtolower($order); ?>"><?php echo $column_customer; ?></a>
						<?php } else { ?>
							<a href="<?php echo $sort_customer; ?>"><?php echo $column_customer; ?></a>
						<?php } ?>
						</td>
						<td class="text-center">
						<?php if ($sort == 'total_without_discount') { ?>
							<a href="<?php echo $sort_total_without_discount; ?>" class="<?php echo strtolower($order); ?>"><?php echo $column_total_without_discount; ?></a>
						<?php } else { ?>
							<a href="<?php echo $sort_total_without_discount; ?>"><?php echo $column_total_without_discount; ?></a>
						<?php } ?>
						</td>
						<td class="text-center">
						<?php if ($sort == 'discount') { ?>
							<a href="<?php echo $sort_discount; ?>" class="<?php echo strtolower($order); ?>"><?php echo $column_discount; ?></a>
						<?php } else { ?>
							<a href="<?php echo $sort_discount; ?>"><?php echo $column_discount; ?></a>
						<?php } ?>
						</td>
						<td class="text-center">
						<?php if ($sort == 'total') { ?>
							<a href="<?php echo $sort_total; ?>" class="<?php echo strtolower($order); ?>"><?php echo $column_total; ?></a>
						<?php } else { ?>
							<a href="<?php echo $sort_total; ?>"><?php echo $column_total; ?></a>
						<?php } ?>
						</td>
						<td class="text-center">
						<?php if ($sort == 'datetime_added') { ?>
							<a href="<?php echo $sort_datetime_added; ?>" class="<?php echo strtolower($order); ?>"><?php echo $column_datetime_added; ?></a>
						<?php } else { ?>
							<a href="<?php echo $sort_datetime_added; ?>"><?php echo $column_datetime_added; ?></a>
						<?php } ?>
						</td>
						<td class="text-center"><?php echo $column_action; ?>
						</td>
					</tr>
				</thead>
				<tbody>
				<?php if ($histories) { ?>
		    		<?php foreach ($histories as $history) { ?>
		    		<?php if (!$history['status']) { ?>
		    		<tr class="danger">
		    		<?php } else { ?>
		    		<tr>
		    		<?php } ?>
		    			<td class="text-center">
							<input type="checkbox" name="selected[]" value="<?php echo $history['card_history_id']; ?>" />
						</td>
		    			<td class="text-center">
		    			<?php if ($history['order_id']) { ?>
							<a href="index.php?route=sale/order/info&token=<?php echo $token; ?>&order_id=<?php echo $history['order_id']; ?>" target="_blank"><?php echo $history['order_id']; ?></a>
						<?php } ?>
		    			</td>
		    			<td class="text-center">
		    				<a href="<?php echo $card_edit; ?>&card_id=<?php echo $history['card_id']; ?>"><?php echo $history['code']; ?></a>
		    			</td>
		    			<td class="text-center">
		    			<?php if ($history['cardowner_id']) { ?>
							<a href="index.php?route=customer/customer/edit&token=<?php echo $token; ?>&customer_id=<?php echo $history['cardowner_id']; ?>" target="_blank"><?php echo $history['cardowner']; ?></a>
						<?php } ?>
		    			</td>
		    			<td class="text-center"><?php echo $history['customer']; ?></td>
		    			<td class="text-center"><?php echo $history['total_without_discount']; ?></td>
		    			<td class="text-center"><?php echo $history['discount']; ?></td>
		    			<td class="text-center"><?php echo $history['total']; ?></td>
		    			<td class="text-center"><?php echo $history['datetime_added']; ?></td>
		    			<td class="text-center">
		    				<button type="button" onclick="confirm('<?php echo $text_confirm; ?>') ? deleteData(this, 'cardHistory') : false;" class="btn btn-danger" data-toggle="tooltip" title="<?php echo $button_delete; ?>"><i class="fa fa-trash-o" aria-hidden="true"></i></button>
		    			</td>
		    		</tr>	
		    		<?php } ?>
		    	<?php } else { ?>
		    		<tr>
		        		<td class="text-center" colspan="10"><?php echo $text_no_results; ?></td>
		        	</tr>
		      	<?php } ?>
				</tbody>
			</table>
		</div>
		<div class="row">
			<div class="col-sm-6 text-left"><?php echo $pagination; ?></div>
			<div class="col-sm-6 text-right"><?php echo $results; ?></div>
		</div>
	</div>
</div>
<script type="text/javascript"><!--
$(function() {
	$('#cards-history-filter_order_id').autocomplete( {
		'source': function(request, response) {
			$.ajax({
				url: 'index.php?route=module/membership/autocomplete&token=<?php echo $token; ?>',
				dataType: 'json',
				type: 'POST',
				data: 'filter_order_id=' +  encodeURIComponent(request) + '&cardHistory=1',
				beforeSend: function () {},
				complete: function () {},
				success: function(json) {
					response($.map(json, function(item) {
						return {
							label: '<i class="fa fa-shopping-cart" aria-hidden="true"></i> ' + item['order_id'],
							value: item['order_id']
						}
					}));
				}
			});
		},
		'select': function(item) {
			$(this).val(item['value']);
		}
	} );
	
	$('#cards-history-filter_code').autocomplete( {
		'source': function(request, response) {
			$.ajax({
				url: 'index.php?route=module/membership/autocomplete&token=<?php echo $token; ?>',
				dataType: 'json',
				type: 'POST',
				data: 'filter_code=' +  encodeURIComponent(request) + '&cardHistory=1',
				beforeSend: function () {},
				complete: function () {},
				success: function(json) {
					response($.map(json, function(item) {
						return {
							label: '<i class="fa fa-credit-card" aria-hidden="true"></i> ' + item['code'],
							value: item['code']
						}
					}));
				}
			});
		},
		'select': function(item) {
			$(this).val(item['value']);
		}
	} );
	
	$('#cards-history-filter_cardowner').autocomplete( {
		'source': function(request, response) {
			$.ajax({
				url: 'index.php?route=module/membership/autocomplete&token=<?php echo $token; ?>',
				dataType: 'json',
				type: 'POST',
				data: 'filter_cardowner=' +  encodeURIComponent(request) + '&cardHistory=1',
				beforeSend: function () {},
				complete: function () {},
				success: function(json) {
					response($.map(json, function(item) {
						return {
							label: '<i class="fa fa-user" aria-hidden="true"></i> ' + item['cardowner'],
							value: item['cardowner']
						}
					}));
				}
			});
		},
		'select': function(item) {
			$(this).val(item['value']);
		}
	} );
	
	$('#cards-history-filter_customer').autocomplete( {
		'source': function(request, response) {
			$.ajax({
				url: 'index.php?route=module/membership/autocomplete&token=<?php echo $token; ?>',
				dataType: 'json',
				type: 'POST',
				data: 'filter_customer=' +  encodeURIComponent(request) + '&cardHistory=1',
				beforeSend: function () {},
				complete: function () {},
				success: function(json) {
					response($.map(json, function(item) {
						return {
							label: '<i class="fa fa-user" aria-hidden="true"></i> ' + item['customer'],
							value: item['customer']
						}
					}));
				}
			});
		},
		'select': function(item) {
			$(this).val(item['value']);
		}
	} );

	$('#button-cards-history-filter').on('click', function() {
		var 
			url = 'index.php?route=module/membership/getCardsHistory&token=<?php echo $token; ?>',
			filter_order_id = $('#cards-history-filter_order_id').val(),
			filter_code = $('#cards-history-filter_code').val(),
			filter_cardowner = $('#cards-history-filter_cardowner').val(),
			filter_customer = $('#cards-history-filter_customer').val();

		if (filter_order_id) {
			url += '&filter_order_id=' + encodeURIComponent(filter_order_id);
		}
		
		if (filter_code) {
			url += '&filter_code=' + encodeURIComponent(filter_code);
		}
		
		if (filter_cardowner) {
			url += '&filter_cardowner=' + encodeURIComponent(filter_cardowner);
		}
		
		if (filter_customer) {
			url += '&filter_customer=' + encodeURIComponent(filter_customer);
		}

		$('#cards-history').load(url);
	} );
} );	
//--></script>