<div class="table-responsive">
<div class="pull-right" style="padding-bottom: 7px;">
	<button type="button" title="<?php echo $button_delete; ?>" class="btn btn-danger" data-toggle="tooltip" id="button-delete-card-histories" onclick="confirm('<?php echo $text_confirm; ?>') ? deleteData(this, 'cardHistory') : false;" disabled="disabled"><i class="fa fa-trash-o"></i></button>
</div>
	<table class="table table-bordered table-hover" id="table-history">
		<thead>
			<tr>
				<td style="width: 1px;" class="text-center"><input type="checkbox" onclick="$('input[name*=\'selected\']').prop('checked', this.checked).trigger('change');" /></td>
				<td class="text-center">
				<?php if ($sort == 'order_id') { ?>
					<a href="<?php echo $sort_order_id; ?>" class="<?php echo strtolower($order); ?>"><?php echo $column_order_id; ?></a>
				<?php } else { ?>
					<a href="<?php echo $sort_order_id; ?>"><?php echo $column_order_id; ?></a>
				<?php } ?>	
				</td>
				<td class="text-center">
				<?php if ($sort == 'customer') { ?>
					<a href="<?php echo $sort_customer; ?>" class="<?php echo strtolower($order); ?>"><?php echo $column_customer; ?></a>
				<?php } else { ?>
					<a href="<?php echo $sort_customer; ?>"><?php echo $column_customer; ?></a>
				<?php } ?>
				</td>
				<td class="text-center">
				<?php if ($sort == 'total_without_discount') { ?>
					<a href="<?php echo $sort_total_without_discount; ?>" class="<?php echo strtolower($order); ?>"><?php echo $column_total_without_discount; ?></a>
				<?php } else { ?>
					<a href="<?php echo $sort_total_without_discount; ?>"><?php echo $column_total_without_discount; ?></a>
				<?php } ?>
				</td>
				<td class="text-center">
				<?php if ($sort == 'discount') { ?>
					<a href="<?php echo $sort_discount; ?>" class="<?php echo strtolower($order); ?>"><?php echo $column_discount; ?></a>
				<?php } else { ?>
					<a href="<?php echo $sort_discount; ?>"><?php echo $column_discount; ?></a>
				<?php } ?>
				</td>
				<td class="text-center">
				<?php if ($sort == 'total') { ?>
					<a href="<?php echo $sort_total; ?>" class="<?php echo strtolower($order); ?>"><?php echo $column_total; ?></a>
				<?php } else { ?>
					<a href="<?php echo $sort_total; ?>"><?php echo $column_total; ?></a>
				<?php } ?>
				</td>
				<td class="text-center">
				<?php if ($sort == 'datetime_added') { ?>
					<a href="<?php echo $sort_datetime_added; ?>" class="<?php echo strtolower($order); ?>"><?php echo $column_datetime_added; ?></a>
				<?php } else { ?>
					<a href="<?php echo $sort_datetime_added; ?>"><?php echo $column_datetime_added; ?></a>
				<?php } ?>
				</td>
				<td class="text-center"><?php echo $column_action; ?></td>
			</tr>
    	</thead>
    	<tbody>
    	<?php if ($histories) { ?>
    		<?php foreach ($histories as $history) { ?>
    		<?php if (!$history['status']) { $history_total--; ?>
    		<tr class="danger">
    		<?php } else { ?>
    		<tr>
    		<?php } ?>
    			<td class="text-center">
					<input type="checkbox" name="selected[]" value="<?php echo $history['card_history_id']; ?>" />
				</td>
    			<td class="text-center">
    			<?php if ($history['order_id']) { ?>
					<a href="index.php?route=sale/order/info&token=<?php echo $token; ?>&order_id=<?php echo $history['order_id']; ?>" target="_blank"><?php echo $history['order_id']; ?></a>
				<?php } ?>
    			</td>
    			<td class="text-center"><?php echo $history['customer']; ?></td>
    			<td class="text-center"><?php echo $history['total_without_discount']; ?></td>
    			<td class="text-center"><?php echo $history['discount']; ?></td>
    			<td class="text-center"><?php echo $history['total']; ?></td>
    			<td class="text-center"><?php echo $history['datetime_added']; ?></td>
    			<td class="text-center"><button type="button" onclick="confirm('<?php echo $text_confirm; ?>') ? deleteData(this, 'cardHistory') : false;" class="btn btn-danger" data-toggle="tooltip" title="<?php echo $button_delete; ?>"><i class="fa fa-trash-o" aria-hidden="true"></i></button></td>
    		</tr>	
    		<?php } ?>
    	<?php } else { ?>
    		<tr>
        		<td class="text-center" colspan="8"><?php echo $text_no_results; ?></td>
        	</tr>
      	<?php } ?>
    	</tbody>
    	<tfoot style="font-weight: bold;">
    		<tr>
    			<td colspan="8"><strong><?php echo $text_total; ?>:</strong></td>
    		</tr>
    		<tr>
    			<td></td>
    			<td class="text-center" style="vertical-align: middle;"><?php echo $history_total; ?></td>
    			<td></td>
    			<td id="history-total_without_discount" class="text-center" style="vertical-align: middle;"><?php echo $card_totals['total_without_discount']; ?></td>
    			<td id="history-discount" class="text-center" style="vertical-align: middle;"><?php echo $card_totals['discount']; ?></td>
    			<td id="history-total" class="text-center" style="vertical-align: middle;"><?php echo $card_totals['total']; ?></td>
    			<td></td>
    			<td class="text-center"><button type="button" id="button-edit-totals" class="btn btn-primary" data-toggle="tooltip" title="<?php echo $button_edit; ?>"><i class="fa fa-pencil" aria-hidden="true"></i></button></td>
    		</tr>
		</tfoot>
    </table>
</div>
<div class="row">
  <div class="col-sm-6 text-left"><?php echo $pagination; ?></div>
  <div class="col-sm-6 text-right"><?php echo $results; ?></div>
</div>
<script type="text/javascript"><!--
$(function() {
	$('#table-history').on('click', '#button-edit-totals', function() {
		var 
			$td_total_without_discount = $('#history-total_without_discount'),
			$td_discount = $('#history-discount'),
			$td_total = $('#history-total'),
			$input_total_without_discount = $('<input type="text" name="history_total_without_discount" value="' + $td_total_without_discount.text() + '" id="input-history-total_without_discount" class="form-control">'),
			$input_discount = $('<input type="text" name="history_discount" value="' + $td_discount.text() + '" id="input-history-discount" class="form-control">'),
			$input_total = $('<input type="text" name="history_total" value="' + $td_total.text() + '" id="input-history-total" class="form-control">');
		
		$td_total_without_discount.html($input_total_without_discount);
		$td_discount.html($input_discount);
		$td_total.html($input_total);
		
		$(this).replaceWith('<button type="button" id="button-save-totals" class="btn btn-success" data-toggle="tooltip" title="<?php echo $button_save; ?>"><i class="fa fa-check" aria-hidden="true"></i></button>');
	} );

	$('#table-history').on('click', '#button-save-totals', function() {
		var 
			$input_total_without_discount = $('#input-history-total_without_discount'),
			$input_discount = $('#input-history-discount'),
			$input_total = $('#input-history-total');
		
		$.ajax( {
			url: 'index.php?route=module/membership/changeCardTotals&token=<?php echo $token; ?>',
			type: 'POST',
			data: '&card_id=<?php echo $card_id; ?>&total_without_discount=' +  parseFloat($input_total_without_discount.val()) + '&discount=' +  parseFloat($input_discount.val()) + '&total=' +  parseFloat($input_total.val()),
			dataType: 'json',
			success: function(json) {
				if (typeof json['card_history_id'] != 'undefined') {
					var row = '<tr style="display: none;">';
			
					row += '<td class="text-center"><input type="checkbox" name="selected[]" value="' + json['card_history_id'] + '" /></td>';
					row += '<td class="text-center">0</td>';
					row += '<td></td>';
					row += '<td class="text-center">' + json['total_without_discount'] + '</td>';
					row += '<td class="text-center">' + json['discount'] + '</td>';
					row += '<td class="text-center">' + json['total'] + '</td>';
					row += '<td></td>';
					row += '<td class="text-center"><button type="button" onclick="deleteData(this, \'deleteCardHistory\');" data-toggle="tooltip" title="<?php echo $button_delete; ?>" class="btn btn-danger"><i class="fa fa-trash-o" aria-hidden="true"></i></button></td>';
					row += '</tr>';
						
					$('#table-history tbody').append(row).find('tr:last').show('slow');
				}
			}
		});

		$input_total_without_discount.parent().html($input_total_without_discount.val());
		$input_discount.parent().html($input_discount.val());
		$input_total.parent().html($input_total.val());
		
		$(this).replaceWith('<button type="button" id="button-edit-totals" class="btn btn-primary" data-toggle="tooltip" title="<?php echo $button_edit; ?>"><i class="fa fa-pencil" aria-hidden="true"></i></button>');
	} );
} );
//--></script>