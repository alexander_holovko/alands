<?php if ($error_warning) { ?>
<div class="alert alert-danger">
  	<i class="fa fa-exclamation-circle"></i> <?php echo $error_warning; ?><button type="button" class="close" data-dismiss="alert">&times;</button>
 </div>
<?php } ?>
<?php if ($success) { ?>
<div class="alert alert-success"><i class="fa fa-check-circle"></i> <?php echo $success; ?>
	<button type="button" class="close" data-dismiss="alert">&times;</button>
</div>
<?php } ?>
<div class="well">
	<div class="row">
		<div class="col-sm-4">
			<label class="control-label" for="cards-list-filter_cardowner"><?php echo $entry_cardowner; ?></label>
			<input type="text" name="filter_cardowner" value="<?php echo $filter_cardowner; ?>" placeholder="<?php echo $entry_cardowner; ?>" id="cards-list-filter_cardowner" class="form-control" />
		</div>
		<div class="col-sm-4">
			<label class="control-label" for="cards-list-filter_telephone"><?php echo $entry_telephone; ?></label>
			<input type="text" name="filter_telephone" value="<?php echo $filter_telephone; ?>" placeholder="<?php echo $entry_telephone; ?>" id="cards-list-filter_telephone" class="form-control" />
		</div>
		<div class="col-sm-4">
			<label class="control-label" for="cards-list-filter_code"><?php echo $entry_code; ?></label>
			<input type="text" name="filter_code" value="<?php echo $filter_code; ?>" placeholder="<?php echo $entry_code; ?>" id="cards-list-filter_code" class="form-control" />
		</div>
	</div>
	<div class="row" style="padding-top: 15px;">
		<div class="col-sm-4 col-sm-offset-8">
			<button type="button" id="button-cards-list-filter" class="btn btn-primary pull-right"><i class="fa fa-search"></i> <?php echo $button_filter; ?></button>
		</div>
	</div>
</div>
<div class="panel panel-default">
	<div class="panel-heading clearfix">
		<h3 class="panel-title pull-left" style="padding-top: 9px;"><i class="fa fa-list"></i> <?php echo $text_discount_cards_list; ?></h3>
		<div class="pull-right">
			<a href="<?php echo $add; ?>" id="button-card-add" data-toggle="tooltip" title="<?php echo $button_add; ?>" class="btn btn-primary"><i class="fa fa-plus"></i></a>
			<button type="button" title="<?php echo $button_delete; ?>" class="btn btn-danger" data-toggle="tooltip" id="button-delete-cards-list" onclick="confirm('<?php echo $text_confirm; ?>') ? deleteData(this, 'card') : false;" disabled="disabled"><i class="fa fa-trash-o"></i></button>
		</div>
	</div>
	<div class="panel-body">
		<div class="table-responsive">
			<table class="table table-bordered table-hover">
				<thead>
					<tr>
						<td style="width: 1px;" class="text-center"><input type="checkbox" onclick="$('input[name*=\'selected\']').prop('checked', this.checked).trigger('change');" /></td>
						<td class="text-center">
						<?php if ($sort == 'cardowner') { ?>
							<a href="<?php echo $sort_cardowner; ?>" class="<?php echo strtolower($order); ?>"><?php echo $column_cardowner; ?></a>
						<?php } else { ?>
							<a href="<?php echo $sort_cardowner; ?>"><?php echo $column_cardowner; ?></a>
						<?php } ?>
						</td>
						<td class="text-center">
						<?php if ($sort == 'code') { ?>
							<a href="<?php echo $sort_code; ?>" class="<?php echo strtolower($order); ?>"><?php echo $column_code; ?></a>
						<?php } else { ?>
							<a href="<?php echo $sort_code; ?>"><?php echo $column_code; ?></a>
						<?php } ?>
						</td>
						<td class="text-center"><?php echo $column_discount; ?></td>
						<td class="text-center">
						<?php if ($sort == 'total') { ?>
							<a href="<?php echo $sort_total; ?>" class="<?php echo strtolower($order); ?>"><?php echo $column_balance; ?></a>
						<?php } else { ?>
							<a href="<?php echo $sort_total; ?>"><?php echo $column_balance; ?></a>
						<?php } ?>
						</td>
						<td class="text-center">
						<?php if ($sort == 'date_start') { ?>
							<a href="<?php echo $sort_date_start; ?>" class="<?php echo strtolower($order); ?>"><?php echo $column_date_start; ?></a>
						<?php } else { ?>
							<a href="<?php echo $sort_date_start; ?>"><?php echo $column_date_start; ?></a>
						<?php } ?>
						</td>
						<td class="text-center">
						<?php if ($sort == 'date_expiry') { ?>
							<a href="<?php echo $sort_date_expiry; ?>" class="<?php echo strtolower($order); ?>"><?php echo $column_date_expiry; ?></a>
						<?php } else { ?>
							<a href="<?php echo $sort_date_expiry; ?>"><?php echo $column_date_expiry; ?></a>
						<?php } ?>
						</td>
						<td class="text-center">
						<?php if ($sort == 'status') { ?>
							<a href="<?php echo $sort_status; ?>" class="<?php echo strtolower($order); ?>"><?php echo $column_status; ?></a>
						<?php } else { ?>
							<a href="<?php echo $sort_status; ?>"><?php echo $column_status; ?></a>
						<?php } ?>
						</td>
						<td class="text-center"><?php echo $column_action; ?></td>
					</tr>
				</thead>
				<tbody>
				<?php if ($cards) { ?>
					<?php foreach ($cards as $card) { ?>
					<tr>
						<td class="text-center">
							<input type="checkbox" name="selected[]" value="<?php echo $card['card_id']; ?>" />
						</td>
						<td>
						<?php if ($card['cardowner']) { ?>
							<a href="index.php?route=customer/customer/edit&token=<?php echo $token; ?>&customer_id=<?php echo $card['customer_id']; ?>" target="_blank"><?php echo $card['cardowner']; ?></a>
						<?php } ?>
						</td>
						<td class="text-center"><?php echo $card['code']; ?></td>
						<td class="text-center"><?php echo $card['discount']; ?></td>
						<td class="text-right"><?php echo $card['total']; ?></td>
						<td class="text-center"><?php echo $card['date_start']; ?></td>
						<td class="text-center"><?php echo $card['date_expiry']; ?></td>
						<td class="text-center">
						<?php if ($card['status'] == 2) { ?>
							<span class="label label-warning" style="cursor: pointer;" onclick="changeStatus(this, 'card');" oncontextmenu="changeStatus(this, 'card', 'all'); return false;">	
						<?php } elseif ($card['status'] == 1) { ?>	
							<span class="label label-success" style="cursor: pointer;" onclick="changeStatus(this, 'card');" oncontextmenu="changeStatus(this, 'card', 'all'); return false;">
						<?php } else { ?>
							<span class="label label-danger" style="cursor: pointer;" onclick="changeStatus(this, 'card');" oncontextmenu="changeStatus(this, 'card', 'all'); return false;">
						<?php } ?>		
							<?php echo $card['status_text']; ?>
							</span>
						</td>
						<td class="text-center">
							<a href="<?php echo $card['edit']; ?>" id="button-card-edit-<?php echo $card['card_id']; ?>" title="<?php echo $button_edit; ?>" class="btn btn-primary" data-toggle="tooltip"><i class="fa fa-pencil"></i></a>
							<button type="button" onclick="confirm('<?php echo $text_confirm; ?>') ? deleteData(this, 'card') : false;" data-toggle="tooltip" data-value="" title="<?php echo $button_delete; ?>" class="btn btn-danger"><i class="fa fa-trash-o"></i></button>
						</td>
					</tr>
					<?php } ?>
				<?php } else { ?>
					<tr>
						<td class="text-center" colspan="9"><?php echo $text_no_results; ?></td>
					</tr>
				<?php } ?>
				</tbody>
			</table>
		</div>
		<div class="row">
			<div class="col-sm-6 text-left"><?php echo $pagination; ?></div>
			<div class="col-sm-6 text-right"><?php echo $results; ?></div>
		</div>
	</div>
</div>
<script type="text/javascript"><!--
$(function() {
	$('#cards-list-filter_cardowner').autocomplete( {
		'source': function(request, response) {
			$.ajax( {
				url: 'index.php?route=module/membership/autocomplete&token=<?php echo $token; ?>',
				dataType: 'json',
				type: 'POST',
				data: 'filter_cardowner=' +  encodeURIComponent(request),
				beforeSend: function () {},
				complete: function () {},
				success: function(json) {
					response($.map(json, function(item) {
						return {
							label: '<i class="fa fa-user" aria-hidden="true"></i> ' + item['cardowner'] + ' <i class="fa fa-phone" aria-hidden="true"></i> ' + item['telephone'],
							value: item['cardowner']
						}
					} ));
				}
			} );
		},
		'select': function(item) {
			$(this).val(item['value']);
		}
	} );
	
	$('#cards-list-filter_telephone').autocomplete( {
		'source': function(request, response) {
			$.ajax( {
				url: 'index.php?route=module/membership/autocomplete&token=<?php echo $token; ?>',
				dataType: 'json',
				type: 'POST',
				data: 'filter_telephone=' +  encodeURIComponent(request),
				beforeSend: function () {},
				complete: function () {},
				success: function(json) {
					response($.map(json, function(item) {
						return {
							label: '<i class="fa fa-phone" aria-hidden="true"></i> ' + item['telephone'] + ' <i class="fa fa-user" aria-hidden="true"></i> ' + item['cardowner'],
							value: item['telephone']
						}
					} ));
				}
			} );
		},
		'select': function(item) {
			$(this).val(item['value']);
		}
	} );
	
	$('#cards-list-filter_code').autocomplete( {
		'source': function(request, response) {
			$.ajax( {
				url: 'index.php?route=module/membership/autocomplete&token=<?php echo $token; ?>',
				dataType: 'json',
				type: 'POST',
				data: 'filter_code=' +  encodeURIComponent(request),
				beforeSend: function () {},
				complete: function () {},
				success: function(json) {
					response($.map(json, function(item) {
						return {
							label: '<i class="fa fa-credit-card" aria-hidden="true"></i> ' + item['code'],
							value: item['code']
						}
					} ));
				}
			} );
		},
		'select': function(item) {
			$(this).val(item['value']);
		}
	} );	
	
	$('#button-cards-list-filter').on('click', function() {
		var 
			url = 'index.php?route=module/membership/getCardsList&token=<?php echo $token; ?>'
			filter_cardowner = $('#cards-list-filter_cardowner').val(),
			filter_telephone = $('#cards-list-filter_telephone').val(),
			filter_code = $('#cards-list-filter_code').val();

		if (filter_cardowner) {
			url += '&filter_cardowner=' + encodeURIComponent(filter_cardowner);
		}

		if (filter_telephone) {
			url += '&filter_telephone=' + encodeURIComponent(filter_telephone);
		}

		if (filter_code) {
			url += '&filter_code=' + encodeURIComponent(filter_code);
		}

		$('#cards-list').load(url);
	} );
} );	
//--></script>