<?php echo $header; ?><?php echo $column_left; ?>
<div id="content">
    <div class="page-header">
        <div class="container-fluid">
            <div class="pull-right">
                <button type="submit" form="form-zoom-pretty" data-toggle="tooltip" title="<?php echo $button_save; ?>" class="btn btn-primary"><i class="fa fa-save"></i></button>
                <a href="<?php echo $cancel; ?>" data-toggle="tooltip" title="<?php echo $button_cancel; ?>" class="btn btn-default"><i class="fa fa-reply"></i></a></div>
            <h1><img src="//mmosolution.com/image/mmosolution.com_34.png"><?php echo $heading_title; ?></h1>
            <ul class="breadcrumb">
                <?php foreach ($breadcrumbs as $breadcrumb) { ?>
                <li><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a></li>
                <?php } ?>
            </ul>
        </div>
    </div>
    <div class="container-fluid">
        <?php if ($error_warning) { ?>
        <div class="alert alert-danger"><i class="fa fa-exclamation-circle"></i> <?php echo $error_warning; ?>
            <button type="button" class="close" data-dismiss="alert">&times;</button>
        </div>
        <?php } ?>
        <div class="panel panel-default">
            <div class="panel-heading">
                <h3 class="panel-title"><i class="fa fa-pencil"></i><?php echo $text_edit_module; ?></h3>
            </div>
			<br>
           <ul class="nav nav-tabs">
               <li class="active"><a href="#tab-setting" data-toggle="tab"><?php echo $tab_setting; ?></a></li>
                <li><a href="#supporttabs" data-toggle="tab"><?php echo $tab_support; ?></a></li>
                <li id="mmos-offer"></li>
                <li class="pull-right"><a  class="link" href="http://www.opencart.com/index.php?route=extension/extension&filter_username=mmosolution" target="_blank" class="text-success"><img src="//mmosolution.com/image/opencart.gif"> More Extension...</a></li>
                <li class="pull-right"><a  class="text-link"  href="http://mmosolution.com" target="_blank" class="text-success"><img src="//mmosolution.com/image/mmosolution_20x20.gif">More Extension...</a></li>
            </ul>
            <div class="tab-content">
                <div class="tab-pane active" id="tab-setting">
                    <div class="panel-body">
                        <form action="<?php echo $action; ?>" method="post" enctype="multipart/form-data" id="form-zoom-pretty" class="form-horizontal">
                            <div class="form-group">
                                <label class="col-sm-2 control-label" for="input-status"><?php echo $text_status; ?></label>
                                <div class="col-sm-10">
                                    <select name="mmos_zoom_pretty[status]" id="input-status" class="form-control">
                                        <?php if ($mmos_zoom_pretty['status'] == 1) { ?>
                                        <option value="1" selected="selected"><?php echo $text_enabled; ?></option>
                                        <option value="0"><?php echo $text_disabled; ?></option>
                                        <?php } else { ?>
                                        <option value="1"><?php echo $text_enabled; ?></option>
                                        <option value="0" selected="selected"><?php echo $text_disabled; ?></option>
                                        <?php } ?>
                                    </select>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-2 control-label" for="input-theme"><?php echo $text_theme_popup; ?></label>
                                <div class="col-sm-10">
                                    <select name="mmos_zoom_pretty[theme_popup]" id="input-theme" class="form-control">
                                        <option value="pp_default" <?php echo ($mmos_zoom_pretty['theme_popup'] == 'pp_default') ? 'selected' : '';?>><?php echo 'Default'; ?></option>
                                        <option value="light_rounded" <?php echo ($mmos_zoom_pretty['theme_popup'] == 'light_rounded') ? 'selected' : '';?>><?php echo 'Light Rounded'; ?></option>
                                        <option value="dark_rounded" <?php echo ($mmos_zoom_pretty['theme_popup'] == 'dark_rounded') ? 'selected' : '';?>><?php echo 'Dark Rounded'; ?></option>
                                        <option value="light_square" <?php echo ($mmos_zoom_pretty['theme_popup'] == 'light_square') ? 'selected' : '';?>><?php echo 'Light Square'; ?></option>
                                        <option value="dark_square" <?php echo ($mmos_zoom_pretty['theme_popup'] == 'dark_square') ? 'selected' : '';?>><?php echo 'Dark Square'; ?></option>
                                        <option value="facebook" <?php echo ($mmos_zoom_pretty['theme_popup'] == 'facebook') ? 'selected' : '';?>><?php echo 'Facebook'; ?></option>
                                    </select>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-2 control-label" for="input-speed-popup"><?php echo $text_speed_popup; ?></label>
                                <div class="col-sm-10">
                                    <select name="mmos_zoom_pretty[speed_popup]" id="input-speed-popup" class="form-control">
                                        <option value="slow" <?php echo ($mmos_zoom_pretty['speed_popup'] == 'slow') ? 'selected' : '';?>><?php echo 'Slow'; ?></option>
                                        <option value="normal" <?php echo ($mmos_zoom_pretty['speed_popup'] == 'normal') ? 'selected' : '';?>><?php echo 'Normal'; ?></option>
                                        <option value="fast" <?php echo ($mmos_zoom_pretty['speed_popup'] == 'fast') ? 'selected' : '';?>><?php echo 'Fast'; ?></option>
                                    </select>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-2 control-label" for="input-title"><?php echo $text_show_title; ?></label>
                                <div class="col-sm-10">
                                    <select name="mmos_zoom_pretty[title_popup]" id="input-title" class="form-control">
                                        <option value="1" <?php echo ($mmos_zoom_pretty['title_popup'] == 1) ? 'selected' : '';?>><?php echo $text_enabled; ?></option>
                                        <option value="0" <?php echo ($mmos_zoom_pretty['title_popup'] == 0) ? 'selected' : '';?>><?php echo $text_disabled; ?></option>
                                    </select>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-2 control-label" for="input-autoplay"><?php echo $text_autoplay_slideshow; ?></label>
                                <div class="col-sm-10">
                                    <select name="mmos_zoom_pretty[autoplay]" id="input-autoplay" class="form-control">
                                        <option value="1" <?php echo ($mmos_zoom_pretty['autoplay'] == 1) ? 'selected' : '';?>><?php echo $text_enabled; ?></option>
                                        <option value="0" <?php echo ($mmos_zoom_pretty['autoplay'] == 0) ? 'selected' : '';?>><?php echo $text_disabled; ?></option>
                                    </select>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-2 control-label" for="input-resize-popup"><?php echo $text_resize_popup; ?></label>
                                <div class="col-sm-10">
                                    <select name="mmos_zoom_pretty[resize_popup]" id="input-resize-popup" class="form-control">
                                        <option value="1" <?php echo ($mmos_zoom_pretty['resize_popup'] == 1) ? 'selected' : '';?>><?php echo $text_enabled; ?></option>
                                        <option value="0" <?php echo ($mmos_zoom_pretty['resize_popup'] == 0) ? 'selected' : '';?>><?php echo $text_disabled; ?></option>
                                    </select>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-2 control-label" for="input-speed-slide"><span data-toggle="tooltip" title="<?php echo $help_slideshow; ?>"><?php echo $text_slideshow; ?></span></label>
                                <div class="col-sm-10">
                                    <input type="text" name="mmos_zoom_pretty[speed_slide]" value="<?php echo $mmos_zoom_pretty['speed_slide']; ?>" id="input-speed-slide" class="form-control"/>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-2 control-label" for="input-opacity"><span data-toggle="tooltip" title="<?php echo $help_opacity; ?>"><?php echo $text_opacity; ?></span></label>
                                <div class="col-sm-10">
                                    <input type="text" name="mmos_zoom_pretty[opacity]" value="<?php echo $mmos_zoom_pretty['opacity']; ?>" id="input-opacity" class="form-control"/>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-2 control-label" for="input-popup-button"><?php echo $text_show_popup_button; ?></label>
                                <div class="col-sm-10">
                                    <select name="mmos_zoom_pretty[popup_button]" id="input-popup-button" class="form-control">
                                        <option value="1" <?php echo ($mmos_zoom_pretty['popup_button'] == 1) ? 'selected' : '';?>><?php echo $text_enabled; ?></option>
                                        <option value="0" <?php echo ($mmos_zoom_pretty['popup_button'] == 0) ? 'selected' : '';?>><?php echo $text_disabled; ?></option>
                                    </select>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-2 control-label" for="input-social-button"><?php echo $text_social_button; ?></label>
                                <div class="col-sm-10">
                                    <select name="mmos_zoom_pretty[social_button]" id="input-social-button" class="form-control">
                                        <option value="1" <?php echo ($mmos_zoom_pretty['social_button'] == 1) ? 'selected' : '';?>><?php echo $text_enabled; ?></option>
                                        <option value="0" <?php echo ($mmos_zoom_pretty['social_button'] == 0) ? 'selected' : '';?>><?php echo $text_disabled; ?></option>
                                    </select>
                                </div>
                            </div>             
                            <div class="form-group hidden">
                                <label class="col-sm-2 control-label" for="input-title-cloudzoom"><?php echo $text_show_title_cloud_zoom; ?></label>
                                <div class="col-sm-10">
                                    <select name="mmos_zoom_pretty[title_cloudzoom]" id="input-title-cloudzoom" class="form-control">
                                        <option value="1" <?php echo ($mmos_zoom_pretty['title_cloudzoom'] == 1) ? 'selected' : '';?>><?php echo $text_enabled; ?></option>
                                        <option value="0" <?php echo ($mmos_zoom_pretty['title_cloudzoom'] == 0) ? 'selected' : '';?>><?php echo $text_disabled; ?></option>
                                    </select>
                                </div>
                            </div>                          
                            <div class="form-group">
                                <label class="col-sm-2 control-label" for="input-border-cloudzoom"><span data-toggle="tooltip" title="<?php echo $help_border_cloud_zoom; ?>"><?php echo $text_border_cloud_zoom; ?></span></label>
                                <div class="col-sm-10">
                                    <div class="row">
                                        <div class="col-sm-2">
                                            <input type="text" name="mmos_zoom_pretty[border_cloudzoom]" value="<?php echo $mmos_zoom_pretty['border_cloudzoom']; ?>" id="input-border-cloudzoom" class="form-control"/>
                                            </select>
                                        </div>
                                        <label class="col-sm-1 control-label" for="input-color-border-cloudzoom"><?php echo $color_border_cloud_zoom; ?></label>
                                        <div class="col-sm-3">
                                            <input type="text" name="mmos_zoom_pretty[color_border_cloudzoom]" value="<?php echo $mmos_zoom_pretty['color_border_cloudzoom']; ?>" id="input-color-border-cloudzoom" class="form-control colorpicker" />                                       
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-2 control-label" for="input-nav_but_color"><span data-toggle="tooltip" title="<?php echo $help_border_cloud_zoom; ?>"><?php echo $text_nav_but_color; ?></span></label>
                                <div class="col-sm-10">
                                  <input type="text" name="mmos_zoom_pretty[nav_but_color]" value="<?php echo $mmos_zoom_pretty['nav_but_color']; ?>" id="input-nav_but_color" class="form-control colorpicker" /> 
                                </div>
                            </div>

							<div class="form-group">
                                <label class="col-sm-2 control-label" for="input-zoom-size"><span data-toggle="tooltip" title="<?php echo $help_border_cloud_zoom; ?>"><?php echo $text_resize_cloud_zoom; ?></span></label>
                                <div class="col-sm-10">
                                    <input type="text" name="mmos_zoom_pretty[zoom_size]" value="<?php echo $mmos_zoom_pretty['zoom_size']; ?>" id="input-zoom-size" class="form-control"/>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-2 control-label" for="input-carousel"><?php echo $text_carousel; ?></label>
                                <div class="col-sm-10">
                                    <select name="mmos_zoom_pretty[carousel]" id="input-carousel" class="form-control">
                                        <option value="1" <?php echo ($mmos_zoom_pretty['carousel'] == 1) ? 'selected' : '';?>><?php echo $text_enabled; ?></option>
                                        <option value="0" <?php echo ($mmos_zoom_pretty['carousel'] == 0) ? 'selected' : '';?>><?php echo $text_disabled; ?></option>
                                    </select>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-2 control-label" for="input-item-carousel"><span data-toggle="tooltip" title="<?php echo $help_item_carousel; ?>"><?php echo $text_item_carousel; ?></span></label>
                                <div class="col-sm-10">
                                    <input type="text" name="mmos_zoom_pretty[item_carousel]" value="<?php echo $mmos_zoom_pretty['item_carousel']; ?>" id="input-item-carousel" class="form-control"/>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
                <div class="tab-pane" id="supporttabs">
                    <div class="panel">
                        <div class=" clearfix">
                            <div class="panel-body">
                                <h4> About <?php echo $heading_title; ?></h4>
                                <h5>Installed Version: V.<?php echo $MMOS_version; ?> </h5>
                                <h5>Latest version: <span id="mmos_latest_version"><a href="http://mmosolution.com/index.php?route=product/search&search=<?php echo trim(strip_tags($heading_title)); ?>" target="_blank">Unknown -- Check</a></span></h5>
                                <hr>
                                <h4>About Author</h4>
                                <div id="contact-infor">
                                    <i class="fa fa-envelope-o"></i> <a href="mailto:support@mmosolution.com?Subject=<?php echo trim(strip_tags($heading_title)).' OC '.VERSION; ?>" target="_top">support@mmosolution.com</a></br>
                                    <i class="fa fa-globe"></i> <a href="http://mmosolution.com" target="_blank">http://mmosolution.com</a> </br>
                                    <i class="fa fa-ticket"></i> <a href="http://mmosolution.com/support/" target="_blank">Open Ticket</a> </br>


                                    <br>
                                    <h4>Our on Social</h4>
                                    <a href="http://www.facebook.com/mmosolution" target="_blank"><i class="fa fa-2x fa-facebook-square"></i></a>
                                    <a class="text-success" href="http://plus.google.com/+Mmosolution" target="_blank"><i class="fa  fa-2x fa-google-plus-square"></i></a>
                                    <a class="text-warning" href="http://mmosolution.com/mmosolution_rss.rss" target="_blank"><i class="fa fa-2x fa-rss-square"></i></a>
                                    <a href="http://twitter.com/mmosolution" target="_blank"><i class="fa fa-2x fa-twitter-square"></i></a>
                                    <a class="text-danger" href="http://www.youtube.com/mmosolution" target="_blank"><i class="fa fa-2x fa-youtube-square"></i></a>
                                </div>
                                <div id="relate-products">


                                </div>

                            </div>

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript" src="//mmosolution.com/support.js"></script>
<script type="text/javascript"><!--
    var productcode = '<?php echo $MMOS_code_id ;?>';
    $('.colorpicker').colorpicker();
//--></script>
<?php echo $footer; ?>