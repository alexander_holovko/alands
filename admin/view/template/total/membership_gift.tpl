<?php echo $header; ?><?php echo $column_left; ?>
<div id="content">
	<div class="page-header">
		<div class="container-fluid">
			<ul class="breadcrumb">
			<?php foreach ($breadcrumbs as $breadcrumb) { ?>
				<li><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a></li>
			<?php } ?>
			</ul>
		</div>
	</div>
	<div class="container-fluid">
    <?php if ($error_warning) { ?>
    	<div class="alert alert-danger"><i class="fa fa-exclamation-circle"></i> <?php echo $error_warning; ?>
    		<button type="button" class="close" data-dismiss="alert">&times;</button>
    	</div>
    <?php } ?>
    	<div class="panel panel-default">
    		<div class="panel-heading clearfix">
    			<h3 class="panel-title pull-left" style="padding-top: 9px;"><i class="fa fa-gift" aria-hidden="true"></i> <?php echo $heading_title; ?></h3>
    			<div class="pull-right">
	    			<button type="submit" form="form-membership-gift" data-toggle="tooltip" title="<?php echo $button_save; ?>" class="btn btn-primary"><i class="fa fa-save"></i></button>
					<a href="<?php echo $cancel; ?>" data-toggle="tooltip" title="<?php echo $button_cancel; ?>" class="btn btn-default"><i class="fa fa-reply"></i></a>
    			</div>
    		</div>
    		<div class="panel-body">
    			<form action="<?php echo $action; ?>" method="post" enctype="multipart/form-data" id="form-membership-gift" class="form-horizontal">
    				<div class="form-group">
	      				<label class="col-sm-2 control-label" for="status-enabled"><span data-toggle="tooltip" title="<?php echo $help_status; ?>"><?php echo $entry_status; ?></span></label>
	      				<div class="col-sm-4">
	      					<div class="radio-switch">
	      					<?php if ($membership_gift_status) { ?>
						        <input type="radio" name="status" value="0" id="status-disabled">
                                <label class="col-sm-4" for="status-enabled"><?php echo $text_disabled; ?></label>
	      						<input type="radio" name="status" value="1" id="status-enabled" checked>
                                <label class="col-sm-4" for="status-disabled"><?php echo $text_enabled; ?></label>
						    <?php } else { ?>
                                <input type="radio" name="status" value="0" id="status-disabled" checked>
                                <label class="col-sm-4" for="status-enabled"><?php echo $text_disabled; ?></label>
                                <input type="radio" name="status" value="1" id="status-enabled">
                                <label class="col-sm-4" for="status-disabled"><?php echo $text_enabled; ?></label>  
						    <?php } ?>
	      					</div>
	      				</div>
	      			</div>
    				<div class="form-group">
    					<label class="col-sm-2 control-label" for="input-sort-order"><span data-toggle="tooltip" title="<?php echo $help_sort_order; ?>"><?php echo $entry_sort_order; ?></span></label>
    					<div class="col-sm-10">
    						<input type="text" name="membership_gift_sort_order" value="<?php echo $membership_gift_sort_order; ?>" placeholder="<?php echo $entry_sort_order; ?>" id="input-sort-order" class="form-control" />
    					</div>
    				</div>
    			</form>
    		</div>
    	</div>
    </div>
</div>
<?php echo $footer; ?>