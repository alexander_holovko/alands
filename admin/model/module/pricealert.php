<?php 
class ModelModulePriceAlert extends Model {
	private $moduleName				= 'PriceAlert';
	private $moduleNameSmall		= 'pricealert';
	private $moduleData_module		= 'pricealert_module';
	private $moduleModel			= 'model_module_pricealert';	
	
	public function install(){
		$query = $this->db->query("CREATE TABLE IF NOT EXISTS `" . DB_PREFIX . "pricealert`
		(`pricealert_id` INT(11) NOT NULL AUTO_INCREMENT, 
		 `customer_email` VARCHAR(200) NULL DEFAULT NULL,
		 `customer_name` VARCHAR(100) NULL DEFAULT NULL,
		 `product_id` INT(11) NULL DEFAULT '0',
		 `date_created` DATETIME  NOT NULL DEFAULT '0000-00-00 00:00:00',
		 `customer_notified` TINYINT(1) NOT NULL DEFAULT '0',
		 `store_id` int(11) NOT NULL DEFAULT 0,
		  PRIMARY KEY (`pricealert_id`));");	
	  
	}
	
	public function uninstall()	{
		$this->db->query("DROP TABLE IF EXISTS `" . DB_PREFIX . "pricealert`");
		  
	}
	
	public function viewcustomers($store_id=0, $page=1, $limit=8, $sort="id", $order="DESC") {	
		if ($page) {
				$start = ($page - 1) * $limit;
			}
			$query =  $this->db->query("SELECT super.*, product.name as product_name FROM `" . DB_PREFIX . "pricealert` super 
			JOIN `" . DB_PREFIX . "product_description` product on super.product_id = product.product_id
			WHERE customer_notified=0 and language_id = " . (int)$this->config->get('config_language_id') . " AND store_id='".$store_id."'
			ORDER BY `date_created` DESC
			LIMIT ".$start.", ".$limit);
			
		return $query->rows; 
	}
	
	public function viewnotifiedcustomers($store_id=0, $page=1, $limit=8, $sort="id", $order="DESC") {	
		if ($page) {
				$start = ($page - 1) * $limit;
			}
			$query =  $this->db->query("SELECT super.*, product.name as product_name FROM `" . DB_PREFIX . "pricealert` super 
			JOIN `" . DB_PREFIX . "product_description` product on super.product_id = product.product_id
			WHERE customer_notified=1 and language_id = " . (int)$this->config->get('config_language_id') . " AND store_id='".$store_id."'
			ORDER BY `date_created` DESC
			LIMIT ".$start.", ".$limit);
			
		return $query->rows; 
	}
	
	public function getTotalCustomers($store_id=0){
			$query = $this->db->query("SELECT COUNT(*) as `count`  FROM `" . DB_PREFIX . "pricealert` WHERE customer_notified=0 AND store_id='".$store_id."'");
		return $query->row['count']; 
	}
	
	public function getTotalNotifiedCustomers($store_id=0){
			$query = $this->db->query("SELECT COUNT(*) as `count`  FROM `" . DB_PREFIX . "pricealert` WHERE customer_notified=1 AND store_id='".$store_id."'");
		return $query->row['count']; 
	}
	
	public function sendEmailWhenAvailable($product_id) {
		$this->load->model('setting/setting');
		
		$product_info = $this->model_catalog_product->getProduct($product_id);
		$this->load->model('tool/image');
		if ($product_info['image']) { $image = $this->model_tool_image->resize($product_info['image'], 200, 200); } else { $image = false; }
			
		$customers = $this->db->query("SELECT * FROM `" . DB_PREFIX . "pricealert` 
			WHERE product_id = ".$product_id." AND customer_notified=0
			ORDER BY `date_created` DESC");					
								
		foreach($customers->rows as $cust) {
			$moduleSettings			= $this->model_setting_setting->getSetting($this->moduleNameSmall, $cust['store_id']);
       		$PriceAlert				= (isset($moduleSettings[$this->moduleName])) ? $moduleSettings[$this->moduleName] : array();
			
			if(!isset($PriceAlert['EmailText'][$this->config->get('config_language_id')])){
				$EmailText = '';
				$EmailSubject = '';
			} else {
				$EmailText = $PriceAlert['EmailText'][$this->config->get('config_language_id')];
				$EmailSubject = $PriceAlert['EmailSubject'][$this->config->get('config_language_id')];
			}
			
			$string = html_entity_decode($EmailText);
			$patterns = array();
			$patterns[0] = '/{c_name}/';
			$patterns[1] = '/{p_name}/';
			$patterns[2] = '/{p_image}/';
			$patterns[3] = '/http:\/\/{p_link}/';
			$replacements = array();
			$replacements[0] = $cust['customer_name'];
			$replacements[1] = "<a href='".HTTP_CATALOG."index.php?route=product/product&product_id=".$product_id."' target='_blank'>".$product_info['name']."</a>";
			$replacements[2] = "<img src='".$image."' />";
			$replacements[3] = HTTP_CATALOG."index.php?route=product/product&product_id=".$product_id;

			$text = preg_replace($patterns, $replacements, $string);
			
			$mailToUser = new Mail($this->config->get('config_mail'));
			$mailToUser->setTo($cust['customer_email']);
			$mailToUser->setFrom($this->config->get('config_email'));
			$mailToUser->setSender($this->config->get('config_name'));
			$mailToUser->setSubject(html_entity_decode($EmailSubject, ENT_QUOTES, 'UTF-8'));
			$mailToUser->setHtml($text);
			$mailToUser->send();
		}
		
		$update_customers = $this->db->query("UPDATE `" . DB_PREFIX . "pricealert` SET customer_notified=1 WHERE product_id = ".$product_id."");	
	}
}
?>