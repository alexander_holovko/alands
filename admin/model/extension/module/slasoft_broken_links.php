<?php
class ModelExtensionModuleSlasoftBrokenLinks extends Model {

    public function __construct($registry) {
        parent::__construct($registry);
        $this->_moduleName = "Slasoft Broken Links";
        $this->_moduleSysName = "slasoft_broken_links";
    }

	public function clear() {
		$sql = "TRUNCATE TABLE `" . DB_PREFIX . "broken_notfound`";
		$this->db->query($sql);
		return true;
	}

	public function not_foundDelete() {
		if (isset($this->request->post['selected'])) {
			foreach ($this->request->post['selected'] as $notfound_id) {
				$sql = "DELETE FROM `" . DB_PREFIX . "broken_notfound` WHERE `notfound_id` = '" . (int)$notfound_id . "'";
				$this->db->query($sql);
			}
		}
		return true;
	}

	public function getTotalRecords($data=array()) {
		$sql = "SELECT COUNT(*) as total FROM `" . DB_PREFIX . "broken_notfound` WHERE 1";
		if (isset($data['filter']) && $data['filter']) {
			$data_filter = $data['filter'];
			if (isset($data_filter['request']) && $data_filter['request']) {
				$sql .= " AND request_uri LIKE '%" . $this->db->escape($data_filter['request']) . "%'";
			} 
			if (isset($data_filter['browser']) && $data_filter['browser']) {
				$sql .= " AND browser LIKE '%" . $this->db->escape($data_filter['browser']) . "%'";
			} 
			if (isset($data_filter['referer']) && $data_filter['referer']) {
				if ($data_filter['referer'] !== '!') {
					$sql .= " AND referer LIKE '%" . $this->db->escape($data_filter['referer']) . "%'";
				} else {
					$sql .= " AND referer != ''";
				}
			} 
		}
		
		$query = $this->db->query($sql);
		return $query->row['total'];
	}
	public function getRecords($data=array()) {
		$sql = "SELECT * FROM `" . DB_PREFIX . "broken_notfound` WHERE 1";
		
		if (isset($data['filter']) && $data['filter']) {
			$data_filter = $data['filter'];
			if (isset($data_filter['request']) && $data_filter['request']) {
				$sql .= " AND request_uri LIKE '%" . $this->db->escape($data_filter['request']) . "%'";
			} 
			if (isset($data_filter['browser']) && $data_filter['browser']) {
				$sql .= " AND browser LIKE '%" . $this->db->escape($data_filter['browser']) . "%'";
			} 
			if (isset($data_filter['referer']) && $data_filter['referer']) {
				if ($data_filter['referer'] !== '!') {
					$sql .= " AND referer LIKE '%" . $this->db->escape($data_filter['referer']) . "%'";
				} else {
					$sql .= " AND referer != ''";
				}
			} 
		}
		
		if (isset($data['sort']) && in_array($data['sort'], $sort_data)) {
			$sql .= " ORDER BY " . $data['sort'];	
		} else {
			$sql .= " ORDER BY date_record";	
		}
			
		if (isset($data['order']) && ($data['order'] == 'ASC')) {
			$sql .= " ASC";
		} else {
			$sql .= " DESC";
		}
	
		if (isset($data['start']) || isset($data['limit'])) {
			if ($data['start'] < 0) {
				$data['start'] = 0;
			}				

			if ($data['limit'] < 1) {
				$data['limit'] = 20;
			}	
			
			$sql .= " LIMIT " . (int)$data['start'] . "," . (int)$data['limit'];
		}	

		$query = $this->db->query($sql);
		return $query->rows;
	}

    public function checkTable() {
		$this->install();
	}

    public function install() {

        $sql = "CREATE TABLE IF NOT EXISTS `" . DB_PREFIX ."broken_notfound` (
			`notfound_id` int(11) NOT NULL AUTO_INCREMENT,
			`ip` varchar(255) NOT NULL,
			`browser` varchar(1000) NOT NULL,
			`request_uri` varchar(1000) NOT NULL,
			`referer` varchar(1000) NOT NULL,
			`date_record` datetime NOT NULL,
			PRIMARY KEY (`notfound_id`)
			) DEFAULT CHARSET=utf8 AUTO_INCREMENT=1;
        ";

        $this->db->query($sql);
        return true;
    }

    public function uninstall() {
        $sql = "DROP TABLE IF EXISTS `" . DB_PREFIX ."broken_notfound` ";
        $this->db->query($sql);
        return true;
    }

}
