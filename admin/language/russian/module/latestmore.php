<?php
// Heading
$_['heading_title']    = 'Последние товары с Ajax-подгрузкой';

// Text
$_['text_module']      = 'Модули';
$_['text_success']     = 'Настройки успешно изменены!';
$_['text_edit']        = 'Редактирование';

// Entry
$_['entry_name']       = 'Название модуля';
$_['entry_limit']      = 'Количество товаров';
$_['entry_width']      = 'Ширина фото';
$_['entry_height']     = 'Высота фото';
$_['entry_view']       = 'Представление';
$_['text_grid']        = 'Плитка';
$_['text_list']        = 'Список';
$_['entry_status']     = 'Статус';
$_['loadmore_button_name'] 			= 'Загрузить ещё';
$_['loadmore_button_name_title'] 	= 'Текст на кнопке';
$_['loadmore_style_title'] 			= 'CSS кнопки';

// Error
$_['error_permission'] = 'У вас недостаточно прав для внесения изменений!';
$_['error_name']       = 'Название должно содержать от 3 до 64 символов!';
$_['error_width']      = 'Укажите Ширину!';
$_['error_height']     = 'Укажите Высоту!';