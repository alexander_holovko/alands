<?php
// Heading
$_['heading_title']    			= '<em><strong>Opencart - Топ стикеры</strong></em> by <a target="_blank" href="https://opencartforum.com/profile/11962-wadamir/">Wadamir</a>';
$_['heading_header']    		= 'Opencart - Топ стикеры by Wadamir';


// Text
$_['text_module']      			= 'Модуль';
$_['text_success']     			= 'Вы успешно изменили модуль!';
$_['text_edit']        			= 'Настройка модуля';
$_['text_topleft']				= 'Сверху слева';
$_['text_topright']				= 'Сверху справа';


// Tab headers
$_['tab_topstickers']				= 'Кастомные стикеры';
$_['entry_topstickers']				= 'Подключить кастомные стикеры';

$_['text_tab_settings']				= 'Основные настройки';
$_['text_tab_settings_title']		= 'Основные настройки модуля';
$_['text_tab_sold'] 				= 'Стикер ПРОДАНО';
$_['text_tab_sold_title']			= 'Настройки стикера ПРОДАНО';
$_['text_tab_sale'] 				= 'Стикер РАСПРОДАЖА';
$_['text_tab_sale_title']			= 'Настройки стикера РАСПРОДАЖА';
$_['text_tab_bestseller']			= 'Стикер ХИТ ПРОДАЖ';
$_['text_tab_bestseller_title']		= 'Настройки стикера ХИТ ПРОДАЖ';
$_['text_tab_novelty'] 				= 'Стикер НОВИНКА';
$_['text_tab_novelty_title']		= 'Настройки стикера НОВИНКА';
$_['text_tab_last'] 				= 'Стикер ПОСЛЕДНИЙ';
$_['text_tab_last_title']			= 'Настройки стикера ПОСЛЕДНИЙ';
$_['text_tab_freeshipping']			= 'Стикер БЕСПЛАТНАЯ ДОСТАВКА';
$_['text_tab_freeshipping_title']	= 'Настройки стикера БЕСПЛАТНАЯ ДОСТАВКА';
$_['text_tab_custom']				= 'Кастомные стикеры';
$_['text_tab_custom_title']			= 'Настройки кастомных стикеров';

$_['button_remove']					= 'Удалить кастомный стикер';
$_['button_custom_topsticker_add']	= 'Добавить кастомный стикер';


// Entry
$_['entry_topstickers_status'] 		= 'Включить модуль';
$_['entry_topstickers_position'] 	= 'Позиция стикера';

$_['entry_sold_text'] 				= 'SOLD sticker text';
$_['entry_sold_status'] 	    	= 'SOLD sticker status';
$_['entry_sold_bg'] 				= 'SOLD sticker background';

$_['entry_sale_text'] 				= 'SALE sticker text';
$_['entry_sale_status'] 	    	= 'SALE sticker status';
$_['entry_sale_bg'] 				= 'SALE sticker background';

$_['entry_bestseller_text'] 		= 'BESTSELLER sticker text';
$_['entry_bestseller_status'] 		= 'BESTSELLER sticker status';
$_['entry_bestseller_bg'] 			= 'BESTSELLER sticker background';
$_['entry_bestseller_numbers']		= 'Максимальное количество товаров-бестселлеров';

$_['entry_novelty_text'] 			= 'NOVELTY sticker text';
$_['entry_novelty_status'] 	    	= 'NOVELTY sticker status';
$_['entry_novelty_bg'] 				= 'NOVELTY sticker background';
$_['entry_novelty_days'] 			= 'Сколько дней товар считается новинкой?';

$_['entry_last_text'] 				= 'LAST sticker text';
$_['entry_last_status'] 	    	= 'LAST sticker status';
$_['entry_last_bg'] 				= 'LAST sticker background';
$_['entry_last_numbers'] 			= 'Минимальное количество товара в наличии';

$_['entry_freeshipping_text'] 		= 'FREE SHIPPING sticker text';
$_['entry_freeshipping_status'] 	= 'FREE SHIPPING sticker status';
$_['entry_freeshipping_bg'] 		= 'FREE SHIPPING sticker background';
$_['entry_freeshipping_price'] 		= 'Минимальная цена для бесплатной доставки';

$_['entry_custom_topstickers_title'] 	= 'Название кастомного стикера';
$_['entry_custom_topstickers_text'] 	= 'Текст на кастомном стикере';
$_['entry_custom_topstickers_bg'] 		= 'Цвет кастомного стикера';
$_['entry_custom_topstickers_status'] 	= 'Статус';


// Default
$_['default_sold_text']			= 'ПРОДАНО';
$_['default_sold_bg'] 			= '#990033';

$_['default_sale_text'] 		= 'РАСПРОДАЖА';
$_['default_sale_bg'] 			= '#CC0000';

$_['default_bestseller_text'] 	= 'ХИТ ПРОДАЖ';
$_['default_bestseller_bg'] 	= '#3300CC';

$_['default_novelty_text'] 		= 'НОВИНКА';
$_['default_novelty_bg'] 		= '#00CCCC';

$_['default_last_text'] 		= 'ПОСЛЕДНИЙ';
$_['default_last_bg'] 			= '#CC6600';

$_['default_freeshipping_text'] = 'БЕСПЛАТНАЯ ДОСТАВКА';
$_['default_freeshipping_bg'] 	= '#00CC66';


// Error
$_['error_permission'] 			= 'У вас нет доступа для редактирования модуля!';