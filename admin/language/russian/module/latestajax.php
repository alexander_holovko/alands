<?php
// Heading
$_['heading_title']    = 'Новые поступления с Ajax by <a href="https://opencartforum.com/index.php?app=core&module=search&do=user_activity&search_app=downloads&mid=11962">Wadamir</a>';

// Text
$_['text_module']      = 'Модули';
$_['text_success']     = 'Настройки модуля "Новые поступления с Ajax" успешно изменены!';
$_['text_edit']        = 'Настройки модуля "Новые поступления с Ajax"';

// Entry
$_['entry_name'] 	   = 'Название модуля';
$_['entry_limit']      = 'Лимит';
$_['entry_full_limit'] = 'Количество рядов';
$_['entry_width']      = 'Ширина';
$_['entry_height']     = 'Высота';
$_['entry_status']     = 'Статус';

// Error
$_['error_permission'] = 'У Вас нет прав для управления данным модулем!';
$_['error_name'] = 'Название модуля должно содержать от 3 до 64 символов!';
$_['error_width'] = 'Введите ширину изображения!';
$_['error_height'] = 'Введите высоту изображения!';

