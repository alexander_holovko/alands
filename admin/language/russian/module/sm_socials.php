<?php
// Heading
$_['heading_title']       	= 'Мы в соцсетях';
$_['button_save_stay']      = 'Применить';
$_['text_edit']       	  	= 'Редактирование Мы в соцсетях';
$_['text_module']        	= 'Модули';
$_['entry_status']        	= 'Статус';
$_['entry_name']        	= 'Название модуля';

// Left side modules list
$_['entry_title']    		= 'Заголовок';
$_['entry_media']    		= 'Соцсеть';
$_['entry_link']    		= 'URL ссылка <small>вместе с http://</small>';
$_['entry_blank']    		= 'Открывать в новом окне?';
$_['entry_tooltip']    		= 'Подсказка';

// Notifications
$_['text_success']        	= 'Успешно: Вы изменили модуль Мы в соцсетях';
$_['error_permission']      = 'Внимание: У вас нет доступа к модулю Мы в соцсетях';