<?php
// Heading
$_['heading_title']           = 'Боковое Меню';
$_['module_title']            = 'Боковое Меню';

// Text
$_['text_module']             = 'Модули';
$_['text_success']            = 'Модуль "Боковое Меню" успешно сохранен!';
$_['text_apply']              = 'Модуль успешно обновлен!';
$_['text_edit']               = 'Редактирование модуля';

$_['text_author']             = 'Автор';
$_['text_author_link']        = '#';
$_['text_support']            = 'Поддержка';
$_['text_more']               = 'Больше дополнений';

$_['text_expanded']           = 'Развернутое';
$_['text_minimized']          = 'Свернутое';

$_['text_tree']               = 'Полное дерево категорий';
$_['text_brands']             = 'Производители';
$_['text_current']            = 'Подкатегории текущей категории';
$_['text_parent']             = 'Подкатегории главной категории';

$_['text_am']                 = 'Меню "Accordion"';
$_['text_fm']                 = 'Меню "Flyout"';
$_['text_pm']                 = 'Меню "Drill Down"';

$_['text_all_categories']     = 'Все категории';
$_['text_current_categories'] = 'Выборочные категории';
$_['text_all_brands']         = 'Все производители';
$_['text_all_customers']      = 'Все группы клиентов и гости';
$_['text_current_brands']     = 'Выборочные производители';
$_['text_count']              = 'Кол-во товаров категории';
$_['text_one_column']         = 'Стандартное';
$_['text_multi_column']       = 'Расширенное';
$_['text_save_view']          = 'Сохранять выбор пользователя (кэш браузера)';
$_['text_cat_autocomplete']   = 'Выбор категории...';

$_['text_level_1']            = '1-ый уровень';
$_['text_level_2']            = '2-ой уровень';
$_['text_level_3']            = '3-ий уровень';
$_['text_level_4']            = '4-ый уровень';
$_['text_level_5']            = '5-ый уровень';

$_['text_toggle']             = 'Кнопка навигации';
$_['text_icons_status']       = 'Иконки элементов меню';

$_['text_center']             = 'По центру';
$_['text_left']               = 'Слева';

$_['text_default_asc']        = 'Числовой порядок';
$_['text_viewed_asc']         = 'Популярные';
$_['text_rating_desc']        = 'Рейтинг';
$_['text_date_desc']          = 'Последние';
$_['text_name_asc']           = 'Наименование (А - Я)';
$_['text_name_desc']          = 'Наименование (Я - А)';
$_['text_price_asc']          = 'Цена (по возрастанию)';
$_['text_price_desc']         = 'Цена (по убыванию)';

// Entry
$_['entry_categories']        = 'Категории:';
$_['entry_brands']            = 'Производители:';
$_['entry_title']             = 'Заголовок меню:';
$_['entry_minimized']         = 'Вид меню:';
$_['entry_menu_type']         = 'Тип меню:';
$_['entry_menu_items']        = 'Элементы меню:';
$_['entry_location']          = 'Показать в категориях:';
$_['entry_prod_by_cat']       = 'Товары категории:';
$_['entry_prod_by_brand']     = 'Товары бренда:';
$_['entry_levels']            = 'Уровни вложенности:';
$_['entry_sub_limit']         = 'Лимит подкатегорий:';
$_['entry_products_limit']    = 'Лимит товаров:';
$_['entry_menu_design']       = 'Стиль меню:';
$_['entry_flyout_column']     = 'Всплывающее окно:';
$_['entry_item_column']       = 'Кол-во столбцов:';
$_['entry_banner']            = 'Баннер:';
$_['entry_item_info']         = 'Описание:';
$_['entry_info_limit']        = 'Лимит описания (символы):';
$_['entry_image_status']      = 'Изображения:';
$_['entry_size']              = 'Размер (Ш x В), пикс:';
$_['entry_image_position']    = 'Позиция изображения:';
$_['entry_effect']            = 'Эффект (анимация | длительность):';

$_['entry_name']              = 'Название модуля:';
$_['entry_status']            = 'Статус:';
$_['entry_store']             = 'Магазины:';
$_['entry_customers']         = 'Группы клиентов:';
$_['entry_sort_order']        = 'Сортировка:';
$_['entry_class']             = 'Класс блока меню:';

// Tab
$_['tab_menu_setting']        = 'Параметры меню';
$_['tab_module_setting']      = 'Настройки модуля';

// Button
$_['button_apply']            = 'Применить';

// Error
$_['error_permission']        = 'У Вас нет прав для изменения модуля "Боковое Меню"!';
$_['error_name']              = 'Введите название модуля (минимум 3 символа)!';
?>