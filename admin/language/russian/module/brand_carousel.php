<?php
// Heading
$_['heading_title']			= 'Карусель производителей';
// Text
$_['text_edit']				= 'Редактирование карусели производителей';
$_['text_yes']				= 'Да';
$_['text_no']				= 'Нет';
$_['text_enabled']			= 'Включено';
$_['text_disabled']			= 'Отключено';
$_['text_module']			= 'Модули';
$_['text_success']			= 'Модуль Карусель производителей успешно обновлен!';
// Button
$_['button_save']			= 'Сохранить';
$_['button_cancel']			= 'Отменить';
$_['tab_support']			= 'О модуле';
// Entry
$_['entry_name']			= 'Название в админ панели:';
$_['entry_name_block']			= 'Заголовок блока';
$_['entry_width']			= 'Ширина';
$_['entry_height']			= 'Высота';
$_['entry_status']			= 'Статус:';
$_['entry_scroll_limit']	= 'Видимых элементов карусели:';
$_['entry_scroll_auto']		= 'Авто старт:';
$_['entry_scroll_pause']	= 'Пауза при наведении:';
$_['entry_scroll']			= 'Элементы для прокрутки:';
$_['entry_image']			= 'Размеры изображения (Ширина x Высота):';
$_['entry_infinite']        = 'В цикле:';
$_['entry_animation_speed']	= 'Скорость анимации:';
$_['entry_interval']		= 'Задержка при прокрутке:';
//Help
$_['help_name_block']		= 'Если оставить данное поле пустое, то заголовк блока не будет выводиться!';
// Error
$_['error_permission']		= 'Warning: You do not have permission to modify rand carousel!';
$_['error_width']			= 'Укажите ширину!';
$_['error_height']			= 'Укажите высоту!';
$_['error_name']			= 'Название в админ панели должно содержать от 3 до 64 символов!';
$_['error_animation_speed'] = 'Укажите скорость анимации!';
$_['error_interval']				= 'Укажите задержку при прокрутке!';
$_['error_scroll_limit']	= 'Укажите количество видимых элементов карусели!';
$_['error_scroll']			= 'Укажите количество элементов для прокрутки!';
$_['support_text']			= '<br>Версия модуля: <b>0.9</b><br><br> Поддержка модуля на сайте <a href="http://oc.byhelp.info/" target="_blank">ByHelp</a> или в теме поддержки на сайте <a href="https://opencartforum.com/topic/63548-karusel-proizvoditeley/" target="_blank">opencartforum</a><br><br>';

?>
