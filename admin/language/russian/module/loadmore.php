<?php
// Heading
$_['heading_title']     = 'Ajax Product 111';

// Text
$_['text_module']       = 'Модули';
$_['text_success']      = 'Отлично! Вы успешно отредактировали модуль "Ajax Product Load More"!';
$_['text_edit']         = 'Редактировать модуль "Ajax Product Load More"';

// Entry
$_['loadmore_button_name'] 			= 'Загрузить ещё';
$_['loadmore_button_name_title'] 	= 'Текст на кнопке';
$_['loadmore_style_title'] 			= 'CSS кнопки';
$_['loadmore_default_style_title'] 	= 'CSS умолчанию';
$_['loadmore_arrow_status_title'] 	= 'Показывать стрелку "Наверх"';
$_['loadmore_status_title'] 		= 'Статус';

// Error
$_['error_permission']  = 'Warning: You do not have permission to modify "Ajax Product Load More" module!';