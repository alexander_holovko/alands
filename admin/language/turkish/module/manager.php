<?php

$_['heading_title']    			= "Order Manager";
$_['heading_version']   		= "2.2.1";

$_['entry_buttons']   			= "Düğmeler";
$_['help_buttons']   			= "Gösterilmesini istediğiniz düğmeleri seçin.";

$_['button_history']       		= "Tarih Ekle";
$_['button_invoice']       		= "Fatura Bas";
$_['button_shipping']       	= "Teslimat Adresi Bas";
$_['button_delete']       		= "Sipariş Sil";
$_['button_create']       		= "Yeni Sipariş";
$_['button_minimize']       	= "Listeyi Aç / Kapat";
$_['button_toggle']       		= "Tümünü Aç / Kapat";
$_['button_filter']       		= "Sipariş Filtrele";
$_['button_clear']       		= "Filtre Temizle";
$_['button_edit_customer']      = "Müşteri Düzenle";
$_['button_view_order']       	= "Sipariş Görüntüle";
$_['button_edit_order']       	= "Sipariş Düzenle";

$_['entry_columns']   			= "Sütünlar";
$_['help_columns']   			= "Listede Görünecek Sütünlar.";

$_['column_select']       		= "Select";
$_['column_order_id']       	= "ID";
$_['column_order_status_id']	= "Durum";
$_['column_customer'] 			= "Müşteri";
$_['column_recipient'] 			= "Alıcı";
$_['column_company']  			= "Firma";
$_['column_products']       	= "Ürünler";
$_['column_date_added']    		= "Ekleme Tarihi";
$_['column_date_modified'] 		= "Güncelleme Tarihi";
$_['column_payment'] 			= "Ödeme";
$_['column_shipping']			= "Kargo";
$_['column_subtotal']    		= "KDV Hariç";
$_['column_total']    			= "Toplam";
$_['column_actions']        	= "Eylemler";

$_['entry_statuses']   			= "Sipariş Durumu";
$_['help_statuses']   			= "Orders with selected statuses and set their colors.";

$_['entry_payments']   			= "Ödeme Metodları";
$_['help_payments']   			= "Set colors for payment methods.";

$_['entry_shippings']   		= "Kargo Metotları";
$_['help_shippings']   			= "Set colors for shipping methods.";

$_['entry_mode']				= "Display Mode";
$_['help_mode']					= "Full - all orders, Custom - orders with selected statuses only.";

$_['text_mode_full']        	= "Full";
$_['text_mode_custom']    		= "Custom";

$_['entry_notice']				= "Kısa Bildirim";
$_['help_notice']				= "Will be displayed in module header on dashboard (HTML allowed).";

$_['entry_hide_dashboard']		= "Gösterge Tablosunu Gizle";
$_['help_hide_dashboard']		= "Gösterge tablosunda yalnızca Sipariş Yöneticisi modülü görüntülenir.";

$_['entry_filters']				= "Filtreleri Göster";
$_['help_filters']				= "Sipariş listesindeki sütun filtrelerini görüntüleme.";

$_['entry_notify']				= "Müşteriye Bildir";
$_['help_notify']				= "Send notification to customer, when order status is changed.";

$_['entry_default_limit']		= "Varsayılan Sınır";
$_['help_default_limit']		= "Default number of orders per page.";

$_['entry_default_links']		= "Varsayılan Bağlantılar";
$_['help_default_links']		= "Default number of links (pages) in pagination.";

$_['entry_default_sort']		= "Varsayılan Sıralama";
$_['help_default_sort']			= "Default sorting column.";

$_['text_sort_order_id']        = "Order ID";
$_['text_sort_customer']    	= "Müşteri";
$_['text_sort_company']     	= "Firma";
$_['text_sort_order_status_id'] = "Durum";
$_['text_sort_date_added']  	= "Ekleme Tarihi";
$_['text_sort_date_modified']	= "Güncelleme Tarihi";
$_['text_sort_payment']     	= "Ödeme";
$_['text_sort_shipping']    	= "Kargo";
$_['text_sort_subtotal']       	= "KDV Hariç";
$_['text_sort_total']       	= "Toptam";

$_['entry_default_order']		= "Varsayılan Sipariş";
$_['help_default_order']		= "Varsayılan sipariş yönü.";

$_['text_order_desc']			= "Azalan";
$_['text_order_asc']        	= "Artan";

$_['entry_name_format']			= "İsim Biçimi";
$_['help_name_format']			= "İsim formatını seçin.";

$_['text_firstname']    		= "Adı, Soyadı";
$_['text_lastname']       		= "Soyadı, Adı";

$_['entry_address_format']		= "Adres Formatı";
$_['help_address_format']		= "Available placeholders: {name}, {company}, {telephone}, {email}, {address}, {country}, {city}, {zone}, {postcode}.";

$_['entry_address_default']		= "<b>{name}</b>\nStore: {store}\n{company}\n<a href='skype:+{telephone}?call'>{telephone}</a>\n<a href='mailto:{email}'>{email}</a>\n{address}\n{country}, {city}, {zone}\n{postcode}";

$_['entry_date_format']			= "Date Format";
$_['help_date_format']			= "Define order date format, following PHP date() function rules (e.g.: d.m.Y).";

$_['entry_addips']				= "Add IPs";
$_['help_addips']				= "Automatically add new IP addresses to API (OC 2.1 and higher).";

$_['text_color']       			= "Renk";
$_['text_missing']       		= "Missing";

/* Orders list */
			
$_['text_title']				= "Siparişler Modülü";

$_['button_comment']			= "Yorum Ekle";
$_['button_change']				= "Durum Değiştir";
			
$_['text_sort']					= "Sort by this column";
$_['text_products']				= "Ürün Sayısı: %s";
$_['text_toggle_address']		= "Adres Değiştir";
$_['text_toggle_products']		= "Ürünleri Aç / Kapat";
$_['text_any']					= "Any";
$_['text_yes']					= "Evet";
$_['text_no']					= "Hayır";
$_['text_unspecified']			= "Belirtilmemiş.";
$_['text_total_orders']			= "Toplam Siparişler: %s";
$_['text_delete_confirm']		= "Do you really wish to delete selected orders? There's no undone!";
$_['text_comment']				= "Comment text";
$_['text_tracking']				= "Takip Numarası";
$_['text_empty_list']			= "No orders added or match given criteria.";
$_['text_add_info']				= "Add information";

$_['button_close']				= "Kapalı";

$_['message_delete_success']	= "Selected orders were successfully deleted.";
$_['message_history_success']	= "History was successfully added to selected orders.";

$_['error_selected']			= "Error: No orders were selected, operation aborted.";
$_['error_api_id']				= "Error: API not found. Check your API settings.";
$_['error_api_login']			= "Error: Failed to login to API. Check your API settings.";
	
/* Generic language strings */

$_['heading_latest']   		= "You have the latest version: %s";
$_['heading_future']   		= "Wow! You have version %s and it's from THE FUTURE!";
$_['heading_update']   		= "A new version available: %s. Click <a href='http://thekrotek.com/profile/my-orders' title='Download new version' target='_blank'>here</a> to download.";

$_['entry_customer_groups'] = "Customer groups";
$_['help_customer_groups']  = "Extension will work for selected groups only.";

$_['entry_geo_zone']   		= "Geo Zone";
$_['help_geo_zone']   		= "Extension will work for selected geo zone only.";

$_['entry_tax_class']  		= "Tax Class";
$_['help_tax_class']   		= "Tax class, which will be applied for this extension";

$_['entry_status']     		= "Status";
$_['help_status']   		= "Enable or disable this extension";

$_['entry_sort_order'] 		= "Sort Order";
$_['help_sort_order']   	= "Position in the list of extensions of the same type.";

$_['text_edit_title']       = "Edit %s";

$_['text_total']    		= "Total";
$_['text_module']    		= "Modules";
$_['text_shipping']    		= "Shipping";
$_['text_payment']    		= "Payment";

$_['button_apply']      	= "Apply";

$_['text_content_top']    	= "Content Top";
$_['text_content_bottom'] 	= "Content Bottom";
$_['text_column_left']    	= "Column Left";
$_['text_column_right']   	= "Column Right";

$_['entry_module_layout']   = "Layout:";
$_['entry_module_position'] = "Position:";
$_['entry_module_status']   = "Status:";
$_['entry_module_sort']    	= "Sort Order:";

$_['message_success']     	= "Success: You have modified %s!";

$_['error_permission'] 		= "Warning: You do not have permission to modify %s!";
$_['error_version'] 		= "Impossible to get version information: no connection to server.";
$_['error_fopen'] 			= "Impossible to get version information: allow_url_fopen option is disabled.";
$_['error_numerical'] 		= "Error: %s value should be numerical.";
$_['error_percent'] 		= "Error: %s value should be numerical or in percent.";
$_['error_positive'] 		= "Error: %s value should be zero or more.";
$_['error_curl']      		= "cURL error: (%s) %s. Fix it (if necessary) and try to reinstall.";

?>