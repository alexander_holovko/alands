<?php
// Heading
$_['heading_title']           = 'Бокове Меню';
$_['module_title']            = 'Бокове Меню';

// Text
$_['text_module']             = 'Модулі';
$_['text_success']            = 'Налаштування модуля "Бокове Меню" успішно змінено!';
$_['text_apply']              = 'Модуль успішно оновлено!';
$_['text_edit']               = 'Редагування модуля';

$_['text_author']             = 'Автор';
$_['text_author_link']        = '';
$_['text_support']            = 'Підтримка';
$_['text_more']               = 'Більше доповнень';

$_['text_expanded']           = 'Розгорнуте';
$_['text_minimized']          = 'Згорнуте';

$_['text_tree']               = 'Повне дерево категорій';
$_['text_brands']             = 'Виробники';
$_['text_current']            = 'Підкатегорії поточної категорії';
$_['text_parent']             = 'Підкатегорії головної категорії';

$_['text_am']                 = 'Меню "Accordion"';
$_['text_fm']                 = 'Меню "Flyout"';
$_['text_pm']                 = 'Меню "Drill Down"';

$_['text_all_categories']     = 'Всі категорії';
$_['text_current_categories'] = 'Вибіркові категорії';
$_['text_all_brands']         = 'Всі виробники';
$_['text_all_customers']      = 'Всі групи клієнтів та гості';
$_['text_current_brands']     = 'Вибіркові виробники';
$_['text_count']              = 'Кількість товарів категорії';
$_['text_one_column']         = 'Стандартне';
$_['text_multi_column']       = 'Розширене';
$_['text_save_view']          = 'Зберігати вибір користувача (кеш браузера)';
$_['text_cat_autocomplete']   = 'Вибір категорії...';

$_['text_level_1']            = '1 рівень';
$_['text_level_2']            = '2 рівень';
$_['text_level_3']            = '3 рівень';
$_['text_level_4']            = '4 рівень';
$_['text_level_5']            = '5 рівень';

$_['text_toggle']             = 'Кнопка навігації';
$_['text_icons_status']       = 'Іконки елементів меню';

$_['text_center']             = 'По центру';
$_['text_left']               = 'Зліва';

$_['text_default_asc']        = 'Числовий порядок';
$_['text_viewed_asc']         = 'Популярні';
$_['text_rating_desc']        = 'Рейтинг';
$_['text_date_desc']          = 'Останні';
$_['text_name_asc']           = 'Найменування (А - Я)';
$_['text_name_desc']          = 'Найменування (Я - А)';
$_['text_price_asc']          = 'Ціна (зростаюча)';
$_['text_price_desc']         = 'Ціна (спадаюча)';

// Entry
$_['entry_categories']        = 'Категорії:';
$_['entry_brands']            = 'Виробники:';
$_['entry_title']             = 'Заголовок меню:';
$_['entry_minimized']         = 'Вид меню:';
$_['entry_menu_type']         = 'Тип меню:';
$_['entry_menu_items']        = 'Елементи меню:';
$_['entry_location']          = 'Показувати меню тільки в цих категоріях:';
$_['entry_prod_by_cat']       = 'Товари категорії:';
$_['entry_prod_by_brand']     = 'Товари виробника:';
$_['entry_levels']            = 'Рівні вкладеності:';
$_['entry_sub_limit']         = 'Ліміт підкатегорій:';
$_['entry_products_limit']    = 'Ліміт товарів:';
$_['entry_menu_design']       = 'Стиль меню:';
$_['entry_flyout_column']     = 'Вид спливаючого вікна:';
$_['entry_item_column']       = 'Кількість стовпчиків:';
$_['entry_banner']            = 'Банер:';
$_['entry_item_info']         = 'Опис:';
$_['entry_info_limit']        = 'Ліміт опису (символи):';
$_['entry_image_status']      = 'Зображення елементу:';
$_['entry_size']              = 'Розмір (Ш x В), пікс:';
$_['entry_image_position']    = 'Позиція зображення:';
$_['entry_effect']            = 'Ефект (анімація | тривалість):';

$_['entry_name']              = 'Назва модуля:';
$_['entry_status']            = 'Статус:';
$_['entry_store']             = 'Магазини:';
$_['entry_customers']         = 'Групи клієнтів:';
$_['entry_sort_order']        = 'Сортування:';
$_['entry_class']             = 'Клас блоку меню:';

// Tab
$_['tab_menu_setting']        = 'Параметри меню';
$_['tab_module_setting']      = 'Налаштування модуля';

// Button
$_['button_apply']            = 'Застосувати';

// Error
$_['error_permission']        = 'Ви не маєте повноважень для налаштування модуля "Бокове Меню"!';
$_['error_name']              = 'Введіть назву модуля (мінімум 3 символи)!';
?>