<?php
// CKEditor 2+ (4.5.7)
// author: DataIc - www.dataic.eu

// Heading
$_['heading_title']  = 'CKEditor 2+ (4.5.7)';

// Text
$_['text_module']    = 'Modules';
$_['text_success']   = 'Success: You have modified module CKEditor 2+ (4.5.7)!';

$_['tab_general']    = 'General';
$_['tab_info'] 		 = 'Info';

$_['text_enabled']  = 'Enabled';
$_['text_disabled'] = 'Disabled';

$_['text_edit']     = 'Edit CKEditor 2+ (4.5.7) Module';

// settings
$_['entry_language']      	   = 'Language: ';
$_['entry_language_info']      = 'Choose language of ckEditor';

$_['entry_skin']  			 = 'Theme:';
$_['entry_skin_info']        = 'Choose skin/theme of ckEditor';

$_['entry_height']  			 = 'Height:';
$_['entry_height_info']          = 'Set the default height of the editor.';

$_['entry_enhanced']  			 = 'Enhanced Mode:';
$_['entry_enhanced_info']        = 'Enable Enhanced Mode to enable extra plugins, such as youtube, google maps, bootstrap layouts, slideshow, font-awesome and more...';



$_['entry_status']        	 = 'Status:';

$_['error_permission'] = 'Warning: You do not have permission to modify CKEditor 2+ (4.5.7) module!';

?>