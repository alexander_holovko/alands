<?php
// Heading
$_['heading_title']    			= '<em><strong>Opencart - Top stickers</strong></em> by <a target="_blank" href="https://opencartforum.com/profile/11962-wadamir/">Wadamir</a> (<a target="_blank" href="http://xdomus.ru/opencart/stikery-dlya-opencart-2-ustanovka-i-nastrojka/">read FAQ</a>)';
$_['heading_header']    		= 'Opencart - Top stickers by Wadamir';


// Text
$_['text_module']      			= 'Module';
$_['text_success']     			= 'Success: You have modified module!';
$_['text_edit']        			= 'Edit Module';
$_['text_topleft']				= 'Top left';
$_['text_topright']				= 'Top right';


// Tab headers
$_['tab_topstickers']			= 'CUSTOM stickers';
$_['entry_topstickers']			= 'Show custom stickers';

$_['text_tab_settings']			= 'Main settings';
$_['text_tab_settings_title']	= 'Module main settings';
$_['text_tab_sold'] 			= 'SOLD sticker';
$_['text_tab_sold_title']		= 'SOLD sticker settings';
$_['text_tab_sale'] 			= 'SALE sticker';
$_['text_tab_sale_title']		= 'SALE sticker settings';
$_['text_tab_bestseller']		= 'BESTSELLER sticker';
$_['text_tab_bestseller_title']	= 'BESTSELLER sticker settings';
$_['text_tab_novelty'] 			= 'NOVELTY sticker';
$_['text_tab_novelty_title']	= 'NOVELTY sticker settings';
$_['text_tab_last'] 			= 'LAST sticker';
$_['text_tab_last_title']		= 'LAST sticker settings';
$_['text_tab_freeshipping']			= 'FREE SHIPPING sticker';
$_['text_tab_freeshipping_title']	= 'FREE SHIPPING sticker settings';
$_['text_tab_custom']			= 'CUSTOM stickers';
$_['text_tab_custom_title']		= 'CUSTOM stickers settings';

$_['button_remove']						= 'Remove custom sticker';
$_['button_custom_topsticker_add']		= 'Add custom sticker';


// Entry
$_['entry_topstickers_status'] 		= 'Module status';
$_['entry_topstickers_position'] 	= 'Sticker position';

$_['entry_sold_text'] 			= 'SOLD sticker text';
$_['entry_sold_status'] 	    = 'SOLD sticker status';
$_['entry_sold_bg'] 			= 'SOLD sticker background';

$_['entry_sale_text'] 			= 'SALE sticker text';
$_['entry_sale_status'] 	    = 'SALE sticker status';
$_['entry_sale_bg'] 			= 'SALE sticker background';

$_['entry_bestseller_text'] 	= 'BESTSELLER sticker text';
$_['entry_bestseller_status'] 	= 'BESTSELLER sticker status';
$_['entry_bestseller_bg'] 		= 'BESTSELLER sticker background';
$_['entry_bestseller_numbers']	= 'Maximum number of bestsellers';

$_['entry_novelty_text'] 		= 'NOVELTY sticker text';
$_['entry_novelty_status'] 	    = 'NOVELTY sticker status';
$_['entry_novelty_bg'] 			= 'NOVELTY sticker background';
$_['entry_novelty_days'] 		= 'How many days is the product considered a novelty?';

$_['entry_last_text'] 			= 'LAST sticker text';
$_['entry_last_status'] 	    = 'LAST sticker status';
$_['entry_last_bg'] 			= 'LAST sticker background';
$_['entry_last_numbers'] 		= 'Minimum quantity of product in stock';

$_['entry_freeshipping_text'] 	= 'FREE SHIPPING sticker text';
$_['entry_freeshipping_status'] = 'FREE SHIPPING sticker status';
$_['entry_freeshipping_bg'] 	= 'FREE SHIPPING sticker background';
$_['entry_freeshipping_price'] 	= 'Free shipping price';

$_['entry_custom_topstickers_title'] 	= 'CUSTOM sticker title';
$_['entry_custom_topstickers_text'] 	= 'CUSTOM sticker text';
$_['entry_custom_topstickers_bg'] 		= 'CUSTOM sticker background';
$_['entry_custom_topstickers_status'] 	= 'CUSTOM sticker status';


// Default
$_['default_sold_text']			= 'SOLD OUT';
$_['default_sold_bg'] 			= '#990033';

$_['default_sale_text'] 		= 'SALE';
$_['default_sale_bg'] 			= '#CC0000';

$_['default_bestseller_text'] 	= 'BESTSELLER';
$_['default_bestseller_bg'] 	= '#3300CC';

$_['default_novelty_text'] 		= 'NOVELTY';
$_['default_novelty_bg'] 		= '#00CCCC';

$_['default_last_text'] 		= 'LAST';
$_['default_last_bg'] 			= '#CC6600';

$_['default_freeshipping_text'] = 'FREE SHIPPING';
$_['default_freeshipping_bg'] 	= '#00CC66';


// Error
$_['error_permission'] 			= 'Warning: You do not have permission to modify special_countdowns module!';