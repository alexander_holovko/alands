<?php
// Heading
$_['heading_title']      	= 'Brands Carousel';
// Text
$_['text_edit']      		= 'Edit Banner Module';
$_['text_yes']      		= 'Yes';
$_['text_no']      			= 'No';
$_['text_enabled']      	= 'Enabled';
$_['text_disabled']      	= 'Disabled';
$_['text_module']      		= 'Modules';
$_['text_success']      	= 'Success: You have modified module brand carousel!';
// Button
$_['button_save']      		= 'Save';
$_['button_cancel']      	= 'Cancel';
// Entry
$_['entry_name']       		= 'Title admin panel:';
$_['entry_name_block'] 		= 'Title block';
$_['entry_width']     		= 'Width';
$_['entry_height']  		= 'Height';
$_['entry_status']     		= 'Status';
$_['entry_scroll_limit'] 	= 'Visible carousel items:';
$_['entry_scroll_auto']  	= 'Auto Start:';
$_['entry_scroll_pause'] 	= 'Pause on hover:';
$_['entry_scroll']       	= 'Scroll:';
$_['entry_image']       	= 'Image (W x H):';
$_['entry_infinite']      	= 'In the cycle:';
$_['entry_animation_speed']	= 'Animation speed:';
$_['entry_interval']       	= 'Interval scrolling:';

// Error
$_['error_permission'] 		= 'Warning: You do not have permission to modify rand carousel!';
$_['error_width']      		= 'Width required!';
$_['error_height']    		= 'Height required!';
$_['error_name']     		= 'Module Name must be between 3 and 64 characters!';
$_['error_animation_speed']	= 'Animation speed required!';
$_['error_interval']     	= 'Interval required!';
$_['error_scroll_limit']	= 'Scroll limit required!';
$_['error_scroll']   		= 'Scroll required!';
$_['support_text']			= '<br>ВModule version: <b>0.9</b><br><br> Module support on <a href="http://oc.byhelp.info/" target="_blank">ByHelp</a> or in the support topic on the <a href="https://opencartforum.com/topic/63548-karusel-proizvoditeley/" target="_blank">opencartforum</a><br><br>';
?>
