<?php
// Heading
$_['heading_title']           = 'Sidebar Menu';
$_['module_title']            = 'Sidebar Menu';

// Text
$_['text_module']             = 'Modules';
$_['text_success']            = 'Success: You have modified module "Sidebar Menu"!';
$_['text_apply']              = 'Update Successful!';
$_['text_edit']               = 'Edit Module';

$_['text_author']             = 'Author';
$_['text_author_link']        = '';
$_['text_support']            = 'Support';
$_['text_more']               = 'More Extensions';

$_['text_expanded']           = 'Expanded';
$_['text_minimized']          = 'Minimized';

$_['text_tree']               = 'Categories';
$_['text_brands']             = 'Brands';
$_['text_current']            = 'Subcategories of current category';
$_['text_parent']             = 'Subcategories of parent category';

$_['text_am']                 = 'Accordion Menu';
$_['text_fm']                 = 'Flyout Menu';
$_['text_pm']                 = 'Drill Down Menu';

$_['text_all_categories']     = 'All Categories';
$_['text_current_categories'] = 'Featured Categories';
$_['text_all_brands']         = 'All Brands';
$_['text_all_customers']      = 'All Customer Group and Guests';
$_['text_current_brands']     = 'Featured Brands';
$_['text_count']              = 'Product Count';
$_['text_one_column']         = 'Default';
$_['text_multi_column']       = 'Extended';
$_['text_save_view']          = 'Save the users choice';
$_['text_cat_autocomplete']   = 'categories...';

$_['text_level_1']            = 'Level 1';
$_['text_level_2']            = 'Level 2';
$_['text_level_3']            = 'Level 3';
$_['text_level_4']            = 'Level 4';
$_['text_level_5']            = 'Level 5';

$_['text_toggle']             = 'Toggle Button';
$_['text_icons_status']       = 'Item Icons';

$_['text_center']             = 'Center';
$_['text_left']               = 'Left';

$_['text_default_asc']        = 'Numerical Order';
$_['text_viewed_asc']         = 'Popular';
$_['text_rating_desc']        = 'Rating';
$_['text_date_desc']          = 'Latest';
$_['text_name_asc']           = 'Name (A - Z)';
$_['text_name_desc']          = 'Name (Z - A)';
$_['text_price_asc']          = 'Price (Low - High)';
$_['text_price_desc']         = 'Price (High - Low)';

// Entry
$_['entry_categories']        = 'Categories:';
$_['entry_brands']            = 'Brands:';
$_['entry_title']             = 'Menu Title:';
$_['entry_menu_items']        = 'Menu Items:';
$_['entry_minimized']         = 'View:';
$_['entry_menu_design']       = 'Style:';
$_['entry_location']          = 'Show only in these Categories:';
$_['entry_prod_by_cat']       = 'Products by Category:';
$_['entry_prod_by_brand']     = 'Products by Brand:';
$_['entry_levels']            = 'Category Levels:';
$_['entry_sub_limit']         = 'Limit (subcategories):';
$_['entry_products_limit']    = 'Products Limit:';
$_['entry_flyout_column']     = 'Flyout Window:';
$_['entry_item_column']       = 'Columns:';
$_['entry_banner']            = 'Banner:';
$_['entry_item_info']         = 'Item Description:';
$_['entry_info_limit']        = 'Description Limit, (symbol):';
$_['entry_image_status']      = 'Images:';
$_['entry_size']              = 'Image Size (W x H), px:';
$_['entry_image_position']    = 'Image Position:';
$_['entry_effect']            = 'Effect (easing | duration):';

$_['entry_name']              = 'Module Name:';
$_['entry_status']            = 'Status:';
$_['entry_store']             = 'Stores:';
$_['entry_customers']         = 'Customer Group:';
$_['entry_sort_order']        = 'Sort Order:';

$_['entry_class']             = 'Container Class:';

// Tab
$_['tab_menu_setting']        = 'Menu Configuration';
$_['tab_module_setting']      = 'Module Settings';

// Button
$_['button_apply']            = 'Apply';

// Error
$_['error_permission']        = 'Warning: You do not have permission to modify module Sidebar Menu!';
$_['error_name']              = 'Module Name must be between 3 and 64 characters!';
?>