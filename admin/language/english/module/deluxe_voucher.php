<?php
$_['heading_title'] = 'Deluxe Gift Voucher';
$_['module'] = 'Modules';
$_['success'] = 'Your changes have been saved!';
$_['error_permission'] = 'Warning: You do not have permission to modify!';

$_['text_edit'] = 'Edit ' . $_['heading_title'];

$_['price_list'] = 'Price List';
$_['price_list_help'] = 'Separate prices using comma. Leave blank to allow customers to enter their own amount';
$_['default_price'] = 'Default Price';
$_['default_price_help'] = 'Price to display on page load';
$_['cron'] = 'CRON Command';
$_['cron_help'] = 'i.e. sending voucher once a day at 12AM';
$_['max_day'] = 'Date Picker Max Days';
$_['max_day_help'] = 'Number of days from today that date can be selected';
$_['date_format_help'] = 'yy=year, mm=month, dd=date';
$_['disable_module'] = 'Disable Module';
$_['hide_label'] = 'Hide Theme Name';
$_['hide_label_help'] = 'Hide theme name next to image (Storefront only)';
$_['hide_image'] = 'Hide Theme Image';
$_['hide_image_help'] = 'Storefront only';
$_['image_size'] = 'Theme Image Size (px)';
$_['width'] = 'Width: ';
$_['height'] = 'Height: ';
$_['hide_date'] = 'Hide Delivery Date';
$_['hide_date_help'] = '(Storefront only)';
$_['balance_check_module'] = 'Voucher Balance Check and Apply Module';
$_['status'] = 'Status';
$_['entry_deliverydate'] = 'Delivery Date';
$_['entry_order_id'] = 'Order ID';

$_['button_random_string'] = 'Random String';
$_['tab_setting'] = 'General Settings';
?>
