<?php

$_['heading_title1'] = 'Cloud Zoom & Pretty Photos';
$_['heading_title'] = 'Cloud Zoom & Pretty Photos <a style="color: #00FF00; float: right;" target="_blank" href="http://mmosolution.com" title="Contact to developer"><strong>Www.MMOSolution.com</strong></a>';

// Text
$_['text_module'] = 'Modules';
$_['tab_support'] = '<i class = "fa fa-send"></i> Contact Support';
$_['tab_setting'] = '<i class="fa fa-gear"></i> Setting';
$_['text_success'] = 'Success: You have modified module Cloud Zoom & Pretty Photos!';
$_['text_show_title_cloud_zoom'] = 'Title On Cloud Zoom';
$_['text_status'] = 'Status';
$_['text_border_cloud_zoom'] = 'Cloud Zoom Border';
$_['color_border_cloud_zoom'] = 'Color';
$_['text_resize_cloud_zoom'] = 'Cloud Zoom Size';
$_['text_show_popup_button'] = 'Popup Button';
$_['text_autoplay_slideshow'] = 'Autoplay Popup Slideshow';
$_['text_opacity'] = 'Popup Opacity';
$_['text_show_title'] = 'Popup Title';
$_['text_theme_popup'] = 'Popup Theme';
$_['text_resize_popup'] = 'Allow Popup Resize';
$_['text_speed_popup'] = 'Animation Popup Speed';
$_['text_slideshow'] = 'Popup Slideshow Speed';
$_['text_border_popup'] = 'Popup Border';
$_['text_social_button'] = 'Social Popup Button';
$_['text_help_thumbnail'] = 'Help Popup Thumbnail';
$_['text_carousel'] = 'Carousel';
$_['text_nav_but_color'] = 'Navigation Color Buttons';
$_['text_item_carousel'] = 'Items Carousel';
$_['text_edit_module'] = 'Edit module Cloud Zoom & Pretty Photos';;
$_['help_slideshow'] = 'E.g: 2000 = 2 second';
$_['help_opacity'] = 'Value between 0 and 1';
$_['help_border_cloud_zoom'] = 'e.g: 1 = 1px';
$_['help_item_carousel'] = 'Quantity of item in carousel enable';

// button
$_['button_save'] = 'Save';
$_['button_cancel'] = 'Cancel';
// Error
$_['error_permission'] = 'Warning: You do not have permission to modify module Cloud Zoom & Pretty Photos!';
?>