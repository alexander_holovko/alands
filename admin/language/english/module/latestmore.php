<?php
// Heading
$_['heading_title']    = 'Latest products with Ajax-loading';

// Text
$_['text_module']      = 'Modules';
$_['text_success']     = 'Success: You have modified latest module!';
$_['text_edit']        = 'Edit Latest Module';

// Entry
$_['entry_name']       = 'Module Name';
$_['entry_limit']      = 'Limit';
$_['entry_width']      = 'Width';
$_['entry_height']     = 'Height';
$_['entry_status']     = 'Status';
$_['entry_view']       = 'View';
$_['text_grid']        = 'Grid';
$_['text_list']        = 'List';
$_['loadmore_button_name'] 			= 'Load more';
$_['loadmore_button_name_title'] 	= 'Text on button';
$_['loadmore_style_title'] 			= 'CSS for button';

// Error
$_['error_permission'] = 'Warning: You do not have permission to modify latest module!';
$_['error_name']       = 'Module Name must be between 3 and 64 characters!';
$_['error_width']      = 'Width required!';
$_['error_height']     = 'Height required!';