<?php
// HTTP
define('HTTP_SERVER', 'http://test.alands.com.ua/admin/');
define('HTTP_CATALOG', 'http://test.alands.com.ua/');

// HTTPS
define('HTTPS_SERVER', 'https://test.alands.com.ua/admin/');
define('HTTPS_CATALOG', 'https://test.alands.com.ua/');

// DIR
define('DIR_APPLICATION', '/home/alands/alands.com.ua/test/admin/');
define('DIR_SYSTEM', '/home/alands/alands.com.ua/test/system/');
define('DIR_LANGUAGE', '/home/alands/alands.com.ua/test/admin/language/');
define('DIR_TEMPLATE', '/home/alands/alands.com.ua/test/admin/view/template/');
define('DIR_CONFIG', '/home/alands/alands.com.ua/test/system/config/');
define('DIR_IMAGE', '/home/alands/alands.com.ua/test/image/');
define('DIR_CACHE', '/home/alands/alands.com.ua/test/system/storage/cache/');
define('DIR_DOWNLOAD', '/home/alands/alands.com.ua/test/system/storage/download/');
define('DIR_LOGS', '/home/alands/alands.com.ua/test/system/storage/logs/');
define('DIR_MODIFICATION', '/home/alands/alands.com.ua/test/system/storage/modification/');
define('DIR_UPLOAD', '/home/alands/alands.com.ua/test/system/storage/upload/');
define('DIR_CATALOG', '/home/alands/alands.com.ua/test/catalog/');

// DB
define('DB_DRIVER', 'mysqli');
define('DB_HOSTNAME', 'alands.mysql.tools');
define('DB_USERNAME', 'alands_test');
define('DB_PASSWORD', '0&2Mc1(Atd');
define('DB_DATABASE', 'alands_test');
define('DB_PORT', '3306');
define('DB_PREFIX', 'oc_');
