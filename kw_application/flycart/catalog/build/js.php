<?php

$www = parse_url($this->url->link(false));

if( preg_match('/www/', $this->request->server['HTTP_HOST']) && !preg_match('/www/', $www['host']) ) {
	$url = $www['scheme'] . '://www.' . $www['host'] . $www['path'];
	$url = str_replace('index.php', '', $url);
	$url = str_replace('/admin', '', $url);
}
else {
	$url = $this->url->link(false);
	$url = str_replace('admin/index.php?route=', '', $url);
	$url = str_replace('http://', '//', $url);
$url = str_replace('https://', '//', $url);
}

$langs = array();

foreach ($this->model_localisation_language->getLanguages() as $lang) {
	$language = new Language($lang['directory']);
	$language->load('module/kw_flycart');

	$langs['widget_text'][$lang['language_id']] = '{{[items]}}';
	$langs['cont_btn'][$lang['language_id']] = $language->get('cont_btn');
	$langs['check_btn'][$lang['language_id']] = $language->get('check_btn');
	$langs['tocart_btn'][$lang['language_id']] = $language->get('tocart_btn');
	$langs['heading_title'][$lang['language_id']] = $language->get('heading_title');
	$langs['header_title'][$lang['language_id']] = $language->get('header_title');
	$langs['text_empty'][$lang['language_id']] = $language->get('text_empty');
	$langs['file_btn'][$lang['language_id']] = $language->get('file_btn');
	$langs['add_btn'][$lang['language_id']] = $language->get('add_btn');
	$langs['noti_title_1'][$lang['language_id']] = $language->get('noti_title_1');
	$langs['sku'][$lang['language_id']] = $language->get('sku');
	$langs['manuf'][$lang['language_id']] = $language->get('manuf');
	$langs['model'][$lang['language_id']] = $language->get('model');
	$langs['stock'][$lang['language_id']] = $language->get('stock');
	$langs['instock'][$lang['language_id']] = $language->get('instock');
	$langs['noti_title_2'][$lang['language_id']] = $language->get('noti_title_2');
	$langs['items_1'][$lang['language_id']] = $language->get('items_1');
	$langs['items_2'][$lang['language_id']] = $language->get('items_2');
	$langs['items_3'][$lang['language_id']] = $language->get('items_3');
	$langs['items_b1'][$lang['language_id']] = $language->get('items_b1');
	$langs['items_b2'][$lang['language_id']] = $language->get('items_b2');
	$langs['items_b3'][$lang['language_id']] = $language->get('items_b3');
	$langs['items_n1'][$lang['language_id']] = $language->get('items_n1');
	$langs['items_n2'][$lang['language_id']] = $language->get('items_n2');
	$langs['items_n3'][$lang['language_id']] = $language->get('items_n3');
	$langs['items_t1'][$lang['language_id']] = $language->get('items_t1');
	$langs['items_t2'][$lang['language_id']] = $language->get('items_t2');
	$langs['items_t3'][$lang['language_id']] = $language->get('items_t3');
	$langs['text_added'][$lang['language_id']] = $language->get('text_added');
	$langs['b_total'][$lang['language_id']] = $language->get('b_total');
	$langs['options_text'][$lang['language_id']] = $language->get('options_text');
	$langs['require_text'][$lang['language_id']] = $language->get('require_text');
	$langs['adding_btn'][$lang['language_id']] = $language->get('adding_btn');
	$langs['today_txt'][$lang['language_id']] = $language->get('today_txt');
	$langs['now_txt'][$lang['language_id']] = $language->get('now_txt');
	$langs['date_txt'][$lang['language_id']] = $language->get('date_txt');
	$langs['time_txt'][$lang['language_id']] = $language->get('time_txt');
	$langs['clear_txt'][$lang['language_id']] = $language->get('clear_txt');
	$langs['close_txt'][$lang['language_id']] = $language->get('close_txt');
	$langs['select_text'][$lang['language_id']] = $language->get('select_text');
	$langs['sum_weight'][$lang['language_id']] = $language->get('sum_weight');
	$langs['coupon_btn'][$lang['language_id']] = $language->get('coupon_btn');
	$langs['coupon_placeholder'][$lang['language_id']] = $language->get('coupon_placeholder_lang');
	$langs['selected_coupon'][$lang['language_id']] = $language->get('selected_coupon');
	$langs['coupon_error'][$lang['language_id']] = $language->get('coupon_error');
}


$output = json_encode(array(
	'url' => $url,
	'options' => isset($tools['options']) ? $tools['options'] : 'true',
	'retina' => isset($tools['retina']) ? $tools['retina'] : 'true',
	'module_type' => isset($tools['module_type']) ? $tools['module_type'] : 'widget',
	'standart_cart' => isset($tools['standart_cart']) ? $tools['standart_cart'] : '#cart button',
	'button_others' => isset($tools['button_others']) ? $tools['button_others'] : '[onclick*=\'cart.add\'], [onclick*=\'addToCart\']',
	'button_product' => isset($tools['button_product']) ? $tools['button_product'] : '#button-cart',
	'product_id_all' => (isset($tools['product_id_all']) && $tools['product_id_all'] !== '') ? $tools['product_id_all'] : 'angular.element(this).attr(\'onclick\')',
	'continue_btn' => isset($tools['continue_btn']) ? $tools['continue_btn'] : 'true',
	'viewcart_btn' => isset($tools['viewcart_btn']) ? $tools['viewcart_btn'] : 'false',
	'checkout_btn' => isset($tools['checkout_btn']) ? $tools['checkout_btn'] : 'true',
	'show_totals' => isset($tools['show_totals']) ? $tools['show_totals'] : 'true',
	'widget_empty' => isset($tools['widget_empty']) ? $tools['widget_empty'] : 'true',
	'click_action' => isset($tools['click_action']) ? $tools['click_action'] : 'flycart',
	'click_action_link' => isset($tools['click_action_link']) ? $tools['click_action_link'] : 'index.php?route=checkout/cart',
	'click_action_mod' => isset($tools['click_action_mod']) ? $tools['click_action_mod'] : 'flycart',
	'click_action_mod_link' => isset($tools['click_action_mod_link']) ? $tools['click_action_mod_link'] : 'index.php?route=checkout/cart',
	'noti_type' => isset($tools['noti_type']) ? $tools['noti_type'] : 'advanced',
	'noti_image' => isset($tools['noti_image']) ? $tools['noti_image'] : 'true',
	'noti_image_width' => isset($tools['noti_image_width']) ? $tools['noti_image_width'] : '70',
	'noti_image_height' => isset($tools['noti_image_height']) ? $tools['noti_image_height'] : '70',
	'light_image' => isset($tools['light_image']) ? $tools['light_image'] : 'true',
	'light_image_width' => isset($tools['light_image_width']) ? $tools['light_image_width'] : '115',
	'light_image_height' => isset($tools['light_image_height']) ? $tools['light_image_height'] : '115',
	'advanced_image' => isset($tools['advanced_image']) ? $tools['advanced_image'] : 'true',
	'advanced_image_width' => isset($tools['advanced_image_width']) ? $tools['advanced_image_width'] : '115',
	'advanced_image_height' => isset($tools['advanced_image_height']) ? $tools['advanced_image_height'] : '115',
	'noti_timeout' => isset($tools['noti_timeout']) ? $tools['noti_timeout'] : '7',
	'popup_imgage' => isset($tools['popup_imgage']) ? $tools['popup_imgage'] : 'true',
	'popup_imgage_width' => isset($tools['popup_imgage_width']) ? $tools['popup_imgage_width'] : '47',
	'popup_imgage_height' => isset($tools['popup_imgage_height']) ? $tools['popup_imgage_height'] : '47',
	'module_imgage_width' => isset($tools['module_imgage_width']) ? $tools['module_imgage_width'] : '156',
	'module_imgage_height' => isset($tools['module_imgage_height']) ? $tools['module_imgage_height'] : '156',
	'effect_type' => isset($tools['effect_type']) ? $tools['effect_type'] : 'product',
	'effect_product_img' => isset($tools['effect_product_img']) ? $tools['effect_product_img'] : 'true',
	'effect_rotate' => isset($tools['effect_rotate']) ? $tools['effect_rotate'] : 'true',
	'effect_frame_speed' => isset($tools['effect_frame_speed']) ? $tools['effect_frame_speed'] : '1000',
	'widget_null' => isset($tools['widget_null']) ? $tools['widget_null'] : 'true',
	'popup_footer_items' => isset($tools['popup_footer_items']) ? $tools['popup_footer_items'] : 'true',
	'popup_footer_weight' => isset($tools['popup_footer_weight']) ? $tools['popup_footer_weight'] : 'true',
	'popup_footer_total' => isset($tools['popup_footer_total']) ? $tools['popup_footer_total'] : 'true',
	'popup_footer_names' => isset($tools['popup_footer_names']) ? $tools['popup_footer_names'] : 'false',
	'effect_frame_offset_top' => isset($tools['effect_frame_offset_top']) ? $tools['effect_frame_offset_top'] : '0',
	'effect_frame_offset_left' => isset($tools['effect_frame_offset_left']) ? $tools['effect_frame_offset_left'] : '0',
	'advanced_right' => isset($tools['advanced_right']) ? $tools['advanced_right'] : 'true',
	'advanced_name' => isset($tools['advanced_name']) ? $tools['advanced_name'] : 'true',
	'advanced_price' => isset($tools['advanced_price']) ? $tools['advanced_price'] : 'true',
	'advanced_sku' => isset($tools['advanced_sku']) ? $tools['advanced_sku'] : 'true',
	'advanced_brand' => isset($tools['advanced_brand']) ? $tools['advanced_brand'] : 'true',
	'advanced_model' => isset($tools['advanced_model']) ? $tools['advanced_model'] : 'true',
	'advanced_options' => isset($tools['advanced_options']) ? $tools['advanced_options'] : 'true',
	'continue_advanced_btn' => isset($tools['continue_advanced_btn']) ? $tools['continue_advanced_btn'] : 'true',
	'advanced_continue_btn' => isset($tools['advanced_continue_btn']) && !empty($tools['advanced_continue_btn'][$lang_id]) ? $tools['advanced_continue_btn'] : $langs['cont_btn'],
	'viewcart_advanced_btn' => isset($tools['viewcart_advanced_btn']) ? $tools['viewcart_advanced_btn'] : 'true',
	'advanced_viewcart_btn' => isset($tools['advanced_viewcart_btn']) && !empty($tools['advanced_viewcart_btn'][$lang_id]) ? $tools['advanced_viewcart_btn'] : $langs['check_btn'],
	'advanced_right_items' => isset($tools['advanced_right_items']) ? $tools['advanced_right_items'] : 'true',
	'continue_light_btn' => isset($tools['continue_light_btn']) ? $tools['continue_light_btn'] : 'true',
	'light_continue_btn' => isset($tools['light_continue_btn']) && !empty($tools['light_continue_btn'][$lang_id]) ? $tools['light_continue_btn'] : $langs['cont_btn'],
	'viewcart_light_btn' => isset($tools['viewcart_light_btn']) ? $tools['viewcart_light_btn'] : 'true',
	'light_viewcart_btn' => isset($tools['light_viewcart_btn']) && !empty($tools['light_viewcart_btn'][$lang_id]) ? $tools['light_viewcart_btn'] : $langs['check_btn'],
	'continue_noti_btn' => isset($tools['continue_noti_btn']) ? $tools['continue_noti_btn'] : 'true',
	'noti_continue_btn' => isset($tools['noti_continue_btn']) && !empty($tools['noti_continue_btn'][$lang_id]) ? $tools['noti_continue_btn'] : $langs['tocart_btn'],
	'viewcart_noti_btn' => isset($tools['viewcart_noti_btn']) ? $tools['viewcart_noti_btn'] : 'true',
	'noti_viewcart_btn' => isset($tools['noti_viewcart_btn']) && !empty($tools['noti_viewcart_btn'][$lang_id]) ? $tools['noti_viewcart_btn'] : $langs['check_btn'],
	'widget_title' => isset($tools['widget_title']) ? $tools['widget_title'] : 'false',
	'widget_text' => isset($tools['widget_text']) && !empty($tools['widget_text'][$lang_id]) ? $tools['widget_text'] : $langs['widget_text'],
	'widget_title_text' => isset($tools['widget_title_text']) && !empty($tools['widget_title_text'][$lang_id]) ? $tools['widget_title_text'] : $langs['heading_title'],
	'show_module_header' => isset($tools['show_module_header']) ? $tools['show_module_header'] : 'true',
	'show_module_items' => isset($tools['show_module_items']) ? $tools['show_module_items'] : 'true',
	'show_module_names' => isset($tools['show_module_names']) ? $tools['show_module_names'] : 'true',
	'show_module_weight' => isset($tools['show_module_weight']) ? $tools['show_module_weight'] : 'true',
	'show_module_image' => isset($tools['show_module_image']) ? $tools['show_module_image'] : 'true',
	'show_module_name' => isset($tools['show_module_name']) ? $tools['show_module_name'] : 'true',
	'show_module_remove' => isset($tools['show_module_remove']) ? $tools['show_module_remove'] : 'true',
	'popup_alert_on' => isset($tools['popup_alert_on']) ? $tools['popup_alert_on'] : 'true',
	'popup_info_on' => isset($tools['popup_info_on']) ? $tools['popup_info_on'] : 'true',
	'show_module_price' => isset($tools['show_module_price']) ? $tools['show_module_price'] : 'true',
	'show_module_quantity' => isset($tools['show_module_quantity']) ? $tools['show_module_quantity'] : 'true',
	'module_max_on' => isset($tools['module_max_on']) ? $tools['module_max_on'] : 'true',
	'module_alert_on' => isset($tools['module_alert_on']) ? $tools['module_alert_on'] : 'true',
	'module_info_on' => isset($tools['module_info_on']) ? $tools['module_info_on'] : 'true',
	'show_module_totals' => isset($tools['show_module_totals']) ? $tools['show_module_totals'] : 'true',
	'module_empty_type' => isset($tools['module_empty_type']) ? $tools['module_empty_type'] : 'text',
	'module_empty_text_lang' => isset($tools['module_empty_text_lang']) && !empty($tools['module_empty_text_lang'][$lang_id]) ? $tools['module_empty_text_lang'] : $langs['text_empty'],
	'module_empty_image' => isset($tools['module_empty_image']['src']) ? $tools['module_empty_image']['src'] : 'emptycart-1.png',
	'module_empty_image_width' => isset($tools['module_empty_image']['width']) ? $tools['module_empty_image']['width'] : '140',
	'module_empty_image_height' => isset($tools['module_empty_image']['height']) ? $tools['module_empty_image']['height'] : '120',
	'tocart_module_btn' => isset($tools['tocart_module_btn']) ? $tools['tocart_module_btn'] : 'true',
	'module_tocart_btn_text' => isset($tools['module_tocart_btn_text']) && !empty($tools['module_tocart_btn_text'][$lang_id]) ? $tools['module_tocart_btn_text'] : $langs['tocart_btn'],
	'checkout_module_btn' => isset($tools['checkout_module_btn']) ? $tools['checkout_module_btn'] : 'true',
	'module_checkout_btn_text' => isset($tools['module_checkout_btn_text']) && !empty($tools['module_checkout_btn_text'][$lang_id]) ? $tools['module_checkout_btn_text'] : $langs['check_btn'],
	'header' => isset($tools['header']) && !empty($tools['header'][$lang_id]) ? $tools['header'] : $langs['header_title'],
	'popup_header_items' => isset($tools['popup_header_items']) ? $tools['popup_header_items'] : 'true',
	'popup_header_weight' => isset($tools['popup_header_weight']) ? $tools['popup_header_weight'] : 'true',
	'popup_header_total' => isset($tools['popup_header_total']) ? $tools['popup_header_total'] : 'true',
	'popup_header_names' => isset($tools['popup_header_names']) ? $tools['popup_header_names'] : 'false',
	'popup_name' => isset($tools['popup_name']) ? $tools['popup_name'] : 'true',
	'popup_premove' => isset($tools['popup_premove']) ? $tools['popup_premove'] : 'true',
	'popup_counter' => isset($tools['popup_counter']) ? $tools['popup_counter'] : 'true',
	'popup_price' => isset($tools['popup_price']) ? $tools['popup_price'] : 'true',
	'popup_empty_type' => isset($tools['popup_empty_type']) ? $tools['popup_empty_type'] : 'text',
	'popup_empty_text' => isset($tools['popup_empty_text_lang']) && !empty($tools['popup_empty_text_lang'][$lang_id]) ? $tools['popup_empty_text_lang'] : $langs['text_empty'],
	'popup_empty_image' => isset($tools['popup_empty_image']['src']) ? $tools['popup_empty_image']['src'] : 'emptycart-1.png',
	'popup_empty_image_width' => isset($tools['popup_empty_image']['width']) ? $tools['popup_empty_image']['width'] : '140',
	'popup_empty_image_height' => isset($tools['popup_empty_image']['height']) ? $tools['popup_empty_image']['height'] : '120',
	'continue_btn_txt' => isset($tools['continue_btn_txt']) && !empty($tools['continue_btn_txt'][$lang_id]) ? $tools['continue_btn_txt'] : $langs['cont_btn'],
	'viewcart_btn_txt' => isset($tools['viewcart_btn_txt']) && !empty($tools['viewcart_btn_txt'][$lang_id]) ? $tools['viewcart_btn_txt'] : $langs['tocart_btn'],
	'checkout_btn_txt' => isset($tools['checkout_btn_txt']) && !empty($tools['checkout_btn_txt'][$lang_id]) ? $tools['checkout_btn_txt'] : $langs['check_btn'],
	'popup_max_on' => isset($tools['popup_max_on']) ? $tools['popup_max_on'] : 'false',
	'options_file_btn_txt' => isset($tools['options_file_btn_txt']) && !empty($tools['options_file_btn_txt'][$lang_id]) ? $tools['options_file_btn_txt'] : $langs['file_btn'],
	'options_btn_txt' => isset($tools['options_btn_txt']) && !empty($tools['options_btn_txt'][$lang_id]) ? $tools['options_btn_txt'] : $langs['add_btn'],
	'cart' => str_replace('/admin', '', $this->url->link('checkout/cart')),
	'checkout' => str_replace('/admin', '', $this->url->link('checkout/checkout', '', 'SSL')),
	'heading_title' => $langs['header_title'],
	'sku' => $langs['sku'],
	'manuf' => $langs['manuf'],
	'model' => $langs['model'],
	'stock' => $langs['stock'],
	'instock' => $langs['instock'],
	'items_1' => $langs['items_1'],
	'items_2' => $langs['items_2'],
	'items_3' => $langs['items_3'],
	'items_b1' => $langs['items_b1'],
	'items_b2' => $langs['items_b2'],
	'items_b3' => $langs['items_b3'],
	'items_n1' => $langs['items_n1'],
	'items_n2' => $langs['items_n2'],
	'items_n3' => $langs['items_n3'],
	'items_t1' => $langs['items_t1'],
	'items_t2' => $langs['items_t2'],
	'items_t3' => $langs['items_t3'],
	'text_added' => $langs['text_added'],
	'b_total' => $langs['b_total'],
	'options_text' => $langs['options_text'],
	'require_text' => $langs['require_text'],
	'adding_btn' => $langs['adding_btn'],
	'today_txt' => $langs['today_txt'],
	'now_txt' => $langs['now_txt'],
	'date_txt' => $langs['date_txt'],
	'time_txt' => $langs['time_txt'],
	'clear_txt' => $langs['clear_txt'],
	'close_txt' => $langs['close_txt'],
	'select_text' => $langs['select_text'],
	'sum_weight' => $langs['sum_weight'],
	'selected_coupon' => $langs['selected_coupon'],
	'coupon_error' => $langs['coupon_error'],
	'minimal_on' => $tools['minimal_on'] ? $tools['minimal_on'] : 'true',
	'popup_weight' => isset($tools['popup_weight']) ? $tools['popup_weight'] : 'false',
	'show_module_p_weight' => isset($tools['show_module_p_weight']) ? $tools['show_module_p_weight'] : 'false',
	'popup_stock' => isset($tools['popup_stock']) ? $tools['popup_stock'] : 'false',
	'popup_stock_color' => isset($tools['popup_stock_color']) ? json_encode($tools['popup_stock_color']) : array(),
	'advanced_stock' => isset($tools['advanced_stock']) ? $tools['advanced_stock'] : 'false',
	'advanced_stock_color' => isset($tools['advanced_stock_color']) ? json_encode($tools['advanced_stock_color']) : array(),
	'show_module_stock' => isset($tools['show_module_stock']) ? $tools['show_module_stock'] : 'false',
	'module_stock_color' => isset($tools['module_stock_color']) ? json_encode($tools['module_stock_color']) : array(),
	'popup_coupon' => isset($tools['popup_coupon']) ? $tools['popup_coupon'] : 'true',
	'coupon_btn_txt' => isset($tools['coupon_btn_txt']) && !empty($tools['coupon_btn_txt'][$lang_id]) ? $tools['coupon_btn_txt'] : $langs['coupon_btn'],
	'coupon_placeholder' => isset($tools['coupon_placeholder']) && !empty($tools['coupon_placeholder'][$lang_id]) ? $tools['coupon_placeholder'] : $langs['coupon_placeholder'],
	'module_coupon' => isset($tools['module_coupon']) ? $tools['module_coupon'] : 'true',
	'module_coupon_btn_txt' => isset($tools['module_coupon_btn_txt']) && $tools['module_coupon_btn_txt'][$lang_id] ? $tools['module_coupon_btn_txt'] : $langs['coupon_btn'],
	'module_coupon_placeholder' => isset($tools['module_coupon_placeholder']) && !empty($tools['module_coupon_placeholder'][$lang_id]) ? $tools['module_coupon_placeholder'] : $langs['coupon_placeholder'],
	'advanced_left_title_text' => isset($tools['advanced_left_title_text']) && !empty($tools['advanced_left_title_text'][$lang_id]) ? $tools['advanced_left_title_text'] : $langs['noti_title_1'],
	'advanced_right_title_text' => isset($tools['advanced_right_title_text']) && !empty($tools['advanced_right_title_text'][$lang_id]) ? $tools['advanced_right_title_text'] : $langs['noti_title_2']
));

$locale = ($this->config->get('config_language') && $this->config->get('config_language') === 'ru') ? 'ru-ru' : 'en';
$kw_bundle = file_get_contents('../kw_application/flycart/catalog/app/kw_bundle.js');
$kw_locale = file_get_contents('../kw_application/flycart/catalog/app/i18n/angular-locale_' . $locale . '.js');
$kw_build = file_get_contents('../kw_application/flycart/catalog/build/flycart-build.js');

$js = <<<EOD
/** KW FlyCart v{$this->version} */\n
{$kw_bundle}
{$kw_locale}

var flycartTools = {$output};

{$kw_build}
EOD;

if (isset($tools['effect_type']) && $tools['effect_type'] === 'custom' && isset($tools['effect_custom_js']) && $tools['effect_custom_js'] !== '') {
	$js .= "function FlycartCustomEffects() {";
	$js .= "var kw = this;";
	$js .= "kw.params();";
	$js .= $tools['effect_custom_js'];
	$js .= "}";
}

if (isset($tools['custom_js']) && $tools['custom_js'] !== '') {
	$js .= $tools['custom_js'];
}

?>