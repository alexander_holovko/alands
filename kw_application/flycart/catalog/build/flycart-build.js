// JournalQuickView iframe init
function JournalQuickView() {
	this.init();
}

(function () {
	'use strict';

	$.ajaxPrefilter(function (options, originalOptions, jqXHR) {
		if (options.url === "index.php?route=checkout/cart/add") {
			jqXHR.abort();
		}
	});

	var kwFlycart = angular.module('kwFlycart', ['ngResource', 'ngSanitize', 'ngAnimate', 'ngRetina', 'ui.bootstrap', 'ui.bootstrap.datetimepicker', 'flow']);

	kwFlycart.config(['$httpProvider', '$locationProvider', 'flowFactoryProvider', function ($httpProvider, $locationProvider, flowFactoryProvider) {

		flowFactoryProvider.defaults = {
			singleFile: true,
			target: 'index.php?route=tool/upload',
			testChunks: false,
			fileParameterName: 'file',
			successStatuses: [200, 201, 202]
		};

		$httpProvider.defaults.headers.post['Content-Type'] = 'application/x-www-form-urlencoded;charset=utf-8';

		$httpProvider.defaults.transformRequest = [function (data) {
			return angular.isObject(data) && String(data) !== '[object File]' ? jQuery.param(data) : data;
		}];

		$locationProvider.html5Mode(true);
	}]);

	kwFlycart.controller('kwFlycartController', ['$scope', '$http', '$timeout', '$attrs', '$document', '$templateCache', '$compile', '$interpolate', '$templateRequest',
		function ($scope, $http, $timeout, $attrs, $document, $templateCache, $compile, $interpolate, $templateRequest) {

			$scope.tools = flycartTools;
			$scope.optionsLoading = false;
			$scope.event = null;
			$scope.popupLoader = false;
			$scope.lang_id = $attrs.langid;
			$scope.coupon_error = null;

			$scope.dateOptions = {
				readonlyInput: true,
				showWeeks: false,
				startingDay: 1
			};

			$scope.timeOptions = {
				showMeridian: false
			};

			$scope.flycartWidgetAppend = function () {
				var widget = angular.element('[flycart="widget"]'),
					module = angular.element('[flycart="module"]'),
					products = angular.element('[flycart="products"]');

				if (widget.length) {
					$templateRequest(flycartTools.url + 'kw_application/flycart/catalog/tmpl/flycart-widget.html').then(function (tmpl) {
						if (widget.length > 1) {
							widget.each(function () {
								if (this.id !== 'kw-base-widget') {
									angular.element(this).html($compile(tmpl)($scope));
								}
							});
						}
						else {
							angular.element('body').append($compile(tmpl)($scope));
						}
					});
				}

				if (module.length) {
					$templateRequest(flycartTools.url + 'kw_application/flycart/catalog/tmpl/flycart-module.html').then(function (tmpl) {
						module.html($compile(tmpl)($scope));
					});
				}

				if (products.length) {
					$templateRequest(flycartTools.url + 'kw_application/flycart/catalog/tmpl/flycart-products.html').then(function (tmpl) {
						products.html($compile(tmpl)($scope));
					});
				}
			};

			$scope.widgetContent = function (content) {
				content = content.replace('[items]', 'cart.count');
				content = content.replace('[name]', 'cart.products.length');
				content = content.replace('[total]', 'cart.total');
				return $interpolate(content)($scope);
			};

			$scope.flycartUpdate = function () {
				var width, height;

				if (flycartTools.module_type === 'module') {
					width = flycartTools.module_imgage_width;
					height = flycartTools.module_imgage_height;
				}
				else {
					width = flycartTools.popup_imgage_width;
					height = flycartTools.popup_imgage_height;
				}

				$http.post(flycartTools.url + 'index.php?route=module/kw_flycart/loader', {'width': width, 'height': height}).then(function (response) {
					$scope.cart = response.data || {};
					$scope.widget_empty = ($scope.cart.count > 0) ? 'true' : flycartTools.widget_empty;
					$scope.coupon_error = null;
					$scope.coupon = response.data.couponCode;

					$scope.relatedHover();
				});
			};

			$scope.flycartWidgetAppend();
			$scope.flycartUpdate();

			$scope.flycartOpen = function (name) {

				var width, height;

				if (flycartTools.module_type === 'module') {
					width = flycartTools.module_imgage_width;
					height = flycartTools.module_imgage_height;
				}
				else {
					width = flycartTools.popup_imgage_width;
					height = flycartTools.popup_imgage_height;
				}

				if (flycartTools.click_action === 'flycart' || flycartTools.click_action_mod === 'flycart' && name === 'standart') {
					$http.post(flycartTools.url + 'index.php?route=module/kw_flycart/loader', {'width': width, 'height': height}).then(function (response) {
						$scope.cart = response.data || {};
						$scope.popupOpen(flycartTools.url + 'kw_application/flycart/catalog/tmpl/flycart-products.html', 'default');
						$scope.coupon_error = null;
						$scope.coupon = response.data.couponCode;
					});
				}
				else if (flycartTools.click_action === 'page' && name === 'widget') {
					location.href = flycartTools.click_action_link;
				}
			};

			$scope.$on('quantity-update', function (event, key, quantity) {
				var object = {};
				object[key] = quantity;

				$scope.popupLoader = true;

				$http.post(flycartTools.url + 'index.php?route=module/kw_flycart/quantityUpdate', {'quantity': object}).then(function (response) {
					$scope.cart = response.data || {};
					$scope.standartCartUpdate();
					$scope.relatedHover();
					$scope.coupon_error = null;
					$scope.coupon = response.data.couponCode;

					$timeout(function () {
						$scope.popupLoader = false;
					});
				});
			});

			$scope.removeProduct = function (key) {
				$http.post(flycartTools.url + 'index.php?route=module/kw_flycart/removeProduct', {'remove': key}).then(function (response) {
					$scope.cart = response.data || {};
					$scope.standartCartUpdate();
					$scope.relatedHover();
					$scope.widget_empty = ($scope.cart.count > 0) ? 'true' : flycartTools.widget_empty;
					$scope.coupon_error = null;
					$scope.coupon = response.data.couponCode;
				});
			};

			$.ajaxPrefilter(function (options, originalOptions, jqXHR) {
				var param = $.unserialize(options.url);

				if (param.remove) {
					$scope.flycartUpdate();
				}
			});

			$scope.target = null;
			$scope.targetOffset = null;

			$scope.calledButtons = function () {
				$timeout(function () {
					$document.on('click', flycartTools.button_others, function () {
						$scope.target = this;
						$scope.targetOffset = angular.element(this).offset();
						$scope.showNotice = false;

						var product_id = 0,
							timezone = jstz.determine();

						if (angular.isDefined(flycartTools.product_id_all) && flycartTools.product_id_all !== 'angular.element(this).attr(\'onclick\')') {
							product_id = eval(flycartTools.product_id_all);
						}
						else {
							if (angular.element(this).is('[onclick]')) {
								product_id = angular.element(this).attr('onclick').split('(')[1].replace(/[^\d+,]/g, '').split(',')[0];
							}
							else if (angular.element(this).is('[data-id]')) {
								product_id = angular.element(this).data('id').replace(/[^\d+,]/g, '');
							}
						}

						if (flycartTools.minimal_on === 'true') {
							$http.post(flycartTools.url + 'index.php?route=module/kw_flycart/productMinimal', {'product_id': product_id}).then(function (quantity) {
								$http.post(flycartTools.url + 'index.php?route=module/kw_flycart/addProduct', {'product_id': product_id, 'quantity': quantity.data, 'timezone': timezone.name()}).then(function (response) {

									if (!response.data.redirect) {
										var data = response.data || {};
										$scope.widget_empty = 'true';
										$scope.productEffects(product_id, data);
									}
									else if (response.data.redirect && flycartTools.options === 'true') {
										$scope.productOptions(response.data.options);
									}
									else {
										location.href = response.data.redirect;
									}

								});
							});
						}
						else {
							$http.post(flycartTools.url + 'index.php?route=module/kw_flycart/addProduct', {'product_id': product_id, 'quantity': 1, 'timezone': timezone.name()}).then(function (response) {

								if (!response.data.redirect) {
									var data = response.data || {};
									$scope.widget_empty = 'true';
									$scope.productEffects(product_id, data);
								}
								else if (response.data.redirect && flycartTools.options === 'true') {
									$scope.productOptions(response.data.options);
								}
								else {
									location.href = response.data.redirect;
								}

							});
						}
					});
				});
			};

			$scope.calledProduct = function () {
				$timeout(function () {
					$document.on('click', flycartTools.button_product, function () {
						$scope.target = this;
						$scope.targetOffset = angular.element(this).offset();
						$scope.showNotice = false;

						var product_id = angular.element('[name*="product_id"]').val(),
							quantity = angular.isDefined(angular.element('[name*="quantity"]').val()) ? angular.element('[name*="quantity"]').val() : 1,
							options = angular.element('[name*="option"]'),
							optionObject = {}, checkboxesArray = [], nameObject = {}, checkboxes = {};

						for (var key in options) {
							if (options.hasOwnProperty(key) && options[key].name) {
								if (options[key].type !== "checkbox" && options[key].type !== "radio") {
									nameObject[options[key].name] = options[key].value;
									angular.extend(optionObject, nameObject);
								}
								else if (options[key].type === "radio" && angular.element(options[key]).is(':checked')) {
									nameObject[options[key].name] = options[key].value;
									angular.extend(optionObject, nameObject);
								}
								else if (options[key].type === "checkbox" && angular.element(options[key]).is(':checked')) {
									checkboxesArray.push(options[key].value);
									nameObject[options[key].name] = checkboxesArray;
									angular.extend(optionObject, nameObject);
								}
							}
						}

						angular.extend(optionObject, {'quantity': quantity}, {'product_id': product_id});

						$scope.addToCartOptions(false, optionObject, product_id)
					});
				});
			};

			$scope.calledButtons();
			$scope.calledProduct();

			// JournalQuickView iframe init
			JournalQuickView.prototype.init = function () {
				$scope.flycartUpdate();
			};

			$scope.getCoupon = function (coupon) {

				$scope.popupLoader = true;
				$scope.coupon_error = null;

				$http.post(flycartTools.url + 'index.php?route=module/kw_flycart/getCoupon', {'coupon': coupon}).then(function (response) {
					if (response.data.status !== 'error') {
						$scope.cart = response.data || {};
						$scope.standartCartUpdate();
						$scope.relatedHover();
						$scope.coupon = response.data.couponCode;
					}
					else {
						$scope.coupon_error = true;
					}

					$timeout(function () {
						$scope.popupLoader = false;
					});
				});
			};

			$scope.productEffects = function (product_id, data) {
				angular.element('#flycart-flyer').remove();

				$timeout(function () {

					var finishOffset = angular.element('#flycart-widget').offset();

					if (flycartTools.module_type === 'standart') {
						finishOffset = angular.element(flycartTools.standart_cart).offset();
					}
					else if (flycartTools.module_type === 'module') {
						finishOffset = angular.element('.flycart-module-container').offset();
					}

					var flyer = '<div id="flycart-flyer" />';

					if (flycartTools.effect_type === 'product' || flycartTools.effect_type === 'custom' && flycartTools.effect_product_img === 'true') {
						$http.post(flycartTools.url + 'index.php?route=module/kw_flycart/productImage', {'product_id': product_id}).then(function (response) {

							flyer = '<div id="flycart-flyer" style="display: none;"><img src="' + response.data + '" alt /></div>';

							angular.element('body').append(flyer);
							$scope.flyAnimation(data, product_id, finishOffset);
						});
					}
					else {
						angular.element('body').append(flyer);
						$scope.flyAnimation(data, product_id, finishOffset);
					}
				}, 300);
			};

			$scope.flyAnimation = function (data, product_id, finishOffset) {
				if (flycartTools.effect_type === 'off') {
					$scope.cart = data;
					$scope.standartCartUpdate();
					$scope.flycartNotification(product_id);
					return;
				}

				$timeout(function () {

					var flyer = angular.element('#flycart-flyer'),
						target = angular.element($scope.target),
						targetOffset = target.offset(),
						finish = angular.element('#flycart-widget');

					if (flycartTools.effect_rotate === 'true') {
						flyer.addClass('fly-rotate');
					}

					if (flycartTools.module_type === 'standart') {
						finish = angular.element(flycartTools.standart_cart);
					}
					else if (flycartTools.module_type === 'module') {
						finish = angular.element('.flycart-module-container');
					}

					flyer.css({'display': 'block', 'top': ($scope.targetOffset.top - target.height()), 'left': $scope.targetOffset.left});

					if (flycartTools.effect_type !== 'custom') {
						var top = ( finishOffset.top + (finish[0].clientHeight / 2) - (flyer[0].clientHeight / 2) + parseInt(flycartTools.effect_frame_offset_top) ),
							left = ( finishOffset.left + parseInt(flycartTools.effect_frame_offset_left) );

						if (flycartTools.module_type === 'module') {
							top = ( finishOffset.top + (finish[0].clientHeight / 2) );
							left = finishOffset.left;
						}

						if (flycartTools.module_type === 'widget') {
							left -= finish[0].clientWidth;
						}

						flyer.animate({
							'top': top,
							'left': left
						}, Number(flycartTools.effect_frame_speed), function () {
							angular.element(flyer[0]).remove();

							$timeout(function () {
								$scope.countChange = true;
							}, 100);

							$timeout(function () {
								$scope.cart = data;
								$scope.standartCartUpdate();
								$scope.flycartNotification(product_id);
							}, 200);

							$timeout(function () {
								$scope.countChange = false;
							}, 600);

						});
					}

					if (flycartTools.effect_type === 'custom') {
						FlycartCustomEffects.prototype.params = function () {
							this.flyer = flyer;
							this.target = target;
							this.targetOffset = targetOffset;
							this.finish = finish;
						};

						FlycartCustomEffects.prototype.callback = function () {
							$timeout(function () {
								$scope.cart = data;
								$scope.standartCartUpdate();
								$scope.flycartNotification(product_id);
							}, 200);
						};

						new FlycartCustomEffects();
					}
				});
			};

			$scope.productOptions = function (data) {
				$scope.options = data;
				$scope.optionsTotal = parseFloat(data.total.replace(/[^\d+\\.]+/g, '')) / parseInt(data.minimum);
				$scope.uploadErrorMessage = [];
				$scope.uploadSuccessMessage = [];

				if (data.special) {
					$scope.optionsSpecial = parseFloat(data.special.replace(/[^\d+\\.]+/g, '')) / parseInt(data.minimum);
				}

				$scope.optionsQuantity = data.minimum;
				$scope.flyoptions = {};

				$scope.popupOpen(flycartTools.url + 'kw_application/flycart/catalog/tmpl/flycart-options.html', 'default options');

				$scope.$watch('flyoptions.quantity', function (newVal, oldVal) {
					$scope.optionsQuantity = newVal;
					$scope.priceRecalc();
				}, true);
			};

			$scope.uploadErrorMessage = [];
			$scope.uploadSuccessMessage = [];

			$scope.uploadSuccess = function ($file, $message, id) {
				var message = JSON.parse($message);

				if (message.success) {
					$scope.uploadSuccessMessage[id] = message.success;
					$scope.flyoptions[id] = message.code;
				}
				else {
					$scope.uploadErrorMessage[id] = message.error;
				}

				$file.flowObj.files = [];
			};

			$scope.priceFormat = function (element, total, pattern) {
				var format = total.toFixed(2).toString().split('');

				element.replace(',', '.').replace(/[-+]?([^0-9]\.?[^0-9]+|[^0-9\\.]+)/g, function (symbol0, symbol1, index, data) {
					if (symbol0 !== '' && symbol0 !== ' ') {
						if (index > 0) {
							format.push(symbol0);
						} else {
							format.unshift(symbol0);
						}

						format = format.join('');
					}
				});

				return format;
			};

			$scope.priceRecalc = function () {
				$timeout(function () {
					if (angular.isDefined($scope.optionsQuantity)) {
						var total = $scope.optionsTotal,
							special = $scope.optionsSpecial;

						for (var key in $scope.options) {
							if ($scope.options.hasOwnProperty(key) &&
								angular.isObject($scope.options[key]) &&
								Object.keys($scope.flyoptions).indexOf($scope.options[key].product_option_id) > -1) {

								var option = $scope.options[key].option_value;

								if (angular.isArray(option)) {
									for (var i = 0; i < option.length; i++) {
										if (option[i].price && $scope.flyoptions[$scope.options[key].product_option_id] && $scope.flyoptions[$scope.options[key].product_option_id].indexOf(option[i].product_option_value_id) > -1) {

											var price = parseFloat(option[i].price.replace(/[^\d+\\.]+/g, ''));

											if (option[i].price_prefix === '+') {
												total += price;
												special += price;
											}
											if (option[i].price_prefix === '-') {
												total -= price;
												special -= price;
											}
										}
									}
								}
							}
						}

						total = total * parseInt($scope.optionsQuantity);
						special = special * parseInt($scope.optionsQuantity);

						$scope.options.total = $scope.priceFormat($scope.options.total, total);

						if ($scope.options.special) {
							$scope.options.special = $scope.priceFormat($scope.options.special, special);
						}
					}
				});
			};

			$scope.addToCartOptions = function (form, options, product_id) {
				if (form && form.$valid) {
					$.magnificPopup.close();

					$scope.optionsLoading = true;

					var quantity = options.quantity,
						width = 100, height = 100,
						timezone = jstz.determine();

					switch (flycartTools.noti_type) {
						case 'notice':
							width = flycartTools.noti_image_width;
							height = flycartTools.noti_image_height;
							break;
						case 'light':
							width = flycartTools.light_image_width;
							height = flycartTools.light_image_height;
							break;
						case 'advanced':
							width = flycartTools.advanced_image_width;
							height = flycartTools.advanced_image_height;
							break;
					}

					for (var key in options) {
						if (options.hasOwnProperty(key) && angular.isDate(options[key])) {
							options[key] = Date.parse(options[key]);
						}
					}

					$http.post(flycartTools.url + 'index.php?route=module/kw_flycart/addProduct',
						{
							'product_id': product_id,
							'quantity': quantity,
							'option': options,
							'timezone': timezone.name()
						}
					).then(function (response) {
						if (!response.data.redirect) {
							var data = response.data || {};
							$scope.widget_empty = 'true';
							$scope.productEffects(product_id, data);
							$scope.optionsLoading = false;
						}
					});
				}
				else if (form && form.$invalid) {
					$scope.optionsLoading = false;
					angular.forEach(form.$error.required, function (field) {
						form[field.$name].$dirty = true;
					});
				}
				else if (form === false) {
					$http.post(flycartTools.url + 'index.php?route=module/kw_flycart/addProduct', options).then(function (response) {
						if (response.data.error) {
							$('.alert, .text-danger').remove();
							$('.form-group').removeClass('has-error');

							for (var i in response.data.error.option) {
								var element = $('#input-option' + i.replace('_', '-'));

								if (element.parent().hasClass('input-group')) {
									element.parent().after('<div class="text-danger">' + response.data.error.option[i] + '</div>');
								}
								else {
									element.after('<div class="text-danger">' + response.data.error.option[i] + '</div>');
								}
							}

							if (response.data.error.recurring) {
								$('select[name=\'recurring_id\']').after('<div class="text-danger">' + response.data.error.option.recurring + '</div>');
							}

							$('.text-danger').parent().addClass('has-error');
						}
						else {
							$scope.widget_empty = 'true';
							$scope.productEffects(product_id, response.data);
						}
						$scope.optionsLoading = false;
					});
				}
			};

			$scope.popupOpen = function (template, mainClass) {
				$scope.showNotice = false;

				var scrollTop = $(window).scrollTop();

				$templateRequest(template).then(function (tmpl) {
					$timeout(function () {
						$.magnificPopup.open({
							items: {
								src: $compile(tmpl)($scope),
								type: 'inline'
							},
							removalDelay: 300,
							mainClass: 'kw-flycart ' + mainClass,
							fixedContentPos: false,
							fixedBgPos: true,
							midClick: true,
							showCloseBtn: false,
							callbacks: {
								open: function () {
									angular.element('html').addClass('no-scroll');
									angular.element('body').css('top', -scrollTop);
									$scope.relatedHover();
									angular.element('.scroll-detector').remove();
								},
								close: function () {
									angular.element('html').removeClass('no-scroll');
									angular.element('body').css('top', '');
									$(window).scrollTop(scrollTop);
								}
							}
						});
					});
				});

				function getScrollBarWidth() {
					var div = document.createElement("div");
					div.className = "scroll-detector";
					div.style.overflow = "scroll";
					div.style.visibility = "hidden";
					div.style.position = 'absolute';
					div.style.width = '100px';
					document.body.appendChild(div);

					return div.offsetWidth - div.clientWidth;
				}
			};

			$scope.popupClose = function () {
				$timeout(function () {
					$.magnificPopup.close();
				});
			};

			$scope.standartOpen = function () {

				$scope.showNotice = false;
				if (flycartTools.click_action_mod !== 'none') {
					$document.on('mouseover mouseenter click', flycartTools.standart_cart, function (event) {
						return false;
					});

					angular.element(flycartTools.standart_cart).on('click', function () {
						if (flycartTools.click_action_mod === 'flycart') {
							$scope.flycartOpen('standart');
						}
						else if (flycartTools.click_action_mod === 'page') {
							location.href = flycartTools.click_action_mod_link;
						}
						return false;
					});
				}
			};

			$scope.standartOpen();

			$scope.standartCartUpdate = function () {
				$('#cart').load('index.php?route=common/cart/info', function () {
					$scope.standartOpen();
				});
			};

			$scope.noticeEnter = false;

			$scope.flycartNotice = function (data) {
				$templateRequest(flycartTools.url + 'kw_application/flycart/catalog/tmpl/flycart-notice.html').then(function (tmpl) {
					$scope.notice = data;

					angular.element('body').append(
						$compile(tmpl)($scope)
					);

					$timeout(function () {
						$scope.showNotice = true;
						$scope.relatedHover();
					}, 100);

					$timeout(function () {
						$scope.noticeClose();
					}, flycartTools.noti_timeout * 1000);
				});
			};

			$scope.noticeClose = function () {
				$scope.showNotice = false;
				$timeout(function () {
					angular.element('#flycart-notice').remove();
				}, 100);
			};

			$scope.flycartNotification = function (product_id) {

				var notyImage = {
					width: function () {
						switch (flycartTools.noti_type) {
							case 'notice':
								return flycartTools.noti_image_width;
								break;
							case 'light':
								return flycartTools.light_image_width;
								break;
							case 'advanced':
								return flycartTools.advanced_image_width;
								break;
							default:
								return 100;
						}
					},
					height: function () {
						switch (flycartTools.noti_type) {
							case 'notice':
								return flycartTools.noti_image_height;
								break;
							case 'light':
								return flycartTools.light_image_height;
								break;
							case 'advanced':
								return flycartTools.advanced_image_height;
								break;
							default:
								return 100;
						}
					}
				};

				$http.post(flycartTools.url + 'index.php?route=module/kw_flycart/loader', {'product_id': product_id, width: notyImage.width, height: notyImage.height}).then(function (response) {

					if (flycartTools.noti_type !== 'cart' && flycartTools.noti_type !== 'off') {
						var notification = angular.element('[flycart="notification"]');
					}

					var data = response.data,
						length = data.products.length, i = 0;

					$scope.notifyProduct = {};
					$scope.notifyProduct.count = data.count;
					$scope.notifyProduct.names = length;
					$scope.notifyProduct.totals = data.totals;

					for (; i < length; i++) {
						if (Number(data.products[i].id) === Number(product_id)) {
							angular.extend($scope.notifyProduct, data.products[i]);
						}
					}

					switch (flycartTools.noti_type) {
						case 'notice':
							$scope.flycartNotice($scope.notifyProduct);
							break;
						case 'light' :
							$scope.popupOpen(flycartTools.url + 'kw_application/flycart/catalog/tmpl/flycart-notification.html', 'light-overlay');
							break;
						case 'advanced':
							$scope.popupOpen(flycartTools.url + 'kw_application/flycart/catalog/tmpl/flycart-notification.html', 'noti-overlay');
							break;
						case 'cart':
							$scope.flycartOpen();
							break;
					}

					$scope.relatedHover();
				});
			};

			$scope.noticeMessage = function (item, text) {
				return text.replace('[product]', '<a href="' + item.href + '" class="light"><span>' + item.name + '</span></a>');
				;
			};

			$scope.relatedHover = function () {
				$timeout(function () {
					$('.related-links').relatedHover({'class': 'hover', 'parent': '.related-parent'});
				});
			};

			$scope.relatedHover();

		}]);

	kwFlycart.directive('kwNumber', ['$timeout', function ($timeout) {
		return {
			restrict: 'E',
			transclude: true,
			replace: true,
			scope: {
				model: '=', key: '@', min: '@', max: '@', options: '='
			},
			template: '<div class="field number">' +
			'<input type="text" ng:model="model" ng:keydown="keycode($event)" ng:blur="limits()" />' +
			'<span class="number_actions">' +
			'<span class="number_plus" ng:click="plus()"></span>' +
			'<span class="number_minus" ng:click="minus()"></span>' +
			'</span>' +
			'<div class="clearfix"></div>' +
			'</div>',
			link: function (scope, element, attrs) {
				var keys = [8, 9, 37, 38, 39, 40, 48, 49, 50, 51, 52, 53, 54, 55, 56, 57, 96, 97, 98, 99, 100, 101, 102, 103, 104, 105, 110];

				var min = (angular.isDefined(scope.min) && parseInt(scope.min) > 0) ? parseInt(scope.min) : 1,
					max = angular.isDefined(scope.max) ? parseInt(scope.max) : 99999;

				scope.model = angular.isDefined(scope.model) ? parseInt(scope.model) : 1;

				scope.plus = function () {
					if (scope.model < max) {
						scope.model++;
						if (!scope.options) {
							scope.$emit('quantity-update', scope.key, scope.model);
						}
					}
				};

				scope.minus = function () {
					if (scope.model > min) {
						scope.model--;
						if (!scope.options) {
							scope.$emit('quantity-update', scope.key, scope.model);
						}
					}
				};

				scope.keycode = function (event) {
					if ($.inArray(event.which, keys) === -1) {
						event.preventDefault();
					}

					switch (event.keyCode || event.which) {
						case 38:
							scope.plus();
							break;

						case 40:
							scope.minus();
							break;
					}
				};

				$timeout(function () {
					$(document).keyup(function (event) {
						var keycode = (event.keyCode ? event.keyCode : event.which);
						if (keycode === '13' && scope.model >= min || keycode === 13 && scope.model >= min) {
							$(this).blur();
							scope.$emit('quantity-update', scope.key, scope.model);
						}
					});
				});

				scope.limits = function () {
					scope.model = parseInt(scope.model);

					if (scope.model < min) {
						scope.model = min;
					}

					if (scope.model >= max && scope.model >= min) {
						scope.model = max;
					}

					if (!scope.options && scope.model >= min) {
						scope.$emit('quantity-update', scope.key, scope.model);
					}
				};
			}
		}
	}]);

	/**
	 * Checklist-model
	 * AngularJS directive for list of checkboxes
	 */

	kwFlycart.directive('checklistModel', ['$parse', '$compile', function ($parse, $compile) {
		// contains
		function contains(arr, item) {
			if (angular.isArray(arr)) {
				for (var i = 0; i < arr.length; i++) {
					if (angular.equals(arr[i], item)) {
						return true;
					}
				}
			}
			return false;
		}

		// add
		function add(arr, item) {
			arr = angular.isArray(arr) ? arr : [];
			for (var i = 0; i < arr.length; i++) {
				if (angular.equals(arr[i], item)) {
					return arr;
				}
			}
			arr.push(item);
			return arr;
		}

		// remove
		function remove(arr, item) {
			if (angular.isArray(arr)) {
				for (var i = 0; i < arr.length; i++) {
					if (angular.equals(arr[i], item)) {
						arr.splice(i, 1);
						break;
					}
				}
			}
			return arr;
		}

		// http://stackoverflow.com/a/19228302/1458162
		function postLinkFn(scope, elem, attrs) {
			// compile with `ng-model` pointing to `checked`
			$compile(elem)(scope);

			// getter / setter for original model
			var getter = $parse(attrs.checklistModel);
			var setter = getter.assign;

			// value added to list
			var value = $parse(attrs.checklistValue)(scope.$parent);

			// required added to list
			var required = $parse(attrs.checklistRequired)(scope.$parent);

			scope.required = (required === "1") ? true : false;

			// watch UI checked change
			scope.$watch('checked', function (newValue, oldValue) {
				if (newValue === oldValue) {
					return;
				}
				var current = getter(scope.$parent);
				if (newValue === true) {
					setter(scope.$parent, add(current, value));
				} else {
					setter(scope.$parent, remove(current, value));
				}
			});

			// watch original model change
			scope.$parent.$watch(attrs.checklistModel, function (newArr, oldArr) {
				scope.checked = contains(newArr, value);

				if (!newArr) {
					return;
				}

				if (required === "1" && newArr.length > 0) {
					scope.required = false;
				}
				else if (required === "1" && newArr.length === 0) {
					scope.required = true;
				}
			}, true);


		}

		return {
			restrict: 'A',
			priority: 1000,
			terminal: true,
			scope: true,
			compile: function (tElement, tAttrs) {
				if (tElement[0].tagName !== 'INPUT' || tAttrs.type !== 'checkbox') {
					throw 'checklist-model should be applied to `input[type="checkbox"]`.';
				}

				if (!tAttrs.checklistValue) {
					throw 'You should provide `checklist-value`.';
				}

				// exclude recursion
				tElement.removeAttr('checklist-model');

				// local scope var storing individual checkbox model
				tElement.attr('ng-model', 'checked');

				tElement.attr('ng-required', 'required');

				return postLinkFn;
			}
		};
	}]);


	/**
	 * parseInt Filter
	 */

	kwFlycart.filter('num', function () {
		return function (input) {
			return parseFloat(input);
		}
	});


	/**
	 * declension Filter
	 */

	kwFlycart.filter('declension', function () {
		return function (count, title1, title2, title3) {
			if (!count) {
				return '0 ' + title3;
			}

			/**
			 * Declension Filter
			 * @params { count (int), title1 (str), title2 (str), title3 (str)  }
			 * @return num
			 */

			var cases = {0: title3, 1: title1, 2: title2, 3: title2, 4: title2, 5: title3},
				str = (count % 100 > 4 && count % 100 < 20) ? title3 : cases[Math.min(count % 10, 5)];
			return count + ' ' + str;
		};
	});

}());