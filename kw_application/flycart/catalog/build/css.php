<?php

$www = parse_url($this->url->link(false));

if( preg_match('/www/', $this->request->server['HTTP_HOST']) && !preg_match('/www/', $www['host']) ) {
	$url = $www['scheme'] . '://www.' . $www['host'] . $www['path'];
	$url = str_replace('index.php', '', $url);
	$url = str_replace('/admin', '', $url);
}
else {
	$url = $this->url->link(false);
	$url = str_replace('admin/index.php?route=', '', $url);
	$url = str_replace('http://', '//', $url);
$url = str_replace('https://', '//', $url);
}

$css = "/** KW FlyCart v{$this->version} */\n";
$css .= file_get_contents('../kw_application/flycart/catalog/css/kw_flycart.css');

if (isset($tools['click_action']) && $tools['click_action'] === 'flycart' || isset($tools['options']) && $tools['options'] === 'true' || isset($tools['click_action_mod']) && $tools['click_action_mod'] === 'flycart') {

	$css .= ".mfp-bg.default {";
	if (isset($tools['overlay_bg_on']) && $tools['overlay_bg_on'] === 'true') {
		$overlay_bg = isset($tools['overlay_bg']) ? $tools['overlay_bg'] : 'rgba(0,0,0,0.9)';

		$css .= "background-color: {$overlay_bg};";
	}
	if (isset($tools['overlay_image_on']) && $tools['overlay_image_on'] === 'true') {
		$overlay_image = isset($tools['overlay_image']['src']) ? $tools['overlay_image']['src'] : 'carbon.png';

		$css .= "background-image: url({$url}kw_application/flycart/images/bg/{$overlay_image});";
	}
	$css .= "}";
	$css .= "#flycart-dialog, #flycart-options-dialog {";

	$min_width = isset($tools['min_width']) ? $tools['min_width'] : '300';
	$max_width = isset($tools['max_width']) ? $tools['max_width'] : '560';
	$min_height = isset($tools['min_height']) ? $tools['min_height'] : '200';
	$max_height = isset($tools['max_height']) ? $tools['max_height'] : '560';

	$css .= "min-width: {$min_width}px;";
	$css .= "max-width: {$max_width}px;";
	$css .= "min-height: {$min_height}px;";
	$css .= "max-height: {$max_height}px;";

	if (isset($tools['popup_shadow_on']) && $tools['popup_shadow_on'] === 'true') {
		$popup_shadow = (isset($tools['popup_shadowx'])     ? $tools['popup_shadowx']     : '0') . "px "
								  . (isset($tools['popup_shadowy'])     ? $tools['popup_shadowy']     : '0') . "px "
									. (isset($tools['popup_shadowblur'])  ? $tools['popup_shadowblur']  : '0') . "px "
									. (isset($tools['popup_shadowcolor']) ? $tools['popup_shadowcolor'] : 'rgba(0, 0, 0, 0)');

		$css .= "-webkit-box-shadow: {$popup_shadow};";
		$css .= "box-shadow: {$popup_shadow};";
	}

	if (isset($tools['popup_radius']) && $tools['popup_radius'] === 'true') {
		$popup_radius = (isset($tools['popup_radiust']) ? $tools['popup_radiust'] : '0') . (isset($tools['popup_radiusval']) ? $tools['popup_radiusval'] : 'px') . " "
									. (isset($tools['popup_radiusr']) ? $tools['popup_radiusr'] : '0') . (isset($tools['popup_radiusval']) ? $tools['popup_radiusval'] : 'px') . " "
									. (isset($tools['popup_radiusb']) ? $tools['popup_radiusb'] : '0') . (isset($tools['popup_radiusval']) ? $tools['popup_radiusval'] : 'px') . " "
									. (isset($tools['popup_radiusl']) ? $tools['popup_radiusl'] : '0') . (isset($tools['popup_radiusval']) ? $tools['popup_radiusval'] : 'px');

		$css .= "-webkit-border-radius: {$popup_radius};";
		$css .= "border-radius: {$popup_radius};";
	}

	$popup_bg = isset($tools['popup_bg']) ? $tools['popup_bg'] : 'rgba(250,250,250,1)';
	$popup_text_color = isset($tools['popup_text_color']) ? $tools['popup_text_color'] : 'rgba(76,76,76,1)';

	$css .= "background-color: {$popup_bg};";
	$css .= "color: {$popup_text_color};";
	$css .= "}";

	$css .= ".flycart-options-header {";

	$popup_header_bg = isset($tools['popup_header_bg']) ? $tools['popup_header_bg'] : 'rgba(245,245,245,1)';
	$css .= "background-color: {$popup_header_bg};";

	if (isset($tools['popup_header_shadow_on']) && $tools['popup_header_shadow_on'] === 'true') {
		$popup_header_shadow = isset($tools['popup_header_shadow']) ? $tools['popup_header_shadow'] : 'rgba(167,167,167,0.05)';

		$css .= "-webkit-box-shadow: 0px 1px 0px {$popup_header_shadow};";
		$css .= "box-shadow: 0px 1px 0px {$popup_header_shadow};";
	}

	$popup_header_text = isset($tools['popup_header_text']) ? $tools['popup_header_text'] : 'rgba(75,75,75,1)';
	$css .= "color: {$popup_header_text};";

	if (isset($tools['popup_radius']) && $tools['popup_radius'] === 'true') {
		$popup_radius_head = (isset($tools['popup_radiust']) ? $tools['popup_radiust'] : '0') . (isset($tools['popup_radiusval']) ? $tools['popup_radiusval'] : 'px') . " "
											 . (isset($tools['popup_radiusr']) ? $tools['popup_radiusr'] : '0') . (isset($tools['popup_radiusval']) ? $tools['popup_radiusval'] : 'px');

		$css .= "-webkit-border-radius: {$popup_radius_head} 0 0;";
		$css .= "border-radius: {$popup_radius_head} 0 0;";
	}

	$css .= "}";

	if (isset($tools['popup_header_items']) && $tools['popup_header_items'] === 'true' ||
		isset($tools['popup_header_names']) && $tools['popup_header_names'] === 'true' ||
		isset($tools['popup_header_weight']) && $tools['popup_header_weight'] === 'true' ||
		isset($tools['popup_header_total']) && $tools['popup_header_total'] === 'true') {

		$popup_header_info = isset($tools['popup_header_info']) ? $tools['popup_header_info'] : 'rgba(75,75,75,1)';
		$css .= ".flycart-header-info {color: {$popup_header_info};}";
	}

	$popup_header_close = isset($tools['popup_header_close']) ? $tools['popup_header_close'] : 'rgba(123,123,123,1)';
	$popup_header_close_hover = isset($tools['popup_header_close_hover']) ? $tools['popup_header_close_hover'] : 'rgba(153,144,144,1)';
	$popup_imgage_width = isset($tools['popup_imgage_width']) ? $tools['popup_imgage_width'] + 18 : '65';
	$popup_link_color = isset($tools['popup_link_color']) ? $tools['popup_link_color'] : 'rgba(56,176,227,1)';
	$popup_link_hover_color = isset($tools['popup_link_hover_color']) ? $tools['popup_link_hover_color'] : 'rgba(119,201,236,1)';
	$popup_alert_color = isset($tools['popup_alert_color']) ? $tools['popup_alert_color'] : 'rgba(255,0,0,1)';
	$popup_info_color = isset($tools['popup_info_color']) ? $tools['popup_info_color'] : 'rgba(255,0,0,1)';
	$popup_price_color = isset($tools['popup_price_color']) ? $tools['popup_price_color'] : 'rgba(0,0,0,1)';
	$popup_price_old_color = isset($tools['popup_price_old_color']) ? $tools['popup_price_old_color'] : 'rgba(0,0,0,1)';
	$popup_price_special_color = isset($tools['popup_price_special_color']) ? $tools['popup_price_special_color'] : 'rgba(0,0,0,1)';
	$popup_remove = isset($tools['popup_remove']) ? $tools['popup_remove'] : 'rgba(163,163,163,1)';
	$popup_remove_hover = isset($tools['popup_remove_hover']) ? $tools['popup_remove_hover'] : 'rgba(186,185,185,1)';

	$css .= ".flycart-options-close {fill: {$popup_header_close};}";
	$css .= ".flycart-options-close:hover {fill: {$popup_header_close_hover};}";

	if (isset($tools['popup_imgage']) && $tools['popup_imgage'] === 'true') {
		$css .= ".flycart-product-name {margin-left: {$popup_imgage_width}px;}";
	}

	$css .= ".flycart-product-name a {color: {$popup_link_color};}";
	$css .= ".flycart-product-name a:hover {color: {$popup_link_hover_color};}";
	$css .= ".maximum-alert {color: {$popup_alert_color};}";
	$css .= ".minimum-alert {color: {$popup_info_color};}";
	$css .= ".flycart-product-total, .flycart-options-footer {color: {$popup_price_color};}";
	$css .= ".flycart-product-total .kw-price-old, .flycart-options-footer .flycart-options-price-old {color: {$popup_price_old_color};}";
	$css .= ".flycart-product-total .kw-price-new, .flycart-options-footer .flycart-options-price-special {color: {$popup_price_special_color};}";
	$css .= ".flycart-product-remove-button {fill: {$popup_remove};}";
	$css .= ".flycart-product-remove-button:hover {fill: {$popup_remove_hover};}";

	if (isset($tools['popup_empty_type']) && $tools['popup_empty_type'] === 'text') {
		$popup_empty_text = isset($tools['popup_empty_text']) ? $tools['popup_empty_text'] : 'rgba(125,125,125,1)';
		$css .= "#flycart-dialog .empty-cart {color: {$popup_empty_text};}";
	}

	if (isset($tools['module_empty_type']) && $tools['module_empty_type'] === 'text') {
		$module_empty_text = isset($tools['module_empty_text']) ? $tools['module_empty_text'] : 'rgba(75,75,75,1)';
		$css .= ".flycart-module-container .empty-cart {color: {$module_empty_text};}";
	} else {
		$module_empty_image_height = isset($tools['module_empty_image']['height']) ? $tools['module_empty_image']['height'] : '156';
		$css .= ".flycart-module-container .empty-cart {min-height: {$module_empty_image_height}px;}";
	}

	$css .= ".flycart-options-footer {";

	$popup_footer_bg = isset($tools['popup_footer_bg']) ? $tools['popup_footer_bg'] : 'rgba(245,245,245,1)';
	$css .= "background-color: {$popup_footer_bg};";

	if (isset($tools['popup_footer_shadow_on']) && $tools['popup_footer_shadow_on'] === 'true') {
		$popup_footer_shadow = isset($tools['popup_footer_shadow']) ? $tools['popup_footer_shadow'] : 'rgba(167,167,167,0.05)';

		$css .= "-webkit-box-shadow: 0px -1px 0px {$popup_footer_shadow};";
		$css .= "box-shadow: 0px -1px 0px {$popup_footer_shadow};";
	}

	if (isset($tools['popup_radius']) && $tools['popup_radius'] === 'true') {
		$popup_radius_footer = (isset($tools['popup_radiusb']) ? $tools['popup_radiusb'] : '0') . (isset($tools['popup_radiusval']) ? $tools['popup_radiusval'] : 'px') . " "
												 . (isset($tools['popup_radiusl']) ? $tools['popup_radiusl'] : '0') . (isset($tools['popup_radiusval']) ? $tools['popup_radiusval'] : 'px');

		$css .= "-webkit-border-radius: 0 0 {$popup_radius_footer};";
		$css .= "border-radius: 0 0 {$popup_radius_footer};";
	}
	$css .= "}";

	if (isset($tools['popup_footer_items']) && $tools['popup_footer_items'] === 'true' ||
		isset($tools['popup_footer_names']) && $tools['popup_footer_names'] === 'true' ||
		isset($tools['popup_footer_weight']) && $tools['popup_footer_weight'] === 'true' ||
		isset($tools['popup_footer_total']) && $tools['popup_footer_total'] === 'true') {

		$popup_footer_info = isset($tools['popup_footer_info']) ? $tools['popup_footer_info'] : 'rgba(75,75,75,1)';
		$css .= ".flycart-footer-info {color: {$popup_footer_info};}";
	}

	if (isset($tools['continue_btn']) && $tools['continue_btn'] === 'true') {
		$continue_btn_bg = isset($tools['continue_btn_bg']) ? $tools['continue_btn_bg'] : 'rgba(189,189,189,1.00)';
		$continue_btn_text = isset($tools['continue_btn_text']) ? $tools['continue_btn_text'] : 'rgba(255,255,255,1)';
		$continue_btn_bg_hover = isset($tools['continue_btn_bg_hover']) ? $tools['continue_btn_bg_hover'] : 'rgba(177,177,177,1.00)';
		$continue_btn_text_hover = isset($tools['continue_btn_text_hover']) ? $tools['continue_btn_text_hover'] : 'rgba(255,255,255,1)';

		$css .= ".close-button {background-color: {$continue_btn_bg} !important;color: {$continue_btn_text} !important;fill: {$continue_btn_text} !important;}";
		$css .= ".close-button:hover {background-color: {$continue_btn_bg_hover} !important;color: {$continue_btn_text_hover} !important;fill: {$continue_btn_text_hover} !important;}";
	}

	if (isset($tools['viewcart_btn']) && $tools['viewcart_btn'] === 'true') {
		$viewcart_btn_bg = isset($tools['viewcart_btn_bg']) ? $tools['viewcart_btn_bg'] : 'rgba(242,166,19,1)';
		$viewcart_btn_text = isset($tools['viewcart_btn_text']) ? $tools['viewcart_btn_text'] : 'rgba(255,255,255,1)';
		$viewcart_btn_bg_hover = isset($tools['viewcart_btn_bg_hover']) ? $tools['viewcart_btn_bg_hover'] : 'rgba(241,146,9,1)';
		$viewcart_btn_text_hover = isset($tools['viewcart_btn_text_hover']) ? $tools['viewcart_btn_text_hover'] : 'rgba(255,255,255,1)';

		$css .= ".tocart-button {background-color: {$viewcart_btn_bg} !important;color: {$viewcart_btn_text} !important;fill: {$viewcart_btn_text} !important;}";
		$css .= ".tocart-button:hover {background-color: {$viewcart_btn_bg_hover} !important;color: {$viewcart_btn_text_hover} !important;fill: {$viewcart_btn_text_hover} !important;}";
	}

	if (isset($tools['checkout_btn']) && $tools['checkout_btn'] === 'true') {
		$checkout_btn_bg = isset($tools['checkout_btn_bg']) ? $tools['checkout_btn_bg'] : 'rgba(34,178,43,1)';
		$checkout_btn_text = isset($tools['checkout_btn_text']) ? $tools['checkout_btn_text'] : 'rgba(255,255,255,1)';
		$checkout_btn_bg_hover = isset($tools['checkout_btn_bg_hover']) ? $tools['checkout_btn_bg_hover'] : 'rgba(7,153,22,1.00)';
		$checkout_btn_text_hover = isset($tools['checkout_btn_text_hover']) ? $tools['checkout_btn_text_hover'] : 'rgba(255,255,255,1)';

		$css .= ".checkout-button {background-color: {$checkout_btn_bg} !important;color: {$checkout_btn_text} !important;fill: {$checkout_btn_text} !important;}";
		$css .= ".checkout-button:hover {background-color: {$checkout_btn_bg_hover} !important;color: {$checkout_btn_text_hover} !important;fill: {$checkout_btn_text_hover} !important;}";
	}

	$options_btn_bg = isset($tools['options_btn_bg']) ? $tools['options_btn_bg'] : 'rgba(34,178,43,1)';
	$options_btn_text = isset($tools['options_btn_text']) ? $tools['options_btn_text'] : 'rgba(255,255,255,1)';
	$options_btn_bg_hover = isset($tools['options_btn_bg_hover']) ? $tools['options_btn_bg_hover'] : 'rgba(7,153,22,1.00)';
	$options_text_hover = isset($tools['options_text_hover']) ? $tools['options_text_hover'] : 'rgba(255,255,255,1)';
	$options_btn_bg_disabled = isset($tools['options_btn_bg_disabled']) ? $tools['options_btn_bg_disabled'] : 'rgba(167,167,167,1)';
	$options_btn_text_disabled = isset($tools['options_btn_text_disabled']) ? $tools['options_btn_text_disabled'] : 'rgba(255,255,255,1)';
	$options_file_btn_bg = isset($tools['options_file_btn_bg']) ? $tools['options_file_btn_bg'] : 'rgba(18,163,226,1)';
	$options_file_btn_text = isset($tools['options_file_btn_text']) ? $tools['options_file_btn_text'] : 'rgba(255,255,255,1)';
	$options_file_btn_bg_hover = isset($tools['options_file_btn_bg_hover']) ? $tools['options_file_btn_bg_hover'] : 'rgba(30,138,210,1)';
	$options_file_text_hover = isset($tools['options_file_text_hover']) ? $tools['options_file_text_hover'] : 'rgba(255,255,255,1)';

	$css .= ".options-button {background-color: {$options_btn_bg} !important;color: {$options_btn_text} !important;}";
	$css .= ".options-button:hover {background-color: {$options_btn_bg_hover} !important;color: {$options_text_hover} !important;}";
	$css .= ".flycart-options-button.flycart-button.options-button.loadind, .flycart-options-button.flycart-button.options-button.loadind:hover, .flycart-options-button.flycart-button.options-button.invalid, .flycart-options-button.flycart-button.options-button.invalid:hover {background-color: {$options_btn_bg_disabled} !important;color: {$options_btn_text_disabled} !important;}";
	$css .= ".flycart-file-button {background-color: {$options_file_btn_bg} !important;color: {$options_file_btn_text} !important;}";
	$css .= ".flycart-file-button.hover {background-color: {$options_file_btn_bg_hover} !important;color: {$options_file_text_hover} !important;}";

	if (isset($tools['popup_coupon']) && $tools['popup_coupon'] === 'true') {
		$coupon_continue_btn_bg = isset($tools['coupon_btn_bg']) ? $tools['coupon_btn_bg'] : 'rgba(189,189,189,1.00)';
		$coupon_continue_btn_text = isset($tools['coupon_btn_text']) ? $tools['coupon_btn_text'] : 'rgba(255,255,255,1)';
		$coupon_continue_btn_bg_hover = isset($tools['coupon_btn_bg_hover']) ? $tools['coupon_btn_bg_hover'] : 'rgba(177,177,177,1.00)';
		$coupon_continue_btn_text_hover = isset($tools['coupone_text_hover']) ? $tools['coupone_text_hover'] : 'rgba(255,255,255,1)';

		$css .= ".coupon-button {background-color: {$coupon_continue_btn_bg} !important;color: {$coupon_continue_btn_text} !important;fill: {$continue_btn_text} !important;}";
		$css .= ".coupon-button:hover {background-color: {$coupon_continue_btn_bg_hover} !important;color: {$coupon_continue_btn_text_hover} !important;fill: {$continue_btn_text_hover} !important;}";
	}
}

if (isset($tools['module_type']) && $tools['module_type'] === 'widget') {
	$widget_image_src = isset($tools['widget_image']['src']) ? $tools['widget_image']['src'] : 'circle-with-blue-cart.png';
	$widget_image_size = (isset($tools['widget_image']['width']) ? $tools['widget_image']['width'] : '76') . "px "
										 . (isset($tools['widget_image']['height']) ? $tools['widget_image']['height'] : '76') . "px";

	$widget_width = isset($tools['widget_width']) ? $tools['widget_width'] : '76';
	$widget_height = isset($tools['widget_height']) ? $tools['widget_height'] : '76';
	$widget_count_position = isset($tools['widget_count_position']) ? $tools['widget_count_position'] : 'fixed';
	$widget_count_t = is_numeric($tools['widget_count_t']) ? $tools['widget_count_t'] . 'px' : 'auto';
	$widget_count_r = is_numeric($tools['widget_count_r']) ? $tools['widget_count_r'] . 'px' : 'auto';
	$widget_count_b = is_numeric($tools['widget_count_b']) ? $tools['widget_count_b'] . 'px' : 'auto';
	$widget_count_l = is_numeric($tools['widget_count_l']) ? $tools['widget_count_l'] . 'px' : 'auto';

	$css .= "#flycart-widget {";
	$css .= "background: url({$url}kw_application/flycart/images/widget/{$widget_image_src}) no-repeat;";
	$css .= "background-size: {$widget_image_size};";
	$css .= "width: {$widget_width}px;";
	$css .= "height: {$widget_height}px;";
	$css .= "position: {$widget_count_position};";
	$css .= "top: {$widget_count_t};";
	$css .= "right: {$widget_count_r};";
	$css .= "bottom: {$widget_count_b};";
	$css .= "left: {$widget_count_l};";
	$css .= "}";
	$css .= "#flycart-widget .flycart-widget-count {";

	if (isset($tools['widget_count_bg_tools']) && $tools['widget_count_bg_tools'] === 'true') {
		$widget_count_bg = isset($tools['widget_count_bg']) ? $tools['widget_count_bg'] : 'rgba(24,166,229,1)';

		$widget_count_padding = (isset($tools['widget_count_pt']) ? $tools['widget_count_pt'] : '8') . "px "
													. (isset($tools['widget_count_pr']) ? $tools['widget_count_pr'] : '8') . "px "
													. (isset($tools['widget_count_pb']) ? $tools['widget_count_pb'] : '8') . "px "
													. (isset($tools['widget_count_pl']) ? $tools['widget_count_pl'] : '8') . "px";

		$widget_radius = (isset($tools['widget_count_rt']) ? $tools['widget_count_rt'] : '50') . (isset($tools['widget_count_val']) ? $tools['widget_count_val'] : '%') . " "
									 . (isset($tools['widget_count_rr']) ? $tools['widget_count_rr'] : '50') . (isset($tools['widget_count_val']) ? $tools['widget_count_val'] : '%') . " "
									 . (isset($tools['widget_count_rb']) ? $tools['widget_count_rb'] : '50') . (isset($tools['widget_count_val']) ? $tools['widget_count_val'] : '%') . " "
									 . (isset($tools['widget_count_rl']) ? $tools['widget_count_rl'] : '50') . (isset($tools['widget_count_val']) ? $tools['widget_count_val'] : '%');

		$css .= "background: {$widget_count_bg};";
		$css .= "padding: {$widget_count_padding};";
		$css .= "-webkit-border-radius: {$widget_radius};";
		$css .= "border-radius: {$widget_radius};";
	}

	$widget_count_posleft = (isset($tools['widget_count_posleft']) ? $tools['widget_count_posleft'] : '10') . (isset($tools['widget_count_posval']) ? $tools['widget_count_posval'] : 'px');
	$widget_count_postop = (isset($tools['widget_count_postop']) ? $tools['widget_count_postop'] : '13') . (isset($tools['widget_count_posval']) ? $tools['widget_count_posval'] : 'px');
	$widget_count_fontsize = isset($tools['widget_count_fontsize']) ? $tools['widget_count_fontsize'] : '12';
	$widget_count_color = isset($tools['widget_count_color']) ? $tools['widget_count_color'] : 'rgba(255,255,255,1)';

	$css .= "left: {$widget_count_posleft};";
	$css .= "top: {$widget_count_postop};";
	$css .= "font-size: {$widget_count_fontsize}px;";
	$css .= "line-height: {$widget_count_fontsize}px;";
	$css .= "color: {$widget_count_color};";
	$css .= "}";
}

if (isset($tools['noti_type']) && $tools['noti_type'] === 'notice') {
	$css .= "#flycart-notice {";
	if (isset($tools['noti_radius']) && $tools['noti_radius'] === 'true') {
		$noti_radius = (isset($tools['noti_radiust']) ? $tools['noti_radiust'] : '3') . (isset($tools['noti_radiusval']) ? $tools['noti_radiusval'] : 'px') . " "
								 . (isset($tools['noti_radiusr']) ? $tools['noti_radiusr'] : '3') . (isset($tools['noti_radiusval']) ? $tools['noti_radiusval'] : 'px') . " "
								 . (isset($tools['noti_radiusb']) ? $tools['noti_radiusb'] : '3') . (isset($tools['noti_radiusval']) ? $tools['noti_radiusval'] : 'px') . " "
								 . (isset($tools['noti_radiusl']) ? $tools['noti_radiusl'] : '3') . (isset($tools['noti_radiusval']) ? $tools['noti_radiusval'] : 'px');

		$css .= "-webkit-border-radius: {$noti_radius};";
		$css .= "border-radius: {$noti_radius};";
	}

	if (isset($tools['noti_shadow_on']) && $tools['noti_shadow_on'] === 'true') {
		$noti_shadow = (isset($tools['noti_shadowx']) ? $tools['noti_shadowx'] : '0') . "px "
								 . (isset($tools['noti_shadowy']) ? $tools['noti_shadowy'] : '0') . "px "
								 . (isset($tools['noti_shadowblur']) ? $tools['noti_shadowblur'] : '10') . "px "
								 . (isset($tools['noti_shadowcolor']) ? $tools['noti_shadowcolor'] : 'rgba(0,0,0,0.15)');

		$css .= "-webkit-box-shadow: {$noti_shadow};";
		$css .= "box-shadow: {$noti_shadow};";
	}

	$noti_bg = isset($tools['noti_bg']) ? $tools['noti_bg'] : 'rgba(255,255,255,1)';
	$noti_width_min = isset($tools['noti_width_min']) ? $tools['noti_width_min'] : '50';
	$noti_width_max = isset($tools['noti_width_max']) ? $tools['noti_width_max'] : '300';
	$noti_height_min = isset($tools['noti_height_min']) ? $tools['noti_height_min'] : '50';
	$noti_height_max = isset($tools['noti_height_max']) ? $tools['noti_height_max'] : '200';
	$noti_pos_t = isset($tools['noti_pos_t']) ? $tools['noti_pos_t'] : '20';
	$noti_pos_r = isset($tools['noti_pos_r']) ? $tools['noti_pos_r'] : '20';
	$noti_pos_b = isset($tools['noti_pos_b']) ? $tools['noti_pos_b'] : 'auto';
	$noti_pos_l = isset($tools['noti_pos_l']) ? $tools['noti_pos_l'] : 'auto';
	$noti_image_width = isset($tools['noti_image_width']) ? $tools['noti_image_width'] : '70';

	$css .= "background: {$noti_bg};";
	$css .= "min-width: {$noti_width_min}px;";
	$css .= "max-width: {$noti_width_max}px;";
	$css .= "min-height: {$noti_height_min}px;";
	$css .= "max-height: {$noti_height_max}px;";
	$css .= "top: {$noti_pos_t}px;";
	$css .= "right: {$noti_pos_r}px;";
	$css .= "bottom: {$noti_pos_b}px;";
	$css .= "left: {$noti_pos_l}px;";
	$css .= "}";
	$css .= "#flycart-notice .notice-product-image {width: {$noti_image_width}px;}";
	$css .= "#flycart-notice .notice-product-name {";

	if (isset($tools['noti_image']) && $tools['noti_image'] === 'true') {
		$noti_image_left = isset($tools['noti_image_width']) ? ($tools['noti_image_width'] + 10) : '80';
		$css .= "margin-left: {$noti_image_left}px;";
	}

	$noti_text = isset($tools['noti_text']) ? $tools['noti_text'] : 'rgba(101,101,101,1)';
	$noti_link = isset($tools['noti_link']) ? $tools['noti_link'] : 'rgba(56,176,227,1)';
	$noti_link_hover = isset($tools['noti_link_hover']) ? $tools['noti_link_hover'] : 'rgba(56,176,227,1)';

	$css .= "color: {$noti_text};";
	$css .= "}";
	$css .= "#flycart-notice a {color: {$noti_link};}";
	$css .= "#flycart-notice a:hover {color: {$noti_link_hover};}";

	if (isset($tools['continue_noti_btn']) && $tools['continue_noti_btn'] === 'true') {
		$continue_noti_btn_bg = isset($tools['continue_noti_btn_bg']) ? $tools['continue_noti_btn_bg'] : 'rgba(189,189,189,1)';
		$continue_noti_btn_text = isset($tools['continue_noti_btn_text']) ? $tools['continue_noti_btn_text'] : 'rgba(255,255,255,1)';
		$continue_noti_btn_bg_hover = isset($tools['continue_noti_btn_bg_hover']) ? $tools['continue_noti_btn_bg_hover'] : 'rgba(177,177,177,1)';
		$continue_noti_btn_text_hover = isset($tools['continue_noti_btn_text_hover']) ? $tools['continue_noti_btn_text_hover'] : 'rgba(255,255,255,1)';

		$css .= "#flycart-notice .close-button {background-color: {$continue_noti_btn_bg} !important;color: {$continue_noti_btn_text} !important;}";
		$css .= "#flycart-notice .close-button:hover {background-color: {$continue_noti_btn_bg_hover} !important;color: {$continue_noti_btn_text_hover} !important;}";
	}

	if (isset($tools['viewcart_noti_btn']) && $tools['viewcart_noti_btn'] === 'true') {
		$viewcart_noti_btn_bg = isset($tools['viewcart_noti_btn_bg']) ? $tools['viewcart_noti_btn_bg'] : 'rgba(30,138,210,1)';
		$viewcart_noti_btn_text = isset($tools['viewcart_noti_btn_text']) ? $tools['viewcart_noti_btn_text'] : 'rgba(255,255,255,1)';
		$viewcart_noti_btn_bg_hover = isset($tools['viewcart_noti_btn_bg_hover']) ? $tools['viewcart_noti_btn_bg_hover'] : 'rgba(44,156,230,1)';
		$viewcart_noti_btn_text_hover = isset($tools['viewcart_noti_btn_text_hover']) ? $tools['viewcart_noti_btn_text_hover'] : 'rgba(255,255,255,1)';

		$css .= "#flycart-notice .tocart-button {background-color: {$viewcart_noti_btn_bg} !important;color: {$viewcart_noti_btn_text} !important;}";
		$css .= "	#flycart-notice .tocart-button:hover {background-color: {$viewcart_noti_btn_bg_hover} !important;color: {$viewcart_noti_btn_text_hover} !important;}";
	}

	$noti_cross = isset($tools['noti_cross']) ? $tools['noti_cross'] : 'rgba(123,123,123,1)';
	$noti_cross_hover = isset($tools['noti_cross_hover']) ? $tools['noti_cross_hover'] : 'rgba(69,65,65,1)';

	$css .= "#flycart-notice .flycart-options-close {fill: {$noti_cross};}";
	$css .= "#flycart-notice .flycart-options-close:hover {fill: {$noti_cross_hover};}";
}

if (isset($tools['noti_type']) && $tools['noti_type'] === 'light') {
	$css .= ".mfp-bg.light-overlay {";
	if (isset($tools['light_bg_on']) && $tools['light_bg_on'] === 'true') {
		$light_overlay_bg = isset($tools['light_overlay_bg']) ? $tools['light_overlay_bg'] : 'rgba(0,0,0,0.6)';
		$css .= "background-color: {$light_overlay_bg};";
	}
	if (isset($tools['light_image_on']) && $tools['light_image_on'] === 'true') {
		$light_overlay_image = isset($tools['light_overlay_image']['src']) ? $tools['light_overlay_image']['src'] : 'carbon.png';
		$css .= "background-image: url({$url}kw_application/flycart/images/bg/{$light_overlay_image});";
	}
	$css .= "}";
	$css .= "#flycart-notification.popup {";

	$light_min_width = isset($tools['light_min_width']) ? $tools['light_min_width'] : '300';
	$light_max_width = isset($tools['light_max_width']) ? $tools['light_max_width'] : '560';
	$light_min_height = isset($tools['light_min_height']) ? $tools['light_min_height'] : '100';
	$light_max_height = isset($tools['light_max_height']) ? $tools['light_max_height'] : '560';

	$css .= "min-width: {$light_min_width}px;";
	$css .= "max-width: {$light_max_width}px;";
	$css .= "min-height: {$light_min_height}px;";
	$css .= "max-height: {$light_max_height}px;";

	if (isset($tools['light_shadow_on']) && $tools['light_shadow_on'] === 'true') {
		$light_shadow = (isset($tools['light_shadowx']) ? $tools['light_shadowx'] : '0') . "px "
									. (isset($tools['light_shadowy']) ? $tools['light_shadowy'] : '0') . "px "
									. (isset($tools['light_shadowblur']) ? $tools['light_shadowblur'] : '0') . "px "
									. (isset($tools['light_shadowcolor']) ? $tools['light_shadowcolor'] : 'rgba(255,255,255,1)');

		$css .= "-webkit-box-shadow: {$light_shadow};";
		$css .= "box-shadow: {$light_shadow};";
	}

	if (isset($tools['light_radius']) && $tools['light_radius'] === 'true') {
		$light_radius = (isset($tools['light_radiust']) ? $tools['light_radiust'] : '3') . (isset($tools['light_radiusval']) ? $tools['light_radiusval'] : 'px') . " "
									. (isset($tools['light_radiusr']) ? $tools['light_radiusr'] : '3') . (isset($tools['light_radiusval']) ? $tools['light_radiusval'] : 'px') . " "
									. (isset($tools['light_radiusb']) ? $tools['light_radiusb'] : '3') . (isset($tools['light_radiusval']) ? $tools['light_radiusval'] : 'px') . " "
									. (isset($tools['light_radiusl']) ? $tools['light_radiusl'] : '3') . (isset($tools['light_radiusval']) ? $tools['light_radiusval'] : 'px');

		$css .= "-webkit-border-radius: {$light_radius};";
		$css .= "border-radius: {$light_radius};";
	}

	$light_bg = isset($tools['light_bg']) ? $tools['light_bg'] : 'rgba(255,255,255,1)';
	$light_text_color = isset($tools['light_text_color']) ? $tools['light_text_color'] : 'rgba(76,76,76,1)';

	$css .= "background: {$light_bg};";
	$css .= "color: {$light_text_color};";
	$css .= "}";

	if (isset($tools['light_image']) && $tools['light_image'] === 'true') {
		$light_image_left = isset($tools['light_image_width']) ? ($tools['light_image_width'] + 20) : '135';
		$css .= "#flycart-notification.popup .notification-product-right {margin-left: {$light_image_left}px;}";
	}

	$light_link_color = isset($tools['light_link_color']) ? $tools['light_link_color'] : 'rgba(56,176,227,1)';
	$light_link_hover_color = isset($tools['light_link_hover_color']) ? $tools['light_link_hover_color'] : 'rgba(56,176,227,1)';

	$css .= "#flycart-notification.popup a {color: {$light_link_color};}";
	$css .= "#flycart-notification.popup a:hover {color: {$light_link_hover_color};}";

	if (isset($tools['continue_light_btn']) && $tools['continue_light_btn'] === 'true') {
		$continue_light_btn_bg = isset($tools['continue_light_btn_bg']) ? $tools['continue_light_btn_bg'] : 'rgba(189,189,189,1)';
		$continue_light_btn_text = isset($tools['continue_light_btn_text']) ? $tools['continue_light_btn_text'] : 'rgba(255,255,255,1)';
		$continue_light_btn_bg_hover = isset($tools['continue_light_btn_bg_hover']) ? $tools['continue_light_btn_bg_hover'] : 'rgba(177,177,177,1)';
		$continue_light_btn_text_hover = isset($tools['continue_light_btn_text_hover']) ? $tools['continue_light_btn_text_hover'] : 'rgba(255,255,255,1)';

		$css .= "#flycart-notification.popup .close-button {background-color: {$continue_light_btn_bg} !important;color: {$continue_light_btn_text} !important;}";
		$css .= "#flycart-notification.popup .close-button:hover {background-color: {$continue_light_btn_bg_hover} !important;color: {$continue_light_btn_text_hover} !important;}";
	}

	if (isset($tools['viewcart_light_btn']) && $tools['viewcart_light_btn'] === 'true') {
		$viewcart_light_btn_bg = isset($tools['viewcart_light_btn_bg']) ? $tools['viewcart_light_btn_bg'] : 'rgba(30,138,210,1)';
		$viewcart_light_btn_text = isset($tools['viewcart_light_btn_text']) ? $tools['viewcart_light_btn_text'] : 'rgba(255,255,255,1)';
		$viewcart_light_btn_bg_hover = isset($tools['viewcart_light_btn_bg_hover']) ? $tools['viewcart_light_btn_bg_hover'] : 'rgba(44,156,230,1)';
		$viewcart_light_btn_text_hover = isset($tools['viewcart_light_btn_text_hover']) ? $tools['viewcart_light_btn_text_hover'] : 'rgba(255,255,255,1)';

		$css .= "#flycart-notification.popup .tocart-button {background-color: {$viewcart_light_btn_bg} !important;color: {$viewcart_light_btn_text} !important;}";
		$css .= "#flycart-notification.popup .tocart-button:hover {background-color: {$viewcart_light_btn_bg_hover} !important;color: {$viewcart_light_btn_text_hover} !important;}";
	}

	$light_cross = isset($tools['light_cross']) ? $tools['light_cross'] : 'rgba(163,163,163,1)';
	$light_cross_hover = isset($tools['light_cross_hover']) ? $tools['light_cross_hover'] : 'rgba(186,185,185,1)';

	$css .= "#flycart-notification.popup .flycart-options-close {fill: {$light_cross};}";
	$css .= "#flycart-notification.popup .flycart-options-close:hover {fill: {$light_cross_hover};}";
}

if (isset($tools['noti_type']) && $tools['noti_type'] === 'advanced') {
	$css .= ".mfp-bg.noti-overlay {";
	if (isset($tools['advanced_bg_on']) && $tools['advanced_bg_on'] === 'true') {
		$advanced_overlay_bg = isset($tools['advanced_overlay_bg']) ? $tools['advanced_overlay_bg'] : 'rgba(0,0,0,0.6)';
		$css .= "background-color: {$advanced_overlay_bg};";
	}

	if (isset($tools['advanced_image_on']) && $tools['advanced_image_on'] === 'true') {
		$advanced_overlay_image = isset($tools['advanced_overlay_image']['src']) ? $tools['advanced_overlay_image']['src'] : 'carbon.png';
		$css .= "background-image: url({$url}kw_application/flycart/images/bg/{$advanced_overlay_image});";
	}
	$css .= "}";

	$css .= "#flycart-notification.popup {";

	$advanced_min_width = isset($tools['advanced_min_width']) ? $tools['advanced_min_width'] : '300';
	$advanced_max_width = isset($tools['advanced_max_width']) ? $tools['advanced_max_width'] : '700';
	$advanced_min_height = isset($tools['advanced_min_height']) ? $tools['advanced_min_height'] : '50';
	$advanced_max_height = isset($tools['advanced_max_height']) ? $tools['advanced_max_height'] : '700';

	$css .= "min-width: {$advanced_min_width}px;";
	$css .= "max-width: {$advanced_max_width}px;";
	$css .= "min-height: {$advanced_min_height}px;";
	$css .= "max-height: {$advanced_max_height}px;";

	if (isset($tools['advanced_shadow_on']) && $tools['advanced_shadow_on'] === 'true') {
		$advanced_shadow = (isset($tools['advanced_shadowx']) ? $tools['advanced_shadowx'] : '1') . "px "
										 . (isset($tools['advanced_shadowy']) ? $tools['advanced_shadowy'] : '1') . "px "
										 . (isset($tools['advanced_shadowblur']) ? $tools['advanced_shadowblur'] : '5') . "px "
										 . (isset($tools['advanced_shadowcolor']) ? $tools['advanced_shadowcolor'] : 'rgba(46,46,46,0.36)');

		$css .= "-webkit-box-shadow: {$advanced_shadow};";
		$css .= "box-shadow: {$advanced_shadow};";
	}

	if (isset($tools['advanced_radius']) && $tools['advanced_radius'] === 'true') {
		$advanced_radius = (isset($tools['advanced_radiust']) ? $tools['advanced_radiust'] : '3') . (isset($tools['advanced_radiusval']) ? $tools['advanced_radiusval'] : 'px') . " "
										 . (isset($tools['advanced_radiusr']) ? $tools['advanced_radiusr'] : '3') . (isset($tools['advanced_radiusval']) ? $tools['advanced_radiusval'] : 'px') . " "
										 . (isset($tools['advanced_radiusb']) ? $tools['advanced_radiusb'] : '3') . (isset($tools['advanced_radiusval']) ? $tools['advanced_radiusval'] : 'px') . " "
										 . (isset($tools['advanced_radiusl']) ? $tools['advanced_radiusl'] : '3') . (isset($tools['advanced_radiusval']) ? $tools['advanced_radiusval'] : 'px');

		$css .= "-webkit-border-radius: {$advanced_radius};";
		$css .= "border-radius: {$advanced_radius};";
	}

	$advanced_bg = isset($tools['advanced_bg']) ? $tools['advanced_bg'] : 'rgba(255,255,255,1)';

	$css .= "background:{$advanced_bg};";
	$css .= "}";

	$css .= ".flycart-noti-header {";
	if (isset($tools['advanced_radius']) && $tools['advanced_radius'] === 'true') {
		$advanced_radius_head = (isset($tools['advanced_radiust']) ? $tools['advanced_radiust'] : '3') . (isset($tools['advanced_radiusval']) ? $tools['advanced_radiusval'] : 'px') . " "
													. (isset($tools['advanced_radiusr']) ? $tools['advanced_radiusr'] : '3') . (isset($tools['advanced_radiusval']) ? $tools['advanced_radiusval'] : 'px');

		$css .= "-webkit-border-radius: {$advanced_radius_head} 0 0;";
		$css .= "border-radius: {$advanced_radius_head} 0 0;";
	}
	$css .= "background: {$advanced_bg};";
	$css .= "}";

	$advanced_left_title = isset($tools['advanced_left_title']) ? $tools['advanced_left_title'] : 'rgba(34,178,43,1)';
	$css .= "#flycart-notification.popup h3 {color: {$advanced_left_title};}";

	if (isset($tools['advanced_right']) && $tools['advanced_right'] === 'false') {
		$advanced_max_width_right = isset($tools['advanced_max_width']) ? ($tools['advanced_max_width'] / 2) : '350';

		$css .= "#flycart-notification.popup {max-width: {$advanced_max_width_right}px;}";
		$css .= "#flycart-notification.popup:after {display: none;}";
		$css .= "#flycart-notification.popup .notification-product-block {border: 0 none;}";
	}

	if (isset($tools['advanced_image']) && $tools['advanced_image'] === 'true' && isset($tools['advanced_right']) && $tools['advanced_right'] === 'true') {
		$advanced_image_left = isset($tools['advanced_image_width']) ? ($tools['advanced_image_width'] + 20) : '135';
		$css .= "#flycart-notification.popup .notification-product-info {margin-left: {$advanced_image_left}px;}";
	}

	if (isset($tools['advanced_image']) && $tools['advanced_image'] === 'true' && isset($tools['advanced_right']) && $tools['advanced_right'] === 'false') {
		$advanced_image_full_left = isset($tools['advanced_image_width']) ? ($tools['advanced_image_width'] + 30) : '230';
		$css .= "#flycart-notification.popup .full-width .notification-product-image {margin-left: calc(50% - {$advanced_image_full_left}px);}";
	}

	$advanced_link_color = isset($tools['advanced_link_color']) ? $tools['advanced_link_color'] : 'rgba(56,176,227,1)';
	$advanced_link_hover_color = isset($tools['advanced_link_hover_color']) ? $tools['advanced_link_hover_color'] : '';

	$css .= "#flycart-notification.popup a {color: {$advanced_link_color};}";
	$css .= "#flycart-notification.popup a span {border-color: {$advanced_link_color};}";
	$css .= "#flycart-notification.popup a:hover {color: {$advanced_link_hover_color};}";

	if (isset($tools['advanced_options']) && $tools['advanced_options'] === 'true') {
		$advanced_options_color = isset($tools['advanced_options_color']) ? $tools['advanced_options_color'] : 'rgba(171,171,171,1)';
		$css .= "#flycart-notification.popup .notification-product-options {color: {$advanced_options_color};}";
	}

	if (isset($tools['advanced_price']) && $tools['advanced_price'] === 'true') {
		$advanced_price_color = isset($tools['advanced_price_color']) ? $tools['advanced_price_color'] : 'rgba(76,76,76,1)';
		$advanced_price_old_color = isset($tools['advanced_price_old_color']) ? $tools['advanced_price_old_color'] : 'rgba(129,129,129,1)';
		$advanced_price_special_color = isset($tools['advanced_price_special_color']) ? $tools['advanced_price_special_color'] : 'rgba(255,13,13,1)';

		$css .= "#flycart-notification.popup .notification-product-price {color: {$advanced_price_color};}";
		$css .= "#flycart-notification.popup .notification-product-price-old {color: {$advanced_price_old_color};}";
		$css .= "#flycart-notification.popup .notification-product-price-special {color: {$advanced_price_special_color};}";
	}

	if (isset($tools['advanced_brand']) && $tools['advanced_brand'] === 'true' || isset($tools['advanced_model']) && $tools['advanced_model'] === 'true' || isset($tools['advanced_sku']) && $tools['advanced_sku'] === 'true') {

		$advanced_params = isset($tools['advanced_params']) ? $tools['advanced_params'] : 'rgba(101,101,101,1)';
		$css .= "#flycart-notification.popup .notification-product-params {color: {$advanced_params};}";
	}

	if (isset($tools['continue_advanced_btn']) && $tools['continue_advanced_btn'] === 'true') {
		$continue_advanced_btn_bg = isset($tools['continue_advanced_btn_bg']) ? $tools['continue_advanced_btn_bg'] : 'rgba(189,189,189,1)';
		$continue_advanced_btn_text = isset($tools['continue_advanced_btn_text']) ? $tools['continue_advanced_btn_text'] : 'rgba(255,255,255,1)';
		$continue_advanced_btn_bg_hover = isset($tools['continue_advanced_btn_bg_hover']) ? $tools['continue_advanced_btn_bg_hover'] : 'rgba(177,177,177,1)';
		$continue_advanced_btn_text_hover = isset($tools['continue_advanced_btn_text_hover']) ? $tools['continue_advanced_btn_text_hover'] : 'rgba(255,255,255,1)';

		$css .= "#flycart-notification.popup .close-button {background-color: {$continue_advanced_btn_bg} !important;color: {$continue_advanced_btn_text} !important;}";
		$css .= "#flycart-notification.popup .close-button:hover {background-color: {$continue_advanced_btn_bg_hover} !important;color: {$continue_advanced_btn_text_hover} !important;}";
	}

	if (isset($tools['viewcart_advanced_btn']) && $tools['viewcart_advanced_btn'] === 'true') {
		$viewcart_advanced_btn_bg = isset($tools['viewcart_advanced_btn_bg']) ? $tools['viewcart_advanced_btn_bg'] : 'rgba(30,138,210,1)';
		$viewcart_advanced_btn_text = isset($tools['viewcart_advanced_btn_text']) ? $tools['viewcart_advanced_btn_text'] : 'rgba(255,255,255,1)';
		$viewcart_advanced_btn_bg_hover = isset($tools['viewcart_advanced_btn_bg_hover']) ? $tools['viewcart_advanced_btn_bg_hover'] : 'rgba(44,156,230,1)';
		$viewcart_advanced_btn_text_hover = isset($tools['viewcart_advanced_btn_text_hover']) ? $tools['viewcart_advanced_btn_text_hover'] : 'rgba(255,255,255,1)';

		$css .= "#flycart-notification.popup .tocart-button {background-color: {$viewcart_advanced_btn_bg} !important;color: {$viewcart_advanced_btn_text} !important;}";
		$css .= "#flycart-notification.popup .tocart-button:hover {background-color: {$viewcart_advanced_btn_bg_hover} !important;color: {$viewcart_advanced_btn_text_hover} !important;}";
	}

	$advanced_cross = isset($tools['advanced_cross']) ? $tools['advanced_cross'] : '';
	$advanced_cross_hover = isset($tools['advanced_cross_hover']) ? $tools['advanced_cross_hover'] : '';

	$css .= "#flycart-notification.popup .flycart-options-close {fill: {$advanced_cross};}";
	$css .= "#flycart-notification.popup .flycart-options-close:hover {fill: {$advanced_cross_hover};}";

	if (isset($tools['advanced_right']) && $tools['advanced_right'] === 'true') {
		$advanced_right_bg = isset($tools['advanced_right_bg']) ? $tools['advanced_right_bg'] : 'rgba(250,250,250,1)';
		$advanced_right_text_color = isset($tools['advanced_right_text_color']) ? $tools['advanced_right_text_color'] : 'rgba(101,101,101,1)';
		$advanced_right_title = isset($tools['advanced_right_title']) ? $tools['advanced_right_title'] : 'rgba(75,75,75,1)';

		$css .= "#flycart-notification.advanced:after {background: {$advanced_right_bg};}";
		$css .= "#flycart-notification.popup .right-notification {color: {$advanced_right_text_color};}";
		$css .= "#flycart-notification.popup .right-notification h3 {color: {$advanced_right_title};}";
	}
}

if (isset($tools['effect_type']) && $tools['effect_type'] !== 'off' && isset($tools['effect_type']) && $tools['effect_type'] !== 'custom') {
	$css .= "#flycart-flyer {";

	$effect_frame_width = isset($tools['effect_frame_width']) ? $tools['effect_frame_width'] : '70';
	$effect_frame_height = isset($tools['effect_frame_height']) ? $tools['effect_frame_height'] : '70';

	$css .= "width: {$effect_frame_width}px;";
	$css .= "height: {$effect_frame_height}px;";

	if (isset($tools['effect_frame']) && $tools['effect_frame'] === 'true') {
		$effect_frame_border = (isset($tools['effect_frame_size']) ? $tools['effect_frame_size'] : '1') . "px " . (isset($tools['effect_frame_color']) ? $tools['effect_frame_color'] : 'rgba(231,231,231,1)');

		$effect_frame_bg = (isset($tools['effect_frame_bg']) && $tools['effect_frame_bg'] !== '') ? $tools['effect_frame_bg'] : 'none';

		$effect_frame_padding = (isset($tools['effect_frame_pt']) ? $tools['effect_frame_pt'] : '2') . "px "
													. (isset($tools['effect_frame_pr']) ? $tools['effect_frame_pr'] : '2') . "px "
													. (isset($tools['effect_frame_pb']) ? $tools['effect_frame_pb'] : '2') . "px "
													. (isset($tools['effect_frame_pl']) ? $tools['effect_frame_pl'] : '2') . "px";

		$css .= "border: {$effect_frame_border} solid;";
		$css .= "background-color: {$effect_frame_bg};";
		$css .= "padding: {$effect_frame_padding};";
	}

	if (isset($tools['effect_rotate']) && $tools['effect_rotate'] === 'true') {
		$effect_frame_speed = isset($tools['effect_frame_speed']) ? $tools['effect_frame_speed'] / 1000 : '1000';

		$css .= "-webkit-animation-duration: {$effect_frame_speed}s;";
		$css .= "animation-duration: {$effect_frame_speed}s;";
	}

	if (isset($tools['effect_frame_radius']) && $tools['effect_frame_radius'] === 'true') {
		$effect_frame_radius = (isset($tools['effect_frame_radiust']) ? $tools['effect_frame_radiust'] : '3') . (isset($tools['effect_radiusval']) ? $tools['effect_radiusval'] : 'px') . " "
												 . (isset($tools['effect_frame_radiusr']) ? $tools['effect_frame_radiusr'] : '3') . (isset($tools['effect_radiusval']) ? $tools['effect_radiusval'] : 'px') . " "
												 . (isset($tools['effect_frame_radiusb']) ? $tools['effect_frame_radiusb'] : '3') . (isset($tools['effect_radiusval']) ? $tools['effect_radiusval'] : 'px') . " "
												 . (isset($tools['effect_frame_radiusl']) ? $tools['effect_frame_radiusl'] : '3') . (isset($tools['effect_radiusval']) ? $tools['effect_radiusval'] : 'px');

		$css .= "-webkit-border-radius: {$effect_frame_radius};";
		$css .= "border-radius: {$effect_frame_radius};";
	}

	if (isset($tools['effect_type']) && $tools['effect_type'] === 'image') {
		$effect_image = isset($tools['effect_image']['src']) ? $tools['effect_image']['src'] : 'PurpleCoinStar.png';

		$effect_image_size = (isset($tools['effect_image']['width']) ? $tools['effect_image']['width'] : '63') . "px " . (isset($tools['effect_image']['height']) ? $tools['effect_image']['height'] : '64') . "px";

		$css .= "background-image: url({$url}kw_application/flycart/images/effects/{$effect_image});";
		$css .= "background-size: {$effect_image_size};";
		$css .= "background-position: 50% 50%;";
		$css .= "background-repeat: no-repeat;";
	}
	$css .= "}";
}

if (isset($tools['module_type']) && $tools['module_type'] === 'module') {
	if (isset($tools['tocart_module_btn']) && $tools['tocart_module_btn'] === 'true') {
		$tocart_module_btn_bg = isset($tools['tocart_module_btn_bg']) ? $tools['tocart_module_btn_bg'] : 'rgba(189,189,189,1)';
		$tocart_module_btn_text = isset($tools['tocart_module_btn_text']) ? $tools['tocart_module_btn_text'] : 'rgba(255,255,255,1)';
		$tocart_module_btn_bg_hover = isset($tools['tocart_module_btn_bg_hover']) ? $tools['tocart_module_btn_bg_hover'] : 'rgba(177,177,177,1)';
		$tocart_module_btn_text_hover = isset($tools['tocart_module_btn_text_hover']) ? $tools['tocart_module_btn_text_hover'] : 'rgba(255,255,255,1)';

		$css .= ".flycart-module-container .module-tocart-button {background-color: {$tocart_module_btn_bg};color: {$tocart_module_btn_text};}";
		$css .= ".flycart-module-container .module-tocart-button:hover {background-color: {$tocart_module_btn_bg_hover};color: {$tocart_module_btn_text_hover}}";
	}

	if (isset($tools['checkout_module_btn']) && $tools['checkout_module_btn'] === 'true') {
		$checkout_module_btn_bg = isset($tools['checkout_module_btn_bg']) ? $tools['checkout_module_btn_bg'] : 'rgba(30,170,231,1)';
		$checkout_module_btn_text = isset($tools['checkout_module_btn_text']) ? $tools['checkout_module_btn_text'] : 'rgba(255,255,255,1)';
		$checkout_module_btn_bg_hover = isset($tools['checkout_module_btn_bg_hover']) ? $tools['checkout_module_btn_bg_hover'] : 'rgba(15,101,138,1)';
		$checkout_module_btn_text_hover = isset($tools['checkout_module_btn_text_hover']) ? $tools['checkout_module_btn_text_hover'] : 'rgba(255,255,255,1)';

		$css .= ".flycart-module-container .module-checkout-button	{background-color: {$checkout_module_btn_bg};color: {$checkout_module_btn_text};}";
		$css .= ".flycart-module-container .module-checkout-button:hover	{background-color: {$checkout_module_btn_bg_hover};color: {$checkout_module_btn_text_hover};}";
	}

	if (isset($tools['show_module_image']) && $tools['show_module_image'] === 'true' && isset($tools['module_imgage_width']) && ($tools['module_imgage_width'] + 6) < 100) {
		$module_imgage_left = isset($tools['module_imgage_width']) ? ($tools['module_imgage_width'] + 6) : '162';
		$css .= ".flycart-module .flycart-module-product-title {margin-left: {$module_imgage_left}px;}";
	}

	if (isset($tools['module_imgage_width']) && ($tools['module_imgage_width'] + 6) > 100) {
		$css .= ".flycart-module .flycart-module-image {float: none;margin-bottom: 3px;}";
	}


	if (isset($tools['module_alert_on']) && $tools['module_alert_on'] === 'true') {
		$module_alert_color = isset($tools['module_alert_color']) ? $tools['module_alert_color'] : 'rgba(255,0,0,1)';
		$css .= ".flycart-module .maximum-alert {color: {$module_alert_color};}";
	}

	if (isset($tools['module_info_on']) && $tools['module_info_on'] === 'true') {
		$module_info_color = isset($tools['module_info_color']) ? $tools['module_info_color'] : 'rgba(255,0,0,1)';
		$css .= ".flycart-module .minimum-alert {color: {$module_info_color};}";
	}

	if (isset($tools['module_coupon']) && $tools['module_coupon'] === 'true') {
		$coupon_module_btn_bg = isset($tools['module_coupon_btn_bg']) ? $tools['module_coupon_btn_bg'] : 'rgba(189,189,189,1)';
		$coupon_module_btn_text = isset($tools['module_coupon_btn_text']) ? $tools['module_coupon_btn_text'] : 'rgba(255,255,255,1)';
		$coupon_module_btn_bg_hover = isset($tools['module_coupon_btn_bg_hover']) ? $tools['module_coupon_btn_bg_hover'] : 'rgba(177,177,177,1)';
		$coupon_module_btn_text_hover = isset($tools['module_coupone_text_hover']) ? $tools['module_coupone_text_hover'] : 'rgba(255,255,255,1)';

		$css .= ".flycart-module-container .module-coupon-button {background-color: {$coupon_module_btn_bg};color: {$coupon_module_btn_text};}";
		$css .= ".flycart-module-container .module-coupon-button:hover {background-color: {$coupon_module_btn_bg_hover};color: {$coupon_module_btn_text_hover}}";
	}
}

if (isset($tools['effect_type']) && $tools['effect_type'] === 'custom') {
	$css .= $tools['effect_custom_css'];
}

if (isset($tools['custom_css'])) {
	$css .= $tools['custom_css'];
}
?>