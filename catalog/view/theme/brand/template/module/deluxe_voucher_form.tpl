<div class="panel panel-default deluxe-voucher col-sm-6">
  <div class="panel-heading"><?php echo $lng['voucher_balance_heading']; ?></div>
  <div class="panel-body">
    <div class="input-group btn-block check-balanse-form">
      <input id="first_name" type="text" class="form-control voucher-code" placeholder="<?php echo $lng['voucher_balance_entry']; ?>" />
        <button id="say" type="button" data-toggle="tooltip" data-original-title="<?php echo $lng['voucher_balance_button']; ?>" class="btn btn-primary" onclick="getBalance(this);"><?php echo $lng['voucher_balance_button']; ?><i class="fa fa-chevron-right"></i></button>
    </div>
    <div class="voucher-balance"></div>
  </div>
</div>
<div class="col-sm-6 cart-pic-balanse"><div id="result" class="numb-cart"></div></div>
<script type="text/javascript">
function getBalance(obj) {
  $.ajax({
    url: 'index.php?route=module/deluxe_voucher/getBalance',
    data: 'code=' + $(obj).parents('.deluxe-voucher').find('.voucher-code').val(),
    type: 'post',
    dataType: 'html',
    success: function(html) {
      $(obj).parents('.deluxe-voucher').find('.voucher-balance').html(html);
    }
  });
} //getBalance end

function say_hi() {
    var fname = document.getElementById('first_name').value; 
 
    var html = ' <b>' + fname + '</b> ';
 
    document.getElementById('result').innerHTML = html;
}
 
document.getElementById('say').addEventListener('click', say_hi);

function applyVoucher(obj, voucher_code) {
  $.ajax({
    url: 'index.php?route=total/voucher/voucher',
    data: 'voucher=' + voucher_code,
    type: 'post',
    dataType: 'json',
    success: function(json) {
      $(obj).parents('.voucher-balance').find('.alert-success').show();
      
      if (getURLVar('route') == 'checkout/cart' || getURLVar('route') == 'checkout/checkout') {
				location = 'index.php?route=checkout/cart';
			} else {
				$('#cart').load('index.php?route=common/cart/info');
			}
    }
  });
} //applyVoucher end
</script>
