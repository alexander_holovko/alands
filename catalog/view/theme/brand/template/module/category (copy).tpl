<style type="text/css">
	ul#menuLeftMain { padding: 5px 0px 0px 10px; }
	ul#menuLeftMain a { font-weight: bold; }
	ul#menuLeftMain li {border: none; list-style: none;}

	ul#menuLeftMain ul { padding: 0px 0px 0px 20px;}	
	ul#menuLeftMain ul li {border: none; list-style: none;}
	ul#menuLeftMain ul a {font-weight: normal; font-style: normal; text-decoration: none;}
	ul#menuLeftMain ul a:hover {border: none; background: transparent; text-decoration: underline;}
	ul#menuLeftMain ul li.active {list-style: disc;}
	ul#menuLeftMain ul li.active a {font-weight: bold;}
	ul#menuLeftMain ul li.active li {list-style: none;}
	ul#menuLeftMain ul li.active li a {font-weight: normal; text-decoration: none;}
	ul#menuLeftMain ul li.active ul {display: block; visibility: visible; height: auto;  transform: scaleY(1);} 
	ul#menuLeftMain ul li.active span.caret {display: none; visibility: hidden;}

	ul#menuLeftMain ul li.active li.open4 a {text-decoration: underline;} 
	ul#menuLeftMain ul li.active li.open4 li a {text-decoration: none;} 
	ul#menuLeftMain ul li.active li.open5 a {font-style: italic; text-decoration: none;}

	ul#menuLeftMain ul ul {	height: 0px; transform-origin: top; transition: transform 0.5s ease; background: #fff;
							transform: scaleY(0); 
   							-moz-transform: scaleY(0);
							-ms-transform: scaleY(0);
							-webkit-transform: scaleY(0);
							-o-transform: scaleY(0);
	} 
	ul#menuLeftMain ul li:hover ul {display: block; visibility: visible; height: auto; background: #fff;
									transform: scaleY(1); 
	    							-moz-transform: scaleY(1);
    								-ms-transform: scaleY(1);
    								-webkit-transform: scaleY(1);
    								-o-transform: scaleY(1);
	} 

div.mfilter-options {width: 95%}
</style>

<div class="menu-group">
<?php 
foreach ($categories as $category) { if ($category['category_id'] == $category_id) { // Вывести составляющую главной категории

	echo '<h3><a href="' . $category['href'] . '">' . $category['name'] . '</a></h3>' . PHP_EOL;

	if ($category['children']) {
		echo '<ul id="menuLeftMain" class="level2">' . PHP_EOL;

		foreach ($category['children'] as $child2) {
			echo '<li><a href="' . $child2['href'] . '">' . $child2['name'] . '</a></li>' . PHP_EOL;

			if ($child2['level3']){
				echo '<ul class="level3">' . PHP_EOL;
				foreach ($child2['level3'] as $child3) {
					echo '<li ';
					if ($child3['category_id'] == $lvl3) { echo ' class="active" ';}
					echo  '><a href="' . $child3['href'] . '" >' . $child3['name'] . '</a>' . PHP_EOL;


					if ($child3['level4']){
						echo '<span class="caret"></span><ul class="level4">' . PHP_EOL;
						foreach ($child3['level4'] as $child4) {
							echo '<li ';
							if ($child4['category_id'] == $lvl4) { echo ' class="active open4" ';}
							echo  '><a href="' . $child4['href'] . '" >' . $child4['name'] . '</a>' . PHP_EOL;


							if ($child4['level5']){
								echo '<span class="caret"></span><ul class="level5">' . PHP_EOL;
								foreach ($child4['level5'] as $child5) {
									echo '<li ';
									if ($child5['category_id'] == $lvl5) { echo ' class="active open5" ';}
									echo  '><a href="' . $child5['href'] . '" >' . $child5['name'] . '</a></li>' . PHP_EOL;
								}
								echo '</ul><!-- level5 -->' . PHP_EOL;
							}//5
							echo '</li>' . PHP_EOL;
						}
						echo '</ul><!-- level4 -->' . PHP_EOL;
					echo '</li>' . PHP_EOL;
					}// 4
					
				}
				echo '</ul><!-- level3 -->' . PHP_EOL;
			}//3
		}
		echo '</ul><!-- level2 -->' . PHP_EOL;
	}//2
	

}}// вывод главной категории м/ж
?>
</div>





<?php
/** Old menu

<div class="list-group">
  <?php foreach ($categories as $category) { ?>
  <?php if ($category['category_id'] == $category_id) { ?>
  	<a href="<?php echo $category['href']; ?>" class="list-group-item active"><?php echo $category['name']; ?></a>
  	<?php if ($category['children']) { ?>
  		<?php foreach ($category['children'] as $child) { ?>
  			<?php if ($child['category_id'] == $child_id) { ?>
  			<a href="<?php echo $child['href']; ?>" class="list-group-item active">&nbsp;&nbsp;&nbsp;- <?php echo $child['name']; ?></a>
    
		 <?php if ($child['level3']) { ?>
		 <?php foreach($child['level3'] as $child3) { ?>
		 <?php if ($child3['category_id'] == $lvl3) { ?>
	     <a href="<?php echo $child3['href']; ?>" class="list-group-item lvl3 active">&nbsp;&nbsp;&nbsp;- <?php echo $child3['name']; ?></a>
		 
		 <?php if ($child3['level4']) { ?>
		 <?php foreach($child3['level4'] as $child4) { ?>
		 <?php if ($child4['category_id'] == $lvl4) { ?>
	     <a href="<?php echo $child4['href']; ?>" class="list-group-item lvl4 active">&nbsp;&nbsp;&nbsp;- <?php echo $child4['name']; ?></a>
		 
		  <?php if ($child4['level5']) { ?>
		 <?php foreach($child4['level5'] as $child5) { ?>
		 <?php if ($child5['category_id'] == $lvl5) { ?>
	     <a href="<?php echo $child5['href']; ?>" class="list-group-item lvl5 active">&nbsp;&nbsp;&nbsp;- <?php echo $child5['name']; ?></a>
		 <?php } else { ?>

		 <a href="<?php echo $child5['href']; ?>" class="list-group-item lvl5">&nbsp;&nbsp;&nbsp;- <?php echo $child5['name']; ?></a>
		 <?php } ?>
		 <?php } ?>
		 <?php } ?>
		 
		 
		 
		 <?php } else { ?>
		 <?php if(1 || !$lvl4) { ?>
		 <a href="<?php echo $child4['href']; ?>" class="list-group-item lvl4">&nbsp;&nbsp;&nbsp;- <?php echo $child4['name']; ?></a>
		 <?php } ?>
		 <?php } ?>
		 <?php } ?>
		 <?php } ?>
		 
		 
		 
		 
		 
		 <?php } else { ?>
		 
		 <?php if(1 || !$lvl3) { ?>
		 <a href="<?php echo $child3['href']; ?>" class="list-group-item lvl3">&nbsp;&nbsp;&nbsp;- <?php echo $child3['name']; ?></a>
		 <?php } ?>
		 
		 <?php } ?>
		 <?php } ?>
		 <?php } ?>
		 
		 
  <?php } else { ?>
  <?php if(1 || !$child_id) { ?>
  <a href="<?php echo $child['href']; ?>" class="list-group-item">&nbsp;&nbsp;&nbsp;- <?php echo $child['name']; ?></a>
  <?php } ?>
  
  <?php } ?>
  <?php } ?>
  <?php } ?>
  <?php } else { ?>
  <a href="<?php echo $category['href']; ?>" class="list-group-item"><?php echo $category['name']; ?></a>
  <?php } ?>
  <?php } ?>
</div>
*/
?>