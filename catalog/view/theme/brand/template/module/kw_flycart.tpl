<?php if (isset($tools['status_module']) && $tools['status_module'] === 'true') : ?>
<!-- kw_flycart
---------------------------------------------------------------->

<div id="kwFlycart" ng:app="kwFlycart" ng:controller="kwFlycartController" data-langId="<?php echo $lang_id; ?>">

	<?php if (isset($tools['module_type']) && $tools['module_type'] === 'widget') : ?>
	<div flycart="widget" id="kw-base-widget"></div>
	<?php endif; ?>
	
	<?php if (isset($tools['module_type']) && $tools['module_type'] === 'module') : ?>
	<div flycart="module"></div>
	<?php endif; ?>

	<?php if (isset($tools['noti_type']) && $tools['noti_type'] === 'notice' || isset($tools['noti_type']) && $tools['noti_type'] === 'light' || isset($tools['noti_type']) && $tools['noti_type'] === 'advanced') : ?>
	<div flycart="notification"></div>
	<?php endif; ?>

</div>

<!-- /kw_flycart
---------------------------------------------------------------->
<?php endif; ?>