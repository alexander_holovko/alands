<footer>
  <div class="container">
    <div class="row foot-col">
      <?php if ($informations) { ?>
      <div class="col-md-3 col-xs-12">
        <div id="logo"> 
         <a href="/index.php?route=common/home"><p>Concept Store
            <span>Мировых Брендов</span></p></a>
        </div>
        <p>Интернет магазин брендовой одежды, обуви и акссесуаров.</p>
      </div>
      <div class="col-md-3 col-xs-12">
        <h5>Клиентам</h5>
        <ul class="list-unstyled">
          <?php foreach ($informations as $information) { ?>
          <li><a href="<?php echo $information['href']; ?>"><?php echo $information['title']; ?></a></li>
          <?php } ?>
        </ul>
      </div>
      <?php } ?>
      <div class="col-md-3 col-xs-12">
        <h5>Fashion Club</h5>
        <ul class="list-unstyled">
          <li><a href="">Программа привилегий</a></li>
          <li><a href="<?php echo $voucher; ?>">Подарочные карты</a></li>
          <li><a href="">Магазины</a></li> 
        </ul>
      </div>
      <div class="col-md-3 col-xs-12">
        <h5>Контакты</h5>
        <ul class="list-unstyled"> 
          <li><a href="tel:08007897896">0 800 789 7896</a></li>
          <li><a href="tel:+380955658980">+38 (095) 565 8980</a></li>
          <li><a href="<?php echo $contact; ?>">Напишите нам</a></li>
          <li><a href="">Заказать звонок</a></li>
        </ul>
      </div>
      <div class="col-md-3 col-xs-12">
        <h5>Почему мы</h5>
        <ul class="list-unstyled"> 
          <li><a href="">Мировые бренды онлайн</a></li>
          <li><a href="<?php echo $order; ?>">Доставка по всей Украине</a></li>
          <li><a href="<?php echo $wishlist; ?>">Удобный способ оплаты</a></li>
          <li><a href="<?php echo $newsletter; ?>">Консультация стилиста</a></li>
          <li><a href="<?php echo $newsletter; ?>">Гарантия качества</a></li>
          <li><a href="<?php echo $newsletter; ?>">Постоянные скидки и акции</a></li>
        </ul>
      </div>
    </div>
    <hr>
    <div class="row">
    <div class="ico col-md-6">2002-2017. "..........." . Все права защищены</div>
    <div class="icos col-md-6">
      <div class="icos-footliqpay"></div>
<div class="icos-footvisa-pay-logo"></div>
<div class="icos-footmaster-card-logo"></div>
<div class="icos-footinstagram-logo-1"></div>
<div class="icos-footmaster-card-logo-1"></div></div></div>
  </div>
</footer>

<!--
OpenCart is open source software and you are free to remove the powered by OpenCart if you want, but its generally accepted practise to make a small donation.
Please donate via PayPal to donate@opencart.com
//-->

<!-- Theme created by Welford Media for OpenCart 2.0 www.welfordmedia.co.uk -->

</body></html>