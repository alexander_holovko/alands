<?php echo $header; ?>
<div class="container">
  <ul class="breadcrumb">
    <?php foreach ($breadcrumbs as $breadcrumb) { ?>
    <li><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a></li>
    <?php } ?>
  </ul>
  <?php if ($error_warning) { ?>
  <div class="alert alert-danger"><i class="fa fa-exclamation-circle"></i> <?php echo $error_warning; ?></div>
  <?php } ?>
  <div class="row"><?php echo $column_left; ?>
    <?php if ($column_left && $column_right) { ?>
    <?php $class = 'col-sm-6'; ?>
    <?php } elseif ($column_left || $column_right) { ?>
    <?php $class = 'col-sm-9'; ?>
    <?php } else { ?>
    <?php $class = 'col-sm-12'; ?>
    <?php } ?>
    <div id="content" class="<?php echo $class; ?>"><?php echo $content_top; ?>
      <div id="header111" style="text-transform: uppercase;text-align: center;"><h1><?php echo $heading_title; ?></h1></div>
	  <br><br><br>
	  <div id="lineall" style="display: flex;"><div id="line" style="margin:auto;">Корзина для покупок</div><div id="line" style="margin:auto;">Доставка и Оплата</div><div id="line" style="margin:auto;">Подтверждение заказа</div></div>
	  <br>
	  <div id="pg" style="display:flex;">
	  <div id="pg1" style="width: 37px;
	height: 2px;
	background-color: #45a95a;margin:auto;"></div>
	
	<div id="pg2" style="width: 37px;
	height: 2px;
	background-color: #45a95a;margin:auto;"></div>
	
	 <div id="pg3" style="width: 37px;
	height: 2px;
	background-color: #dcdcdc;margin:auto;"></div></div>
	
	
	
	
	  <div id="gline" style="border: none;
    background-color: #45a95a;
	height: 1px;
    	width: 50%;"></div>
	
	<div id="grline" style="border: none;
    background-color: #dcdcdc;
    height: 1px;
	width: 50%;
	float: right;
	margin-top: -1px;"></div>
	
	<div id="pg" style="display:flex;">
	  <div id="pg4" style="width: 37px;
	height: 2px;
	background-color: #45a95a;margin:auto;"></div>
	
	<div id="pg5" style="width: 37px;
	height: 3px;
	background-color: #45a95a;margin:auto;margin-top: -1px;"></div>
	
	 <div id="pg6" style="width: 37px;
	height: 3px;
	background-color: #dcdcdc;margin:auto;margin-top: -1px;"></div></div>
	
	 
	
	<br><br><br>