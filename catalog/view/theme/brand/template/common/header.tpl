<!DOCTYPE html>
<!--[if IE]><![endif]-->
<!--[if IE 8 ]><html dir="<?php echo $direction; ?>" lang="<?php echo $lang; ?>" class="ie8"><![endif]-->
<!--[if IE 9 ]><html dir="<?php echo $direction; ?>" lang="<?php echo $lang; ?>" class="ie9"><![endif]-->
<!--[if (gt IE 9)|!(IE)]><!-->
<html dir="<?php echo $direction; ?>" lang="<?php echo $lang; ?>">
<!--<![endif]-->
<head>
<meta charset="UTF-8" />
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<title><?php echo $title; ?></title>
<base href="<?php echo $base; ?>" />
<?php if ($description) { ?>
<meta name="description" content="<?php echo $description; ?>" />
<?php } ?>
<?php if ($keywords) { ?>
<meta name="keywords" content= "<?php echo $keywords; ?>" />
<?php } ?>
<link href="https://alands.com.ua/image/favicon.png" type="image/x-icon" rel="shortcut icon"> 
 


<script src="catalog/view/javascript/jquery/jquery-2.1.1.min.js" type="text/javascript"></script>
<link href="catalog/view/javascript/bootstrap/css/bootstrap.min.css" rel="stylesheet" media="screen" />
<script src="catalog/view/javascript/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
<link href="catalog/view/javascript/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css" />
<link href="catalog/view/theme/brand/stylesheet/stylesheet.css" rel="stylesheet">
<link href="catalog/view/theme/brand/stylesheet/checkbox.css" rel="checkbox">
<?php foreach ($styles as $style) { ?>
<link href="<?php echo $style['href']; ?>" type="text/css" rel="<?php echo $style['rel']; ?>" media="<?php echo $style['media']; ?>" />
<?php } ?>
<script src="catalog/view/javascript/common.js" type="text/javascript"></script>
<script src="catalog/view/javascript/jquery.equalheights.min.js"></script>  
<?php foreach ($links as $link) { ?>
<link href="<?php echo $link['href']; ?>" rel="<?php echo $link['rel']; ?>" />
<?php } ?>
<?php foreach ($scripts as $script) { ?>
<script src="<?php echo $script; ?>" type="text/javascript"></script>
<?php } ?>
<?php foreach ($analytics as $analytic) { ?>
<?php echo $analytic; ?>
<?php } ?>


<!-- Global site tag (gtag.js) - AdWords: 819047199 -->
<script async src="https://www.googletagmanager.com/gtag/js?id=AW-819047199"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'AW-819047199');
</script>
<script>
  gtag('event', 'page_view', {
    'send_to': 'AW-819047199',
    'ecomm_pagetype': 'replace with value',
    'ecomm_prodid': 'replace with value',
    'ecomm_totalvalue': 'replace with value',
    'user_id': 'replace with value'
  });
</script>



<script type="text/javascript" >
$(document).ready(function() {
$('.my_minus').click(function () {
var $input = $(this).parent().find('.quant');
var count = parseInt($input.val()) - 1;
count = count < 1 ? 1 : count;
$input.val(count);
$input.change();
return false;
});
$('.my_plus').click(function () {
var $input = $(this).parent().find('.quant');
$input.val(parseInt($input.val()) + 1);
$input.change();
return false;
});
});
</script>
<!-- Yandex.Metrika counter -->
<!-- Yandex.Metrika counter -->
<script type="text/javascript" >
    (function (d, w, c) {
        (w[c] = w[c] || []).push(function() {
            try {
                w.yaCounter47523964 = new Ya.Metrika2({
                    id:47523964,
                    clickmap:true,
                    trackLinks:true,
                    accurateTrackBounce:true,
                    webvisor:true,
                    trackHash:true
                });
            } catch(e) { }
        });

        var n = d.getElementsByTagName("script")[0],
            s = d.createElement("script"),
            f = function () { n.parentNode.insertBefore(s, n); };
        s.type = "text/javascript";
        s.async = true;
        s.src = "https://mc.yandex.ru/metrika/tag.js";

        if (w.opera == "[object Opera]") {
            d.addEventListener("DOMContentLoaded", f, false);
        } else { f(); }
    })(document, window, "yandex_metrika_callbacks2");
</script>
<noscript><div><img src="https://mc.yandex.ru/watch/47523964" style="position:absolute; left:-9999px;" alt="" /></div></noscript>


<!-- /Yandex.Metrika counter -->
<?php echo isset($tc_og)?$tc_og:''; //microdatapro 7.0 ?>
<script>
	(function(i,s,o,g,r,a,m){
	i["esSdk"] = r;
	i[r] = i[r] || function() {
		(i[r].q = i[r].q || []).push(arguments)
	}, a=s.createElement(o), m=s.getElementsByTagName(o)[0]; a.async=1; a.src=g;
	m.parentNode.insertBefore(a,m)}
	) (window, document, "script", "https://esputnik.com/scripts/v1/public/scripts?apiKey=eyJhbGciOiJSUzI1NiJ9.eyJzdWIiOiI0NTI0ZWZhYTJkYzI2MGRmYTM4YTE1NDBlMWFlYmU0OGQ2OTAzNTEyM2M0MGRlODA0MGE5NGY3MjljOTY0ZDRmNmFlM2Y3MzE1YWJlYmUyOTJjMDk1N2Y0OTM3Mjg3NWM1ZWI1YjcxZjQxNzUzNWVkZjE0OTE0ODM2YTU5YmU0MWE5Y2FiMzI2NzIyYzNiMDViNzZiYjEyM2IxODg0MDE0MDE4ZWUwOTI5YjFhNjUxMyJ9.ZevhLFV-QQAKgQjm9B9HhzJ3bTi1uDcuT0YEjOaAdQZXMr7GovmwzFWTkEr7xYTBU2mwOthBoRSSo_1AhFYPYw&domain=DD466438-522A-4870-8CA6-1F252DF54B8F", "es");
	es("pushOn");
</script>
<!-- Facebook Pixel Code -->
<script>
  !function(f,b,e,v,n,t,s)
  {if(f.fbq)return;n=f.fbq=function(){n.callMethod?
  n.callMethod.apply(n,arguments):n.queue.push(arguments)};
  if(!f._fbq)f._fbq=n;n.push=n;n.loaded=!0;n.version='2.0';
  n.queue=[];t=b.createElement(e);t.async=!0;
  t.src=v;s=b.getElementsByTagName(e)[0];
  s.parentNode.insertBefore(t,s)}(window, document,'script',
  'https://connect.facebook.net/en_US/fbevents.js');
  fbq('init', '1258211120975496');
  fbq('track', 'PageView');
</script>
<noscript><img height="1" width="1" style="display:none"
  src="https://www.facebook.com/tr?id=1258211120975496&ev=PageView&noscript=1"
/></noscript>
<!-- End Facebook Pixel Code -->
</head>
<body class="<?php echo $class; ?>">
<!-- Google Tag Manager (noscript) -->
<noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-5PMZ7JH"
height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
<!-- End Google Tag Manager (noscript) -->
<!-- Global site tag (gtag.js) - AdWords: 819047199 -->
<script async src="https://www.googletagmanager.com/gtag/js?id=AW-819047199"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'AW-819047199');
</script>


<nav id="top">
  <div class="container">
    <div class="row">
      <div class="col-sm-12 ">
        <ul>
        <li><a href=""></a>Экспресс-доставка 1-2 дня</li>
        <li><a href=""></a>Легкий Возврат и Обмен</li>
        <li><a href=""></a>Оплата после получения</li>
        <li><a href=""></a>Скидки до 10% по Дисконту</li>
      </ul></div>
    </div> 
  </div>
</nav>
<header>
  <div class="container">
    <div class="row nav-row">
      <div class="col-sm-2 logo text-center">
          <?php if ($logo) { ?>
		  
		 
		  
		  
		  
          <a href="<?php echo $home; ?>"><!-- <img src="<?php echo $logo; ?>" title="<?php echo $name; ?>" alt="<?php echo $name; ?>" class="img-responsive" /> --> <div class="logos-b">aland's</div></a><p><span>Мировые Бренды</span></p>
          <?php } else { ?>
          <h1><a href="<?php echo $home; ?>"><?php echo $name; ?></a></h1>
          <?php } ?>
      </div>
      <div class="col-sm-2 col-sm-offset-5"><!--<button class="btn nav-btn" data-toggle="modal" data-target="#myModal">Заказать звонок</button>--></div>

      <div class="col-sm-1 acc-block"><div class="dropdown"><a href="<?php echo $account; ?>" title="<?php echo $text_account; ?>" class="dropdown-toggle" data-toggle="dropdown"><img src="/catalog/view/theme/brand/image/user.png" width=16px; height=16px; alt=""><!-- <span class="hidden-xs hidden-sm hidden-md"><?php echo $text_account; ?></span>  --><span class="caret"></span></a>
          <ul class="dropdown-menu dropdown-menu-right">
            <?php if ($logged) { ?>
            <li><a href="<?php echo $account; ?>"><?php echo $text_account; ?></a></li>
            <li><a href="<?php echo $order; ?>"><?php echo $text_order; ?></a></li>
<!--             <li><a href="<?php echo $transaction; ?>"><?php echo $text_transaction; ?></a></li>
            <li><a href="<?php echo $download; ?>"><?php echo $text_download; ?></a></li> -->
            <li><a href="<?php echo $logout; ?>"><?php echo $text_logout; ?></a></li>
            <?php } else { ?>
            <li><a href="<?php echo $register; ?>"><?php echo $text_register; ?></a></li>
            <li><a href="<?php echo $login; ?>"><?php echo $text_login; ?></a></li>
            <?php } ?>
          </ul>
        </div>
      </div>
      <div class="col-sm-1"><?php echo $cart; ?></div>
    </div>
  </div>
</header>
        <div class="serch serch-block"><?php echo $search; ?> </div>
<?php if ($categories) { ?>
<div class="container-fluid mainmenu">
  <div class="container">
  <nav id="menu" class="navbar">
    <div class="navbar-header"><span id="category" class="visible-xs"><?php echo $text_category; ?></span>
      <button type="button" class="btn btn-navbar navbar-toggle" data-toggle="collapse" data-target=".navbar-ex1-collapse"><i class="fa fa-bars"></i></button>
    </div>
    <div class="collapse navbar-collapse navbar-ex1-collapse">
      <ul class="nav navbar-nav">
        <?php foreach ($categories as $category) { ?>
        <?php if ($category['children']) { ?>
        <li class="dropdown"><a href="<?php echo $category['href']; ?>" class="dropdown-toggle" data-toggle="dropdown"><?php echo $category['name']; ?></a>
          <div class="dropdown-menu">
            <div class="dropdown-inner">
              <?php foreach (array_chunk($category['children'], ceil(count($category['children']) / $category['column'])) as $children) { ?>
              <ul class="list-unstyled">
                <?php foreach ($children as $child) { ?>
                <li><a href="<?php echo $child['href']; ?>"><?php echo $child['name']; ?></a></li>
                <?php } ?>
              </ul>
              <?php } ?>
            </div>
            <a href="<?php echo $category['href']; ?>" class="see-all"><?php echo $text_all; ?> <?php echo $category['name']; ?></a> </div>
        </li>
        <?php } else { ?>
        <li><a href="<?php echo $category['href']; ?>"><?php echo $category['name']; ?></a></li>
        <?php } ?>
        <?php } ?>
        <li><a href="/index.php?route=product/manufacturer">БРЕНДЫ</a></li>
        <li><a href="/index.php?route=product/special"><span class="red">SALE</span></a></li>
        <li><a href="/novinki">Новинки</a></li>
        <li><a href="/podarochnye-karty"><span class="red">ПОДАРОЧНЫЕ КАРТЫ</span></a></li>
        <li><a href="/kontakty"><span class="red">КОНТАКТЫ</span></a></li>
      </ul>
    </div>
    </div>
  </nav>
</div>

<?php } ?>