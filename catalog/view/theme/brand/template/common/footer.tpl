<footer>
  <div class="container">
    <div class="row foot-col">
      <?php if ($informations) { ?>
      <div class="col-md-3 col-xs-12">
        <div id="logo" class=" text-left">  
                    <a href="https://alands.com.ua"><div class="logos-w text-left">aland's</div></a><p><span>Мировые Бренды</span></p>
                 
        </div>
        <p>Интернет магазин<br/> брендовой одежды,<br/> обуви и акссесуаров.</p>
      </div>
      <div class="col-md-3 col-xs-12">
        <h5>Клиентам</h5>
        <ul class="list-unstyled">
          <?php foreach ($informations as $information) { ?>
          <li><a href="<?php echo $information['href']; ?>"><?php echo $information['title']; ?></a></li>
          <?php } ?>
          <li><a href="/index.php?route=product/testimonial">Оставить отзыв</a></li>
        </ul>
      </div>
      <?php } ?>
      <div class="col-md-3 col-xs-12">
        <h5>Fashion Club</h5>
        <ul class="list-unstyled">
          <li><a href="/programma-loyalnosti">Программа лояльности</a></li>
          <!-- <li><a href="/podarochnye-karty">Подарочные карты</a></li> -->
          <li><a href="/kontakty">Магазины</a></li> 
        </ul>
      </div>
      <div class="col-md-3 col-xs-12">
        <h5>Контакты</h5>
        <ul class="list-unstyled"> 
          <li><p><a href="tel:+380507103727">+380507103727</a></p></li> 
		  <li><p><a href="tel:+380953466420">+380953466420</a></p></li> 
          <li><a href="<?php echo $contact; ?>">Напишите нам</a></li>
          <li><a href=""  data-toggle="modal" data-target="#myModal">Заказать звонок</a></li>
        </ul>
      </div>
      <div class="col-md-3 col-xs-12">
        <h5>Почему мы</h5>
        <ul class="list-unstyled"> 
          <li><a href="/index.php?route=product/manufacturer">Мировые бренды онлайн</a></li>
          <li>Доставка по всей Украине</li>
          <li>Удобный способ оплаты</li>
          <li>Консультация стилиста</li>
          <!-- <li><a href="/garantiya-podlinnosti">Гарантия качества</a></li> -->
          <li>Постоянные скидки и акции</li>
        </ul>
      </div>
    </div>
    <hr>
    <div class="row">
    <div class="ico col-md-6">2019. Все права защищены</div>
    <div class="icos col-md-6">
      <!--<a href="" target="_blank"><div class="icos-footliqpay"></div></a>
<a href="" target="_blank"><div class="icos-footvisa-pay-logo"></div></a>
<a href="" target="_blank"><div class="icos-footmaster-card-logo"></div></a>-->
<a href="https://www.instagram.com/alands_ua/" target="_blank"><div class="icos-footinstagram-logo-1"></div></a>
<a href="https://www.facebook.com/alands.com.ua/" target="_blank"><div class="icos-footmaster-card-logo-1"></div></a></div></div>
  </div>
</footer>

<!-- Modal -->
<div id="myModal" class="modal fade back-resp" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header text-center">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Заказать звонок</h4>
        <p>Введите Ваши данные, менеджер<br/> перезвонит Вам в течение 15 минут</p>
      </div>
      <div class="modal-body"> 
        <form  action="https://alands.com.ua/spasibo" method="post" >
          <input type="text" name="name" required placeholder="Ваше Имя">
          <input type="tel" name="tel" required placeholder="Телефон">
          <button class="consl btn-primary" type="submit">ПЕРЕЗВОНИТЕ МНЕ<i class="fa fa-angle-right" aria-hidden="true"></i></button>
        </form>
      </div>
      <div class="modal-footer"> 
      </div>
    </div>

  </div>
</div>
<!-- Modal tab size -->
<div id="myModal-tab-size" class="modal fade sizer" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header text-center">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Таблица размеров</h4> 
      </div>
      <div class="modal-body"> 
<div class="img-sizer">
       <img src="/catalog/view/theme/brand/image/woomsize.png" class="img-responsive" alt="">
       <img src="/catalog/view/theme/brand/image/woomsize2.png" class="img-responsive" alt=""></div>
      </div>
      <div class="modal-footer"> 
      </div>
    </div>

  </div>
</div>


<!-- Modal tab size -->
<div id="myModal-tab-womsize-boots" class="modal fade sizer" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header text-center">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Таблица размеров</h4> 
      </div>
      <div class="modal-body"> 
<div class="img-sizer"> 
       <img src="/catalog/view/theme/brand/image/woomsize3.png" class="img-responsive" alt=""></div>
      </div>
      <div class="modal-footer"> 
      </div>
    </div>

  </div>
</div>

<!-- Modal tab size -->
<div id="myModal-tab-size-boots" class="modal fade sizer" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header text-center">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Таблица размеров</h4> 
      </div>
      <div class="modal-body text-center"> 
 <div class="img-sizer">
       <img src="/catalog/view/theme/brand/image/boots.png" class="img-responsive" alt=""></div>
      </div>
      <div class="modal-footer"> 
      </div>
    </div>

  </div>
</div>
<!--
OpenCart is open source software and you are free to remove the powered by OpenCart if you want, but its generally accepted practise to make a small donation.
Please donate via PayPal to donate@opencart.com
//-->

<!-- Theme created by Welford Media for OpenCart 2.0 www.welfordmedia.co.uk -->
<script src="catalog/view/javascript/jquery.maskedinput.min.js"></script>  
<script src="https://unpkg.com/masonry-layout@4/dist/masonry.pkgd.min.js"></script>
<script  src="https://cdnjs.cloudflare.com/ajax/libs/slick-carousel/1.8.1/slick.min.js"></script>
<?php echo isset($microdatapro)?$microdatapro:''; //microdatapro 7.0 ?>
<script data-skip-moving="true">
        (function(w,d,u){
                var s=d.createElement('script');s.async=1;s.src=u+'?'+(Date.now()/60000|0);
                var h=d.getElementsByTagName('script')[0];h.parentNode.insertBefore(s,h);
        })(window,document,'https://cdn.bitrix24.ua/b8019215/crm/site_button/loader_2_6nuzmn.js');
</script>

<!--Dynamic Remarketing Tag --//-->
<script type="text/javascript">
var google_tag_params = {
dynx_itemid: '<?php echo $dynx_itemid; ?>',
dynx_pagetype: '<?php echo $dynx_pagetype; ?>',
dynx_totalvalue: '<?php echo $dynx_totalvalue; ?>',
};
</script>
<script type="text/javascript">
/* <![CDATA[ */
var google_conversion_id = 539730458;
var google_custom_params = window.google_tag_params;
var google_remarketing_only = true;
/* ]]> */
</script>
<script type="text/javascript" src="//www.googleadservices.com/pagead/conversion.js">
</script>
<noscript>
<div style="display:inline;">
<img height="1" width="1" style="border-style:none;" alt="" src="//googleads.g.doubleclick.net/pagead/viewthroughconversion/539730458/?guid=ON&amp;script=0"/>
</div>
</noscript>


</body></html>