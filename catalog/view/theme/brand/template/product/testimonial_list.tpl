<?php if ($testimonials) { ?>     
<?php foreach ($testimonials as $testimonial) { ?>
<div class="row testim-sing">
	<div class="col-sm-3">
    <?php if ($photo) { ?> 
	  <div class="image"><img src="<?php echo $testimonial['photo']; ?>" alt="" class="img-responsive" /></div> 
	<?php } ?></div>
	<div class="col-sm-9">
	  <?php if ($testimonial['name']) { ?>
	  <p><strong><?php echo $testimonial['name']; ?></strong></p>
	  <?php } ?>
	  <?php if ($testimonial['title']) { ?>
	  <h2><?php echo $testimonial['title']; ?></h2>
	  <?php } ?>
	  <p>
	    <?php if ($rating) { ?>
	    <span class="rating">
		  <?php for ($i = 1; $i <= 5; $i++) { ?>
		  <?php if ($testimonial['rating'] < $i) { ?>
		  <span class="fa fa-stack"><i class="fa fa-star-o fa-stack-2x"></i></span>
		  <?php } else { ?>
		  <span class="fa fa-stack"><i class="fa fa-star fa-stack-2x"></i><i class="fa fa-star-o fa-stack-2x"></i></span>
		  <?php } ?>
		  <?php } ?>
	    </span>
	    <?php } ?>
		<?php if (!$photo) { ?>
		<?php if ($testimonial['name']) { ?>
	    <i><?php echo $testimonial['name']; ?></i>
	    <?php } ?>
		<?php } ?>
		<?php if ($testimonial['city']) { ?>
	    <i><?php echo $testimonial['city']; ?></i>
	    <?php } ?>
		<?php if ($date_added && $testimonial['date_added']) { ?>
	    <i><?php echo $testimonial['date_added']; ?></i>
	    <?php } ?>
	  </p>
	  <?php if ($testimonial['good']) { ?>
	  <p><!-- <strong><?php echo $entry_good; ?></strong><br/> --><?php echo $testimonial['good']; ?></p>
	  <?php } ?>
	  <?php if ($testimonial['bad']) { ?>
	  <p><!-- <strong><?php echo $entry_bad; ?></strong><br/> --><?php echo $testimonial['bad']; ?></p>
	  <?php } ?>
	  <?php if ($testimonial['text']) { ?>
	  <p><!-- <strong><?php echo $entry_text; ?></strong><br/> --><?php echo $testimonial['text']; ?></p>
	  <?php } ?>
	  <?php if ($testimonial['comment']) { ?>
	  <blockquote><strong><?php echo $entry_comment; ?></strong><br/><?php echo $testimonial['comment']; ?></blockquote>
	  <?php } ?></div>
</div> 
<hr>
<?php } ?>
<div class="text-center"><?php echo $pagination; ?></div>
<?php } else { ?>
<p><?php echo $text_no_testimonials; ?></p>
<?php } ?>