<?php echo $header; ?>
<div class="container">
	<ul class="breadcrumb">
	<?php foreach ($breadcrumbs as $breadcrumb) { ?>
		<li><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a></li>
    <?php } ?>
    </ul>
    <div class="row"><?php echo $column_left; ?>
    <?php if ($column_left && $column_right) { ?>
    	<?php $class = 'col-sm-6'; ?>
    <?php } elseif ($column_left || $column_right) { ?>
    	<?php $class = 'col-sm-9'; ?>
    <?php } else { ?>
    	<?php $class = 'col-sm-12'; ?>
    <?php } ?>
    	<div id="content" class="<?php echo $class; ?>"><?php echo $content_top; ?>
    		<h1><i class="fa fa-users" style="font-size:1em!important;"></i> <?php echo $heading_title; ?></h1>
    		<?php if (isset($card_code)) { ?>
    		<div class="table-responsive">
    			<table class="table table-hover">
    				<tbody>
    					<tr>
    						<td><?php echo $text_card_code; ?></td>
    						<td class="text-center"><?php echo $card_code; ?></td>
    					</tr>
    					<tr>
    						<td><?php echo $text_discount; ?></td>
    						<td class="text-center"><?php echo $card_discount; ?></td>
    					</tr>
    					<tr>
    						<td><?php echo $text_total; ?></td>
    						<td class="text-center"><?php echo $card_total; ?></td>
    					</tr>
    					<tr class="success">
    						<td><?php echo $text_savings; ?></td>
    						<td class="text-center"><?php echo $card_savings; ?></td>
    					</tr>
    				</tbody>
    			</table>
    		</div>
    		<?php } else { ?>
    		<p><?php echo $text_not_membership; ?></p>
    		<?php } ?>
    		<?php if (isset($text_condition)) { ?>
    		<p><?php echo $text_condition; ?></p>
    		<?php } ?>
    		<div class="buttons clearfix">
    			<div class="pull-right">
    				<a href="<?php echo $continue; ?>" class="btn btn-primary"><?php echo $button_continue; ?></a>
    			</div>
    		</div>
    		<?php echo $content_bottom; ?>
    	</div>
    	<?php echo $column_right; ?>
    </div>
</div>
<?php echo $footer; ?>