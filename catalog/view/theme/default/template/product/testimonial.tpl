<?php echo $header; ?>
<div class="container">
  <ul class="breadcrumb">
    <?php foreach ($breadcrumbs as $breadcrumb) { ?>
    <li> <a href="<?php echo $breadcrumb['href']; ?>"> <?php echo $breadcrumb['text']; ?> </a> </li>
    <?php } ?>
  </ul>
  <div class="row"><?php echo $column_left; ?>
    <?php if ($column_left && $column_right) { ?>
    <?php $class = 'col-sm-6'; ?>
    <?php } elseif ($column_left || $column_right) { ?>
    <?php $class = 'col-sm-9'; ?>
    <?php } else { ?>
    <?php $class = 'col-sm-12'; ?>
    <?php } ?>
    <div id="content" class="<?php echo $class; ?>"><?php echo $content_top; ?>
      <h1><?php echo $heading_title; ?></h1>
	  <div id="testimonial"></div>
	  <form class="form-horizontal" id="form-testimonial">
	    <h2><?php echo $text_write; ?></h2>
		<?php if ($guest) { ?>
		<?php if ($title > 0) { ?>
		<div class="form-group <?php if ($title == 2) { echo 'required'; }?>">
		  <div class="col-sm-12">
            <label class="control-label" for="input-title"><?php echo $entry_title; ?></label>
            <input type="text" name="title" value="" id="input-title" class="form-control" />
          </div>
        </div>
		<?php } ?>
		<?php if ($city > 0) { ?>
		<div class="form-group <?php if ($city == 2) { echo 'required'; }?>">
		  <div class="col-sm-12">
            <label class="control-label" for="input-city"><?php echo $entry_city; ?></label>
            <input type="text" name="city" value="" id="input-city" class="form-control" />
          </div>
        </div>
		<?php } ?>
		<?php if ($email > 0) { ?>
		<div class="form-group <?php if ($email == 2) { echo 'required'; }?>">
		  <div class="col-sm-12">
            <label class="control-label" for="input-email"><?php echo $entry_email; ?></label>
            <input type="text" name="email" value="" id="input-email" class="form-control" />
          </div>
        </div>
		<?php } ?>
		<?php if ($name > 0) { ?>
		<div class="form-group <?php if ($name == 2) { echo 'required'; }?>">
		  <div class="col-sm-12">
            <label class="control-label" for="input-name"><?php echo $entry_name; ?></label>
            <input type="text" name="name" value="" id="input-name" class="form-control" />
          </div>
        </div>
		<?php } ?>
		<?php if ($text > 0) { ?>
		<div class="form-group <?php if ($text == 2) { echo 'required'; }?>">
          <div class="col-sm-12">
            <label class="control-label" for="input-text"><?php echo $entry_text; ?></label>
            <textarea name="text" rows="5" id="input-text" class="form-control"></textarea>
          </div>
        </div>
		<?php } ?>
		<?php if ($good > 0) { ?>
		<div class="form-group <?php if ($good == 2) { echo 'required'; } ?>">
          <div class="col-sm-12">
            <label class="control-label" for="input-good"><?php echo $entry_good; ?></label>
            <textarea name="good" rows="5" id="input-good" class="form-control"></textarea>
          </div>
        </div>
		<?php } ?>
		<?php if ($bad > 0) { ?>
		<div class="form-group <?php if ($bad == 2) { echo 'required'; } ?>">
          <div class="col-sm-12">
            <label class="control-label" for="input-bad"><?php echo $entry_bad; ?></label>
            <textarea name="bad" rows="5" id="input-bad" class="form-control"></textarea>
          </div>
        </div>
		<?php } ?>
		<?php if ($photo) { ?>		
        <div class="form-group <?php if ($photo == 2) { echo 'required'; } ?>">
          <div class="col-sm-12">
            <label class="control-label"><?php echo $entry_photo; ?></label>
            <button type="button" id="button-upload" data-loading-text="<?php echo $text_loading; ?>" class="btn btn-default"><i class="fa fa-upload"></i> <?php echo $button_upload; ?></button>
            <input type="hidden" name="photo" value="" id="input-photo" />
		  </div>
        </div>
		<?php } ?>
		<?php if ($rating) { ?>		
        <div class="form-group">
          <div class="col-sm-12">
            <label class="control-label"><?php echo $entry_rating; ?></label>
            &nbsp;&nbsp;&nbsp; <?php echo $entry_bad_rating; ?>&nbsp;
            <input type="radio" name="rating" value="1" />
            &nbsp;
            <input type="radio" name="rating" value="2" />
            &nbsp;
            <input type="radio" name="rating" value="3" />
            &nbsp;
            <input type="radio" name="rating" value="4" />
            &nbsp;
            <input type="radio" name="rating" value="5" checked="checked"/>
            &nbsp;<?php echo $entry_good_rating; ?>
		  </div>
        </div>
		<?php } ?>		
		<?php  echo $captcha; ?>
        <div class="buttons clearfix">
          <div class="pull-right">
            <button type="button" id="button-testimonial" data-loading-text="<?php echo $text_loading; ?>" class="btn btn-primary"><?php echo $text_write; ?></button>
          </div>
        </div>
        <?php } else { ?>
        <?php echo $text_login; ?>
        <?php } ?>
      </form>
      <?php echo $content_bottom; ?></div>
    <?php echo $column_right; ?></div>
</div>
<script type="text/javascript"><!--
$('button[id^=\'button-upload\']').on('click', function() {
	var node = this;

	$('#form-upload').remove();

	$('body').prepend('<form enctype="multipart/form-data" id="form-upload" style="display: none;"><input type="file" name="file" /></form>');

	$('#form-upload input[name=\'file\']').trigger('click');

	if (typeof timer != 'undefined') {
    	clearInterval(timer);
	}

	timer = setInterval(function() {
		if ($('#form-upload input[name=\'file\']').val() != '') {
			clearInterval(timer);

			$.ajax({
				url: 'index.php?route=product/testimonial/upload',
				type: 'post',
				dataType: 'json',
				data: new FormData($('#form-upload')[0]),
				cache: false,
				contentType: false,
				processData: false,
				beforeSend: function() {
					$(node).button('loading');
				},
				complete: function() {
					$(node).button('reset');
				},
				success: function(json) {
					$('.text-danger').remove();

					if (json['error']) {
						$(node).parent().find('input').after('<div class="text-danger">' + json['error'] + '</div>');
					}

					if (json['success']) {
						alert(json['success']);

						$(node).parent().find('input').val(json['file']);
					}
				},
				error: function(xhr, ajaxOptions, thrownError) {
					alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
				}
			});
		}
	}, 500);
});
$('#testimonial').delegate('.pagination a', 'click', function(e) {
    e.preventDefault();

    $('#testimonial').fadeOut('slow');

    $('#testimonial').load(this.href);

    $('#testimonial').fadeIn('slow');
}); 

$('#testimonial').load('index.php?route=product/testimonial/testimonial');

$('#button-testimonial').on('click', function() {
	$.ajax({
		url: 'index.php?route=product/testimonial/write',
		type: 'post',
		dataType: 'json',
		data: $("#form-testimonial").serialize(),
		beforeSend: function() {
			$('#button-testimonial').button('loading');
		},
		complete: function() {
			$('#button-testimonial').button('reset');
		},
		success: function(json) {
			$('.alert-success, .alert-danger').remove();

			if (json['error']) {
				$('#testimonial').after('<div class="alert alert-danger"><i class="fa fa-exclamation-circle"></i> ' + json['error'] + '</div>');
			}

			if (json['success']) {
			    $('#testimonial').fadeOut('slow');

				$('#testimonial').load(this.href);

				$('#testimonial').fadeIn('slow');
				
				$('#testimonial').load('index.php?route=product/testimonial/testimonial');
			
				$('#testimonial').after('<div class="alert alert-success"><i class="fa fa-check-circle"></i> ' + json['success'] + '</div>');
				
				$('#form-testimonial').get(0).reset();
			}
		}
	});
});
//--></script>
<?php echo $footer; ?>