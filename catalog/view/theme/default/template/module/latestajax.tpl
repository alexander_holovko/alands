<h3><?php echo $heading_title; ?></h3>
<div class="row product_latest">
  <?php foreach ($products as $product) { ?>
  <div class="product-layout col-lg-3 col-md-3 col-sm-6 col-xs-12">
    <div class="product-thumb transition">
      <div class="image"><a href="<?php echo $product['href']; ?>"><img src="<?php echo $product['thumb']; ?>" alt="<?php echo $product['name']; ?>" title="<?php echo $product['name']; ?>" class="img-responsive" /></a></div>
      <div class="caption">
        <h4><a href="<?php echo $product['href']; ?>"><?php echo $product['name']; ?></a></h4>
        <p><?php echo $product['description']; ?></p>
        <?php if ($product['rating']) { ?>
        <div class="rating">
          <?php for ($i = 1; $i <= 5; $i++) { ?>
          <?php if ($product['rating'] < $i) { ?>
          <span class="fa fa-stack"><i class="fa fa-star-o fa-stack-2x"></i></span>
          <?php } else { ?>
          <span class="fa fa-stack"><i class="fa fa-star fa-stack-2x"></i><i class="fa fa-star-o fa-stack-2x"></i></span>
          <?php } ?>
          <?php } ?>
        </div>
        <?php } ?>
        <?php if ($product['price']) { ?>
        <p class="price">
          <?php if (!$product['special']) { ?>
          <?php echo $product['price']; ?>
          <?php } else { ?>
          <span class="price-new"><?php echo $product['special']; ?></span> <span class="price-old"><?php echo $product['price']; ?></span>
          <?php } ?>
          <?php if ($product['tax']) { ?>
          <span class="price-tax"><?php echo $text_tax; ?> <?php echo $product['tax']; ?></span>
          <?php } ?>
        </p>
        <?php } ?>
      </div>
      <div class="button-group">
        <button type="button" onclick="cart.add('<?php echo $product['product_id']; ?>');"><i class="fa fa-shopping-cart"></i> <span class="hidden-xs hidden-sm hidden-md"><?php echo $button_cart; ?></span></button>
        <button type="button" data-toggle="tooltip" title="<?php echo $button_wishlist; ?>" onclick="wishlist.add('<?php echo $product['product_id']; ?>');"><i class="fa fa-heart"></i></button>
        <button type="button" data-toggle="tooltip" title="<?php echo $button_compare; ?>" onclick="compare.add('<?php echo $product['product_id']; ?>');"><i class="fa fa-exchange"></i></button>
      </div>
    </div>
  </div>
  <?php } ?>
</div>
<div class="row text-center">
	<a href="#" class="load_more_latest" style="display:inline-block; margin:20px auto 20px auto; padding: 0.5em 2em; border: 1px solid #069; border-radius: 5px; text-decoration:none; text-transform:uppercase;"><?php echo $more_text; ?>&nbsp;<?php echo $heading_title; ?></a>
</div>
<div class="row pagination_latest" style="display:none;">
	<div class="col-sm-6 text-left"><?php echo $pagination; ?></div>
	<div class="col-sm-6 text-right"><?php echo $results; ?></div>
</div>
<script>
/************************   AJAX LATEST LOADER   ************************/
/************************    DOMUS159@GMAIL.COM    ************************/
/**********************  проверено на ocStore 2.1.x  **********************/

var waiting = false;

function getNextLatestPage($name, url) {
	var $latest_block = '.row.product_latest';
	var $load_more = '.load_more_latest';
	if (waiting) return;
    // if (pages_count >= pages.length) return;
	waiting = true;
	old_load_more = $($load_more).html();
	$($load_more).html('ЗАГРУЗКА');
	$.ajax({
		url:url, 
		type:"GET", 
		data:'',
		success:function (data) {
			console.log("ajax success!");
			$data = $(data);
			$($load_more).html(old_load_more);
			if ($data) {
				if ($data.find($latest_block).length > 0)    {
					$($load_more).parent().before($data.find($latest_block));
				}
			}
			waiting = false;
		}
	});
}
$(document).ready(function(){
	$pages_latest = []; // массив для ссылок пагинации
	var $name = 'latest';
	var $counter_latest = 0;
	$('.row.pagination_latest').hide();
	if ($('.row.pagination_latest .pagination').length > 0){
		if ($('.row.product_latest').length > 0) {
			$('.row.pagination_latest .pagination').each(function(){
				href = $(this).find('li:last a').attr('href');
				TotalPages = href.substring(href.indexOf("latest_page=")+12);
				First_index = $(this).find('li.active span').html();
				console.log(TotalPages);
				console.log(First_index);
				i = parseInt(First_index) + 1;
				while (i <= TotalPages) {
					$pages_latest.push(href.substring(0,href.indexOf("latest_page=")+12) + i);
					i++;
				}
				console.log($pages_latest);
			});
			
			$('.load_more_latest').click(function(event) {
				event.preventDefault();
				getNextLatestPage($name, $pages_latest[$counter_latest]);
				$counter_latest++;
				if ($counter_latest >= $pages_latest.length) {$('.load_more_latest').parent().hide();}
			});
		}
	} else {$('.load_more_latest').parent().hide();}
});
</script>