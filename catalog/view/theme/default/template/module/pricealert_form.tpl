<?php if($moduleData['Enabled'] != 'no'): ?>   
    <form id="PriceAlertForm">
        <input type="hidden" name="PAProductID" value="<?php echo $PriceAlertProductID; ?>" />     
  		<?php
        $string = html_entity_decode($moduleData['CustomText'][$language_id]);
        $patterns = array();
        $patterns[0] = '/{name_field}/';
        $patterns[1] = '/{email_field}/';
        $patterns[2] = '/{submit_button}/';
        $replacements = array();
        $replacements[0] = '<input type="text" class="form-control" name="PAYourName" id="PAYourName" placeholder="Имя" value="" />';
        $replacements[1] = '<input type="text" class="form-control" name="PAYourEmail" id="PAYourEmail" placeholder="Email" value="" />';
        $replacements[2] = '<a id="PriceAlertSubmit" class="btn btn-primary">'.$PriceAlert_SubmitButton.'</a>';
        echo preg_replace($patterns, $replacements, $string);
        ?>
        <div class="PA_loader"><div class="PA_loading"></div></div>
        <div id="PriceAlertSuccess"></div>  
    </form>
	<script>
    $('#PriceAlertSubmit').on('click', function(){
        $('input#PAYourName').removeClass("PA_popover_field_error");
        $('input#PAYourEmail').removeClass("PA_popover_field_error");
        $('div.PAError').remove();
    
        var email_validate = /^([A-Za-z0-9_\-\.])+\@([A-Za-z0-9_\-\.])+\.([A-Za-z]{2,4})$/;
        if ((document.getElementById("PAYourName").value == 0) )
        {
              $('input#PAYourName').addClass("PA_popover_field_error");
              $('input#PAYourName').after('<div class="PAError"><?php echo $PriceAlert_Error1; ?></div>');
        } else if ((document.getElementById("PAYourEmail").value.length == 0)) {
              $('input#PAYourEmail').addClass("PA_popover_field_error");
              $('input#PAYourEmail').after('<div class="PAError"><?php echo $PriceAlert_Error1; ?></div>');
        } else if (!document.getElementById("PAYourEmail").value.match(email_validate)) {
              $('input#PAYourEmail').addClass("PA_popover_field_error");
              $('input#PAYourEmail').after('<div class="PAError"><?php echo $PriceAlert_Error2; ?></div>');
        } else {
            $.ajax({
                url: 'index.php?route=module/pricealert/submitform',
                type: 'post',
                data: $('#PriceAlertForm').serialize(),
                beforeSend: function(){
                      $('#PriceAlertSubmit').hide();
                      $('.PA_loader').show();
                },
                success: function(response) {
                    $('.PA_loader').hide();
                    $('#PriceAlertSubmit').hide();
                    $('#PriceAlertSuccess').html("<div class='alert alert-success PA' style='display: none;'><?php echo $PriceAlert_Success; ?></div>");
                    $('.PA').fadeIn('slow');
                    $('#PAYourName').val('');
                    $('#PAYourEmail').val('');
                }
            });
        }
    });
    </script>
    <?php if(!empty($moduleData['CustomCSS'])): ?>
		<style>
            <?php echo htmlspecialchars_decode($moduleData['CustomCSS']); ?>
        </style>
    <?php endif; ?>
<?php endif; ?>