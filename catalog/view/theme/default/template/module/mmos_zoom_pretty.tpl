<style>
    .pp_close{
        z-index: 999;
    }
    .flexslider .slides img {
        width: auto;  }


    .flexslider {
        margin: 0 0 20px 0px;
        background: none;
        border: 0px solid #fff;
        position: relative;
        -webkit-border-radius: 0px;
        -moz-border-radius: 0px;
        -o-border-radius: 0px;
        border-radius: 0px;
        -webkit-box-shadow: none;
        -moz-box-shadow: none;
        -o-box-shadow: none;
        box-shadow: none; 
        zoom: 1; 
    }
    .flex-direction-nav a {
        color: <?php echo  isset($mmos_zoom_pretty['nav_but_color']) ? $mmos_zoom_pretty['nav_but_color'] : '#F00'; ?>; 
        overflow: visible;
    }	
    .zoomWindow {
        border: <?php echo $mmos_zoom_pretty['border_cloudzoom'];?>px solid <?php echo $mmos_zoom_pretty['color_border_cloudzoom'];?>;
		top: -1px !important;
	}
	.zoomWrapper {
	   border: 1px solid #ffffff;
	}
    .mmosolutio_button{
        position: absolute;
        right: 1px;
        top: 1px;
        display: block;
        font-size: 2em;
		z-index: 9;
    }
    .thumbnail div{
        margin-left: auto;
        margin-right: auto;
    }
    .zoomWindowContainer div{
        margin-left: 20px;
    }
</style>
<div id="slider" style="margin-bottom: 10px;position: relative;">
    <ul class="slides thumbnail thumb-center">
		<?php if($mmos_zoom_pretty['popup_button'] == 1) { ?>    <?php } ?>
        <?php foreach ($images_mmos_zoom_pretty as $image) { ?>
        <li>
            <a rel="prettyPhoto[pp_gal]"  href="<?php echo $image['popup']; ?>" title="<?php echo $heading_title; ?>">
                <img class="img-responsive jqzoom" data-zoom-image="<?php echo $image['popup']; ?>" src="<?php echo $image['thumb']; ?>" title="<?php echo $heading_title; ?>" alt="<?php echo $heading_title; ?>" /></a>
        </li>
        <?php } ?>
    </ul>
</div>
<?php if(count($images_mmos_zoom_pretty) > 1) { ?>
<?php if($mmos_zoom_pretty['carousel'] == '1') { ?>
<div id="carousel" class="flexslider carousel">
    <ul class="slides">
        <?php foreach ($images_mmos_zoom_pretty as $image) { ?>
        <li>
            <img class="thumbnail" data-large="<?php echo $image['popup']; ?>" data-thumb="<?php echo $image['thumb']; ?>" src="<?php echo $image['addition']; ?>" title="<?php echo $heading_title; ?>" alt="<?php echo $heading_title; ?>" style="margin-top: 15px;"/>       
        </li>
        <?php } ?>
    </ul>
</div>
<?php } else { ?>
<div class=" thumbs-list">
<ul class="thumbnails prod" id="noncarousel">
	<?php foreach ($images_mmos_zoom_pretty as $image) { ?>
	<li class="image-additional">
		<a class="thumbnail" href="" data-image="<?php echo $image['thumb']; ?>" data-zoom-image="<?php echo $image['popup']; ?>"> <img src="<?php echo $image['addition']; ?>" title="<?php echo $heading_title; ?>" alt="<?php echo $heading_title; ?>" /></a>
	</li>
	<?php } ?>
</ul></div>
<?php } ?>
<?php } ?>
<script type="text/javascript">
$(document).ready(function(){
    var lwidth = $('#slider').width();
    var qty = <?php echo $qty_items; ?> ;
	
	<?php if ($mmos_zoom_pretty['carousel'] == '1') { ?>
        curent_id = 'carousel';
    <?php } else { ?>
        curent_id = 'noncarousel';
    <?php } ?>
	
    $('#carousel').flexslider({
            animation: "slide",
            controlNav: false,
            animationLoop: true,
            slideshow: false,
            prevText:	"",
            nextText:	"",
            keyboard:	true,
            direction:	"horizontal",
            itemWidth: <?php echo $width; ?> ,
            itemMargin:10,
            minItems: 1,
            maxItems: <?php echo $qty_items; ?> ,
            asNavFor: '#slider'
    });
    $('#slider').flexslider({
            animation: "fade",
            controlNav: false,
            animationLoop: false,
            slideshow: false,
            prevText:	"",
            nextText:	"",
            sync: "#carousel"
    });
    
    var device_view = 'window';
	if(($(window).width()/$('.thumbnail  .img-responsive').width()) < 2 ) {
        device_view = 'inner';
    }
    
    var mmos_zoom_config = { 
		gallery: curent_id,
        cursor: 'pointer',
        imageCrossfade: true,
        zoomType: device_view,
        zoomWindowFadeIn: 750,
        zoomWindowFadeOut: 1000,
		scrollZoom : true,
        responsive : true,
        zoomWindowWidth: <?php echo $mmos_zoom_pretty['zoom_size']; ?> ,
        zoomWindowHeight: <?php echo $mmos_zoom_pretty['zoom_size']; ?> ,
        borderSize : <?php echo $mmos_zoom_pretty['border_cloudzoom'];?> ,
        borderColour : "<?php echo $mmos_zoom_pretty['color_border_cloudzoom'];?>"

    };
        
    $(".jqzoom").elevateZoom(mmos_zoom_config);

    $("a[rel^='prettyPhoto']").prettyPhoto({
        animation_speed: '<?php echo $mmos_zoom_pretty['speed_popup'] ; ?>',
        slideshow: <?php echo $mmos_zoom_pretty['speed_slide']; ?> ,
        autoplay_slideshow: <?php echo $mmos_zoom_pretty['autoplay']; ?> ,
        opacity: <?php echo $mmos_zoom_pretty['opacity']; ?> ,
        show_title: <?php echo $mmos_zoom_pretty['title_popup']; ?> ,
        allow_resize: <?php echo $mmos_zoom_pretty['resize_popup']; ?> ,
        default_width: <?php echo  $width_default; ?> ,
        default_height: <?php echo  $height_default; ?> ,
        counter_separator_label: '/',
        theme: '<?php echo $mmos_zoom_pretty['theme_popup'] ; ?>',
        modal: false,
        overlay_gallery: true,
        <?php if ($mmos_zoom_pretty['social_button'] == '0') { echo "social_tools: '',"; }  ?>
    }); 
	
	<?php if($mmos_zoom_pretty['carousel'] == 1) { ?>
		$('#carousel ul li').css('width', (lwidth/qty));
	<?php } ?>
});


</script>







