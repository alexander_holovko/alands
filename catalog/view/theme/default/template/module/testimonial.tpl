<h3><?php echo $heading_title; ?></h3>
<div class="row">
  <?php foreach ($testimonials as $testimonial) { ?>
  <div class="product-layout col-lg-3 col-md-3 col-sm-6 col-xs-12">
    <div class="product-thumb transition">
      <div class="caption">
	    <?php if ($photo && $testimonial['photo']) { ?>
        <div class="image"><img src="<?php echo $testimonial['photo']; ?>" alt="" class="img-responsive" /></div>
		<?php } ?>
		<?php if ($title && $testimonial['title']) { ?>
        <h4><?php echo $testimonial['title']; ?></h4>
		<?php } ?>
        <?php if ($good && $testimonial['good']) { ?>
		<p><strong><?php echo $entry_good; ?></strong><br/><?php echo $testimonial['good']; ?></p>
		<?php } ?>
		<?php if ($bad && $testimonial['bad']) { ?>
		<p><strong><?php echo $entry_bad; ?></strong><br/><?php echo $testimonial['bad']; ?></p>
		<?php } ?>
		<?php if ($text && $testimonial['text']) { ?>
		<p><strong><?php echo $entry_text; ?></strong><br/><?php echo $testimonial['text']; ?></p>
		<?php } ?>
		<?php if ($rating) { ?>
        <div class="rating">
          <?php for ($i = 1; $i <= 5; $i++) { ?>
          <?php if ($testimonial['rating'] < $i) { ?>
          <span class="fa fa-stack"><i class="fa fa-star-o fa-stack-2x"></i></span>
          <?php } else { ?>
          <span class="fa fa-stack"><i class="fa fa-star fa-stack-2x"></i><i class="fa fa-star-o fa-stack-2x"></i></span>
          <?php } ?>
          <?php } ?>
        </div>
		<?php } ?>
        <p>
          <?php if ($name_status && $testimonial['name']) { ?>
			<?php echo '<i>' . $testimonial['name'] . '</i>'; ?>
			<?php } ?>
			<?php if ($city && $testimonial['city']) { ?>
			<?php echo ' <i>' . $testimonial['city'] . '</i>'; ?>
			<?php } ?>
			<?php if ($date_added && $testimonial['date_added']) { ?>
			<?php echo ' ' . $testimonial['date_added']; ?>
			<?php } ?>
        </p>
      </div>
    </div>
  </div>
  <?php } ?>
</div>
<div class="row">
  <div class="col-md-12">
    <div class="button-group pull-right">
      <a class="btn btn-primary" href="<?php echo $show_all_url;?>">
	  <?php echo $show_all; ?></a>
    </div>
  </div>
</div>