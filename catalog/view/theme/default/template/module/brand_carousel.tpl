<div id="brand_carousel_content<?php echo $module_brandcarousel; ?>">
	<?php if ($name_block) { ?>
		<center><H1><?php echo $name_block; ?></H1></center>
	<?php } ?>
	<ul id="brand_carousel<?php echo $module_brandcarousel; ?>" class="brand_carousel_skin">
		<?php foreach ($brands as $brand_carusel) { ?>
			<li>
				<a href="<?php echo $brand_carusel['link']; ?>" title="<?php echo $brand_carusel['title']; ?>">
					<img src="<?php echo $brand_carusel['image']; ?>" alt="<?php echo $brand_carusel['title']; ?>" />
				</a>
			</li>
		<?php } ?>
	</ul>
</div>
<script type="text/javascript">
$(window).load(function() {
    $("#brand_carousel<?php echo $module_brandcarousel; ?>").flexisel({
        visibleItems: <?php echo $scroll_limit; ?>,
        itemsToScroll: <?php echo $scroll; ?>,
        animationSpeed: <?php echo $animation_speed; ?>,
        infinite: <?php echo $infinite; ?>,
        navigationTargetSelector: null,
        autoPlay: {
            enable: <?php echo $scroll_auto; ?>,
            interval: <?php echo $interval; ?>,
            pauseOnHover: <?php echo $scroll_pause; ?>
        },
        responsiveBreakpoints: { 
            portrait: { 
                changePoint:480,
                visibleItems: 1,
                itemsToScroll: 1
            }, 
            landscape: { 
                changePoint:640,
                visibleItems: 2,
                itemsToScroll: 2
            },
            tablet: { 
                changePoint:768,
                visibleItems: 3,
                itemsToScroll: 3
            }
        }
    });   
});
</script>
