<?php if ($menu_type == 'tree') { ?>
<div id="yo-<?php echo $module; ?>" class="<?php echo $class; ?>">
  <?php if ($heading_title) { ?>
  <div class="yo-heading yo-toggle <?php echo $minimized ? 'yo-close' : 'yo-open'; ?>"><i class="fa fa-bars"></i><span><?php echo $heading_title; ?></span></div>
  <?php } ?>
  <div class="yo-wrapper" <?php echo $minimized ? 'style="display:none"' : ''; ?>>
    <ul class="yo-pm">
      <?php foreach ($categories as $category) { ?>
      <li <?php echo $category['active'] ? 'class="active"' : ''; ?>>
        <a href="<?php echo $category['href']; ?>" <?php echo $category['active'] ? 'class="active"' : ''; ?>>
          <?php if ($pm_icon && $category['icon']) { ?>
          <img class="pm-icon" src="image/<?php echo $category['icon']; ?>" alt="<?php echo $category['name']; ?>">
          <?php } ?>
          <span><?php echo $category['name']; ?></span>
          <?php if ($count) { ?>
          <span class="pm-badge"><?php echo $category['count']; ?></span>
          <?php } ?>
        </a>
        <?php if ($category['children1']) { ?>
        <ul>
          <li><a href="#" class="pm-back"><span class="pm-back-btn"><?php echo $button_back; ?></span></a></li>
          <?php if ($item_image && $category['thumb']) { ?>
          <li class="pm-image"><a href="<?php echo $category['href']; ?>" title="<?php echo $category['name']; ?>"><img src="<?php echo $category['thumb']; ?>" alt="<?php echo $category['name']; ?>"></a></li>
          <?php } ?>
          <?php foreach ($category['children1'] as $child1) { ?>
          <li <?php echo $child1['active'] ? 'class="active"' : ''; ?>>
            <a href="<?php echo $child1['href']; ?>" <?php echo $child1['active'] ? 'class="active"' : ''; ?>>
              <?php if ($pm_icon && $child1['icon']) { ?>
              <img class="pm-icon" src="image/<?php echo $child1['icon']; ?>" alt="<?php echo $child1['name']; ?>">
              <?php } ?>
              <span><?php echo $child1['name']; ?></span>
              <?php if ($count) { ?>
              <span class="pm-badge"><?php echo $child1['count']; ?></span>
              <?php } ?>
            </a>
            <?php if ($child1['children2']) { ?>
            <ul>
              <li><a href="#" class="pm-back"><span class="pm-back-btn"><?php echo $button_back; ?></span></a></li>
              <?php if ($item_image && $child1['thumb']) { ?>
              <li class="pm-image"><a href="<?php echo $child1['href']; ?>" title="<?php echo $child1['name']; ?>"><img src="<?php echo $child1['thumb']; ?>" alt="<?php echo $child1['name']; ?>"></a></li>
              <?php } ?>
              <?php foreach ($child1['children2'] as $child2) { ?>
              <li <?php echo $child2['active'] ? 'class="active"' : ''; ?>>
                <a href="<?php echo $child2['href']; ?>" <?php echo $child2['active'] ? 'class="active"' : ''; ?>>
                  <?php if ($pm_icon && $child2['icon']) { ?>
                  <img class="pm-icon" src="image/<?php echo $child2['icon']; ?>" alt="<?php echo $child2['name']; ?>">
                  <?php } ?>
                  <span><?php echo $child2['name']; ?></span>
                  <?php if ($count) { ?>
                  <span class="pm-badge"><?php echo $child2['count']; ?></span>
                  <?php } ?>
                </a>
                <?php if ($child2['children3']) { ?>
                <ul>
                  <li><a href="#" class="pm-back"><span class="pm-back-btn"><?php echo $button_back; ?></span></a></li>
                  <?php if ($item_image && $child2['thumb']) { ?>
                  <li class="pm-image"><a href="<?php echo $child2['href']; ?>" title="<?php echo $child2['name']; ?>"><img src="<?php echo $child2['thumb']; ?>" alt="<?php echo $child2['name']; ?>"></a></li>
                  <?php } ?>
                  <?php foreach ($child2['children3'] as $child3) { ?>
                  <li <?php echo $child3['active'] ? 'class="active"' : ''; ?>>
                    <a href="<?php echo $child3['href']; ?>" <?php echo $child3['active'] ? 'class="active"' : ''; ?>>
                      <?php if ($pm_icon && $child3['icon']) { ?>
                      <img class="pm-icon" src="image/<?php echo $child3['icon']; ?>" alt="<?php echo $child3['name']; ?>">
                      <?php } ?>
                      <span><?php echo $child3['name']; ?></span>
                      <?php if ($count) { ?>
                      <span class="pm-badge"><?php echo $child3['count']; ?></span>
                      <?php } ?>
                    </a>
                    <?php if ($child3['children4']) { ?>
                    <ul>
                      <li><a href="#" class="pm-back"><span class="pm-back-btn"><?php echo $button_back; ?></span></a></li>
                      <?php if ($item_image && $child3['thumb']) { ?>
                      <li class="pm-image"><a href="<?php echo $child3['href']; ?>" title="<?php echo $child3['name']; ?>"><img src="<?php echo $child3['thumb']; ?>" alt="<?php echo $child3['name']; ?>"></a></li>
                      <?php } ?>
                      <?php foreach ($child3['children4'] as $child4) { ?>
                      <li <?php echo $child4['active'] ? 'class="active"' : ''; ?>>
                        <a href="<?php echo $child4['href']; ?>" <?php echo $child4['active'] ? 'class="active"' : ''; ?>>
                          <?php if ($pm_icon && $child4['icon']) { ?>
                          <img class="pm-icon" src="image/<?php echo $child4['icon']; ?>" alt="<?php echo $child4['name']; ?>">
                          <?php } ?>
                          <span><?php echo $child4['name']; ?></span>
                          <?php if ($count) { ?>
                          <span class="pm-badge"><?php echo $child4['count']; ?></span>
                          <?php } ?>
                        </a>
                      </li>
                      <?php } ?>
                    </ul>
                    <?php } ?>
                  </li>
                  <?php } ?>
                </ul>
                <?php } ?>
              </li>
              <?php } ?>
            </ul>
            <?php } ?>
          </li>
          <?php } ?>
        </ul>
        <?php } ?>
      </li>
      <?php } ?>
    </ul>
  </div>
</div>
<?php } ?>

<?php if ($menu_type == 'current' || $menu_type == 'parent') { ?>
<?php if ($categories) { ?>
<div id="yo-<?php echo $module; ?>" class="<?php echo $class; ?>">
  <?php if ($heading_title) { ?>
  <div class="yo-heading yo-toggle <?php echo $minimized ? 'yo-close' : 'yo-open'; ?>"><i class="fa fa-bars"></i><span><?php echo $heading_title; ?></span></div>
  <?php } ?>
  <div class="yo-wrapper" <?php echo $minimized ? 'style="display:none"' : ''; ?>>
    <ul class="yo-pm">
      <?php foreach ($categories as $category) { ?>
      <li <?php echo $category['active'] ? 'class="active"' : ''; ?>>
        <a href="<?php echo $category['href']; ?>" <?php echo $category['active'] ? 'class="active"' : ''; ?>>
          <?php if ($pm_icon && $category['icon']) { ?>
          <img class="pm-icon" src="image/<?php echo $category['icon']; ?>" alt="<?php echo $category['name']; ?>">
          <?php } ?>
          <span><?php echo $category['name']; ?></span>
          <?php if ($current_count) { ?>
          <span class="pm-badge"><?php echo $category['count']; ?></span>
          <?php } ?>
        </a>
        <?php if ($category['children2']) { ?>
        <ul>
          <li><a href="#" class="pm-back"><span class="pm-back-btn"><?php echo $button_back; ?></span></a></li>
          <?php if ($item_image && $category['thumb']) { ?>
          <li class="pm-image"><a href="<?php echo $category['href']; ?>" title="<?php echo $category['name']; ?>"><img src="<?php echo $category['thumb']; ?>" alt="<?php echo $category['name']; ?>"></a></li>
          <?php } ?>
          <?php foreach ($category['children2'] as $child2) { ?>
          <li <?php echo $child2['active'] ? 'class="active"' : ''; ?>>
            <a href="<?php echo $child2['href']; ?>" <?php echo $child2['active'] ? 'class="active"' : ''; ?>>
              <?php if ($products_by_category && $child2['price']) { ?>
              <span class="pm-price">
                <?php if (!$child2['special']) { ?>
                <?php echo $child2['price']; ?>
                <?php } else { ?>
                <?php echo $child2['special']; ?>
                <?php } ?>
              </span>
              <?php } ?>
              <?php if (!$products_by_category && $pm_icon && $child2['icon']) { ?>
              <img class="pm-icon" src="image/<?php echo $child2['icon']; ?>" alt="<?php echo $child2['name']; ?>">
              <?php } ?>
              <span><?php echo $child2['name']; ?></span>
              <?php if (!$products_by_category && $current_count) { ?>
              <span class="pm-badge"><?php echo $child2['count']; ?></span>
              <?php } ?>
            </a>
            <?php if ($child2['children3']) { ?>
            <ul>
              <li><a href="#" class="pm-back"><span class="pm-back-btn"><?php echo $button_back; ?></span></a></li>
              <?php if ($item_image && $child2['thumb']) { ?>
              <li class="pm-image"><a href="<?php echo $child2['href']; ?>" title="<?php echo $child2['name']; ?>"><img src="<?php echo $child2['thumb']; ?>" alt="<?php echo $child2['name']; ?>"></a></li>
              <?php } ?>
              <?php foreach ($child2['children3'] as $child3) { ?>
              <li <?php echo $child3['active'] ? 'class="active"' : ''; ?>>
                <a href="<?php echo $child3['href']; ?>" <?php echo $child3['active'] ? 'class="active"' : ''; ?>>
                  <?php if ($pm_icon && $child3['icon']) { ?>
                  <img class="pm-icon" src="image/<?php echo $child3['icon']; ?>" alt="<?php echo $child3['name']; ?>">
                  <?php } ?>
                  <span><?php echo $child3['name']; ?></span>
                  <?php if ($current_count) { ?>
                  <span class="pm-badge"><?php echo $child3['count']; ?></span>
                  <?php } ?>
                </a>
                <?php if ($child3['children4']) { ?>
                <ul>
                  <li><a href="#" class="pm-back"><span class="pm-back-btn"><?php echo $button_back; ?></span></a></li>
                  <?php if ($item_image && $child3['thumb']) { ?>
                  <li class="pm-image"><a href="<?php echo $child3['href']; ?>" title="<?php echo $child3['name']; ?>"><img src="<?php echo $child3['thumb']; ?>" alt="<?php echo $child3['name']; ?>"></a></li>
                  <?php } ?>
                  <?php foreach ($child3['children4'] as $child4) { ?>
                  <li <?php echo $child4['active'] ? 'class="active"' : ''; ?>>
                    <a href="<?php echo $child4['href']; ?>" <?php echo $child4['active'] ? 'class="active"' : ''; ?>
                      <?php if ($pm_icon && $child4['icon']) { ?>
                      <img class="pm-icon" src="image/<?php echo $child4['icon']; ?>" alt="<?php echo $child4['name']; ?>">
                      <?php } ?>
                      <span><?php echo $child4['name']; ?></span>
                      <?php if ($current_count) { ?>
                      <span class="pm-badge"><?php echo $child4['count']; ?></span>
                      <?php } ?>
                    </a>
                  </li>
                  <?php } ?>
                </ul>
                <?php } ?>
              </li>
              <?php } ?>
            </ul>
            <?php } ?>
          </li>
          <?php } ?>
        </ul>
        <?php } ?>
      </li>
      <?php } ?>
    </ul>
  </div>
</div>
<?php } ?>
<?php } ?>

<?php if ($menu_type == 'brands') { ?>
<div id="yo-<?php echo $module; ?>" class="<?php echo $class; ?>">
  <?php if ($heading_title) { ?>
  <div class="yo-heading yo-toggle <?php echo $minimized ? 'yo-close' : 'yo-open'; ?>"><i class="fa fa-bars"></i><span><?php echo $heading_title; ?></span></div>
  <?php } ?>
  <div class="yo-wrapper" <?php echo $minimized ? 'style="display:none"' : ''; ?>>
    <ul class="yo-pm">
      <?php foreach ($manufacturers as $manufacturer) { ?>
      <li <?php echo $manufacturer['active'] || $manufacturer['manufacturer_id'] == $bids ? 'class="active"' : ''; ?>> <a <?php echo $manufacturer['active'] || $manufacturer['manufacturer_id'] == $bids ? 'class="active"' : ''; ?> href="<?php echo $manufacturer['href']; ?>"> <span class="pm-arrow"><?php echo $manufacturer['name']; ?></span> </a>
        <?php if ($manufacturer['children']) { ?>
        <ul>
          <li><a href="#" class="pm-back"><span class="pm-back-btn"><?php echo $button_back; ?></span></a></li>
          <?php if ($item_image && $manufacturer['thumb']) { ?>
          <li class="pm-image"><a href="<?php echo $manufacturer['href']; ?>" title="<?php echo $manufacturer['name']; ?>"><img src="<?php echo $manufacturer['thumb']; ?>" alt="<?php echo $manufacturer['name']; ?>"></a></li>
          <?php } ?>
          <?php foreach ($manufacturer['children'] as $child) { ?>
          <li <?php echo $child['active'] ? 'class="active"' : ''; ?>> <a href="<?php echo $child['href']; ?>" <?php echo $child['active'] ? 'class="active"' : ''; ?>>
            <?php if ($child['price']) { ?>
            <span class="pm-price">
              <?php if (!$child['special']) { ?>
              <?php echo $child['price']; ?>
              <?php } else { ?>
              <?php echo $child['special']; ?>
              <?php } ?>
            </span>
            <?php } ?>
            <span class="pm-arrow"><?php echo $child['name']; ?> </span> </a> </li>
          <?php } ?>
        </ul>
        <?php } ?>
      </li>
      <?php } ?>
    </ul>
  </div>
</div>
<?php } ?>

<script type="text/javascript"><!--

  $('#yo-<?php echo $module; ?> ul.yo-pm').parent().css('overflow', 'hidden');
  $('#yo-<?php echo $module; ?> ul.yo-pm ul').prev().addClass('pm-parent').has('span.pm-badge').closest('ul').children('li').not('.pm-image').find('>a').not('.pm-back').addClass('pm-item');

  $('#yo-<?php echo $module; ?> a.pm-parent').click(function() {
    $(this).closest('ul').addClass('pm-invisible').css('visibility', 'hidden');
    $(this).next().fadeIn(50).css('visibility', 'visible');
    $(this).parent().siblings().find('>ul').hide();

    var height = $(this).next().outerHeight(true);
    $('#yo-<?php echo $module; ?> .yo-pm').parent().animate({'height': height}, 200);

    return false;
  });

  $('#yo-<?php echo $module; ?> .pm-back').click(function() {

    var height = $(this).closest('.pm-invisible').outerHeight(true);
    $('#yo-<?php echo $module; ?> .yo-pm').parent().animate({'height': height}, 200);
    
    $(this).closest('.pm-invisible').removeClass('pm-invisible').css('visibility', 'visible');
    $(this).closest('ul').fadeOut(50);
    return false;
  });

  if (<?php echo $save_view; ?>) {
    if (!localStorage.getItem('yo-pm-<?php echo $module; ?>')) {
      if (<?php echo $minimized; ?>) {
        localStorage.setItem('yo-pm-<?php echo $module; ?>', 'close');
      } else {
        localStorage.setItem('yo-pm-<?php echo $module; ?>', 'open');
      }
    }

    $('#yo-<?php echo $module; ?> .yo-toggle').click(function() {
      $(this).toggleClass('yo-open yo-close').next().slideToggle(<?php echo $easing_d; ?>,'<?php echo $easing; ?>');
      if ($(this).hasClass('yo-open')) {
        localStorage.setItem('yo-pm-<?php echo $module; ?>', 'open');
      } else {
        localStorage.setItem('yo-pm-<?php echo $module; ?>', 'close');
      }
    });

    if (localStorage.getItem('yo-pm-<?php echo $module; ?>') == 'open') { 
      $('#yo-<?php echo $module; ?> .yo-toggle').addClass('yo-open').removeClass('yo-close').next().show(); 
    } else {
      $('#yo-<?php echo $module; ?> .yo-toggle').addClass('yo-close').removeClass('yo-open').next().hide();
    }
  } else {
    localStorage.removeItem('yo-pm-<?php echo $module; ?>');
    $('#yo-<?php echo $module; ?> .yo-toggle').click(function() {
      $(this).toggleClass('yo-open yo-close').next().slideToggle(<?php echo $easing_d; ?>,'<?php echo $easing; ?>');
    });
  }

//--></script>