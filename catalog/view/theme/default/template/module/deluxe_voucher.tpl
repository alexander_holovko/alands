<?php echo $header; ?>
<div class="container">
  <ul class="breadcrumb">
    <?php foreach ($breadcrumbs as $breadcrumb) { ?>
    <li><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a></li>
    <?php } ?>
  </ul>
  <?php if ($error_warning) { ?>
  <div class="alert alert-danger"><i class="fa fa-exclamation-circle"></i> <?php echo $error_warning; ?></div>
  <?php } ?>
  <div class="row"><?php echo $column_left; ?>
    <?php if ($column_left && $column_right) { ?>
    <?php $class = 'col-sm-6'; ?>
    <?php } elseif ($column_left || $column_right) { ?>
    <?php $class = 'col-sm-9'; ?>
    <?php } else { ?>
    <?php $class = 'col-sm-12'; ?>
    <?php } ?>
    <div id="content" class="<?php echo $class; ?>"><?php echo $content_top; ?>
      <h1><?php echo $heading_title; ?></h1>
      <p><?php echo $text_description; ?></p>
      <form action="<?php echo $action; ?>" method="post" enctype="multipart/form-data" class="form-horizontal" id="voucher-form">
        <input name="code" type="hidden" value="<?php echo $code; ?>">
        <div class="form-group required">
          <label class="col-sm-2 control-label" for="input-to-name"><?php echo $entry_to_name; ?></label>
          <div class="col-sm-10">
            <input type="text" name="to_name" value="<?php echo $to_name; ?>" id="input-to-name" class="form-control" />
            <?php if ($error_to_name) { ?>
            <div class="text-danger"><?php echo $error_to_name; ?></div>
            <?php } ?>
          </div>
        </div>
        <div class="form-group required">
          <label class="col-sm-2 control-label" for="input-to-email"><?php echo $entry_to_email; ?></label>
          <div class="col-sm-10">
            <input type="text" name="to_email" value="<?php echo $to_email; ?>" id="input-to-email" class="form-control" />
            <?php if ($error_to_email) { ?>
            <div class="text-danger"><?php echo $error_to_email; ?></div>
            <?php } ?>
          </div>
        </div>
        <div class="form-group required">
          <label class="col-sm-2 control-label" for="input-from-name"><?php echo $entry_from_name; ?></label>
          <div class="col-sm-10">
            <input type="text" name="from_name" value="<?php echo $from_name; ?>" id="input-from-name" class="form-control" />
            <?php if ($error_from_name) { ?>
            <div class="text-danger"><?php echo $error_from_name; ?></div>
            <?php } ?>
          </div>
        </div>
        <div class="form-group required">
          <label class="col-sm-2 control-label" for="input-from-email"><?php echo $entry_from_email; ?></label>
          <div class="col-sm-10">
            <input type="text" name="from_email" value="<?php echo $from_email; ?>" id="input-from-email" class="form-control" />
            <?php if ($error_from_email) { ?>
            <div class="text-danger"><?php echo $error_from_email; ?></div>
            <?php } ?>
          </div>
        </div>
        <div class="form-group required">
          <label class="col-sm-2 control-label"><?php echo $entry_theme; ?></label>
          <div class="col-sm-10">
            <?php foreach ($voucher_themes as $voucher_theme) { ?>
            <div class="radio">
              <label>
                <input type="radio" name="voucher_theme_id" value="<?php echo $voucher_theme['voucher_theme_id']; ?>" <?php if ($voucher_theme_id == $voucher_theme['voucher_theme_id']) echo 'checked'; ?> />
                <?php if ($voucher_theme['thumb']) { ?>
                <img src="<?php echo $voucher_theme['thumb']; ?>" alt="<?php echo $voucher_theme['name']; ?>" class="media-middle" />
                <?php } ?>
                <?php echo $voucher_theme['name']; ?>
              </label>
            </div>
            <?php } ?>
            <?php if ($error_theme) { ?>
            <div class="text-danger"><?php echo $error_theme; ?></div>
            <?php } ?>
          </div>
        </div>
        <div class="form-group">
          <label class="col-sm-2 control-label" for="input-message"><span data-toggle="tooltip" title="<?php echo $help_message; ?>"><?php echo $entry_message; ?></span></label>
          <div class="col-sm-10">
            <textarea name="message" cols="40" rows="5" id="input-message" class="form-control"><?php echo $message; ?></textarea>
          </div>
        </div>
        <div <?php if ($hide_date) echo 'style="display: none;"'; ?> class="form-group">
          <label class="col-sm-2 control-label" for="input-deliverydate"><span data-toggle="tooltip" title="<?php echo $lng['voucher_deliverydate_help']; ?>"><?php echo $lng['voucher_deliverydate_entry']; ?></span></label>
          <div class="col-sm-4">
            <div class="input-group">
              <input type="text" name="deliverydate" value="<?php echo $deliverydate; ?>" id="input-deliverydate" class="form-control date" readonly />
              <span class="input-group-btn">
                <button class="btn btn-warning" type="button" title="<?php echo $lng['button_delete']; ?>" onclick="$('#input-deliverydate').val('');"><i class="fa fa-times"></i></button>
              </span>
            </div>
          </div>
        </div>
        <div class="form-group">
          <label class="col-sm-2 control-label" for="input-amount"><span data-toggle="tooltip" title="<?php echo $help_amount; ?>"><?php echo $entry_amount; ?></span></label>
          <div class="col-sm-2">
            <?php if ($prices) { ?>
            <select name="amount" class="form-control">
              <?php foreach ($prices as $price => $text_price) { ?>
              <option <?php if ($price == $amount) echo 'selected'; ?> value="<?php echo $price; ?>"><?php echo $text_price; ?></option>
              <?php } ?>
            </select>
            <?php } else { ?>
            <input type="text" name="amount" value="<?php echo $amount; ?>" id="input-amount" class="form-control" size="5" />
            <?php if ($error_amount) { ?>
            <div class="text-danger"><?php echo $error_amount; ?></div>
            <?php } ?>
            <?php } ?>
          </div>
        </div>
        <div class="buttons clearfix">
          <div class="pull-right"> <?php echo $text_agree; ?>
            <input type="checkbox" name="agree" value="1" checked="checked" />
            &nbsp;
            <input type="submit" value="<?php echo $button_continue; ?>" class="btn btn-primary" />
            <button type="button" class="btn btn-info" data-toggle="modal" data-target="#voucher-preview" onclick="preview();"><?php echo $lng['voucher_preview_button']; ?></button>
          </div>
        </div>
      </form>
      <?php echo $content_bottom; ?></div>
    <?php echo $column_right; ?></div>
</div>
<div id="voucher-preview" class="modal fade" role="dialog">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title"><?php echo $lng['voucher_preview_heading']; ?></h4>
      </div>
      <div class="modal-body"></div>
    </div>
  </div>
</div>
<script type="text/javascript">
$('.date').datetimepicker({
	minDate: '<?php echo $min_date; ?>',
	<?php if ($max_date) { ?>
	maxDate: '<?php echo $max_date; ?>',
	<?php } ?>
	format: 'YYYY-MM-DD',
	pickTime: false
});

function preview() {
  $('#voucher-preview .modal-body').load('index.php?route=module/deluxe_voucher/preview&' + $('#voucher-form').serialize());
} //preview end
</script>
<?php echo $footer; ?>