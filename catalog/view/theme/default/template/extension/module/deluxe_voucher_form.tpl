<div class="panel panel-default deluxe-voucher">
  <div class="panel-heading"><?php echo $lng['voucher_balance_heading']; ?></div>
  <div class="panel-body">
    <div class="input-group btn-block">
      <input type="text" class="form-control voucher-code" placeholder="<?php echo $lng['voucher_balance_entry']; ?>" />
      <span class="input-group-btn">
        <button type="button" data-toggle="tooltip" data-original-title="<?php echo $lng['voucher_balance_button']; ?>" class="btn btn-primary" onclick="getBalance(this);"><i class="fa fa-check-circle"></i></button>
      </span>
    </div>
    <div class="voucher-balance"></div>
  </div>
</div>
<script type="text/javascript">
function getBalance(obj) {
  $.ajax({
    url: 'index.php?route=extension/module/deluxe_voucher/getBalance',
    data: 'code=' + $(obj).parents('.deluxe-voucher').find('.voucher-code').val(),
    type: 'post',
    dataType: 'html',
    success: function(html) {
      $(obj).parents('.deluxe-voucher').find('.voucher-balance').html(html);
    }
  });
} //getBalance end

function applyVoucher(obj, voucher_code) {
  $.ajax({
    url: 'index.php?route=extension/total/voucher/voucher',
    data: 'voucher=' + voucher_code,
    type: 'post',
    dataType: 'json',
    success: function(json) {
      $(obj).parents('.voucher-balance').find('.alert-success').show();
      
      if (getURLVar('route') == 'checkout/cart' || getURLVar('route') == 'checkout/checkout') {
				location = 'index.php?route=checkout/cart';
			} else {
				$('#cart').load('index.php?route=common/cart/info');
			}
    }
  });
} //applyVoucher end
</script>
