<?php echo $header; ?>
<div class="container j-container" id="container">
  <ul class="breadcrumb">
    <?php foreach ($breadcrumbs as $breadcrumb) { ?>
    <li><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a></li>
    <?php } ?>
  </ul>
  <?php if ($success) { ?>
  <div class="success alert alert-success"><i class="fa fa-check-circle"></i> <?php echo $success; ?></div>
  <?php } ?>
  <div class="row"><?php echo $column_left; ?>
    <?php if($theme_name == 'journal2') { 
      $my_row = 'j-row';
    ?>
    <?php echo $column_right; ?>
    <?php } else{ 
      $my_row = 'row';
    } ?>
    <?php if ($column_left && $column_right) { ?>
    <?php $class = 'col-sm-6'; ?>
    <?php } elseif ($column_left || $column_right) { ?>
    <?php $class = 'col-sm-9'; ?>
    <?php } else { ?>
    <?php $class = 'col-sm-12'; ?>
    <?php } ?>
    <div id="content" class="<?php echo $class; ?>"><?php echo $content_top; ?>
      <div class="dashboard dash-template1 <?php echo $journal_class; ?>">
        <div class="<?php echo $my_row; ?> dash-cols">
          <div class="col-sm-12 xl-100">
            <div class="profile-section">
              <div class="row">
               <div class="col-md-3 col-sm-3 col-xs-12 xl-25 xs-100 text-center">
               <?php if(!empty($profile_picture_thumb) && $picture_status) { ?>
                <div class="user-image">
                  <img src="<?php echo $profile_picture_thumb; ?>" class="img-circle img-responsive" />
                </div>
                <?php } else{ ?>
                <div class="user-icon">
                  <i class="fa fa-user"></i>
                </div>
              <?php } ?>
              </div>
              <div class="col-md-7 col-sm-7 col-xs-12 xl-60 xs-100">
                <div class="user-name"><span><?php echo $customer_name; ?></span></div>
                <div class="info"><?php echo $customer_email; ?></div>
                <div class="info"><?php echo $customer_telephone; ?></div>
              </div>
              <div class="col-md-2 col-sm-2 col-xs-12 xl-15 xs-100 hidden-xs">
                <div class="profile-links">
                  <a href="<?php echo $link_edit; ?>" data-toggle="tooltip" data-placement="left" title="<?php echo $text_edit; ?>"><i class="fa fa-pencil-square-o" aria-hidden="true"></i></a>
                  <a href="<?php echo $link_logout; ?>" data-toggle="tooltip" data-placement="left" title="<?php echo $text_logout; ?>"><i class="fa fa-sign-out" aria-hidden="true"></i></a>
                </div>
              </div>
              <div class="col-md-2 col-sm-2 col-xs-12 xl-15 xs-100 visible-xs">
                <div class="profile-links">
                  <a href="<?php echo $link_edit; ?>" data-toggle="tooltip" data-placement="top" title="<?php echo $text_edit; ?>"><i class="fa fa-pencil-square-o" aria-hidden="true"></i></a>
                  <a href="<?php echo $link_logout; ?>" data-toggle="tooltip" data-placement="top" title="<?php echo $text_logout; ?>"><i class="fa fa-sign-out" aria-hidden="true"></i></a>
                </div>
              </div>
            </div>
            </div>
            <div class="<?php echo $my_row; ?> tile-cols">
              <?php if($display_total_orders) { ?>
              <div class="<?php echo $col_class; ?> col-md-6 col-sm-6 col-xs-12 md-50 xs-100">
                <div class="tile tile-one">
                  <div class="tile-heading clearfix theme-color">
                    <span><?php echo $panel_orders; ?></span> <i class="fa fa-shopping-cart"></i>
                  </div>
                  <div class="tile-body text-center clearfix">
                    <h2><?php echo $total_orders; ?></h2>
                    <div class="view-all"><a href="<?php echo $view_order; ?>"><?php echo $text_view_order; ?></a></div>
                  </div>
                </div>
              </div>
              <?php } ?>
              <?php if($display_total_transactions) { ?>
              <div class="<?php echo $col_class; ?> col-md-6 col-sm-6 col-xs-12 md-50 xs-100">
                <div class="tile tile-one">
                  <div class="tile-heading clearfix theme-color">
                    <span><?php echo $text_transactions ?></span><i class="fa fa-money" aria-hidden="true"></i>
                  </div>
                  <div class="tile-body text-center clearfix">
                    <h2><?php echo $total_transactions; ?></h2>
                    <div class="view-all"><a href="<?php echo $view_transactions; ?>"><?php echo $text_view_transactions; ?></a></div>
                  </div>
                </div>
              </div>
              <?php } ?>
              <?php if($display_total_wishlist) { ?>
              <div class="<?php echo $col_class; ?> col-md-6 col-sm-6 col-xs-12 md-50 xs-100">
                <div class="tile tile-one">
                  <div class="tile-heading clearfix theme-color">
                    <span><?php echo $panel_wishlist; ?></span> <i class="fa fa-heart"></i>
                  </div>
                  <div class="tile-body text-center clearfix">
                    <h2><?php echo $total_wishlist; ?></h2>
                    <div class="view-all"><a href="<?php echo $view_wishlists; ?>"><?php echo $text_view_wishlists; ?></a></div>
                  </div>
                </div>
              </div>
              <?php } ?>
              <?php if($display_reward_points) { ?>
              <div class="<?php echo $col_class; ?> col-md-6 col-sm-6 col-xs-12 md-50 xs-100">
                <div class="tile tile-one">
                  <div class="tile-heading clearfix theme-color">
                    <span><?php echo $panel_reward_points; ?></span><i class="fa fa-trophy" aria-hidden="true"></i>
                  </div>
                  <div class="tile-body text-center clearfix">
                    <h2><?php echo $total_reward_points; ?></h2>
                    <div class="view-all"><a href="<?php echo $view_reward; ?>"><?php echo $text_view_reward; ?></a></div>
                  </div>
                </div>
              </div>
              <?php } ?>
            </div>
          </div>
          <div class="col-sm-12 xl-100">
            <?php if($custom_links) { ?>
            <ul class="list-inline account-links row">
              <?php $link_row = 1; ?>
              <?php foreach($custom_links as $custom_link) { ?>
              <li class="col-sm-6 col-xs-12 col-md-4 xl-33 md-50 xs-50">
                <a href="<?php echo $custom_link['link']; ?>">
                  <div class="wrap clearfix">
                    <span><?php echo $custom_link['title']; ?></span>
                    <?php if($custom_link['type'] == 'icon') { ?>
                    <div class="icon"><i class="fa <?php echo $custom_link['icon']; ?>"></i></div>
                    <?php } else{ ?>
                    <div class="icon"><img width="52" height="50" src="<?php echo $custom_link['image']; ?>"></div>
                    <?php } ?>
                  </div>  
                </a>
              </li>
              <?php $link_row++; } ?>
            </ul>
            <?php } ?>
          </div>  
        </div>
        <div class="<?php echo $my_row; ?>">
        <?php if($display_orders) { ?>
          <div class="col-sm-12 xl-100">
            <div class="grey-table">
              <h3><b><?php echo $text_latest_order; ?> <a href="<?php echo $view_order; ?>" class="pull-right"><i class="fa fa-eye"></i> <?php echo $button_view_all; ?></a></b></h3>
              <div class="table-responsive">
                <table class="table table-bordered table-hover list">
                  <thead>
                    <tr>
                      <td class="text-left"><?php echo $column_order_id; ?></td>
                      <td class="text-left"><?php echo $column_product; ?></td>
                      <td class="text-left"><?php echo $column_status; ?></td>
                      <td class="text-right"><?php echo $column_total; ?></td>
                      <td class="text-left"><?php echo $column_date_added; ?></td>
                      <td class="text-right"><?php echo $column_action; ?></td>
                    </tr>
                  </thead>
                  <tbody>
                    <?php if($orders) { ?>
                  	<?php foreach($orders as $order) { ?>
                    <tr>
          						<td class="text-left">#<?php echo $order['order_id']; ?></td>
          						<td class="text-left"><?php echo $order['products']; ?></td>
          						<td class="text-left"><?php echo $order['status']; ?></td>
          						<td class="text-right"><?php echo $order['total']; ?></td>
          						<td class="text-left"><?php echo $order['date_added']; ?></td>
                     	<td class="text-right">
                        <a class="btn button" href="<?php echo $order['view']; ?>" data-toggle="tooltip" title="" data-original-title="<?php echo $button_view; ?>"><i class="fa fa-eye"></i></a>
                        </td>
                    </tr>
                    <?php } ?>
                    <?php } else{ ?>
                    <tr>
                      <td class="text-center" colspan="6"><?php echo $text_no_results; ?></td>
                    </tr>
                    <?php } ?>
                  </tbody>
                </table>
              </div>
            </div>
          </div>
          <?php } ?>          
        </div>
      </div>
      <?php echo $content_bottom; ?></div>
  	  <?php if($theme_name != 'journal2') { ?>
        <?php echo $column_right; ?>
      <?php } ?>
  </div>
</div>
<style type="text/css">
  <?php if($themecolor) { ?>
  .profile-section{
    border-top: 3px solid <?php echo $themecolor; ?>;
  }
  .profile-section .user-icon{
    border-color: <?php echo $themecolor; ?>;
  }
  .profile-section .user-icon i{
    color: <?php echo $themecolor; ?>;
  }
  .dashboard .dash-cols .tile-heading, .account-links li .wrap, .dashboard .grey-table h3{
    background: <?php echo $themecolor; ?>;
  }
  .account-links li i{
    color: <?php echo $themecolor; ?>;
  }
  
  .tile .tile-body h2{
    color: <?php echo $themecolor; ?>;
  }
  .dashboard .view-all a{
    color: <?php echo $themecolor; ?>;
  }
  .profile-links a:hover i{
    color: <?php echo $themecolor; ?>;
  }
  <?php } ?>

  <?php if($textcolor) { ?>
  .dashboard .dash-cols .tile-heading, .account-links li .wrap, .dashboard .grey-table h3, .dashboard .grey-table h3 a{
    color: <?php echo $textcolor; ?>;
  }
  .account-links li span{
    color: <?php echo $textcolor; ?>; 
  }
  <?php } ?>
  
  <?php  echo $customcss; ?>
</style>
<?php echo $footer; ?>