<?php

class ControllerModuleKwFlyCart extends Controller
{

	public function index()
	{

		if (isset($this->request->server['HTTPS']) && (($this->request->server['HTTPS'] == 'on') || ($this->request->server['HTTPS'] == '1'))) {
			$base_url = $this->config->get('config_ssl');
		} else {
			$base_url = $this->config->get('config_url');
		}

		$this->language->load('module/kw_flycart');
		$this->load->model('setting/setting');
		$this->load->model('tool/image');
		$this->load->model('localisation/language');

		$tools = $this->model_setting_setting->getSetting('kw_flycart');

		$data['tools'] = $tools['kw_flycart_tools'];
		$data['lang_id'] = $this->config->get('config_language_id');

		if (isset($data['tools']['status_module']) && $data['tools']['status_module'] === "true") {
			$this->document->addStyle($base_url . 'kw_application/flycart/catalog/css/kw_flycart.options.css', $rel = 'stylesheet', $media = 'screen');
			$this->document->addScript($base_url . 'kw_application/flycart/catalog/build/kw_flycart.options.js');
		}

		if (file_exists(DIR_TEMPLATE . $this->config->get('config_template') . '/template/module/kw_flycart.tpl')) {
			return $this->load->view($this->config->get('config_template') . '/template/module/kw_flycart.tpl', $data);
		} else {
			return $this->load->view('default/template/module/kw_flycart.tpl', $data);
		}
	}

	/* Load Settinds
	---------------------------------------------------------------------------------------------------------------------------*/
	public function loader()
	{
		/** Models **/
		$this->load->model('extension/extension');
		$this->load->model('setting/setting');
		$this->load->model('tool/image');
		$this->load->model('tool/upload');
		$this->load->model('catalog/product');

		$this->load->model('kw/kw_flycart');

		/** Lang **/
		$this->language->load('module/kw_flycart');

		$tools = $this->model_setting_setting->getSetting('kw_flycart');

		$total_data = array();
		$total = 0;
		$taxes = $this->cart->getTaxes();

		if (isset($this->request->post['width']) && isset($this->request->post['height'])) {
			$width = $this->request->post['width'];
			$height = $this->request->post['height'];
		} elseif ($tools['kw_flycart_tools']['module_type'] === 'module') {
			$width = isset($tools['kw_flycart_tools']['module_imgage_width']) ? $tools['kw_flycart_tools']['module_imgage_width'] : 100;
			$height = isset($tools['kw_flycart_tools']['module_imgage_height']) ? $tools['kw_flycart_tools']['module_imgage_height'] : 100;
		} else {
			$width = isset($tools['kw_flycart_tools']['popup_imgage_width']) ? $tools['kw_flycart_tools']['popup_imgage_width'] : 100;
			$height = isset($tools['kw_flycart_tools']['popup_imgage_height']) ? $tools['kw_flycart_tools']['popup_imgage_height'] : 100;
		}

		/** Display prices **/
		if (($this->config->get('config_customer_price') && $this->customer->isLogged()) || !$this->config->get('config_customer_price')) {
			$sort_order = array();

			$results = $this->model_extension_extension->getExtensions('total');

			foreach ($results as $key => $value) {
				$sort_order[$key] = $this->config->get($value['code'] . '_sort_order');
			}

			array_multisort($sort_order, SORT_ASC, $results);

			foreach ($results as $result) {
				if ($this->config->get($result['code'] . '_status')) {
					$this->load->model('total/' . $result['code']);

					$this->{'model_total_' . $result['code']}->getTotal($total_data, $total, $taxes);
				}
			}

			$sort_order = array();

			foreach ($total_data as $key => $value) {
				$sort_order[$key] = $value['sort_order'];
			}

			array_multisort($sort_order, SORT_ASC, $total_data);
		}

		$totals = array();
		$products = array();
		$vouchers = array();
		$weight = '';
		$product_weight = '';

		foreach ($total_data as $total) {
			$totals[] = array(
				'title' => $total['title'],
				'text' => $this->currency->format($total['value'])
			);
		}

		foreach ($this->cart->getProducts() as $product) {

			if ($product['image']) {
				if (isset($tools['kw_flycart_tools']['retina']) && $tools['kw_flycart_tools']['retina'] === 'true') {
					$image = $this->model_tool_image->resize($product['image'], $width * 2, $height * 2);
				} else {
					$image = $this->model_tool_image->resize($product['image'], $width, $height);
				}
			} else {
				if (isset($tools['kw_flycart_tools']['retina']) && $tools['kw_flycart_tools']['retina'] === 'true') {
					$image = $this->model_tool_image->resize('kw_no_image.png', $width * 2, $height * 2);
				} else {
					$image = $this->model_tool_image->resize('kw_no_image.png', $width, $height);
				}
			}

			$option_data = array();

			foreach ($product['option'] as $option) {
				if ($option['type'] != 'file') {
					$value = $option['value'];
				} else {
					$upload_info = $this->model_tool_upload->getUploadByCode($option['value']);

					if ($upload_info) {
						$value = $upload_info['name'];
					} else {
						$value = '';
					}
				}

				$option_data[] = array(
					'name' => $option['name'],
					'value' => (utf8_strlen($value) > 20 ? utf8_substr($value, 0, 20) . '..' : $value)
				);
			}

			$product_info = $this->model_catalog_product->getProduct($product['product_id']);

			/** Display prices **/
			if (($this->config->get('config_customer_price') && $this->customer->isLogged()) || !$this->config->get('config_customer_price')) {
				$total = $this->currency->format($this->tax->calculate($product['price'], $product['tax_class_id'], $this->config->get('config_tax')) * $product['quantity']);
				$totalAlone = $this->currency->format($this->tax->calculate($product['price'], $product['tax_class_id'], $this->config->get('config_tax')));
			} else {
				$total = false;
				$totalAlone = false;
			}

			if ((float)$product_info['special']) {
				$option_price = 0;
				$special = $this->currency->format($this->tax->calculate($product_info['price'], $product_info['tax_class_id'], $this->config->get('config_tax')) * $product['quantity']);

				$specialAlone = $this->currency->format($this->tax->calculate($product_info['price'], $product_info['tax_class_id'], $this->config->get('config_tax')));

				if ($product['option']) {
					foreach ($product['option'] as $option) {
						if ($option['price_prefix'] == '+') {
							$option_price += $option['price'];
						} elseif ($option['price_prefix'] == '-') {
							$option_price -= $option['price'];
						}
					}

					$special = $this->currency->format($this->tax->calculate(($product_info['price'] + $option_price), $product_info['tax_class_id'], $this->config->get('config_tax')) * $product['quantity']);
					$specialAlone = $this->currency->format($this->tax->calculate(($product_info['price'] + $option_price), $product_info['tax_class_id'], $this->config->get('config_tax')));
				}
			} else {
				$special = false;
				$specialAlone = false;
			}

			if ($this->config->get('config_cart_weight')) {
				$weight = $this->weight->format($this->cart->getWeight(), $this->config->get('config_weight_class_id'), $this->language->get('decimal_point'), $this->language->get('thousand_point'));

				$product_weight = $this->weight->format(
					$this->weight->convert(($product_info['weight'] * $product['quantity']), $product['weight_class_id'], $this->config->get('config_weight_class_id')),
					$this->config->get('config_weight_class_id'),
					$this->language->get('decimal_point'),
					$this->language->get('thousand_point')
				);


			}

			if ($product_info['quantity'] <= 0) {
				$stock = $product_info['stock_status'];
			} elseif ($this->config->get('config_stock_display')) {
				$stock = $product_info['quantity'];
			} else {
				$stock = $this->language->get('instock');
			}

			$stock_status_id = 'default';

			foreach ($this->model_kw_kw_flycart->getStockStatuses() as $status) {
				if ($status['name'] === $stock) {
					$stock_status_id = $status['stock_status_id'];
				}
			}

			$minimum = sprintf($this->language->get('min_error'), $product_info['quantity']);
			$maximum = sprintf($this->language->get('max_error'), $product['minimum']);

			$products[] = array(
				'id' => $product['product_id'],
				'key' => isset($product['key']) ? $product['key'] : $product['cart_id'],
				'thumb' => $image,
				'name' => $product['name'],
				'option' => $option_data,
				'quantity' => $product['quantity'],
				'weight' => $product_weight,
				'total' => $total,
				'href' => htmlspecialchars_decode($this->url->link('product/product', 'product_id=' . $product['product_id'])),
				'recurring' => isset($product['recurring']) ? $product['recurring'] : NULL,
				'profile' => isset($product['profile_name']) ? $product['profile_name'] : NULL,
				'minimum' => $product['minimum'],
				'maximum' => $product_info['quantity'],
				'min_error' => $minimum,
				'max_error' => $maximum,
				'model' => $product_info['model'],
				'sku' => $product_info['sku'],
				'brand_name' => $product_info['manufacturer'],
				'brand_url' => htmlspecialchars_decode($this->url->link('product/manufacturer/info', 'manufacturer_id=' . $product_info['manufacturer_id'])),
				'stock' => $stock,
				'special' => $special,
				'total_alone' => $totalAlone,
				'special_alone' => $specialAlone,
				'stock_status_id' => $stock_status_id,
				'sss' => $product_info['weight']

			);
		}

		/** Gift Voucher **/
		if (!empty($this->session->data['vouchers'])) {
			foreach ($this->session->data['vouchers'] as $key => $voucher) {
				$vouchers[] = array(
					'key' => $key,
					'description' => $voucher['description'],
					'amount' => $this->currency->format($voucher['amount'])
				);
			}
		}

		$this->load->model('total/coupon');

		$couponCode = null;
		$coupon = false;

		if (isset($this->session->data['coupon'])) {
			$coupon = $this->model_total_coupon->getCoupon($this->session->data['coupon']);
			$couponCode = $coupon['code'];
			$coupon = $coupon['name'];
		}

		$this->response->setOutput(json_encode(array(
			'products'      => $products,
			'vouchers'      => $vouchers,
			'totals'        => $totals,
			'count'         => $this->cart->countProducts() + (isset($this->session->data['vouchers']) ? count($this->session->data['vouchers']) : 0),
			'total'         => $this->currency->format($total_data[sizeof($total_data) - 1]['value']),
			'profile'       => $this->language->get('text_payment_profile') ? $this->language->get('text_payment_profile') : NULL,
			'weight'        => $weight,
			'couponCode'    => $couponCode,
			'coupon'	    => $coupon
		)));
	}

	/* Quantity Update
		---------------------------------------------------------------------------------------------------------------------------*/
	public function getCoupon()
	{
		$this->load->model('total/coupon');
		$coupon_info = $this->model_total_coupon->getCoupon($this->request->post['coupon']);

		if ($coupon_info) {
			$this->session->data['coupon'] = $this->request->post['coupon'];

			return $this->loader();
		}

		$this->response->setOutput(json_encode(
			array(
				'status' => 'error'
			)
		));
	}

	/* Quantity Update
	---------------------------------------------------------------------------------------------------------------------------*/
	public function quantityUpdate()
	{
		if (!empty($this->request->post['quantity'])) {
			foreach ($this->request->post['quantity'] as $key => $value) {
				$this->cart->update($key, $value);
			}

			unset($this->session->data['shipping_method']);
			unset($this->session->data['shipping_methods']);
			unset($this->session->data['payment_method']);
			unset($this->session->data['payment_methods']);
			unset($this->session->data['reward']);
		}

		return $this->loader();
	}

	/* Remove Products
	---------------------------------------------------------------------------------------------------------------------------*/
	public function removeProduct()
	{
		if (isset($this->request->post['remove'])) {
			$this->cart->remove($this->request->post['remove']);

			unset($this->session->data['vouchers'][$this->request->post['remove']]);
		}

		return $this->loader();
	}

	/* Add Products
	---------------------------------------------------------------------------------------------------------------------------*/

	public function addProduct()
	{
		$this->language->load('checkout/cart');

		$json = array();

		if (isset($this->request->post['product_id'])) {
			$product_id = $this->request->post['product_id'];
		} else {
			$product_id = 0;
		}

		if (isset($this->request->post['timezone'])) {
			$timezone = $this->request->post['timezone'];
		} else {
			$timezone = 'UTC';
		}

		date_default_timezone_set($timezone);

		$this->load->model('catalog/product');

		$product_info = $this->model_catalog_product->getProduct($product_id);

		if ($product_info) {
			if (isset($this->request->post['quantity'])) {
				$quantity = $this->request->post['quantity'];
			} else {
				$quantity = 1;
			}

			if (isset($this->request->post['option'])) {
				$option = array_filter($this->request->post['option']);
			} else {
				$option = array();
			}

			if (isset($this->request->post['profile_id'])) {
				$profile_id = $this->request->post['profile_id'];
			} else {
				$profile_id = 0;
			}

			$product_options = $this->model_catalog_product->getProductOptions($this->request->post['product_id']);

			foreach ($product_options as $product_option) {
				if ($product_option['required'] && empty($option[$product_option['product_option_id']])) {
					$json['error']['option'][$product_option['product_option_id']] = sprintf($this->language->get('error_required'), $product_option['name']);
				}

				if (count($option) > 0 && $product_option['type'] === 'date' && is_numeric($option[$product_option['product_option_id']])) {
					$option[$product_option['product_option_id']] = date("Y-m-d", $option[$product_option['product_option_id']] / 1000);
				}

				if (count($option) > 0 && $product_option['type'] === 'time' && is_numeric($option[$product_option['product_option_id']])) {
					$option[$product_option['product_option_id']] = date("H:i", $option[$product_option['product_option_id']] / 1000);
				}

				if (count($option) > 0 && $product_option['type'] === 'datetime' && is_numeric($option[$product_option['product_option_id']])) {
					$option[$product_option['product_option_id']] = date("Y-m-d H:i", $option[$product_option['product_option_id']] / 1000);
				}

			}

			$profiles = isset($this->model_catalog_product->getProfiles) ? $this->model_catalog_product->getProfiles($product_info['product_id']) : NULL;

			if ($profiles) {
				$profile_ids = array();

				foreach ($profiles as $profile) {
					$profile_ids[] = $profile['profile_id'];
				}

				if (!in_array($profile_id, $profile_ids)) {
					$json['error']['profile'] = $this->language->get('error_profile_required');
				}
			}

			if (!$json) {
				$this->cart->add($this->request->post['product_id'], $quantity, $option, $profile_id);

				unset($this->session->data['shipping_method']);
				unset($this->session->data['shipping_methods']);
				unset($this->session->data['payment_method']);
				unset($this->session->data['payment_methods']);

				return $this->loader();
			} else {
				$json['redirect'] = str_replace('&amp;', '&', $this->url->link('product/product', 'product_id=' . $this->request->post['product_id']));
				$json['options'] = $this->productOptions($product_id, $timezone);
				$this->response->setOutput(json_encode($json));
			}
		}


	}

	/* Get Product Options
	---------------------------------------------------------------------------------------------------------------------------*/

	public function productOptions($product_id, $timezone)
	{
		$json = array();

		date_default_timezone_set($timezone);

		$this->load->model('extension/extension');
		$this->load->model('catalog/product');
		$this->load->model('tool/image');
		$this->load->model('setting/setting');

		$tools = $this->model_setting_setting->getSetting('kw_flycart');

		$product_info = $this->model_catalog_product->getProduct($product_id);

		if ($product_info) {

			$minimum = ($product_info['minimum'] > 0) ? $product_info['minimum'] : 1;

			if (($this->config->get('config_customer_price') && $this->customer->isLogged()) || !$this->config->get('config_customer_price')) {
				$total = $this->currency->format($this->tax->calculate($product_info['price'], $product_info['tax_class_id'], $this->config->get('config_tax')) * $minimum);
				$total_price = $this->currency->format($this->tax->calculate($product_info['price'], $product_info['tax_class_id'], $this->config->get('config_tax')));
			} else {
				$total = false;
				$total_price = false;
			}

			if ((float)$product_info['special']) {
				$special = $this->currency->format($this->tax->calculate($product_info['special'], $product_info['tax_class_id'], $this->config->get('config_tax')) * $minimum);
				$special_price = $this->currency->format($this->tax->calculate($product_info['special'], $product_info['tax_class_id'], $this->config->get('config_tax')));
			} else {
				$special = false;
				$special_price = false;
			}

			$options = array();

			foreach ($this->model_catalog_product->getProductOptions($product_info['product_id']) as $option) {
				$product_option_value_data = array();

				foreach ($option['product_option_value'] as $option_value) {
					if (!$option_value['subtract'] || ($option_value['quantity'] > 0)) {
						if ((($this->config->get('config_customer_price') && $this->customer->isLogged()) || !$this->config->get('config_customer_price')) && (float)$option_value['price']) {
							$price = $this->currency->format($this->tax->calculate($option_value['price'], $product_info['tax_class_id'], $this->config->get('config_tax') ? 'P' : false));
						} else {
							$price = false;
						}

						$product_option_value_data[] = array(
							'product_option_value_id' => $option_value['product_option_value_id'],
							'option_value_id' => $option_value['option_value_id'],
							'name' => $option_value['name'],
							'image' => $this->model_tool_image->resize($option_value['image'], 50, 50),
							'price' => $price,
							'price_prefix' => $option_value['price_prefix']
						);
					}
				}

				if ($option['type'] == 'date' || $option['type'] == 'datetime' || $option['type'] == 'time') {
					$value = 1000 * strtotime($option['value']);
				} else {
					$value = $option['value'];
				}

				$options[] = array(
					'product_option_id' => $option['product_option_id'],
					'option_value' => $product_option_value_data,
					'option_id' => $option['option_id'],
					'name' => $option['name'],
					'type' => $option['type'],
					'value' => $value,
					'required' => $option['required']
				);

				if ($product_info['image']) {
					if (isset($tools['kw_flycart_tools']['retina']) && $tools['kw_flycart_tools']['retina'] === 'true') {
						$image = $this->model_tool_image->resize($product_info['image'], 60, 60);
					} else {
						$image = $this->model_tool_image->resize($product_info['image'], 30, 30);
					}
				} else {
					if (isset($tools['kw_flycart_tools']['retina']) && $tools['kw_flycart_tools']['retina'] === 'true') {
						$image = $this->model_tool_image->resize('kw_no_image.png', 60, 60);
					} else {
						$image = $this->model_tool_image->resize('kw_no_image.png', 30, 30);
					}
				}

				$options['minimum'] = $minimum;
				$options['maximum'] = $product_info['quantity'];
				$options['product_id'] = $product_info['product_id'];
				$options['name'] = $product_info['name'];
				$options['image'] = $image;
				$options['total'] = $total;
				$options['special'] = $special;
				$options['total_price'] = $total_price;
				$options['special_price'] = $special_price;
			}

			if (!$json) {
				$json = $options;
			}
		}

		return $json;
	}

	/* Get Product Image
	---------------------------------------------------------------------------------------------------------------------------*/

	public function productImage()
	{

		if (isset($this->request->post['product_id'])) {
			$product_id = $this->request->post['product_id'];
		} else {
			$product_id = 0;
		}

		if (isset($this->request->post['width'])) {
			$width = $this->request->post['width'];
		} else {
			$width = 100;
		}
		if (isset($this->request->post['height'])) {
			$height = $this->request->post['height'];
		} else {
			$height = 100;
		}

		$this->load->model('catalog/product');
		$this->load->model('tool/image');

		$product_info = $this->model_catalog_product->getProduct($product_id);
		$json = $product_info['image'] ? $this->model_tool_image->resize($product_info['image'], $width, $height) : $this->model_tool_image->resize('kw_no_image.png', $width, $height);

		$this->response->setOutput($json);
	}

	/* Get Product Minimal
	---------------------------------------------------------------------------------------------------------------------------*/

	public function productMinimal()
	{
		$this->load->model('catalog/product');

		if (isset($this->request->post['product_id'])) {
			$product_id = $this->request->post['product_id'];
		} else {
			$product_id = 0;
		}

		$product_info = $this->model_catalog_product->getProduct($product_id);

		if (isset($product_info['minimum']) && (int)$product_info['minimum'] > 1) {
			$minimum = (int)$product_info['minimum'];
		} else {
			$minimum = 1;
		}


		$this->response->setOutput(json_encode($minimum));
	}
}

?>