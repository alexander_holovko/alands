<?php  
class ControllerModulePriceAlert extends Controller {
	private $moduleName = 'PriceAlert';
	private $moduleNameSmall = 'pricealert';
	private $moduleData_module = 'pricealert_module';
	private $moduleModel = 'model_module_pricealert';	

	public function submitform() {
		$this->load->model('setting/setting');

		$send_email = false;
		if (isset($this->request->post['PAYourName']) && isset($this->request->post['PAYourEmail'])) {
			$run_query = $this->db->query("INSERT INTO `" . DB_PREFIX . "pricealert` (customer_email, customer_name, product_id, date_created, store_id) VALUES ('".$this->db->escape($this->request->post['PAYourEmail'])."', '".$this->db->escape($this->request->post['PAYourName'])."', '".$this->db->escape($this->request->post['PAProductID'])."', NOW(), '".$this->config->get('config_store_id')."')");
			if ($run_query) { 
				echo "Success!";
				$send_email = true;
			}					 
		} else {
			echo "Ошибка! Ничего нет!";
		}
			
		if ($send_email==true) {
			$moduleSettings = $this->model_setting_setting->getSetting($this->moduleNameSmall, $this->config->get('config_store_id'));
			$data['PriceAlert'] = isset($moduleSettings[$this->moduleName]) ? ($moduleSettings[$this->moduleName]) : array();
			if ($data['PriceAlert']['Notifications']=='yes') {
				$text = "Новый клиент следит за ценой товара!<br /><br />
				Имя: ".$this->request->post['PAYourName']."<br />
				Email: ".$this->request->post['PAYourEmail']."<br />";	
							
				if (isset($this->request->post['PAProductID'])) {
					$this->load->model('catalog/product');
					$product = $this->model_catalog_product->getProduct($this->request->post['PAProductID']);
					$text .= "Товар: ".$product['name']."<br /><br />"; 
				}
				$text .= "Подробнее <a href='".HTTP_SERVER."admin/index.php?route=module/pricealert&token=".$this->session->data['token']."'>тут</a>!";
				
				$mailToUser = new Mail($this->config->get('config_mail'));
				$mailToUser->setTo($this->config->get('config_email'));
				$mailToUser->setFrom($this->config->get('config_email'));
				$mailToUser->setSender($this->config->get('config_name'));
				$mailToUser->setSubject(html_entity_decode("Клиент следит за ценой", ENT_QUOTES, 'UTF-8'));
				$mailToUser->setHtml($text);
				$mailToUser->send();
			}
		}
	}
		
	public function showpricealertform() {
		$this->language->load('module/'.$this->moduleNameSmall);
		$this->load->model('setting/setting');
	
		$data['heading_title'] 				= $this->language->get('heading_title');
		$data['PriceAlert_Title'] 			= $this->language->get('PriceAlert_Title');
		$data['PriceAlert_SubmitButton']	= $this->language->get('PriceAlert_SubmitButton');
		$data['PriceAlert_Error1'] 			= $this->language->get('PriceAlert_Error1');
		$data['PriceAlert_Error2'] 			= $this->language->get('PriceAlert_Error2');
		$data['PriceAlert_Success'] 		= $this->language->get('PriceAlert_Success');
		
		$moduleSettings						= $this->model_setting_setting->getSetting($this->moduleNameSmall, $this->config->get('config_store_id'));
        $data['moduleData']					= isset($moduleSettings[$this->moduleName]) ? ($moduleSettings[$this->moduleName]) : array();
		$data['language_id']				= $this->config->get('config_language_id');

		if (isset($this->request->get['product_id'])) {
			$product_id = (int)$this->request->get['product_id'];
		} else {
			$product_id = 0;
		}

		$data['PriceAlertProductID'] = $product_id;		
		
		if (file_exists(DIR_TEMPLATE . $this->config->get('config_template') . '/template/module/'.$this->moduleNameSmall.'.tpl')) {
			$this->response->setOutput($this->load->view($this->config->get('config_template').'/template/module/'.$this->moduleNameSmall.'_form.tpl', $data));
		} else {
			$this->response->setOutput($this->load->view('default/template/module/'.$this->moduleNameSmall.'_form.tpl', $data));
		}								
	}
	
	public function getstatus() {
		$this->load->model('setting/setting');

		$moduleSettings = $this->model_setting_setting->getSetting($this->moduleNameSmall, $this->config->get('config_store_id'));
		$PriceAlert = isset($moduleSettings[$this->moduleName]) ? ($moduleSettings[$this->moduleName]) : array();
		
		$data['PriceAlert'] = false;
		
		$json = array();
		if (isset($PriceAlert) && ($PriceAlert['Enabled'] == 'yes') ) {
				$json['button'] = 'Test';
				$json['enabled'] = true;
				$json['products'] = 'all';
		} else {
			$json['enabled'] = false;
		}
		
		$this->response->setOutput(json_encode($json)); 	
	}
}
?>