<?php
class ControllerModuleDeluxeVoucher extends Controller {
  private $error = array();

  public function index() {
    $data['lng'] = $this->load->language('module/deluxe_voucher');

    $data['success'] = isset($this->session->data['success']) ? $this->session->data['success'] : '';
    unset($this->session->data['success']);

    if (version_compare(VERSION, '2.2', '>=')) {
      return $this->load->view('module/deluxe_voucher_form', $data);
    } elseif (file_exists(DIR_TEMPLATE . $this->config->get('config_template') . '/template/module/deluxe_voucher_form.tpl')) {
      return $this->load->view($this->config->get('config_template') . '/template/module/deluxe_voucher_form.tpl', $data);
    } else {
      return $this->load->view('default/template/module/deluxe_voucher_form.tpl', $data);
    }
  } //index end

  public function getBalance() {
    $this->load->language('module/deluxe_voucher');

    if (version_compare(VERSION, '2.1', '>=')) {
      $this->load->language('total/voucher');
    } else {
      $this->load->language('checkout/voucher');
    }

    $html = '<div class="alert alert-danger">' . $this->language->get('error_voucher') . '</div>';

    if (!empty($this->request->post['code'])) {
      if (version_compare(VERSION, '2.1', '>=')) {
        $this->load->model('total/voucher');
        $voucher_info = $this->model_total_voucher->getVoucher($this->request->post['code']);
      } else {
        $this->load->model('checkout/voucher');
        $voucher_info = $this->model_checkout_voucher->getVoucher($this->request->post['code']);
      }

      if (isset($voucher_info['amount'])) {
        $html = '<div class="alert alert-info">' . str_replace('{balance}', $this->currency->format($voucher_info['amount'], $this->session->data['currency']), $this->language->get('voucher_balance_text')) . '  <a class="btn-link" onclick="applyVoucher(this, \'' . $this->request->post['code'] . '\');">' . $this->language->get('button_voucher') . '</a></div><div class="alert alert-success" style="display: none;">' . $this->language->get('text_success') . '</div>';
      }
    }

    $this->response->setOutput($html);
  } //getBalance end

  public function preview() {
    $this->load->language('mail/voucher');

    $data['title'] = sprintf($this->language->get('text_subject'), $this->request->get['from_name']);
    $data['text_greeting'] = sprintf($this->language->get('text_greeting'), $this->currency->format($this->request->get['amount'], $this->session->data['currency']));
    $data['text_from'] = sprintf($this->language->get('text_from'), $this->request->get['from_name']);
    $data['text_message'] = $this->language->get('text_message');
    $data['text_redeem'] = sprintf($this->language->get('text_redeem'), "XXX XXX XXXX");
    $data['text_footer'] = $this->language->get('text_footer');

    $data['image'] = '';
    if (isset($this->request->get['voucher_theme_id'])) {
      $query = $this->db->query("SELECT * FROM " . DB_PREFIX . "voucher_theme WHERE voucher_theme_id='" . (int)$this->request->get['voucher_theme_id'] . "'");
      if ($query->num_rows) $data['image'] = $this->config->get('config_url') . 'image/' . $query->row['image'];
    }

    $data['store_name'] = $this->config->get('config_name');
    $data['store_url'] = $this->config->get('config_url');
    $data['message'] = nl2br($this->request->get['message']);

    $data['from_name'] = $this->request->get['from_name'];
    $data['to_name'] = $this->request->get['to_name'];
    $data['amount'] = $this->currency->format($this->request->get['amount'], $this->session->data['currency']);
    $data['code'] = 'XXX XXX XXXX';

    if (version_compare(VERSION, '2.2', '>=')) {
      $html = $this->load->view('mail/voucher', $data);
    } elseif (file_exists(DIR_TEMPLATE . $this->config->get('config_template') . '/template/mail/voucher.tpl')) {
      $html = $this->load->view($this->config->get('config_template') . '/template/mail/voucher.tpl', $data);
    } else {
      $html = $this->load->view('default/template/module/deluxe_voucher.tpl', $data);
    }
    $html = preg_replace('/width:\s?\d.+?;/', '', $html);
    
    $this->response->setOutput($html);
  } //preview end

  public function cronSendVoucher() {
    $query = $this->db->query("SELECT v.*, o.order_status_id FROM " . DB_PREFIX . "voucher v JOIN `" . DB_PREFIX . "order` o ON v.order_id = o.order_id WHERE v.deliverydate = CURDATE() AND o.order_status_id IN (" . implode(',', $this->config->get('config_complete_status')) . ")");

    if ($query->num_rows) {
      $this->load->language('module/deluxe_voucher');
      $find = array('{code}', '{order_id}', '{to_name}', '{to_email}', '{delivery_date}');

      $this->load->model('checkout/order');

      foreach ($query->rows as $r) {
        $replace = array($r['code'], $r['order_id'], $r['to_name'], $r['to_email'], $r['deliverydate']);
        $comment = str_replace($find, $replace, $this->language->get('voucher_sent_log'));
        $this->model_checkout_order->addOrderHistory($r['order_id'], $r['order_status_id'], $comment);
        echo $comment;
      }
    } else {
      echo 'There is no voucher scheduled to be sent today ' . date('r');
    }
  } //cronSendVoucher end

  public function removeVoucher() {
    unset($this->session->data['voucher'][$this->request->get['code']]);
  } //removeVoucher end

} //class end
?>
