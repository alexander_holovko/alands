<?php  
class ControllerModuleBrandCarousel extends Controller {
	public function index($setting) {
		static $module_brandcarousel = 0;
		$this->load->model('catalog/manufacturer');
		$this->load->model('tool/image');
		$this->document->addScript('catalog/view/javascript/jquery/jquery.flexisel.js');
		if (version_compare(VERSION, '2.2.0.0') >= 0) { $dir_theme = $this->config->get('theme_default_directory'); } else { $dir_theme = $this->config->get('config_template'); }
		if (file_exists(DIR_TEMPLATE . $dir_theme . '/stylesheet/flexisel.css')) {
			$this->document->addStyle('catalog/view/theme/' . $dir_theme . '/stylesheet/flexisel.css');
		} else {
			$this->document->addStyle('catalog/view/theme/default/stylesheet/flexisel.css');
		}
		$data['name_block'] = $setting['name_block'][$this->config->get('config_language_id')];
		$data['infinite'] = $setting['infinite'];
		$data['scroll_auto'] = $setting['scroll_auto'];
		$data['scroll_pause'] = $setting['scroll_pause'];
		$data['animation_speed'] = $setting['animation_speed'];
		$data['interval'] = $setting['interval'];
		$data['scroll_limit'] = $setting['scroll_limit'];
		$data['scroll'] = $setting['scroll'];
		$data['brands'] = array();
		$results = $this->model_catalog_manufacturer->getManufacturers();
		foreach ($results as $result) {
			if (is_file(DIR_IMAGE . $result['image'])) {
				$data['brands'][] = array(
				'manufacturer_id' => $result['manufacturer_id'],
				'title'           => $result['name'],
				'link'            => $this->url->link('product/manufacturer/info', 'manufacturer_id=' . $result['manufacturer_id']),
				'image' => $this->model_tool_image->resize($result['image'], $setting['width'], $setting['height'])
				);
			}
		}
		$data['module_brandcarousel'] = $module_brandcarousel++;
		if (version_compare(VERSION, '2.2.0.0') >= 0) {
			return $this->load->view('module/brand_carousel.tpl', $data);
		} else {
			if (file_exists(DIR_TEMPLATE . $this->config->get('config_template') . '/template/module/brand_carousel.tpl')) {
				return $this->load->view($this->config->get('config_template') . '/template/module/brand_carousel.tpl', $data);
			} else {
				return $this->load->view('default/template/module/brand_carousel.tpl', $data);
			}
		}
	}
}
