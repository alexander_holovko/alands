<?php 
class ControllerProducttestimonial extends Controller {
	
	public function index() {  
    	$this->language->load('product/testimonial');
		
		$this->load->model('catalog/testimonial');
		
		$this->document->setTitle ($this->language->get('heading_title'));

		$data['breadcrumbs'] = array();

   		$data['breadcrumbs'][] = array(
       		'text'      => $this->language->get('text_home'),
			'href'      => $this->url->link('common/home')
   		);
		
		$data['breadcrumbs'][] = array(
	       		'text'      => $this->language->get('heading_title'),
				'href'      => $this->url->link('product/testimonial')
	   	);
		
		$data['heading_title'] = $this->language->get('heading_title');
		
		$data['text_write'] = $this->language->get('text_write');
		$data['text_note'] = $this->language->get('text_note');
		$data['text_loading'] = $this->language->get('text_loading');
		$data['text_login'] = sprintf($this->language->get('text_login'), $this->url->link('account/login', '', 'SSL'), $this->url->link('account/register', '', 'SSL'));
			
      	$data['entry_title'] = $this->language->get('entry_title');
      	$data['entry_city'] = $this->language->get('entry_city');
		$data['entry_name'] = $this->language->get('entry_name');
		$data['entry_text'] = $this->language->get('entry_text');
		$data['entry_email'] = $this->language->get('entry_email');
		$data['entry_captcha'] = $this->language->get('entry_captcha');
		$data['entry_rating'] = $this->language->get('entry_rating');
		$data['entry_good'] = $this->language->get('entry_good');
		$data['entry_bad'] = $this->language->get('entry_bad');
		$data['entry_good_rating'] = $this->language->get('entry_good_rating');
		$data['entry_bad_rating'] = $this->language->get('entry_bad_rating');
		$data['entry_photo'] = $this->language->get('entry_photo');
		
		$data['button_upload'] = $this->language->get('button_upload');
			
		$data['continue'] = $this->url->link('common/home', '', 'SSL');
		
		$data['review_status'] = $this->config->get('config_review_status');

		if ($this->config->get('config_testimonial_guest') || $this->customer->isLogged()) {
			$data['guest'] = true;
		} else {
			$data['guest'] = false;
		}
		
		if ($this->config->get('config_testimonial_title')) {
			$data['title'] = $this->config->get('config_testimonial_title');
		} else {
			$data['title'] = 0;
		}
		
		if ($this->config->get('config_testimonial_city')) {
			$data['city'] = $this->config->get('config_testimonial_city');
		} else {
			$data['city'] = 0;
		}
		
		if ($this->config->get('config_testimonial_email')) {
			$data['email'] = $this->config->get('config_testimonial_email');
		} else {
			$data['email'] = 0;
		}
		
		if ($this->config->get('config_testimonial_name')) {
			$data['name'] = $this->config->get('config_testimonial_name');
		} else {
			$data['name'] = 0;
		}
		
		if ($this->config->get('config_testimonial_text')) {
			$data['text'] = $this->config->get('config_testimonial_text');
		} else {
			$data['text'] = 0;
		}
		
		if ($this->config->get('config_testimonial_good')) {
			$data['good'] = $this->config->get('config_testimonial_good');
		} else {
			$data['good'] = 0;
		}
		
		if ($this->config->get('config_testimonial_bad')) {
			$data['bad'] = $this->config->get('config_testimonial_bad');
		} else {
			$data['bad'] = 0;
		}
		
		if ($this->config->get('config_testimonial_rating')) {
			$data['rating'] = $this->config->get('config_testimonial_rating');
		} else {
			$data['rating'] = 0;
		}
		
		if ($this->config->get('config_testimonial_photo')) {
			$data['photo'] = $this->config->get('config_testimonial_photo');
		} else {
			$data['photo'] = 0;
		}
		
		// Captcha
		if ($this->config->get($this->config->get('config_captcha') . '_status') && in_array('testimonial', (array)$this->config->get('config_captcha_page'))) {
			$data['captcha'] = $this->load->controller('captcha/' . $this->config->get('config_captcha'));
		} else {
			$data['captcha'] = '';
		}
			
		$data['column_left'] = $this->load->controller('common/column_left');
		$data['column_right'] = $this->load->controller('common/column_right');
		$data['content_top'] = $this->load->controller('common/content_top');
		$data['content_bottom'] = $this->load->controller('common/content_bottom');
		$data['footer'] = $this->load->controller('common/footer');
		$data['header'] = $this->load->controller('common/header');		
			
		if (file_exists(DIR_TEMPLATE . $this->config->get('config_template') . '/template/product/testimonial.tpl')) {
			$this->response->setOutput($this->load->view($this->config->get('config_template') . '/template/product/testimonial.tpl', $data));
		} else {
			$this->response->setOutput($this->load->view('default/template/product/testimonial.tpl', $data));
		}
  	}

	public function testimonial() {
		$this->load->language('product/testimonial');

		$this->load->model('catalog/testimonial');

		$data['text_no_testimonials'] = $this->language->get('text_no_testimonials');
		$data['entry_text'] = $this->language->get('entry_text');
		$data['entry_good'] = $this->language->get('entry_good');
		$data['entry_bad'] = $this->language->get('entry_bad');
		$data['entry_comment'] = $this->language->get('entry_comment');

		if (isset($this->request->get['page'])) {
			$page = $this->request->get['page'];
		} else {
			$page = 1;
		}

		if (isset($this->request->get['limit'])) {
			$limit = (int)$this->request->get['limit'];
		} else {
			$limit = $this->config->get('config_testimonial_limit');
		}
		
		$data['testimonials'] = array();
		
		$this->page_limit = $this->config->get('limit');
		
		$filter_data = array(
				'start'              => ($page - 1) * $limit,
				'limit'              => $limit
		);
		
		$testimonial_total = $this->model_catalog_testimonial->getTotalTestimonials();
			
		$results = $this->model_catalog_testimonial->getTestimonials($filter_data);

		$this->load->model('tool/image');
		
		$data['photo'] = $this->config->get('config_testimonial_photo');
		$data['rating'] = $this->config->get('config_testimonial_rating');
		$data['date_added'] = $this->config->get('config_testimonial_date_added');
		
		foreach ($results as $result) {

			if ($result['photo']) {
				$photo = $this->model_tool_image->resize($result['photo'], $this->config->get('config_testimonial_photo_width'), $this->config->get('config_testimonial_photo_height'));
			} else {
				$photo = $this->model_tool_image->resize('catalog/avatar/no_photo.png', $this->config->get('config_testimonial_photo_width'), $this->config->get('config_testimonial_photo_height'));;
			}
				
			$data['testimonials'][] = array(
				'title'    		=> $result['title'],
				'city'			=> $result['city'],
				'name'			=> $result['name'],
				'text'			=> $result['text'],
				'good'			=> $result['good'],
				'bad'			=> $result['bad'],
				'rating'		=> $result['rating'],
				'photo'			=> $photo,
				'comment'		=> $result['comment'],
				'date_added' 	=> date($this->language->get('date_format_short'), strtotime($result['date_added']))
			);
		}

		$pagination = new Pagination();
		$pagination->total = $testimonial_total;
		$pagination->page = $page;
		$pagination->limit = $limit;
		$pagination->url = $this->url->link('product/testimonial/testimonial', '&page={page}');

		$data['pagination'] = $pagination->render();

		if (file_exists(DIR_TEMPLATE . $this->config->get('config_template') . '/template/product/testimonial_list.tpl')) {
			$this->response->setOutput($this->load->view($this->config->get('config_template') . '/template/product/testimonial_list.tpl', $data));
		} else {
			$this->response->setOutput($this->load->view('default/template/product/testimonial_list.tpl', $data));
		}
	}
	
	public function upload() {
		$this->load->language('tool/upload');

		$json = array();

		if (!empty($this->request->files['file']['name']) && is_file($this->request->files['file']['tmp_name'])) {
			// Sanitize the filename
			$filename = basename(preg_replace('/[^a-zA-Z0-9\.\-\s+]/', '', html_entity_decode($this->request->files['file']['name'], ENT_QUOTES, 'UTF-8')));

			// Validate the filename length
			if ((utf8_strlen($filename) < 3) || (utf8_strlen($filename) > 64)) {
				$json['error'] = $this->language->get('error_filename');
			}

			// Allowed file extension types
			$allowed = array();

			$extension_allowed = 'png,jpe,jpeg,jpg,gif';

			$filetypes = explode(",", $extension_allowed);

			foreach ($filetypes as $filetype) {
				$allowed[] = trim($filetype);
			}

			if (!in_array(strtolower(substr(strrchr($filename, '.'), 1)), $allowed)) {
				$json['error'] = $this->language->get('error_filetype');
			}

			// Allowed file mime types
			$allowed = array();

			$mime_allowed = 'image/png,image/jpeg,image/gif';

			$filetypes = explode(",", $mime_allowed);

			foreach ($filetypes as $filetype) {
				$allowed[] = trim($filetype);
			}

			if (!in_array($this->request->files['file']['type'], $allowed)) {
				$json['error'] = $this->language->get('error_filetype');
			}

			// Check to see if any PHP files are trying to be uploaded
			$content = file_get_contents($this->request->files['file']['tmp_name']);

			if (preg_match('/\<\?php/i', $content)) {
				$json['error'] = $this->language->get('error_filetype');
			}

			// Return any upload error
			if ($this->request->files['file']['error'] != UPLOAD_ERR_OK) {
				$json['error'] = $this->language->get('error_upload_' . $this->request->files['file']['error']);
			}
		} else {
			$json['error'] = $this->language->get('error_upload');
		}

		if (!$json) {
			$folder = 'catalog/avatar/';
			$file = $folder . token(32) . $filename;

			move_uploaded_file($this->request->files['file']['tmp_name'], DIR_IMAGE . $file);

			$this->load->model('catalog/testimonial');
			
			$result = $this->model_catalog_testimonial->resize($file, $folder);
			
			if ($result) {
				$json['file'] = $result;
				$json['success'] = $this->language->get('text_upload');
			} else {
				$json['error'] = $this->language->get('error_upload');
			}
		}

		$this->response->addHeader('Content-Type: application/json');
		$this->response->setOutput(json_encode($json));
	}
	
	public function write() {
		$this->load->language('product/testimonial');

		$json = array();

		if ($this->request->server['REQUEST_METHOD'] == 'POST') {

			if (($this->config->get('config_testimonial_title') == 2) && (utf8_strlen($this->request->post['title']) < 2)) {
				$json['error'] = $this->language->get('error_title');
			}
			
			if (($this->config->get('config_testimonial_city') == 2) && (utf8_strlen($this->request->post['city']) < 2)) {
				$json['error'] = $this->language->get('error_city');
			}
			if($this->config->get('config_testimonial_email') == 2){
				if (utf8_strlen($this->request->post['email']) > 96 || !filter_var($this->request->post['email'], FILTER_VALIDATE_EMAIL)) {
					$json['error'] = $this->language->get('error_email');
				}
			}
			
			if (($this->config->get('config_testimonial_name') == 2) && (utf8_strlen($this->request->post['name']) < 2)) {
				$json['error'] = $this->language->get('error_name');
			} 

			if($this->config->get('config_testimonial_text') == 2){
				if (utf8_strlen($this->request->post['text']) < 25 || utf8_strlen($this->request->post['text']) > 1000) {
					$json['error'] = $this->language->get('error_text');
				}
			}
			
			if($this->config->get('config_testimonial_good') == 2){
				if (utf8_strlen($this->request->post['good']) < 25 || utf8_strlen($this->request->post['good']) > 1000) {
					$json['error'] = $this->language->get('error_good');
				}
			}
			
			if($this->config->get('config_testimonial_bad') == 2){
				if (utf8_strlen($this->request->post['bad']) < 25 || utf8_strlen($this->request->post['bad']) > 1000) {
					$json['error'] = $this->language->get('error_bad');
				}
			}
			
			if ($this->config->get('config_testimonial_photo') == 2) {
				if (utf8_strlen($this->request->post['photo']) == 0) {
					$json['error'] = $this->language->get('error_photo');
				}
			}
			
			// Captcha
			if ($this->config->get($this->config->get('config_captcha') . '_status') && in_array('testimonial', (array)$this->config->get('config_captcha_page'))) {
				$captcha = $this->load->controller('captcha/' . $this->config->get('config_captcha') . '/validate');

				if ($captcha) {
					$json['error'] = $captcha;
				}
			}

			if (!isset($json['error'])) {
				$this->load->model('catalog/testimonial');
				
			if ($this->config->get('config_testimonial_status') == 1) {
				$status = 0;
				$status_message = $this->language->get('text_success_status');
			} else {
				$status = 1;
				$status_message = '';
			}

				$this->model_catalog_testimonial->addTestimonial($this->request->post, $status);

				$json['success'] = $this->language->get('text_success') . ' ' . $status_message;
			}
		}

		$this->response->addHeader('Content-Type: application/json');
		$this->response->setOutput(json_encode($json));
	}
}
?>