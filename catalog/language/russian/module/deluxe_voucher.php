<?php
$_['voucher_balance_heading'] = 'ПРОВЕРКА БАЛАНСА КАРТЫ';
$_['voucher_balance_entry'] = 'Код карты';
$_['voucher_balance_button'] = 'Получить баланс';
$_['voucher_preview_heading'] = 'Предосмотр карты';
$_['voucher_preview_button'] = 'Предосмотр';
$_['voucher_deliverydate_entry'] = 'Дата отправки';
$_['voucher_deliverydate_help'] = 'Leave blank to send voucher after transaction is completed';
$_['voucher_balance_text'] = 'Ваш баланс {balance}.';
$_['voucher_sent_log'] = 'Карта {code} с заказа {order_id} был отправлен {to_name} {to_email} в {delivery_date}';
?>
