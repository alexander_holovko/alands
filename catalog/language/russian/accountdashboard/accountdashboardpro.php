<?php
// Heading
$_['heading_title']      		= 'Личные данные';

// Text
$_['text_account']       		= 'Профиль';
$_['text_my_account']    		= 'Личные данные';
$_['text_my_orders']     		= 'Мои заказы';
$_['text_my_newsletter'] 		= 'Рассылка';
$_['text_edit']          		= 'Редактировать профиль';
$_['text_password']      		= 'Изменить пароль';
$_['text_address']       		= 'Адресная книга';
$_['text_credit_card']   		= 'Manage Stored Credit Cards';
$_['text_wishlist']      		= 'Wish List';
$_['text_order']         		= 'Ваши заказы';
$_['text_download']      		= 'Загрузки';
$_['text_logout']      			= 'Выход';

$_['text_reward']        		= 'Your Reward Points';
$_['text_return']        		= 'View Return Requests';
$_['text_transaction']   		= 'Ваш баланс';
$_['text_newsletter']    		= 'Подписка на рассылку';
$_['text_recurring']     		= 'Recurring Payments';
$_['text_transactions']  		= 'Общий баланс';
$_['text_view_order']  			= 'Просмотреть все заказы';
$_['text_view_transactions']  	= 'Смотреть весь балансe';
$_['text_view_wishlists']  		= 'Просмотреть все списки пожеланий';
$_['text_view_downloads']  		= 'View All Downloads';
$_['text_view_reward']  		= 'Просмотреть все бонусные баллы';

// Panels
$_['panel_orders']		 		= 'ВСЕГО ЗАКАЗОВ';
$_['panel_wishlist']		 	= 'ИТОГ ПОЖЕЛАНИЙ';
$_['panel_downloads']		 	= 'ЗАЗГРУЗКИ';
$_['panel_reward_points']		= 'БОНУСНЫЕ ОЧКИ';
$_['text_address_book']		 	= 'Адресная книга';
$_['text_latest_order']		 	= 'Последние заказы';
$_['text_no_results']		 	= 'Пусто';

// Columns
$_['column_order_id']		 	= 'Номер заказа';
$_['column_product']		 	= 'Продукты';
$_['column_status']			 	= 'Положение дел';
$_['column_total']			 	= 'Всего';
$_['column_date_added']		 	= 'Дата';
$_['column_action']			 	= 'Действие';

// Buttons
$_['button_view_all']  			= 'Последние заказы';