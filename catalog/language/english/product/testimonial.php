<?php
// Heading
$_['heading_title'] 			= 'Customer Testimonials';

// Text
$_['text_no_testimonials']      = 'No testimonials';
$_['text_note'] 				= '<span class="text-danger">Note:</span> HTML is not translated!';
$_['text_success']              = 'Thank you for your testimonial.';
$_['text_success_status']       = 'It has been submitted to the webmaster for approval.';
$_['text_write'] 				= 'Write testimonial';
$_['text_login']                = 'Please <a href="%s">login</a> or <a href="%s">register</a> to review';

// Entry
$_['entry_name']     			= 'Name:';
$_['entry_city']     			= 'City:';
$_['entry_email']    			= 'E-Mail:';
$_['entry_title']	 			= 'Title:';
$_['entry_text']         		= 'Tex:';
$_['entry_rating']      		= 'Rating:';
$_['entry_good']        		= 'Good:';
$_['entry_bad']         		= 'Bad:';
$_['entry_good_rating']        	= 'Good:';
$_['entry_bad_rating']         	= 'Bad:';
$_['entry_photo']        	    = 'Photo:';
$_['entry_comment']        	    = 'Administrator comment:';

// Error
$_['error_title']       		= 'Title must be greater than 2 characters!';
$_['error_city']       			= 'City must be greater than 2 characters!';
$_['error_email']    			= 'E-Mail Address does not appear to be valid!';
$_['error_name']       			= 'Name must be greater than 2 characters!';
$_['error_text']         		= 'Warning: Text must be between 25 and 1000 characters!';
$_['error_good']         		= 'Warning: Text Good must be between 25 and 1000 characters!';
$_['error_bad']         		= 'Warning: Text Bad must be between 25 and 1000 characters!';
$_['error_photo']         		= 'Photo must be uploaded';