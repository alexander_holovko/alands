<?php
// Heading 
$_['heading_title']  = 'Следить за ценой';
$_['PriceAlert_Title'] = $_['heading_title'];
$_['PriceAlert_SubmitButton'] = 'Следить!';
$_['PriceAlert_Error1'] = 'Поле обязательно!';
$_['PriceAlert_Error2'] = 'Не правильный Email!';
$_['PriceAlert_Success'] = 'Спасибо! Вы будете уведомлены, когда цена измениться!';
$_['PriceAlert_Button'] = 'Следить за ценой';
?>