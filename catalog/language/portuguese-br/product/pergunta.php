<?php
// Text 
$_['text_message']   = '<p>Agradecemos pelo seu contato. Retornaremos o mais breve possível!</p>';

$_['titulo']   = 'Faça uma pergunta sobre este produto';

$_['text_pergunta']   = 'Faça uma pergunta sobre este produto';

$_['button_pergunta']   = 'Dúvidas sobre este produto? Clique aqui!';


// Entry Fields
$_['entry_name']     = '<font color="#CC0000">*</font>Nome:';
$_['entry_email']    = '<font color="#CC0000">*</font>E-mail:';
$_['entry_telefone'] = 'Telefone:';
$_['entry_pergunta'] = '<font color="#CC0000">*</font>Dúvida:';

//Error
$_['erro_nome']     = 'Por favor, digite seu nome!';
$_['erro_email']     = 'Por favor, digite um e-mail válido!';
$_['erro_pergunta']     = 'Por favor, digite sua dúvida!';
//Button
$_['enviar'] = 'Enviar';
$_['limpar'] = 'Limpar';
//$_['aviseme'] = 'aviseme';
//Subject e-mail
$_['email_subject']  = 'Gostaria de mais informações sobre o produto - Enviado por %s';
?>