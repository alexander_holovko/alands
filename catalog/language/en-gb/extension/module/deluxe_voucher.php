<?php
$_['voucher_balance_heading'] = 'Check Voucher Balance';
$_['voucher_balance_entry'] = 'Voucher Code';
$_['voucher_balance_button'] = 'Get Balance';
$_['voucher_preview_heading'] = 'Voucher Preview';
$_['voucher_preview_button'] = 'Preview';
$_['voucher_deliverydate_entry'] = 'Delivery Date';
$_['voucher_deliverydate_help'] = 'Leave blank to send voucher after transaction is completed';
$_['voucher_balance_text'] = 'Your balance is {balance}.';
$_['voucher_sent_log'] = 'Voucher {code} from order {order_id} was sent to {to_name} {to_email} on {delivery_date}';
?>
